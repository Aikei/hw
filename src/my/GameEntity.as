package my {
	import my.MyEntity;
	import my.logic.effects.Effect
	import starling.display.Quad;
	import starling.utils.Color;
	/**
	 * ...
	 * @author Aikei
	 */
	

	 
	public class GameEntity extends MyEntity
	{
	
		protected var _effects: Vector.<Effect> = new Vector.<Effect>;
		protected var _targetQuad: Quad = null;
		protected var _targeted: Boolean = false;
		public var m_id: int;
		public var m_index: Vec2 = new Vec2;
		
		public function get effects(): Vector.<Effect>
		{
			return _effects;
		}
		
		public function targeted(): Boolean
		{
			return _targeted;
		}
		
		public function GameEntity(id: int, x: Number=0, y: Number=0) 
		{
			super(x, y);
			m_index.x = -1;
			m_index.y = -1;
			m_id = id;
		}		
		
		public function AddEffect(effect: Effect): void
		{
			_effects.push(effect);
			effect.OnAdded(this);
		}
		
		public function RemoveEffect(name: String): Boolean
		{
			var index = GetEffectIndex(name);
			if (index != -1)
			{
				_effects[index].OnRemoved(this);
				_effects.splice(index, 1);
				return true;
			}
			return false;
		}
		
		public function HasEffect(name: String): Boolean
		{
			return (GetEffectIndex(name) !== -1);
		}
		
		public function GetEffectIndex(name: String): int
		{
			for (var i: int, l: int = _effects.length; i < l; i++)
			{
				if (_effects[i].type === name)
					return i;
			}
			return -1;
		}
		
		public function TickEffects(before: Boolean): void
		{
			//for (var i: int = 0; i < _effects.length; i++)
			//{
				//if (before && _effects[i].type === Effect.FROZEN)
				//{					
					//_effects[i].TickBeforeMoving();
					//return;
				//}
			//}
			for (var i: int = 0; i < _effects.length; i++)
			{
				//if (_effects[i].type !== Effect.FROZEN )
				//{
					if (before)
					{
						if (_effects[i].TickBeforeMoving())
							i--;
					}
					else
					{
						if (_effects[i].TickAfterMoving())
							i--;					
					}
				//}
			}
		}
		
		public function SetTargeted(targeted: Boolean): void
		{
			_targeted = targeted;
			if (targeted)
			{
				if (_targetQuad)
					_targetQuad.removeFromParent(true);
				_targetQuad = new Quad(this.width/scaleX+4, this.height/scaleY+4, Color.GREEN);
				_targetQuad.alpha = 0.5;
				//_targetQuad.alignPivot();
				addChild(_targetQuad);				
			}
			else
			{
				if (_targetQuad)
					_targetQuad.removeFromParent(true);
			}
		}
		
		public function Equals(other: GameEntity): Boolean
		{
			return (m_id === other.m_id);
		}
		
		public function DamageEntity(damage: int)
		{
			
		}
		
		//public function RemoveAllEffects(): int
		//{
			//for (var i: int
		//}
		//
		//override public function Destroy(): void
		//{
			//super.Destroy();
		//}		
	}

}