package my.dbg 
{
	import my.Ball;
	import my.GameEntity;
	import my.logic.abilities.Ability;
	import my.mp.messages.MessageContainer;
	import my.mp.Multiplayer;
	import my.MyWorld;
	import my.mp.messages.*;
	import flash.utils.*;
	import my.mp.Controller;
	/**
	 * ...
	 * @author Aikei
	 */
	public class CommandParser 
	{
		private static var autoCommands: Dictionary = new Dictionary;
		
		public function CommandParser() 
		{
			
		}
		
		private static function AddAutoCommand(command: String, time: Number): void
		{
			autoCommands[command] = time*1000;
			setTimeout(ParseCommand, time * 1000, command);
			MConsole.Write("##", "Starting auto command '", command,"' with interval",time);
		}
		
		private static function StopAutoCommand(command: String): void
		{
			if (autoCommands[command] != null)
			{
				MConsole.Write("##", "Stopping auto command '", command,"'");
				delete autoCommands[command];
			}
		}
		
		static public function ParseCommand(command: String) : void
		{
			var l: int = 0;
			var i: int = 0;
			var j: int = 0;
			var str: String = "";
			var strings: Array = command.split(" ");
			if (strings.length == 0)
				return;
			var msg: MessageContainer = null;
			if (strings[0] === "server")
			{
				if (strings[1] === "info")
				{
					Controller.RequestServerInfo();
				}
				if (strings[1] === "check") 
				{
					if (strings[2] === "field")
					{
						Controller.RequestCheckField(MyWorld.gWorld.m_balls);
					}
				}
				else if (strings[1] === "list")
				{
					if (strings[2] === "players")
					{
						Controller.RequestListPlayers();
					}
				}
				//setTimeout(
			}
			else if (strings[0] === "help")
			{
				MConsole.Write("_i", "'server info' - lists information about the server");
				MConsole.Write("_i", "'server list players' - lists information about all players currently online");
				MConsole.Write("_i", "'server check field' - checks if your field matches the server's");
				MConsole.Write("_i", "'auto [command] [secs]' - automatically repeats [command] every [secs] seconds, e.g.: 'auto server check field 3'");
				MConsole.Write("_i", "'stop auto [command]' - stops automatic execution of [command]");
				MConsole.Write("_i", "'set server local' - set server to your localhost");
				MConsole.Write("_i", "'set server remote' - set server to the main remote server");
				MConsole.Write("_i", "'set server [number]' - set server to a specific server with a given number in the server list");
				MConsole.Write("_i", "'print entropy' - prints current entropy");
				
				MConsole.Write("_i", "'send message [message]' - sends a message [message] to server");
			}
			else if (strings[0] === "auto")
			{
				l = strings.length-1;
				str = "";
				for (i = 1; i < l; i++)
				{
					str += strings[i];
					if (i != l - 1)
						str += " ";
				}
				AddAutoCommand(str, int(strings[l]));
			}
			else if (strings[0] === "stop")
			{
				if (strings[1] === "auto")
				{
					l = strings.length-1;
					str = "";
					for (i = 2; i <= l; i++)
					{
						str += strings[i];
						if (i != l)
							str += " ";
					}					
				}
				StopAutoCommand(str);
			}
			else if (strings[0] === "set")
			{
				if (strings[1] === "server")
				{
					if (strings[2] === "local")
					{
						Controller.m_multiplayer.SetServer(1);
					}
					else if (strings[2] === "remote")
					{
						Controller.m_multiplayer.SetServer(0);
					}
					else
					{
						var n: int = int(strings[2]);
						Controller.m_multiplayer.SetServer(n);
					}
				}
				else if (strings[1] === "log")
				{
					if (strings[2] === "level")
					{
						if (strings[3] != undefined)
							MConsole.logLevel = int(strings[3]);
					}
				}
			}
			else if (strings[0] === "print")
			{
				if (strings[1] === "entropy")
				{
					if (MyWorld.gWorld.m_logic.m_entropy == null)
					{
						MConsole.Write("##", "no entropy, probably not in game");
						return;
					}
					l = MyWorld.gWorld.m_logic.m_entropy.length;
					MConsole.Write("##","entropy length: ", l);
					str = "";
					for (i = 0; i < l; i++)
					{
						str += MyWorld.gWorld.m_logic.m_entropy[i]+", ";
					}
					MConsole.Write("##",str);
				}
				else if (strings[1] === "field")
				{					
					for (i = MyWorld.FIELD_HEIGHT-1; i >= 0; i--)
					{
						str = "";
						for (j = 0; j < MyWorld.FIELD_WIDTH; j++)
						{
							str += String(MyWorld.gWorld.m_balls[j][i].m_type) + " ";
						}
						MConsole.Write("##",str);
					}					
					MConsole.Write("##","Printing field:");
				}
				else if (strings[1] === "dice")
				{
					if (MyWorld.gWorld.m_logic.m_dice == null)
					{
						MConsole.Write("##", "no entropy, probably not in game");
						return;
					}
					l = MyWorld.gWorld.m_logic.m_dice.length;
					MConsole.Write("##","dice length: ", l);
					str = "";
					for (i = 0; i < l; i++)
					{
						str += MyWorld.gWorld.m_logic.m_dice[i]+", ";
					}
					MConsole.Write("##",str);					
				}
			}
			else if (strings[0] === "send")
			{
				if (strings[1] === "message")
				{
					if (strings[2] != undefined && strings[3] != undefined)
					{
						Controller.SendMessageToServer(strings[2], strings[3], undefined);
						//msg = Message.CreateDataMessage(strings[2], strings[3], undefined);
					}
					else if (strings[2] != undefined)
					{
						Controller.SendMessageToServer(strings[2], undefined, undefined);
						//msg = Message.CreateDataMessage(strings[2], undefined, undefined);
					}
					else
					{
						MConsole.Write("##","Wrong message!");
						return;
					}
					//MyWorld.gWorld.m_multiplayer.SerializeAndSend(msg);
				}
			}
			else if (strings[0] === "use")
			{
				if (strings[1] === "ability")
				{
					if (strings[2] != undefined && strings[3] != undefined && strings[4] != undefined)
					{
						var ability: Ability = new Ability(strings[2],MyWorld.gWorld.m_logic.myNumber);
						var index_x: int = int(strings[3]);
						var index_y: int = int(strings[4]);
						var v: Vector.<GameEntity> = new Vector.<GameEntity>;
						v.push(MyWorld.gWorld.m_balls[index_x][index_y]);
						ability.Use(v);
					}
				}
			}
			else
			{
				MConsole.Write("Unknown command");
			}
			if (autoCommands[command] != null)
				setTimeout(ParseCommand, autoCommands[command], command);			
		}
		
	}

}