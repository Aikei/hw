package my.dbg 
{
	import starling.events.*;
	import flash.ui.*;
	import starling.utils.HAlign;
	
	import my.MyWorld;	
	import my.view.TextEntity;
	import my.Misc;
	import my.system.TextEditor;
	import my.view.BaseView;
	/**
	 * ...
	 * @author Aikei
	 */
	public class MConsole extends BaseView
	{
		private var _texts: Vector.<TextEntity>;
		private static const TEXT_HEIGHT: Number = 20;
		private static const MAX_LINES: int = 20;
		private static const FONT_SIZE: int = 16;
		private static const LINE_HEIGHT: int = 25;
		private static const LINE_WIDTH: int = 1024;
		private static const TEXT_ALIGNMENT: String = HAlign.LEFT;
		private static const MAX_COMMANDS: int = 50;
		private var beginner: String = " : ";
		
		private var _command: TextEntity = new TextEntity("", FONT_SIZE, LINE_WIDTH, LINE_HEIGHT, 25, 15);
		private var _beginner: TextEntity = new TextEntity(">", FONT_SIZE, LINE_WIDTH, LINE_HEIGHT, 15, 15);
		//public static var showConsole : Boolean = false;
		
		private var _textEditor: TextEditor = new TextEditor;
		private var _cursorText: TextEntity;
		private var _previousCommands: Vector.<String> = new Vector.<String>;
		private var _upArrowGetIndex: int = -1;		
		private static var _console: MConsole = null;
		
		public static var logLevel: int = 2;
		
		public static function get created():Boolean
		{
			return (_console != null);
		}
		
		//public function MConsole()
		//{
			//_visible = true;
		//}
		
		public static function set showConsole(show: Boolean): void
		{
			if (_console == null)
				return;
			_console.isVisible = show;
		}
		
		public static function get showConsole(): Boolean
		{
			if (_console == null)
				return false;
			return _console.isVisible;
		}
		
		public static function Write(... args): void
		{
			if (_console == null)
				return;
			var startI: int = 0;
			var str: String = new String;
			if (args[0] != "_i" && args[0] != "_ii" && args[0] != "##" && args[0] != "e" && logLevel < 3)
				return;
			if (args[0] != "_i" && args[0] != "##" && args[0] != "e" && logLevel < 2)
				return;
			if (args[0] == "_i" || args[0] == "_ii")
				args.splice(0, 1);
			if (args[0] == "w" || args[0] == "e" || args[0] == "##")
			{
				str += args[0];
				startI = 1;
			}
			str += ": ";
			for (var i : int = startI; i < args.length; i++)
			{
				str += String(args[i]);
				str += " ";
			}
			_console.AddText(str);			
		}
		
		private function AddText(str: String): void
		{
			//var txt: TextEntity = TextEntity(_axGroup.recycle());
			//if (txt == null)
			//{
				//txt = new TextEntity(str, MyWorld.SMALL_FONT, 15, (TEXT_HEIGHT*2) + _texts.length * TEXT_HEIGHT);
				//_axGroup.add(txt);
			//}
			//else
			//{
				//txt.revive();
				//txt.InitText(str, 15, 30 + _texts.length * TEXT_HEIGHT);
			//}
			var txt: TextEntity = CreateTextEntity(str, 15, (TEXT_HEIGHT * 2) + _texts.length * TEXT_HEIGHT);
			txt.SetHAlignment(TEXT_ALIGNMENT);
			addChild(txt);
			_texts.push(txt);
			if (_texts.length > MAX_LINES)
			{
				txt = _texts[0];
				//_texts[0].destroy();
				//_texts[0] = null;
				_texts.splice(0, 1);
				txt.Destroy();
			}
			UpdateTextPositions();
		}
		
		private function CreateTextEntity(str: String, x, y): TextEntity
		{
			var txt: TextEntity = new TextEntity(str, FONT_SIZE, LINE_WIDTH, LINE_HEIGHT, x, y);
			txt.textField.autoScale = true;
			txt.SetHAlignment(HAlign.LEFT);
			return txt;
		}
		
		private function UpdateTextPositions(): void
		{
			for (var i: int = 0, l:int = _texts.length-1; i <= l; i++)
			{
				_texts[l-i].y = i * TEXT_HEIGHT+(TEXT_HEIGHT*2);
			}
		}
		
		//override public function update(): void
		//{
			//if (showConsole)
			//{
				//super.update();
			//}
		//}
		
		//override public function draw(): void
		//{
			//if (showConsole)
				//super.draw();
		//}
		
		public function MConsole() 
		{
			_console = this;
			_texts = new Vector.<TextEntity>;
			_cursorText = CreateTextEntity(_textEditor.cursorChar, FONT_SIZE*2, 15);
			_command = CreateTextEntity("", FONT_SIZE*2, 15);
			_beginner = CreateTextEntity(">", 15, 15);			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnAddedToStage(ev: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_command);
			addChild(_beginner);
			addChild(_cursorText);			
		}
		
		override public function ProcessInput(ev: Event): Boolean
		{
			if (ev.type === KeyboardEvent.KEY_DOWN)
			{
				if (KeyboardEvent(ev).keyCode === Keyboard.ENTER)
				{
					_previousCommands.push(_command.text);
					if (_previousCommands.length >= MAX_COMMANDS)
						_previousCommands.splice(0, 1);
					AddText(_beginner.text+_beginner.text+" "+ _command.text);
					CommandParser.ParseCommand(_command.text);					
					_textEditor.Clear();
					_command.text = "";
					_cursorText.text = "";
					_upArrowGetIndex = -1;
					return true;
				}
				else if (KeyboardEvent(ev).keyCode === Keyboard.UP)
				{
					if (_upArrowGetIndex === -1)
						_upArrowGetIndex = _previousCommands.length-1;
					else
						_upArrowGetIndex--;
					if (_upArrowGetIndex >= 0)
					{						
						_command.text = _previousCommands[_upArrowGetIndex];
					}
				}
				else if (KeyboardEvent(ev).keyCode === Keyboard.DOWN)
				{
					if (_upArrowGetIndex === -1 || _upArrowGetIndex === _previousCommands.length-1)
						return true;
					_upArrowGetIndex++;
					if (_upArrowGetIndex >= 0)
					{						
						_command.text = _previousCommands[_upArrowGetIndex];
					}
				}				
				else
				{
					if (_textEditor.OnKeyboardEvent(KeyboardEvent(ev)))
					{
						_command.text = _textEditor.text;
						_cursorText.text = _textEditor.cursorString;
						return true;
					}
				}
			}
			return false;
		}
		
		
	}

}