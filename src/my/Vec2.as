package my 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import Math;
	public class Vec2 
	{
		public var x:Number, y:Number;
		
		public function Vec2(x:Number = 0, y:Number = 0) 
		{
			this.x = x; 
			this.y = y;
		}
		
		public function Normalize(): void
		{
			var length:Number = Misc.GetVectorLength(x, y);
			x = x / length;
			y = y / length;
		}
		
		public function GetSquaredLength(): Number
		{
			return x * x + y * y;
		}
		
		public function GetLength(): Number
		{
			return Math.sqrt(GetSquaredLength());
		}
		
		public function Scale(scalar:Number): void
		{
			x = x * scalar;
			y = y * scalar;
		}
		
		public function AddVector(right: Vec2): Vec2
		{
			var result: Vec2 = new Vec2;
			result.x = x + right.x;
			result.y = y + right.y;
			return result;
		}
		
		public function AddNumbers(a_x:Number, a_y:Number): Vec2
		{
			var result: Vec2 = new Vec2;
			result.x = x + a_x;
			result.y = y + a_y;
			return result;
		}
		
		public function AddScalar(scalar: Number): Vec2
		{
			var result: Vec2 = new Vec2;
			result.x = x + scalar;
			result.y = y + scalar;
			return result;
		}
		
		public function SubtractVector(right: Vec2): Vec2
		{
			var result: Vec2 = new Vec2;
			result.x = x - right.x;
			result.y = y - right.y;
			return result;
		}
		
		public function SubtractNumbers(a_x:Number, a_y:Number): Vec2
		{
			var result: Vec2 = new Vec2;
			result.x = x - a_x;
			result.y = y - a_y;
			return result;
		}
		
		public function SubtractScalar(scalar: Number): Vec2
		{
			var result: Vec2 = new Vec2;
			result.x = x - scalar;
			result.y = y - scalar;
			return result;
		}
		
		public function CopyFrom(other: Vec2): void
		{
			x = other.x;
			y = other.y;
		}
		
		public function Absolute(): void
		{
			x = Math.abs(x);
			y = Math.abs(y);
		}
		
		public function DotProduct(other: Vec2): Number
		{
			return ((x*other.x)+(y*other.y))
		}
	}

}