package my
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.ui.Keyboard;
	import my.mp.GameProfile;
	import starling.display.Image;
	import starling.display.Sprite;

	import flash.utils.*;
	
	import starling.events.*;
	import starling.utils.*;
	
	import my.system.Language;	
	import my.events.BallsDeselectedEvent;
	import my.mp.messages.*;
	import my.view.BaseView;
	import my.ai.Ai;
	import my.mp.Controller;
	import my.dbg.MConsole;	
	import my.view.*;
	import my.logic.Logic;
	
	import com.greensock.*;	
	import com.greensock.easing.*;
	
	import starling.extensions.PDParticleSystem;
	import starling.extensions.ParticleSystem;
	import starling.core.Starling;
	import my.view.InterfaceFactories;
	import my.system.NameGenerator;
	import my.mp.SocialProfile;
	//import feathers.themes.MetalWorksDesktopTheme;
	
	public class MyWorld extends Sprite
	{
		private var _language: Language = new Language;
		public static var gWorld: MyWorld = null;
		public static var FIELD_WIDTH: int = 6;
		public static var FIELD_HEIGHT: int = 6;
		public static const FIELD_SPACING_X: int = 6;
		public static const FIELD_SPACING_Y: int = 6;
		public static var OVERALL_FIELD_WIDTH: int = FIELD_WIDTH * (Ball.BALL_WIDTH + FIELD_SPACING_X);
		public static var OVERALL_FIELD_HEIGHT: int = FIELD_HEIGHT * (Ball.BALL_HEIGHT + FIELD_SPACING_Y);
		
		public static const PHASE_CONNECTING_TO_SERVER: int = 6;
		public static const PHASE_WAITING_FOR_NEW_GAME: int = 7;
		public static const PHASE_IN_LOBBY: int = 8;
		public static const PHASE_IN_GAME: int = 9;
		public static const PHASE_IN_PROFILE: int = 10;
		
		public static const TIME_STEP: Number = 1 / 60;
		public var m_enemyBalls:Vector.<Ball>;

		
		public var m_bPaused: Boolean = false;


		private var m_phase:int = PHASE_CONNECTING_TO_SERVER;
		private static const REMOVE_HORIZONTAL:int = 0;
		private static const REMOVE_VERTICAL:int = 1;
		private var m_newActorID:int = 0;
		private var m_ai:Ai = null;
		
		private var m_bTimerStarted: Boolean = false;
		private var m_timer:Number = 0;
		
		//public var m_lastTickTime: Number = 0;
		public var m_elapsed: Number = 0;		
		public var m_balls:Vector.<Vector.<Ball>> = null;				
		//public var m_ballsParticleSystems: Vector.<Vector.<BallParticleSystem>> = null;
		private var m_rotated: Number = 0;
		
		
		private var m_againstAI: Boolean = false;
		private var m_console: MConsole = new MConsole;
		//public var m_multiplayer: Multiplayer = new Multiplayer(this);
		
		private var m_views : Vector.<BaseView> = new Vector.<BaseView>;
		
		
		public var m_gameScreen: GameScreen;
		public var m_lobbyView: LobbyView;
		public var m_profileView: ProfileView;

		public var m_logic: Logic;
		public var flashVars: Object;
		
		public var m_socialProfile: SocialProfile = new SocialProfile;
		public var m_gameProfile: GameProfile = null;
		
		public var m_enemySocialProfile: SocialProfile = null;
		public var m_enemyGameProfile: GameProfile = null;
		//public static var gParticleSystem: PDParticleSystem;
		//private var m_ballsCamera: AxCamera = new AxCamera;
		public function MyWorld()
		{
			//new MetalWorksDesktopTheme();
			flashVars = Starling.current.nativeStage.root.loaderInfo.parameters;
			gWorld = this;			
			MyAssets.InitAssests();
			Tweens.InitTweens();
			NameGenerator.Initialize();
			InterfaceFactories.InitInterface();
			new Controller();						
			
			if (stage)
				Start();
			else
				addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function Initialize(): void
		{
			m_gameScreen.Initialize();
			m_lobbyView.Initialize();
			m_profileView.Initialize();
		}
		
		public function InitializeOnGameStart(): void
		{
			m_gameScreen.InitializeOnGameStart();
		}
		
		public function InitializeAfterGame(): void
		{
			m_profileView.Initialize();
		}		
		
		public function OnAddedToStage(ev: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			Start();
		}
		
		public function Start(): void
		{			
			m_gameScreen = new GameScreen(1024,768);
			m_gameScreen.isVisible = false;
			
			//m_interfaceView = new TopTextView;
			
			m_logic = new Logic();
			m_lobbyView = new LobbyView();
			m_profileView = new ProfileView();
			m_profileView.isVisible = false;
			m_lobbyView.isVisible = false;
			
			m_views.push(m_profileView);
			m_views.push(m_gameScreen);			
			m_views.push(m_lobbyView);
			m_views.push(m_console);
			
			if (m_againstAI)
				m_ai = new Ai();
				
			SetPhase(PHASE_CONNECTING_TO_SERVER);
			
			for (var i: int = 0, l: int = m_views.length; i < l; i++)
				addChild(m_views[i]);
			
			addEventListener(Event.ENTER_FRAME, Update);
			
			addEventListener(KeyboardEvent.KEY_DOWN, this.ProcessInput);
			addEventListener(KeyboardEvent.KEY_UP, this.ProcessInput);
			addEventListener(TouchEvent.TOUCH, this.ProcessInput);
		}
		
		public function Update(ev: EnterFrameEvent): void
		{
			if (!m_bPaused)
			{
				m_logic.Update();
				HandleTime(ev.passedTime);
				if (m_againstAI)
					m_ai.Update();				
			}
			for (var i: int = 0, l: int = m_views.length; i < l; i++)
			{
				if (m_views[i].isVisible)
					m_views[i].Update();
			}
		}
		
		private function ProcessInput(event: Event): void
		{
			if (ProcessWorldInput(event))
				return;
			for (var i: int = m_views.length-1; i >= 0; i--)
			{
				if (!m_views[i].isVisible)
					continue;
				if (m_views[i].ProcessInput(event))
					break;
			}
		}
		
		private function ProcessWorldInput(event: Event): Boolean
		{
			if (event.type === KeyboardEvent.KEY_DOWN)
			{
				if (KeyboardEvent(event).keyCode === Keyboard.TAB)
				{
					if (MConsole.showConsole)
						MConsole.Write("^ Closed console ^");
					else
						MConsole.Write("v Called console v");
					MConsole.showConsole = !MConsole.showConsole;
					//event.stopPropagation();
					return true;
				}
			}
			return false;
		}
		
		public function CheckPointCollision(ball:Ball,px:Number,py:Number): Boolean
		{
			if (px > ball.x && px < (ball.x + ball.width) && py > ball.y && py < (ball.y + ball.height))
			{
				return true;
			}
			return false;
		}		
		
		public function CreateNewBall(x: Number, y: Number, index: Vec2, ballNumber: int, specialType: int = 0): Ball
		{
			//var ball: Ball = Ball(m_gameScreen.m_ballsGroup.recycle());
			//if (ball == null)
			//{
				//ball = new Ball(ballNumber, GetNewActorID(), this, index, x, y);
				//m_gameScreen.m_ballsGroup.add(ball);
				////add(ball);
			//}
			//else
			//{
				//ball.revive();
				//ball.InitBall(ballNumber, x, y);
			//}
			//ball.m_index = index;
			//return ball;
			var ball: Ball = new Ball(ballNumber, GetNewActorID(), this, index, x + Ball.BALL_WIDTH / 2 + FIELD_SPACING_X / 2, y + Ball.BALL_HEIGHT / 2 + FIELD_SPACING_Y / 2, specialType);
			m_gameScreen.m_ballsGroup.addChild(ball);
			//m_gameScreen.m_ballsGroup.add(ball);
			//addChild(ball);
			return ball;
		}		
		
		public function CreateRandomBall(x:Number,y:Number, index: Vec2): Ball
		{
			var t = m_logic.m_entropy.pop();
			var specialType = 0;
			if (t === Ball.GREEN_BALL)
			{
				var d = m_logic.m_dice.pop();
				if (d < Preferences.HEALTH_STONE_CHANCE)
					specialType = Ball.BALL_SPECIAL_TYPE_HEALTH;
			}
			else if (t === Ball.RED_BALL)
			{
				d = m_logic.m_dice.pop();
				if (d < Preferences.ATTACK_STONE_CHANCE)
					specialType = Ball.BALL_SPECIAL_TYPE_ATTACK;			
			}
			return CreateNewBall(x, y, index, t, specialType);
		}				
		
		private function GetNewActorID(): int
		{
			return m_newActorID++;
		}
		
		//private function EnemySelectBallsToRemove(): void
		//{
			//if (!m_bTimerStarted)
				//RestartTimer();
			//if (m_timer >= 0.25)
			//{
				//if (m_enemyBalls.length == 0)
				//{
					//StopTimer();
					//SetPhase(PHASE_ENEMY_REMOVING);
					//RemoveBalls(m_selectedBalls);
					//m_selectedBalls = new Vector.<Ball>;					
					//m_enemyBalls = null;
					//return;
				//}				
				//RestartTimer();				
				//SelectBall(m_enemyBalls[0]);
				//m_enemyBalls.splice(0, 1);
			//}
		//}
		
		private function HandleTime(deltaTime: Number): void
		{
			m_elapsed = deltaTime;
			//m_lastTickTime = Ax.now;
			if (m_bTimerStarted)
				m_timer += m_elapsed;
		}
		
		public function RestartTimer(): void
		{
			m_bTimerStarted = true;
			m_timer = 0;			
		}
		
		public function StopTimer(): void
		{
			m_bTimerStarted = false;						
		}
		
		public function IsTimerStarted(): Boolean
		{
			return m_bTimerStarted;
		}
		
		public function GetTimerValue(): Number
		{
			return m_timer;
		}		
		
		public function get phase(): int
		{
			return m_phase;
		}
		
		public function GetPhase():int
		{
			return m_phase;
		}
		
		public function SetPhase(newPhase:int): void
		{
			if (m_phase === PHASE_IN_LOBBY)
			{
				if (newPhase !== PHASE_WAITING_FOR_NEW_GAME)
					m_lobbyView.isVisible = false;
			}
			else if (m_phase === PHASE_IN_PROFILE)
			{
				m_profileView.isVisible = false;
			}
			else if (m_phase === PHASE_CONNECTING_TO_SERVER)
			{
				MConsole.showConsole = false;
			}
			
			m_phase = newPhase;

			if (newPhase === PHASE_WAITING_FOR_NEW_GAME)
			{
				MConsole.Write("Waiting for players...");
				Controller.FindMatch();
				//MConsole.showConsole = true;
			}
			else if (newPhase === PHASE_CONNECTING_TO_SERVER)
			{
				MConsole.Write("type 'help' to get a list of possible commands");
				MConsole.showConsole = true;
			}
			else if (newPhase === PHASE_IN_PROFILE)
			{
				//Console.showConsole = false;
				//m_lobbyView.isVisible = false;
				m_profileView.isVisible = true;				
			}
			else if (newPhase === PHASE_IN_LOBBY)
			{
				//MConsole.showConsole = false;
				m_lobbyView.isVisible = true;
			}
		}				
			
		public function OnGameMessage(m: MessageContainer): void
		{
			m_logic.OnGameMessage(m);
			if (m.s === MessageNames.ksGameStart)
			{
				m_lobbyView.isVisible = false;
				m_gameScreen.isVisible = true;
				SetPhase(PHASE_IN_GAME);
			}
			else if (m.s === MessageNames.ksEndGame)
			{
				setTimeout(function() : void { m_gameScreen.isVisible = false; }, 3000);				
				setTimeout(SetPhase,3000,PHASE_IN_PROFILE);
			}
		}
		
		public function FindBallByPosition(glX: Number, glY: Number): Ball
		{
			var index_x: int = Math.floor(glX / (Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X));
			var index_y: int = Math.floor(glY / (Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_Y));
			if (index_x < 0 || index_x >= MyWorld.FIELD_WIDTH || index_y < 0 || index_y >= MyWorld.FIELD_HEIGHT)
				return null
			else
				return m_balls[index_x][index_y];
		}
	}

}