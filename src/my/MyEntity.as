package my 
{
	import starling.display.DisplayObjectContainer;
	import starling.display.Sprite;
	import my.logic.effects.Effect;
	import starling.display.DisplayObject;
	import my.Misc;
	import my.Ball;
	/**
	 * ...
	 * @author Aikei
	 */
	public class MyEntity extends Sprite 
	{
		protected var _color: uint = 0xffffffff;
		private var _baseColor: uint = 0xffffffff;
		private var _topColor: uint = 0xffffffff;
		public var saveX: Number;
		public var saveY: Number;
		
		public function MyEntity(x: Number = 0, y: Number = 0)
		{
			this.x = x;
			this.y = y;
		}
		
		public function set baseColor(clr: uint): void
		{
			_baseColor = clr;
			color = Misc.BlendHexColors(_baseColor, _topColor);
			for (var i: int = 0, l: int = numChildren; i < l; i++)
			{
				var obj : DisplayObject = getChildAt(i);
				if (obj is MyEntity)
					MyEntity(obj).baseColor = clr;
			}				
		}
		
		public function get baseColor(): uint
		{
			return _baseColor;
		}
		
		public function get topColor(): uint
		{
			return _topColor;
		}
		
		public function set topColor(clr: uint): void
		{
			_topColor = clr;
			color = Misc.BlendHexColors(_baseColor, _topColor);
			for (var i: int = 0, l: int = numChildren; i < l; i++)
			{
				var obj : DisplayObject = getChildAt(i);
				if (obj is MyEntity)
					MyEntity(obj).topColor = clr;
			}					
		}
		
		public function set color(clr: uint): void
		{
			//var blendColor = Misc.BlendHexColors(_baseColor,clr);
			_color = clr;
			//for (var i: int = 0, l: int = numChildren; i < l; i++)
			//{
				//var obj : DisplayObject = getChildAt(i);
				//if (obj is MyEntity)
					//MyEntity(obj).color = _color;
			//}			
		}
		
		//public function set justcolor(clr: uint): void
		//{
			//_color = clr;
			//for (var i: int = 0, l: int = numChildren; i < l; i++)
			//{
				//var obj : DisplayObject = getChildAt(i);
				//if (obj is MyEntity)
					//MyEntity(obj).color = _color;
			//}						
		//}
		
		//public function get justcolor(): uint
		//{
			//return _color;
		//}
		
		public function get color(): uint
		{
			return _color;
		}
		
		public function Destroy(): void
		{
			removeFromParent(true);
		}
		
		public function SendToTop(): void
		{
			if (parent)
			{
				var p: DisplayObjectContainer = parent;
				removeFromParent(false);
				p.addChild(this);
			}
		}
		
		public function ScaleTo(scaleWidth: Number, scaleHeight: Number): void
		{

		}
		
		public function PivotToCenter(): void
		{
			alignPivot();
			
		}
		
		public function SetChildrenAlpha(alpha: Number):void 
		{
			for (var i: int = 0; i < numChildren; i++)
			{
				getChildAt(i).alpha = alpha;
			}
		}
		
		override public function addChild(child: DisplayObject): DisplayObject
		{
			super.addChild(child);
			if (child is GameEntity)
				GameEntity(child).topColor = this.topColor;			
			//if (child is GameEntity)
				//GameEntity(child).color = this.color;
			return child;
		}
								
	}

}