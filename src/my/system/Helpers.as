package my.system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.utils.*;
	import flash.geom.*;
	import flash.system.*;
	import flash.display.*;
	import flash.net.*;
	
	public class Helpers
	{
		/**
		 * 
		 * @param obj
		 * @return Class
		 *
		 */
		static public function GetClass(obj:Object): Class
		{
			return Class( getDefinitionByName( getQualifiedClassName( obj ) ) );
		}
 
		/**
		 * 
		 * @param obj
		 * @return String
		 *
		 */
		static public function GetClassName(obj:Object): String
		{
			return getQualifiedClassName(obj);
		}
 
		/**
		 * Take a annoymous type and try to copy the properies over into the given concrete type
		 * 
		 * @param source Annoymous type
		 * @param targetType Concrete type
		 *
		 */
		static public function CloneIntoR(source: Object, targetType: Class): *
		{
			//assert(source!=null, "CloneIntoR(): source is null! Type=" + GetClassName(targetType));
			var data:* = new targetType();
 
			for (var prop: Object in source)
			{
				//Assert(data.hasOwnProperty(prop), "Helpers.CloneInto(): supplied type didn't have required property " + prop);
 
				try
				{
					data[prop] = source[prop];
				}
				catch (e:Error)
				{
					data[prop] = CloneIntoR( source[prop], GetClass( data[prop] ));
				}
			}
 
			return data;
		}
 
		/**
		 * Import an annoymous type from the server into a concrete type of the client
		 * 
		 * @param source Annoymous type
		 * @param targetType Concrete type
		 *
		 */
		static public function ImportServerDef(source:Object, targetType:Class):*
		{
			var data:* = CloneIntoR( source, targetType );
			data.PostFixUp();
			return data;
		}
		
	}

}