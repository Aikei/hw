package my.system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.logic.globaleffects.*;
	import my.logic.effects.*;
	import my.logic.abilities.*;
	
	public class Classes 
	{
		AbilityFreeze;
		AbilityFire;
		AbilityTurn;
		AbilityFlame;
		AbilityHeal;
		AbilityFrost;
		AbilityPoison;
		AbilityBomb;
		AbilityMeteoriteRain;
		AbilityShatter;
		AbilitySeeNext;
		AbilityEarthquake;
		AbilityStoneOfHealth;
		AbilityShieldOfIce;
		AbilityDarkMadness;
		AbilityChainLightning;
		AbilityGoblinMine;
		AbilityFreezingRain;
		AbilityGreenRage;
		AbilityRedRage;
		AbilityBlueRage;
		AbilityPurpleRage;
		AbilityPowerOfAbyss;
		AbilityInfection;
		AbilityMatchThree;
		AbilityRemoveStone;
		AbilityPowerDrain;
		AbilityIceBlock;
		
		GlobalEffectFreezingRain;
		GlobalEffectRage;
		
		EffectBomb;
		EffectFrozen;
		EffectGoblinMine;
		EffectPoison;
		EffectStoneOfHealth;
		EffectIceBlock;
	}

}