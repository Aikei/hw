package my.system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Misc;
	import my.system.Language;
	public class NameGenerator 
	{
		static private var _syllables: Array;
		
		static public function Initialize()
		{
			var s: String = "al,nu,fu,ho,wo,wi,bo,bu,bi,blo,bla,do,di,dva,dve,ert,slok,ru,ro,ra,ri,al,gno,rno,zno,zi,gi,ta,tya,nya,nu,fog,hu,yok,yom,gom,br,klo,glo,gdo";
			_syllables = s.split(",");			
		}
		
		static public function GenerateName(): String
		{
			var nSyllables = Misc.Random(2, 4);
			var name: String = "";
			for (var i = 0; i < nSyllables; i++)
			{
				var n: int = Misc.Random(0, _syllables.length - 1);
				name += _syllables[n];
			}
			name = Language.TransliterateLatin(name);
			name = name.charAt(0).toUpperCase() + name.substr(1);
			return name;
		}
		
		public function NameGenerator() 
		{
			
		}
		
	}

}