package my.system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Misc;
	import starling.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	public class TextEditor 
	{
		public var text: String = "";
		public var cursorPos: int = 0;
		public var cursorString: String = "";
		public var cursorChar: String = "_";
		
		public function TextEditor() 
		{
			
		}
		
		public function OnKeyboardEvent(event:KeyboardEvent): Boolean
		{
			if (Misc.IsCharCodePrintable(event.charCode))
			{
				if (cursorPos == text.length)
					text += String.fromCharCode(event.charCode);
				else
				{
					text = text.slice(0, cursorPos) + String.fromCharCode(event.charCode) + text.slice(cursorPos, text.length);					
				}
				MoveCursorRight();
				return true;
			}				
			else if (event.keyCode == Keyboard.BACKSPACE)
			{
				if (text.length > 0)
				{
					var str: String = text.slice(0, cursorPos-1);
					if (cursorPos < text.length)
					{
						str += text.slice(cursorPos, text.length);
					}
					text = str;
					MoveCursorLeft();
				}
				return true;
			}
			else if (event.keyCode == Keyboard.LEFT)
			{
				MoveCursorLeft();
				return true;
			}
			else if (event.keyCode == Keyboard.RIGHT)
			{
				MoveCursorRight();
				return true;
			}
			return false;
		}
		
		public function Clear(): void
		{
			text = "";
			UpdateCursor();
		}
		
		private function MoveCursorRight(): void
		{
			if (cursorPos < text.length)
			{
				cursorPos++;
				UpdateCursor();
			}
		}
		
		private function MoveCursorLeft(): void
		{
			if (cursorPos > 0)
			{
				cursorPos--;
				UpdateCursor();
			}
		}		
		
		private function UpdateCursor(): void
		{
			cursorString = text;
			if (cursorPos >= text.length)
			{
				cursorPos = text.length;
				cursorString += cursorChar;
			}
			else
			{
				var str: String = cursorString.slice(0, cursorPos) + cursorChar;
				//if (cursorPos < cursorString.length - 1)
					//str += cursorString.slice(cursorPos + 1, cursorString.length);
				cursorString = str;
			}
		}
		
	}

}