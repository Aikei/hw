package my.system 
{
	import flash.events.Event;
	import flash.utils.Dictionary;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import my.logic.abilities.Ability;
	/**
	 * ...
	 * @author Aikei
	 */
	public class Language 
	{
		private static var _strings : Dictionary = new Dictionary;
		private var myTextLoader:URLLoader = new URLLoader();
		private static var _language: String = "ru";
		public function Language() 
		{
			//myTextLoader.addEventListener(Event.COMPLETE, OnLoaded);
			//myTextLoader.load(new URLRequest("ast/ru.txt"));
			if (_language === "ru")
				LoadRussian();
			else if (_language === "en")
				LoadEnglish();
		}
		
		public function OnLoaded(ev: Event): void
		{
			var arrayOfLines: Array = ev.target.data.split(/\n/);
			for (var i: int = 0, l: int = arrayOfLines.length; i < l; i++)
			{
				var strings: Array = arrayOfLines[i].split(" === ");
				_strings[strings[0]] = strings[1];
			}
		}
		
		public static function GetString(str: String): String
		{
			return _strings[str];
		}
				
		private function LoadEnglish(): void
		{						
			
			_strings["Find match"] = "Find match";
			_strings["Looking for game"] = "Looking for game";
			
			_strings["Cost"] = "Cost";
			_strings["Free"] = "Free";
			
			
			_strings["YouHaveNoMoves"] = "You have no possible moves!";
			_strings["EnemyHasNoMoves"] = "Enemy has no posible moves!";			
			_strings["NoMoves"] = "No possible moves!";
		
			_strings["YourTurn"] = "Your turn";
			_strings["EnemyTurn"] = "Enemy turn";
			_strings["PickSpells"] = "Pick 4 spells";
			_strings["PickTargetStone"] = "Pick target stone";
			_strings["PickTargetYourAbility"] = "Pick your aability as target";
			_strings["Cancel"] = "Cancel";
			_strings["OK"] = "OK";
			_strings["EnemyUsedAbility"] = "Enemy used";
			_strings["YouUsed"] = "You used";
			
			//profile
			_strings["Name"] = "Name";
			_strings["GamesPlayed"] = "Games played";
			_strings["GamesWon"] = "Games won";
			_strings["GamesLost"] = "Games lost";
			_strings["ToLibrary"] = "Library";
			_strings["EloRating"] = "Elo rating";
			_strings["Rank"] = "Rank";
			
			//abilities
			_strings[Ability.POISON] = "Poison";
			_strings["AbilityPoisonHelp"] = "Applies an effect to the target stone which decreases your opponent's health by %damage at the beginning of his every turn, until the stone is destroyed or effect removed.";
			
			_strings[Ability.TURN] = "Transformation";
			_strings["AbilityTurnHelp"] = "Changes color of the stone to any color of your choice.";
			
			_strings[Ability.FIRE] = "Fire";
			_strings["AbilityFireHelp"] = "Destroys the targeted stone as well as its vertical and horizontal neigbors, giving all benefits you would normally get for destroying them (e.g. mana and damage to the opponent).";
			
			_strings[Ability.HEAL] = "Heal";
			_strings["AbilityHealHelp"] = "Heals you %heal health points";
			
			_strings[Ability.FREEZE] = "Freeze";
			_strings["AbilityFreezeHelp"] = "Freezes the target stone and all of its eight neighbors. They can't be destroyed by a normal move (that is, by chain selection) anymore, and all other effects already on them are not effective while they are frozen. They can still be destroyed with long chain bonus effects and abilities. Freezing ends at the start of your next turn.";
			
			_strings[Ability.BOMB] = "Bomb";
			_strings["AbilityBombHelp"] = "Applies an effect to the target stone which gradually ticks for %time turns. When the time is out, it explodes damaging your opponent by %damage health points. If the stone is destroyed in a different way, this effect is not triggered.";
			
			_strings[Ability.METEORITE_RAIN] = "Meteorite Rain";
			_strings["AbilityMeteoriteRainHelp"] = "Randomly destroys 10 stones. You get all benefits you normally get for destroying them.";
			
			_strings[Ability.SHATTER] = "Shatter";
			_strings["AbilityShatterHelp"] = "Destroys all stones in the same column and row as the targeted stone, without giving you any benefits (like mana or damage to your opponent) you would normally get for their destruction.";
			
			_strings[Ability.SEE_NEXT] = "Clear Vision";
			_strings["AbilitySeeNextHelp"] = "If you have this ability, you can see five next stone colors which will be created after destroying stones on your turn. When you use it, you randomly change these next stones to other ones. Note, that new stones are added from left to right, from top to bottom, in turns, one-by-one to each column.";
			
			_strings[Ability.EARTHQUAKE] = "Earthquake";
			_strings["AbilityEarthquakeHelp"] = "Earthquake strikes all stones and removes them, without giving you any benefits for their removal.";
			
			_strings[Ability.STONE_OF_HEALTH] = "Stone of Health";
			_strings["AbilityStoneOfHealthHelp"] = "Applies an effect to the target stone which heals you %heal health points at the start of your turn.";
			
			_strings[Ability.SHIELD_OF_ICE] = "Shield of Ice";
			_strings["AbilityShieldOfIceHelp"] = "Add %shield points of shield. One shield point absorbs one damage point. Shield always resets at the beginning of your next turn.";
			
			_strings[Ability.DARK_MADNESS] = "Dark Frenzy";
			_strings["AbilityDarkMadnessHelp"] = "Destroys all purple stones.";
			
			_strings[Ability.CHAIN_LIGHTNING] = "Chain Lightning";
			_strings["AbilityChainLightningHelp"] = "Destroys the targeted stone and %number random stones.";
			
			_strings[Ability.GOBLIN_MINE] = "Goblin Mine";
			_strings["AbilityGoblinMineHelp"] = "Applies a hidden effect to the targeted stone, which will inflict %damage to the opponent when he selects it.";			
			
			_strings[Ability.FREEZING_RAIN] = "Freezing Rain";
			_strings["AbilityFreezingRainHelp"] = "After you activate this ability, all new stones will be created frozen until the start of your opponent's turn";
			
			_strings[Ability.GREEN_RAGE] = "Green Rage";
			_strings["AbilityGreenRageHelp"] = "Passive ability - always active. When you destroy green stones, you deal %mult1 times more damage, but don't get any mana. If you have another Rage ability, you don't receive mana for destruction of stones of any color, but deal %mult2 times more damage.";
			
			_strings[Ability.BLUE_RAGE] = "Blue Rage";
			_strings["AbilityBlueRageHelp"] = "Passive ability - always active. When you destroy blue stones, you deal %mult1 times more damage, but don't get any mana. If you have another Rage ability, you don't receive mana for destruction of stones of any color, but deal %mult2 times more damage.";

			_strings[Ability.RED_RAGE] = "Red Rage";
			_strings["AbilityRedRageHelp"] = "Passive ability - always active. When you destroy red stones, you deal %mult1 times more damage, but don't get any mana. If you have another Rage ability, you don't receive mana for destruction of stones of any color, but deal %mult2 times more damage.";

			_strings[Ability.PURPLE_RAGE] = "Purple Rage";
			_strings["AbilityPurpleRageHelp"] = "Passive ability - always active. When you destroy purple stones, you deal %mult1 times more damage, but don't get any mana. If you have another Rage ability, you don't receive mana for destruction of stones of any color, but deal %mult2 times more damage.";
			
			_strings[Ability.POWER_OF_ABYSS] = "Power of Abyss";
			_strings["AbilityPowerOfAbyssHelp"] = "You lose %yourDamage health. One of your abilities, randomly chosen from your uncharged active abilities, charges by %additionalMana mana.";
			
			_strings[Ability.INFECTION] = "Infection";
			_strings["AbilityInfectionHelp"] = "Each of your stone effects spreads to %number adjacent randomly chosen stone.";
			
			_strings[Ability.MATCH_THREE] = "Match Three";
			_strings["Ability" + Ability.MATCH_THREE+"Help"] = "Destroys all stones in chains of three or more stones of the same color.";
			
			_strings[Ability.REMOVE_STONE] = "Remove Stone";
			_strings["Ability" + Ability.REMOVE_STONE+"Help"] = "Removes selected stone from the field.";
			
			_strings[Ability.POWER_DRAIN] = "Power Drain";
			_strings["Ability" + Ability.POWER_DRAIN + "Help"] = "Pick a chain containing %shortestChain or more stones of the same color, then pick an ability of yours. This ability is charged by %receiveMana mana, while the stones in the chain are frozen until the start of your opponent's turn.";
			
			_strings[Ability.ICE_BLOCK] = "Ice Block";
			_strings["Ability" + Ability.ICE_BLOCK + "Help"] = "Freeze yourself, skipping your current turn and becoming invulnerable until the start of your next turn.";
						
		}		
		
		private function LoadRussian(): void
		{
			_strings["Find match"] = "Найти игру";
			_strings["Looking for game"] = "Идет поиск игры";
			
			_strings["Cost"] = "Стоимость";
			_strings["Free"] = "Бесплатно";
			
			_strings["YouHaveNoMoves"] = "У вас нет ходов!";
			_strings["EnemyHasNoMoves"] = "У противника нет ходов!";
			_strings["NoMoves"] = "Нет ходов!";

			_strings["YourTurn"] = "Ваш ход";
			_strings["EnemyTurn"] = "Ход соперника";
			_strings["PickSpells"] = "Выберите 4 заклинания";			
			_strings["PickTargetStone"] = "Выберите камень";
			_strings["PickTargetYourAbility"] = "Выберите вашу способность в качестве цели";
			_strings["Cancel"] = "Отмена";
			_strings["OK"] = "OK";
			_strings["EnemyUsedAbility"] = "Противник применил";
			_strings["YouUsed"] = "Вы применили";
			_strings["EloRating"] = "Рейтинг Эло";
			_strings["Rank"] = "Ранг";
			
			//profile
			_strings["Name"] = "Имя";
			_strings["GamesPlayed"] = "Игр сыграно";
			_strings["GamesWon"] = "Победы";
			_strings["GamesLost"] = "Поражения";
			_strings["ToLibrary"] = "Библиотека";
			
			//abilities
			_strings[Ability.POISON] = "Печать скверны";			
			_strings["AbilityPoisonHelp"] = "Накладывает на выбранный камень эффект, наносящий %damage ед. урона противнику в начале каждого его хода.";
			
			_strings[Ability.TURN] = "Трансформация";
			_strings["AbilityTurnHelp"] = "Меняет цвет камня на необходимый.";
			
			_strings[Ability.FIRE] = "Огонь";
			_strings["AbilityFireHelp"] = "Уничтожает выбранный камень и соседние с ним камни по вертикали и горизонтали.";
			
			_strings[Ability.HEAL] = "Лечение";
			_strings["AbilityHealHelp"] = "Лечит вас на %heal очков здоровья.";
			
			_strings[Ability.FREEZE] = "Морозный выстрел";
			//_strings["AbilityFreezeHelp"] = "Замораживает выбранную цепочку до начала вашего следующего хода.";
			_strings["AbilityFreezeHelp"] = "Замораживает выбранный камень и все восемь соседних с ним.";
			
			_strings[Ability.BOMB] = "Чародейская бомба";
			_strings["AbilityBombHelp"] = "Заряжает камень темной энергией, которая вырвется наружу через %time ходов, нанося %damage ед. урона противнику.";
			
			_strings[Ability.METEORITE_RAIN] = "Метеоритный дождь";
			_strings["AbilityMeteoriteRainHelp"] = "Уничтожает %number случайных камней.";
			
			_strings[Ability.SHATTER] = "Разбить вдребезги";
			_strings["AbilityShatterHelp"] = "Уничтожает все камни в тех же вертикальном и горизонтальном рядах, что и выбранный камень. При этом вы не получаете бонусов (таких как мана или нанесение повреждений оппоненту) за уничтожение этих камней.";
			
			_strings[Ability.SEE_NEXT] = "Ясное видение";
			_strings["AbilitySeeNextHelp"] = "Если у вас есть эта способность, вы можете видеть цвета пяти следующих камней, которые будут созданы после уничтожения камней на вашем ходу. Когда вы используете эту способности, эти следующие камни случайно меняются на другие. Имейте в виду: новые камни создаются слева направо, и сверху вниз, по очереди, по одному на каждый вертикальный ряд.";
			
			_strings[Ability.EARTHQUAKE] = "Землетрясение";
			_strings["AbilityEarthquakeHelp"] = "Убирает с поля все камни.";
			
			_strings[Ability.STONE_OF_HEALTH] = "Печать исцеления";
			_strings["AbilityStoneOfHealthHelp"] = "Накладывает на камень эффект, который восстанавливает вам %heal ед. здоровья в начале каждого вашего хода.";
			
			_strings[Ability.SHIELD_OF_ICE] = "Ледяной щит";
			_strings["AbilityShieldOfIceHelp"] = "Добавляет вам %shield ед. защиты. Щит рассеивается в начале вашего следующего хода.";
			
			_strings[Ability.DARK_MADNESS] = "Темное безумие";
			_strings["AbilityDarkMadnessHelp"] = "Уничтожает все фиолетовые камни, не давая вам маны за их уничтожение.";
			
			_strings[Ability.CHAIN_LIGHTNING] = "Цепная молния";
			_strings["AbilityChainLightningHelp"] = "Уничтожает выбранный камень и еще %number случайных.";
			
			_strings[Ability.GOBLIN_MINE] = "Гоблинская мина";
			_strings["AbilityGoblinMineHelp"] = "Накладывает на выбранный камень скрытый эффект, который нанесет %damage ед. урона противнику, когда он выберет этот камень.";
			
			_strings[Ability.FREEZING_RAIN] = "Ледяной дождь";
			_strings["AbilityFreezingRainHelp"] = "После того, как вы активируете эту способность, все новые камни будут создаваться замороженными до начала хода вашего оппонента.";		
			
			_strings[Ability.GREEN_RAGE] = "Зеленая ярость";
			_strings["AbilityGreenRageHelp"] = "Пассивная способность - действует всегда. Когда вы уничтожаете зеленые камни, вы наносите в %mult1 раза больше повреждений, но не получаете ману. Если у вас есть еще одна способность с названием Ярость, вы не получаете ману за уничтожение камней любых цветов, но наносите в %mult2 раза больше повреждений.";
		
			_strings[Ability.BLUE_RAGE] = "Синяя ярость";
			_strings["AbilityBlueRageHelp"] = "Пассивная способность - действует всегда. Когда вы уничтожаете синие камни, вы наносите в %mult1 раза больше повреждений, но не получаете ману. Если у вас есть еще одна способность с названием Ярость, вы не получаете ману за уничтожение камней любых цветов, но наносите в %mult2 раза больше повреждений.";

			_strings[Ability.RED_RAGE] = "Красная ярость";
			_strings["AbilityRedRageHelp"] = "Пассивная способность - действует всегда. Когда вы уничтожаете красные камни, вы наносите в %mult1 раза больше повреждений, но не получаете ману. Если у вас есть еще одна способность с названием Ярость, вы не получаете ману за уничтожение камней любых цветов, но наносите в %mult2 раза больше повреждений.";

			_strings[Ability.PURPLE_RAGE] = "Фиолетовая ярость";
			_strings["AbilityPurpleRageHelp"] = "Пассивная способность - действует всегда. Когда вы уничтожаете фиолетовые камни, вы наносите в %mult1 раза больше повреждений, но не получаете ману. Если у вас есть еще одна способность с названием Ярость, вы не получаете ману за уничтожение камней любых цветов, но наносите в %mult2 раза больше повреждений.";
			
			_strings["AbilityRageHelp"] = "При уничтожении камней, вы наносите на %mult1 больше урона, но получаете на %manaMult меньше маны. Если у вас несколько способностей типа Ярость, их эффекты суммируются.";
			
			_strings[Ability.POWER_OF_ABYSS] = "Сила бездны";
			_strings["AbilityPowerOfAbyssHelp"] = "Вы теряете %yourDamage здоровья. Одна способность, случайно выбранная из ваших активных незаряженных способностей, заряжается на %additionalMana единицы маны.";
			
			_strings[Ability.INFECTION] = "Инфекция";
			_strings["AbilityInfectionHelp"] = "Каждый из ваших эффектов на камнях распространяется на %number соседний камень, выбранный случайным образом.";
			
			_strings[Ability.MATCH_THREE] = "Три в ряд";
			_strings["Ability" + Ability.MATCH_THREE+"Help"] = "Уничтожает все камни, находящиеся в рядах длиной три или более камней одного цвета."
			
			_strings[Ability.REMOVE_STONE] = "Убрать камень";
			_strings["Ability" + Ability.REMOVE_STONE+"Help"] = "Убирает выбранный камень с поля.";
			
			_strings[Ability.POWER_DRAIN] = "Забрать силу";
			_strings["Ability" + Ability.POWER_DRAIN + "Help"] = "Выберите цепочку длиной %shortestChain или более камней, затем выберите вашу способность. Эта способность заряжается на %receiveMana маны, а цепочка замораживается до начала хода вашего противника.";
			
			_strings[Ability.ICE_BLOCK] = "Ледяная глыба";
			_strings["Ability" + Ability.ICE_BLOCK + "Help"] = "Вы замораживаете себя, пропуская этот ход и становясь неуязвимым до начала следующего.";	
			
			//tutrial
			_strings["Tutorial1"] = "Добро пожаловать в Powerstone! Ваша цель состоит в уничтожении противника соединяя в цеочки магические камни и используя мощные заклинания. Нажмите ОК для продолжения.";
			_strings["Tutorial2"] = "Каждый ваш ход вы уничтожаете цепочку из трех или более камней. Давайте научимся выбирать цепочки для уничтожения. Нажмите на подсвеченный камень левой кнопкой мыши и не отпускайте её.";
			_strings["Tutorial3"] = "Хорошо! Теперь, не отпуская левую кнопку мыши, проведите ее по очереди по каждому подсвеченному камню цепочки, затем отпустите кнопку, чтобы уничтожить выбранную цепочку.";
			_strings["Tutorial4"] = "После уничтожения цепочки ваш ход завершается и начинается ход вашего противника.";
		}
		
		public static function TransliterateLatin(word: String): String
		{
			if (_language === "en")
				return word;
			else if (_language === "ru")
			{
				var result: String = "";
				for (var i = 0; i < word.length; i++)
				{
					var ch: String = word.charAt(i);
					switch (ch)
					{
						case "a":
							ch = "а";
							break;
							
						case "b":
							ch = "б";
							break;
							
						case "c":
							ch = "к";
							break;
						case "d":
							ch = "д";
							break;
							
						case "e":
							ch = "е";
							break;
							
						case "f":
							ch = "ф";
							break;
							
						case "g":
							ch = "г";
							break;
							
						case "h":
							ch = "х";
							break;
							
						case "i":
							ch = "и";
							break;
							
						case "j":
							ch = "дж";
							break;
							
						case "k":
							ch = "к";
							break;
							
						case "l":
							ch = "л";
							break;
							
						case "m":
							ch = "м";
							break;
							
						case "n":
							ch = "н";
							break;
							
						case "o":
							ch = "о";
							break;
							
						case "p":
							ch = "п";
							break;
							
						case "q":
							ch = "к";
							break;
							
						case "r":
							ch = "р";
							break;
							
						case "s":
							ch = "с";
							break;
							
						case "t":
							ch = "т";
							break;
							
						case "u":
							ch = "у";
							break;
							
						case "v":
							ch = "в";
							break;
							
						case "w":
							ch = "у";
							break;
							
						case "x":
							ch = "кс";
							break;
							
						case "y":
							ch = "й";
							break;
							
						case "z":
							ch = "з";
							break;						
					}
					result += ch;
				}
				return result;
			}
			else
			{
				return word;
			}
		}			
	}


}