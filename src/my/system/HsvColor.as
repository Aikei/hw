package my.system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.utils.*;
	
	public class HsvColor 
	{
		public var h: Number, s : Number, v : Number, a: Number;
		
		private var min: Number, max: Number, delta: Number;
		private var hh: Number, p: Number, q: Number, t: Number, ff: Number;
		private var i: int;
		
		//public function HsvColor(r: Number, g: Number, b: Number, a: Number) 
		//{
			//this.a = a;
			//FromRGB(r, g, b);
		//}
		
		public function HsvColor(clr: uint) 
		{
			this.a = Number(Color.getAlpha(clr))/255.0;
			FromRGB(Number(Color.getRed(clr))/255.0, Number(Color.getGreen(clr))/255.0, Number(Color.getBlue(clr))/255.0);
		}		
		
		public function FromRGB(r: Number, g: Number, b: Number): void
		{
			min = Math.min(r, g, b);
			max = Math.max(r, b, b);
			v = max;
			delta = max - min;
			if (max > 0)
			{
				s = delta / max;
			}
			else
			{
				s = 0.0;
				h = NaN;
				return;
			}
			if (r >= max)
			{
				h = (g - b) / delta;
			}
			else if (g >= max)
			{
				h = 2.0 + (b - r) / delta;
			}
			else
			{
				h = 4.0 + (r - g) / delta;
			}
			
			h = h * 60;
			
			if (h < 0.0)
				h += 360;			
		}
		
		public function ToRgbUint() : uint
		{
			var resA: int;
			var resR: int;
			var resG: int;
			var resB: int;
			
			resA = a * 255.0;
			
			if (s <= 0.0)
			{
				resR = v*255.0;
				resG = v*255.0;
				resB = v*255.0;
				return Color.argb(resA,resR,resG,resB);
			}
			
			hh = h;
			if (hh >= 360.0)
				hh = 0.0;
			hh /= 60;
			i = Math.floor(hh);
			ff = hh - i;
			p = v * (1.0 - s);
			q = v * (1.0 - (s * ff));
			t = v * (1.0 - (s * (1.0 - ff)));
			switch(i) 
			{
				case 0:
					resR = v*255.0;
					resG = t*255.0;
					resB = p*255.0;
					break;
				case 1:
					resR = q*255.0;
					resG = v*255.0;
					resB = p*255.0;
					break;
				case 2:
					resR = p*255.0;
					resG = v*255.0;
					resB = t*255.0;
					break;

				case 3:
					resR = p*255.0;
					resG = q*255.0;
					resB = v*255.0;
					break;
				case 4:
					resR = t*255.0;
					resG = p*255.0;
					resB = v*255.0;
					break;
				case 5:
				default:
					resR = v*255.0;
					resG = p*255.0;
					resB = q*255.0;
					break;
			}
			return Color.argb(resA, resR, resG, resB);  			
		}		
		
		public function Add(h: Number, s: Number, v: Number): void
		{
			if (isNaN(this.h))
				this.h = h;
			else
				this.h += h;
			if (h >= 360.0)
				this.h = 0.0;
			this.s += s;
			if (this.s > 1.0)
				this.s = 1.0;
			this.v += v;
			if (this.v > 1.0)
				this.v = 1.0;
		}
		
		public function Multiply(h: Number, s: Number, v: Number): void
		{
			if (isNaN(this.h))
				this.h = h;
			else
				this.h *= h;
			if (h >= 360.0)
				this.h = 0.0;
			this.s *= s;
			if (this.s > 1.0)
				this.s = 1.0;
			this.v *= v;
			if (this.v > 1.0)
				this.v = 1.0;
		}		
		
		//public function ToAxColor() : AxColor
		//{
			//var ax : AxColor = new AxColor;
			//ax.alpha = a;
			//if (s <= 0.0)
			//{
				//ax.red = v;
				//ax.green = v;
				//ax.blue = v;
				//return ax;
			//}
			//
			//hh = h;
			//if (hh >= 360.0)
				//hh = 0.0;
			//hh /= 60;
			//i = Math.floor(hh);
			//ff = hh - i;
			//p = v * (1.0 - s);
			//q = v * (1.0 - (s * ff));
			//t = v * (1.0 - (s * (1.0 - ff)));
			//switch(i) 
			//{
				//case 0:
					//ax.red = v;
					//ax.green = t;
					//ax.blue = p;
					//break;
				//case 1:
					//ax.red = q;
					//ax.green = v;
					//ax.blue = p;
					//break;
				//case 2:
					//ax.red = p;
					//ax.green = v;
					//ax.blue = t;
					//break;
//
				//case 3:
					//ax.red = p;
					//ax.green = q;
					//ax.blue = v;
					//break;
				//case 4:
					//ax.red = t;
					//ax.green = p;
					//ax.blue = v;
					//break;
				//case 5:
				//default:
					//ax.red = v;
					//ax.green = p;
					//ax.blue = q;
					//break;
			//}
			//return ax;  			
		//}
		

				
	}
}