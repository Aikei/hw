package my.traits 
{
	import my.MyEntity;

	/**
	 * ...
	 * @author Aikei
	 */
	public class BasicSpriteTrait extends BasicTrait
	{
		private var _sprite:MyEntity;
		
		public function BasicSpriteTrait(entity:MyEntity)
		{
			super(entity);
			_sprite = entity;
		}
		
		override public function Init(x: Number, y: Number): void
		{
			super.Init(x, y);
			//_sprite.fadeIn(0.001, 1);
			//_sprite.stopFlicker();
			//_sprite.scale.x = 1;
			//_sprite.scale.y = 1;
			//_sprite.clearEffects();
			//_sprite.color.alpha = 1;
			//_sprite.grow(0.001, 1, 1);
			//_sprite.clearEffects();
			//_sprite.animation = null;
			//_sprite.texture = null;
		}
	}

}