package my.traits 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import com.greensock.data.TweenLiteVars;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import my.MyEntity;
	
	public class BasicTrait 
	{
		private var _entity: MyEntity;
		private var _tweens: Vector.<TweenLite> = new Vector.<TweenLite>;
		private var _maxTweens: Vector.<TweenMax> = new Vector.<TweenMax>;
		
		public function BasicTrait(entity: MyEntity) 
		{
			_entity = entity;
		}
		
		public function Init(x: Number, y: Number): void
		{
			_entity.x = x;
			_entity.y = y;
			_entity.rotation = 0;
			//_entity.angle = 0;
			//_entity.velocity.x = 0;
			//_entity.velocity.y = 0;
			//_entity.velocity.a = 0;			
			//_entity.acceleration.x = 0;
			//_entity.acceleration.y = 0;
			//_entity.acceleration.a = 0;
			KillAllTweens();
		}
		
		public function AddTween(tween: TweenLite) : void
		{
			_tweens.push(tween);
		}
		
		public function AddMaxTween(tween: TweenLite) : void
		{
			_maxTweens.push(tween);
		}		
		
		public function AddTweensVector(v: Vector.<TweenLite>) : void
		{
			for (var i: int = 0, l: int = v.length; i < l; i++)
			{
				_tweens.push(v[i]);
			}
		}
		
		public function AddMaxTweensVector(v: Vector.<TweenMax>) : void
		{
			for (var i: int = 0, l: int = v.length; i < l; i++)
			{
				_maxTweens.push(v[i]);
			}
		}		
		
		public function ReturnAndKillAllTweens() : void
		{
			for (var i: int = 0, l: int = _tweens.length; i < l; i++)
			{
				_tweens[i].seek(0);
				_tweens[i].kill();
			}			
			_tweens = new Vector.<TweenLite>;
			for (i = 0, l = _maxTweens.length; i < l; i++)
			{
				_maxTweens[i].seek(0);
				_maxTweens[i].kill();
			}
			_maxTweens = new Vector.<TweenMax>;
		}
		
		public function KillAllTweens() : void
		{
			for (var i: int = 0, l: int = _tweens.length; i < l; i++)
			{
				_tweens[i].kill();
			}
			_tweens = new Vector.<TweenLite>;
		}		
	}

}