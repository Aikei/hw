package my.ai.pathfinder 
{
	import flash.display.Graphics;
	import my.Vec2;
	/**
	 * ...
	 * @author Aikei
	 */
	internal class PathingNode 
	{
		public var bPassable: Boolean;
		public var pos : Vec2 = new Vec2;
		public var index : Vec2 = new Vec2;
		public var tolerance: Number = 0.1;
		public var arcs: Vector.<PathingArc> = new Vector.<PathingArc>;
		private var id: int;
		
		public function PathingNode(a_x: Number, a_y: Number, a_bPassbale: Boolean) 
		{
			pos.x = a_x;
			pos.y = a_y;
			bPassable = a_bPassbale;
			index.CopyFrom(pos);
			//index.x = pos.x / PathingGraph.PATHING_NODE_SPACING_X;
			//index.y = pos.y / PathingGraph.PATHING_NODE_SPACING_Y;
			id = PathingGraph.GetNewPathingNodeId();
		}
		
		public function Equals(other: PathingNode): Boolean
		{
			return (id == other.id);
		}
		
		public function AddArc(arc: PathingArc): void
		{
			arcs.push(arc);
		}
		
		public function GetNeighbours() : Vector.<PathingNode>
		{
			var neighbours: Vector.<PathingNode> = new Vector.<PathingNode>;
			for (var i: int = 0, l: int = arcs.length; i < l; i++)
			{
				neighbours.push(arcs[i].GetNeighbour(this));
			}
			return neighbours;
		}
		
		public function ClearArc(arc: PathingArc): void
		{
			var newArcs: Vector.<PathingArc> = new Vector.<PathingArc>;
			for (var i: int = 0, l: int = arcs.length; i < l; i++)
			{
				if (!arc.Equals(arcs[i]))
					newArcs.push(arcs[i]);
			}
			arcs = newArcs;
		}
		
		public function ClearAllArcs(graph: PathingGraph): void
		{
			for each (var arc: PathingArc in arcs)
			{
				graph.ClearArc(arc);
				var neighbour: PathingNode = arc.GetNeighbour(this);
				neighbour.ClearArc(arc);
			}
			arcs = null;
		}
		
		public function GetCostFromNode(node: PathingNode): Number
		{
			var arc: PathingArc = FindArc(node);
			return arc.weight;
		}
		
		public function GetIndex(): Vec2
		{
			return index;
		}
		
		private function FindArc(linkedNode: PathingNode): PathingArc
		{
			for each(var arc: PathingArc in arcs)
			{
				if (arc.GetNeighbour(this).Equals(linkedNode))
					return arc;
			}
			return null;
		}
	}

}