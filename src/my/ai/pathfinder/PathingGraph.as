package my.ai.pathfinder 
{
	import my.Ball;
	import my.Vec2;
	import my.Misc;
	import my.MyWorld;
	/**
	 * ...
	 * @author Aikei
	 */
	public class PathingGraph 
	{
		public var nodes: Vector.<Vector.<PathingNode>> = new Vector.<Vector.<PathingNode>>;
		public var arcs: Vector.<PathingArc> = new Vector.<PathingArc>;
		
		private const PATHING_NODE_SPACING_X : int = Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X;
		private const PATHING_NODE_SPACING_Y : int = Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_Y;
		
		private static var lastPathingNodeId: int = 0;
		private static var lastPathingArcId: int = 0;
		private var world: MyWorld;
		private var longest : Boolean = true;
		
		public function PathingGraph(a_world: MyWorld) 
		{
			world = a_world;
			lastPathingNodeId = 0;
			lastPathingArcId = 0;
			var i: int;
			var j: int;
			//create pathing nodes
			for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
			{
				nodes.push(new Vector.<PathingNode>);
				for (j = 0; j < MyWorld.FIELD_HEIGHT; j++)
				{
					nodes[i].push(new PathingNode(i, j, true));
				}
			}
			
			//link pathing nodes
			for (j = 0; j < nodes.length; j++)
			{
				for (i = 0; i < nodes[j].length; i++)
				{
					if (j + 1 < nodes.length)
						LinkNodes(nodes[j][i], nodes[j + 1][i]);
					if (i - 1 >= 0)
						LinkNodes(nodes[j][i], nodes[j][i - 1]);
					if (j + 1 < nodes.length && i - 1 >= 0)
						LinkNodes(nodes[j][i], nodes[j + 1][i - 1]);
					if (j - 1 >= 0 && i - 1 >= 0)
						LinkNodes(nodes[j][i], nodes[j - 1][i - 1]);
				}
			}			
		}
		
		public function UpdatePassability(ballType: int): void
		{
			for (var i: int = 0; i < nodes.length; i++)
			{
				for (var j: int = 0; j < nodes[i].length; j++)
				{
					if (world.m_balls[i][j].m_type == ballType)
						nodes[i][j].bPassable = true;
					else
						nodes[i][j].bPassable = false;
				}
			}			
		}
		
		public function FindPathBetweenBalls(ball1: Ball, ball2: Ball, a_longest: Boolean): Vector.<Ball>
		{
			longest = a_longest;
			if (ball1.m_type != ball2.m_type || ball1.Equals(ball2))
				return null;
			UpdatePassability(ball1.m_type);
			var plan: PathPlan = FindPathBetweenPositions(ball1.m_index, ball2.m_index);
			if (plan == null)
				return null;
			var result : Vector.<Ball> = new Vector.<Ball>;
			for each (var node: PathingNode in plan.path)
			{
				result.push(world.m_balls[node.pos.x][node.pos.y]);
			}
			return result;
		}		
		
		private function DestroyGraph(): void
		{
			nodes = null;
			arcs = null;
		}
		
		//public function GetNode(x:int, y:int): PathingNode
		//{
			////return nodes
		//}
		
		public function FindClosestNode(pos: Vec2): PathingNode
		{
			return nodes[pos.x][pos.y];
		}
		
		public function FindPathBetweenPositions(startPos: Vec2, endPos: Vec2): PathPlan
		{
			var startNode: PathingNode = FindClosestNode(startPos);
			var goalNode: PathingNode = FindClosestNode(endPos);
			return FindPathBetweenNodes(startNode, goalNode);
		}
		
		public function FindPathBetweenPositionAndNode(startPos: Vec2, goalNode: PathingNode): PathPlan
		{
			var startNode: PathingNode = FindClosestNode(startPos);
			return FindPathBetweenNodes(startNode, goalNode);
		}
		
		public function FindPathBetweenNodes(startNode:PathingNode, goalNode: PathingNode): PathPlan
		{
			var astar: AStar = new AStar(longest);
			return astar.RunAStar(startNode, goalNode);
		}
		
		public function FindArc(arc: PathingArc): int
		{
			for (var i: int = 0; i < arcs.length; i++)
			{
				if (arc.Equals(arcs[i]))
					return i;					
			}
			trace("Arc wasn't found!");
			return -1;
		}
		
		public function ClearArc(arc: PathingArc): void
		{
			for (var i: int = 0; i < arcs.length; i++)
			{
				if (arcs[i].Equals(arc))
				{
					arcs.splice(i, 1);
					return;
				}
			}
		}
		
		public function LinkNodes(node1:PathingNode, node2:PathingNode, weight:int = 1): void
		{
			var newArc: PathingArc = new PathingArc(weight);
			newArc.LinkNodes(node1, node2);
			node1.AddArc(newArc);
			node2.AddArc(newArc);
			arcs.push(newArc);
		}
		
		static public function GetNewPathingNodeId(): int
		{
			return lastPathingNodeId++;
		}
		
		static public function GetNewPathingArcId(): int
		{
			return lastPathingArcId++;
		}		
	}

}