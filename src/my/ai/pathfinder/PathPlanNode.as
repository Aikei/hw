package my.ai.pathfinder 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Misc;
	import my.Vec2;
	internal class PathPlanNode 
	{
		public var previousPlanNode: PathPlanNode;
		public var pathingNode: PathingNode;
		public var goalNode: PathingNode;
		public var bClosed: Boolean;
		public var goalCost: Number;
		public var heuristic: Number;
		public var fitness: Number;
		
		public function PathPlanNode(a_currentNode: PathingNode, a_prevNode: PathPlanNode, a_goalNode: PathingNode) 
		{
			pathingNode = a_currentNode;
			previousPlanNode = a_prevNode;
			goalNode = a_goalNode;
			bClosed = false;
			UpdateHeuristics();
		}
		
		public function UpdatePreviousNode(prev: PathPlanNode): void
		{
			previousPlanNode = prev;
			UpdateHeuristics();
		}
		
		public function Less(other: PathPlanNode): Boolean
		{
			return fitness < other.fitness;
		}
		
		public function More(other: PathPlanNode): Boolean
		{
			return fitness > other.fitness;
		}		
		
		private function UpdateHeuristics(): void
		{
			if (previousPlanNode != null)
				goalCost = previousPlanNode.goalCost + pathingNode.GetCostFromNode(previousPlanNode.pathingNode); 
			else
				goalCost = 0;
			
			if (goalNode != null)
			{
				var res : Vec2 = new Vec2;
				res.CopyFrom(goalNode.pos);
				res = res.SubtractVector(pathingNode.pos);
				res.Absolute();
				var diff: Number;
				if (res.x > res.y)
					diff = res.y;
				else
					diff = res.y;
				//var diff: Number = Misc.GetDistanceBetweenVectors(pathingNode.pos, goalNode.pos);
				heuristic = diff;
			}
			else
				heuristic = 0;
				
			fitness = goalCost + heuristic;
			
		}
	}

}