package my.ai.pathfinder 
{
	/**
	 * ...
	 * @author Aikei
	 */
	internal class PathingArc 
	{
		internal var weight: Number;
		internal var node1: PathingNode;
		internal var node2: PathingNode;
		internal var id: int;
		
		public function PathingArc(a_weight: Number = 1) 
		{
			id = PathingGraph.GetNewPathingArcId();
			weight = a_weight;
		}
		
		public function LinkNodes(a_node1: PathingNode, a_node2: PathingNode): void
		{
			node1 = a_node1;
			node2 = a_node2;
		}
		
		public function GetNeighbour(node: PathingNode): PathingNode
		{
			if (node1.Equals(node))
				return node2;
			else
				return node1;
		}
		
		public function Equals(other:PathingArc): Boolean
		{
			return (id == other.id);
		}
		
	}

}