package my.ai.pathfinder 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.utils.Dictionary;
	import my.Misc;
	
	internal class AStar 
	{
		private var nodeMap: Dictionary = new Dictionary;
		private var startNode: PathingNode = null;
		private var goalNode: PathingNode = null;
		private var openSet: Vector.<PathPlanNode> = new Vector.<PathPlanNode>;
		private var longestPath: Boolean = true;
		
		public function AStar(a_longest: Boolean)
		{
			longestPath = a_longest;
		}
				
		public function RunAStar(a_startNode: PathingNode, a_goalNode: PathingNode): PathPlan
		{
			if (a_goalNode.arcs.length == 0)
				return null;
			if (a_startNode.Equals(a_goalNode))
				return null;
			startNode = a_startNode;
			goalNode = a_goalNode;
			AddToOpenSet(startNode, null);
			
			while (openSet.length > 0)
			{
				var node: PathPlanNode = openSet[0];
				if (node.pathingNode.Equals(goalNode))
					return RebuildPath(node);
				openSet.splice(0, 1);
				AddToClosedSet(node);
				
				var neighbours: Vector.<PathingNode> = node.pathingNode.GetNeighbours();
				
				for each (var nodeToEvaluate: PathingNode in neighbours)
				{
					var findPathPlanNode: PathPlanNode = nodeMap[nodeToEvaluate];
					if (findPathPlanNode != null && findPathPlanNode.bClosed == true)
						continue;
					
					var costForThisPath: Number = node.goalCost + nodeToEvaluate.GetCostFromNode(node.pathingNode);
					var isPathBetter: Boolean = false;
					
					var pathPlanNodeToEvaluate: PathPlanNode = null;
					if (findPathPlanNode != null)
						pathPlanNodeToEvaluate = findPathPlanNode;
					
					if (pathPlanNodeToEvaluate == null)
					{
						if (nodeToEvaluate.bPassable == false)
							AddToClosedFromNull(nodeToEvaluate, node);
						else
							pathPlanNodeToEvaluate = AddToOpenSet(nodeToEvaluate, node);
					}
					else 
					{
						if (!longestPath && costForThisPath < pathPlanNodeToEvaluate.goalCost)
							isPathBetter = true;
						if (longestPath && costForThisPath > pathPlanNodeToEvaluate.goalCost)
							isPathBetter = true;
					}
					
					if (isPathBetter)
					{
						pathPlanNodeToEvaluate.UpdatePreviousNode(node);
						ReinsertNode(pathPlanNodeToEvaluate);
					}
				}
				
			}
			return null;
		}
		
		public function Destroy(): void
		{
			nodeMap = null;
			openSet = null;
			startNode = null;
			goalNode = null;
		}
		
		public function AddToOpenSet(a_node: PathingNode, a_prevNode: PathPlanNode): PathPlanNode
		{
			var findPlanNode: PathPlanNode = nodeMap[a_node];
			var thisNode: PathPlanNode = null;
			if (findPlanNode == null)
			{
				thisNode = new PathPlanNode(a_node, a_prevNode, goalNode);
				nodeMap[a_node] = thisNode;
			}
			else
			{
				thisNode = findPlanNode;
				thisNode.bClosed = false;
			}
			
			InsertNode(thisNode);
			
			return thisNode;
		}
		
		public function AddToClosedSet(a_node: PathPlanNode): void
		{
			a_node.bClosed = true;
		}
		
		public function AddToClosedFromNull(nodeToEvaluate: PathingNode, planNode: PathPlanNode): void
		{
			var findPlanNode: PathPlanNode = nodeMap[nodeToEvaluate];
			var thisNode: PathPlanNode = null;
			if (findPlanNode == null)
			{
				thisNode = new PathPlanNode(nodeToEvaluate, planNode, goalNode);
				nodeMap[nodeToEvaluate] = thisNode;
			}
			else
			{
				thisNode = findPlanNode;
			}
			thisNode.bClosed = true;
		}		
		
		public function InsertNode(node: PathPlanNode, usePriority: Boolean = true): void
		{
			if (openSet.length == 0 || usePriority == false)
			{
				openSet.push(node);
				return;
			}
			
			var iterator: int = 0;
			var pCompare: PathPlanNode = openSet[0];
			if (!longestPath)
			{
				while (pCompare.Less(node))
				{
					iterator++;
					if (iterator != openSet.length)
						pCompare = openSet[iterator];
					else
						break;
				}
			}
			else
			{
				while (pCompare.More(node))
				{
					iterator++;
					if (iterator != openSet.length)
						pCompare = openSet[iterator];
					else
						break;
				}				
			}
			openSet.splice(iterator, 0, node);
		}
		
		public function ReinsertNode(node: PathPlanNode): void
		{
			for (var it: int = 0; it < openSet.length; it++)
			{
				if (node.pathingNode.Equals(openSet[it].pathingNode))
				{
					openSet.splice(it, 1);
					InsertNode(node);
					return;
				}
			}
			trace("Attemping to reinsert node that was never in the open list");
			InsertNode(node);
		}
		
		public function RebuildPath(goalNode: PathPlanNode): PathPlan
		{
			var pathPlan: PathPlan = new PathPlan;
			var node: PathPlanNode = goalNode;
			while (node != null)
			{
				if (node.previousPlanNode != null)
				{
					pathPlan.totalPathLength += Misc.GetDistanceBetweenVectors(node.pathingNode.pos, node.previousPlanNode.pathingNode.pos);
					pathPlan.pathLengthInNodes++;
				}
				pathPlan.AddNode(node.pathingNode);
				node = node.previousPlanNode;
			}
			pathPlan.index = 0;
			return pathPlan;
		}
	}

}