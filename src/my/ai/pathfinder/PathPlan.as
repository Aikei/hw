package my.ai.pathfinder 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Vec2;
	import my.Misc;
	
	internal class PathPlan 
	{
		public var path: Vector.<PathingNode> = new Vector.<PathingNode>;
		public var index: int;
		public var totalPathLength: Number;
		public var pathLengthInNodes: int;
		
		public function PathPlan() 
		{
			index = path.length;
			totalPathLength = 0;
			pathLengthInNodes = 0;
		}
		
		public function RecountLength(): void
		{
			pathLengthInNodes = -1;
			totalPathLength = 0;
			var itnext: int;
			for (var i: int = 0; i < path.length; i++)
			{
				pathLengthInNodes++;
				itnext = i;
				itnext++;
				if (itnext != path.length)
					totalPathLength += Misc.GetDistance(path[i].pos.x, path[i].pos.y, path[itnext].pos.x, path[itnext].pos.y);
			}
			
		}
		
		public function ResetPath(): void
		{
			index = 0;
		}
		
		public function GetCurrentNodePosition(): Vec2
		{
			return path[index].pos;
		}
		
		public function CheckForNextNode(pos: Vec2): Boolean
		{
			if (index == path.length)
				return false;
			var diff: Number = Misc.GetSquaredDistance(pos.x, pos.y, path[index].pos.x, path[index].pos.y);
			if (diff <= Math.pow(path[index].tolerance,2))
			{
				index++;
				return true;
			}
			return false;
		}
		
		public function CheckForEnd(): Boolean
		{
			if (index == path.length)
				return true;
			return false;
		}
		
		//public function AddNode(node: PathingNode)
		//{
			//path.push(node);
		//}
		
		public function AddNode(node: PathingNode): void
		{
			path.splice(0, 0, node);
		}
		
	}

}