package my.ai 
{
	import my.ai.pathfinder.PathingGraph;
	import my.MyWorld;
	import my.Ball;
	import my.Misc;
	import my.MyWorld;
	import my.Vec2;
	/**
	 * ...
	 * @author Aikei
	 */
	public class Ai 
	{
		private const BALLS_TO_CHECK_PER_TICK: int = 12*MyWorld.FIELD_WIDTH*MyWorld.FIELD_HEIGHT;
		
		private var m_ballInfo:Vector.<Vector.<AiBallInfo>>;
		private var m_possibleMoves: Vector.<Vector.<Ball>>;
		private var m_currentCheckMove: Vector.<Ball>;
		private var m_stillOtherWays: Boolean = false;
		private var m_pathingGraph: PathingGraph;
		
		private var s_i: int = 0;
		private var s_j: int = 0;
		private var s_k: int = 0;
		private var s_l: int = 0;
		
			
		public function Ai() 
		{
			m_pathingGraph = new PathingGraph(MyWorld.gWorld);
		}
				
		public function ResetAi(): void
		{
			m_possibleMoves = new Vector.<Vector.<Ball>>;
			m_ballInfo = new Vector.<Vector.<AiBallInfo>>;
			for (var i: int = 0; i < MyWorld.FIELD_WIDTH; i++)
			{
				m_ballInfo.push(new Vector.<AiBallInfo>);
				for (var j: int = 0; j < MyWorld.FIELD_HEIGHT; j++)
				{
					m_ballInfo[i].push(new AiBallInfo);
				}
			}
		}				
		
		public function GetAiMove(): Vector.<Ball>
		{			
			return GetMostScoringMove();
		}
		
		public function Update(): void
		{
			//if (MyWorld.gWorld.GetPhase() == MyWorld.PHASE_ENEMY_SELECT)
			//{
				//if (FindPossibleMoves())
				//{
					//MyWorld.gWorld.m_enemyBalls = GetAiMove();
					//MyWorld.gWorld.SetPhase(MyWorld.PHASE_ENEMY_SELECT_SHOW);
				//}
			//}
		}
		
		private function GetRandomAiMove(): Vector.<Ball>
		{
			var n: int = Misc.Random(0, m_possibleMoves.length - 1);
			return m_possibleMoves[n];			
		}
		
		private function GetShortestAiMove(): Vector.<Ball>
		{
			if (m_possibleMoves.length == 0)
				return null;
			var result: Vector.<Ball> = m_possibleMoves[0];	
			for (var i: int = 0, l: int = m_possibleMoves.length; i < l; i++)
			{
				if (m_possibleMoves[i].length < result.length)
				{
					result = m_possibleMoves[i];
				}
			}
			return result;
		}
		
		private function GetLongestAiMove(): Vector.<Ball>
		{
			if (m_possibleMoves.length == 0)
				return null;
			var result: Vector.<Ball> = m_possibleMoves[0];	
			for (var i: int = 0, l: int = m_possibleMoves.length; i < l; i++)
			{
				if (m_possibleMoves[i].length > result.length)
				{
					result = m_possibleMoves[i];
				}
			}
			return result;
		}
		
		private function GetMostScoringMove(): Vector.<Ball>
		{
			if (m_possibleMoves.length == 0)
				return null;
			var result: Vector.<Ball> = m_possibleMoves[0];
			var resultScore: Number = CountMoveScore(m_possibleMoves[0]);
			for (var i: int = 0, l: int = m_possibleMoves.length; i < l; i++)
			{
				var score: Number = CountMoveScore(m_possibleMoves[i]);
				if (score > resultScore)
				{
					resultScore = score;
					result = m_possibleMoves[i];
				}
			}
			trace("Counted score: ", resultScore);
			if (resultScore > 0)
				return result;
			else
				return GetShortestAiMove();			
		}
		
		private function GetMostHarmfulMove(): Vector.<Ball>
		{
			var result: Vector.<Ball>;
			var resultDamage: int = 0;
			if (m_possibleMoves.length == 0)
				return null;
			result = m_possibleMoves[0];
			for (var i: int = 0, l: int = m_possibleMoves.length; i < l; i++)
			{
				var damage: int = CountMoveDamage(m_possibleMoves[i]);
				if (damage > resultDamage)
				{
					resultDamage = damage;
					result = m_possibleMoves[i];
				}
			}
			if (resultDamage > 0)
				return result;
			else
				return GetShortestAiMove();
		}
		
		private function CountMoveScore(move: Vector.<Ball>): Number
		{
			var score: Number = 0;
			var justDamage: Number = CountMoveDamage(move);
			var allRemoved: Vector.<Ball> = GetAllRemovedBalls(move);
			var damageWithAdditions: Number = CountMoveDamage(allRemoved);
			score += justDamage * 3;
			if (damageWithAdditions > justDamage)
				score += (damageWithAdditions - justDamage) * 2;
			if (allRemoved.length > 8)
			{
				score -= (allRemoved.length - 8);
			}
			return score;
		}
		
		private function CountMoveDamage(move: Vector.<Ball>): int
		{
			var damage: int = 0;
			for (var i: int = 0, l: int = move.length; i < l; i++)
			{
				if (move[i].m_type == Ball.RED_BALL)
					damage++;
			}
			return damage;
		}
		
		private function CountMoveDamageWithAdditionalBalls(move: Vector.<Ball>): int
		{
			var damage: int = 0;
			var v: Vector.<Ball> = GetAllRemovedBalls(move);
			for (var i: int = 0, l: int = move.length; i < l; i++)
			{
				if (move[i].m_type == Ball.RED_BALL)
					damage++;
			}
			return damage;
		}
		
		private function GetAllRemovedBalls(v: Vector.<Ball>): Vector.<Ball>
		{
			var tv: Vector.<Ball> = null;
			var i: int;
			var l: int;
			if (v.length >= 5 && v.length < 9)
			{
				tv = MyWorld.gWorld.m_logic.GetBallsForExplosion(v[v.length - 1]);
			}
			else if (v.length >= 9)
			{
				tv = MyWorld.gWorld.m_logic.GetBallsForEliminateRows(v[v.length - 1]);
			}
			if (tv == null)
				tv = new Vector.<Ball>;
			for (i = 0, l = v.length; i < l; i++)
				tv.push();			
			tv = MyWorld.gWorld.m_logic.RemoveSameBalls(tv);
			return tv;
		}
		
		//private function RemoveSameBalls(v: Vector.<Ball>): Vector.<Ball>
		//{
			//var newVector: Vector.<Ball> = new Vector.<Ball>;
			//for (var i: int = 0, l: int = v.length; i < l; i++)
			//{
				//var alreadyHere: Boolean = false;
				//for (var j: int, l2: int = newVector.length; j < l2; j++)
				//{
					//if (v[i].Equals(newVector[j]))
						//alreadyHere = true;
				//}
				//if (!alreadyHere)
					//newVector.push(v[i]);
			//}
			//return newVector;
		//}
		
		private function FindPossibleMoves(): Boolean
		{
			//var i: int;
			//var j: int;
			//var k: int;
			//var l: int;
			if (s_i == 0 && s_j == 0 && s_k == 0 && s_l == 0)
				m_possibleMoves = new Vector.<Vector.<Ball>>;
			var ballsChecked: int = 0;
			for (s_i = s_i; s_i < MyWorld.FIELD_WIDTH; s_i++)
			{
				for (s_j = s_j; s_j < MyWorld.FIELD_HEIGHT; s_j++)
				{
					for (s_k = s_k; s_k < MyWorld.FIELD_WIDTH; s_k++)
					{
						for (s_l = s_l; s_l < MyWorld.FIELD_HEIGHT; s_l++)
						{
							var path: Vector.<Ball> = m_pathingGraph.FindPathBetweenBalls(MyWorld.gWorld.m_balls[s_i][s_j], MyWorld.gWorld.m_balls[s_k][s_l],true);
							if (path != null && path.length > 2)
								m_possibleMoves.push(path);
							path = m_pathingGraph.FindPathBetweenBalls(MyWorld.gWorld.m_balls[s_i][s_j], MyWorld.gWorld.m_balls[s_k][s_l],false);
							if (path != null && path.length > 2)
								m_possibleMoves.push(path);
							ballsChecked++;
							if (ballsChecked >= BALLS_TO_CHECK_PER_TICK)
							{
								s_l++;								
								return false;
							}
						}
						s_l = 0;
					}
					s_k = 0;
				}
				s_j = 0;
			}
			s_i = 0;		
			
			return true;
		}	

		
		//private function FindPossibleMoves(): void
		//{
			//var i: int;
			//var j: int;
			//m_possibleMoves = new Vector.<Vector.<Ball>>;
			//for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
			//{
				//for (j = 0; j < MyWorld.FIELD_HEIGHT; j++)
				//{
					//CheckBallForMoves(i, j);
				//}
			//}
		//}		
		//
		private function CheckBallForMoves(x: int, y: int): void
		{
			ClearAllInBallInfo();
			do
			{				
				m_stillOtherWays = false;
				ClearSelectedInBallInfo();
				CheckOneMove(MyWorld.gWorld.m_balls[x][y]);
				if (m_currentCheckMove.length > 2)
					m_possibleMoves.push(m_currentCheckMove);
			} while (m_stillOtherWays);
		}
		
		private function CheckOneMove(ball: Ball): void
		{
			var neighboursFound: Boolean = true;
			m_currentCheckMove = new Vector.<Ball>;
			m_currentCheckMove.push(ball);
			m_ballInfo[ball.m_index.x][ball.m_index.y].m_bSelected = true;
			while(neighboursFound)
			{
				neighboursFound = CheckThisBall(m_currentCheckMove[m_currentCheckMove.length - 1]);
			}
		}
		
		private function CheckThisBall(ball: Ball): Boolean
		{
			var neighbours: Vector.<Ball> = MyWorld.gWorld.m_logic.GetBallNeighbours(ball);
			neighbours = GetNeighboursOfType(neighbours, ball.m_type);
			neighbours = GetUncheckedUnselectedNeighbours(neighbours);
			var pos: Vec2;
			if (neighbours.length > 1)
			{
				m_ballInfo[neighbours[0].m_index.x][neighbours[0].m_index.y].m_bTookThisTurn = true;
				m_stillOtherWays = true;
			}
			if (neighbours.length > 0)
			{			
				m_ballInfo[neighbours[0].x][neighbours[0].y].m_bSelected = true;
				m_currentCheckMove.push(neighbours[0]);
				return true;
			}
			return false;
		}
		
		private function GetNeighboursOfType(neighbours: Vector.<Ball>, type: int): Vector.<Ball>
		{
			var results : Vector.<Ball> = new Vector.<Ball>;
			for each (var ball: Ball in neighbours)
			{
				if (ball.m_type == type)
					results.push(ball);
			}
			return results;
		}
		
		private function GetUncheckedUnselectedNeighbours(neighbours: Vector.<Ball>): Vector.<Ball>
		{
			var results : Vector.<Ball> = new Vector.<Ball>;
			for (var i: int = 0, l: int = neighbours.length; i < l; i++ )
			{
				if (m_ballInfo[neighbours[i].m_index.x][neighbours[i].m_index.y].m_bSelected == false && m_ballInfo[neighbours[i].m_index.x][neighbours[i].m_index.y].m_bTookThisTurn == false)
					results.push(MyWorld.gWorld.m_balls[neighbours[i].m_index.x][neighbours[i].m_index.y]);
			}
			return results;
		}
		
		private function ClearSelectedInBallInfo(): void
		{
			for (var i: int = 0, l: int = m_ballInfo.length; i < l; i++)
			{
				for (var j: int = 0, l2: int = m_ballInfo[i].length; j < l2; j++)
				{
					m_ballInfo[i][j].m_bSelected = false;
				}
			}
		}
		
		private function ClearAllInBallInfo(): void
		{
			m_ballInfo = new Vector.<Vector.<AiBallInfo>>;
			for (var i: int = 0; i < MyWorld.FIELD_WIDTH; i++)
			{
				m_ballInfo.push(new Vector.<AiBallInfo>);
				for (var j: int = 0; j < MyWorld.FIELD_HEIGHT; j++)
				{
					m_ballInfo[i].push(new AiBallInfo);
				}
			}
		}
		

		
	}

}