package my.view.elements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.logic.abilities.Ability;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	import my.system.Language;
	import my.Ball;
	import starling.utils.HAlign;
	import starling.text.TextFieldAutoSize;
	
	public class AbilityTooltipWindow extends TextWindow 
	{
		private var _nameText: TextField;
		private var _costText: TextField
		private var _costNumbers: Vector.<TextField> = new Vector.<TextField>;
		private var _fontSize: int = 14;
		private var _ability: Ability;
		private var _spacing: int = 5;
		public function AbilityTooltipWindow(ability:Ability) 
		{
			super(ability.help, TextWindow.VERTICAL_RESIZE, 250, 50, _fontSize);
			_ability = ability;
		}
		
		override public function OnAddedToStage(ev:Event)
		{
			super.OnAddedToStage(ev);
			_nameText = new TextField(250, 30, Language.GetString(_ability.type), "Bold", _fontSize+2, Color.WHITE, true);
			addChild(_nameText);
			_text.y += _nameText.height;
			_costText = new TextField(30, 50, Language.GetString("Cost") + ":", "Regular", _fontSize+2, Color.WHITE, true);
			_costText.autoSize = TextFieldAutoSize.HORIZONTAL;
			_costText.hAlign = HAlign.LEFT;
			_costText.y = _text.y + _quad.height + 25 - (_costText.height / 2);
			addChild(_costText);
			var lastX: int = _costText.x + _costText.width + _spacing;
			var lastY: int = _costText.y;
			
			var s: String = String(_ability.manaCost[_ability.color]);
			if (_ability.manaCost[_ability.color] === 0)
				s = Language.GetString("Free");
		
			var costValueText: TextField = new TextField(30, 50, s, "Regular", _fontSize+2, Ball.GetBallColorByType(_ability.color), true);
			costValueText.autoSize = TextFieldAutoSize.HORIZONTAL;
			costValueText.x = lastX;
			costValueText.y = lastY;
			lastX += costValueText.width + _spacing;
			addChild(costValueText);			
			
			//for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			//{
				//if (_ability.manaCost[i] > 0)
				//{
					//var text: TextField = new TextField(30, 50, String(_ability.manaCost[i]), "Regular", _fontSize+2, Ball.GetBallColorByType(i), true);
					//text.hAlign = HAlign.LEFT;
					//text.x = lastX;
					//text.y = lastY;
					//lastX += text.width + _spacing;
					//addChild(text);
				//}
			//}
			_quad.height += _costText.height+_nameText.height;
		}		
		
	}

}