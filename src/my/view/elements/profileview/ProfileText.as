package my.view.elements.profileview 
{
	import my.MyEntity;
	import starling.display.Shape;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.text.TextField;
	import my.system.Language;
	import my.MyWorld;
	import starling.utils.Color;
	import starling.text.TextFieldAutoSize;
	import feathers.controls.Panel;
	import my.view.InterfaceFactories;
	import starling.display.graphics.*;
	import starling.utils.Color;
	import feathers.layout.VerticalLayout;
	
	public class ProfileText extends Panel 
	{
		//private var lastY: Number = 0;
		//private const SPACING_Y: Number = 10;
		private var _strings: Vector.<TextField> = new Vector.<TextField>;
		private var _shape: Shape = new Shape;
		private var _initialized: Boolean = false;
		private var _textColor: uint = Color.WHITE;
		
		public function ProfileText() 
		{
			super();
			InterfaceFactories.SkinThisVerticalPanel(this);
			VerticalLayout(this.layout).gap = 20;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//addEventListener(Event.ENTER_FRAME, OnEnterFrame);
		}
		
		private function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);						
			var textField: TextField = new TextField(0, 20, Language.GetString("Name") + ": ", "Regular", 18, _textColor);
			_strings.push(textField);

			textField = new TextField(0, 20, Language.GetString("GamesPlayed") + ": ", "Regular", 18, _textColor);
			_strings.push(textField);
			
			textField = new TextField(0, 20, Language.GetString("GamesWon") + ": ", "Regular", 18, _textColor);
			_strings.push(textField);
			
			textField = new TextField(0, 20, Language.GetString("EloRating") + ": ", "Regular", 18, _textColor);
			_strings.push(textField);
			
			for (var i = 0; i < _strings.length; i++)
			{
				addChild(_strings[i]);
				_strings[i].autoSize = TextFieldAutoSize.HORIZONTAL;
			}
			
			parent.addChild(_shape);
		}
		
		public function Initialize(): void
		{
			_strings[0].text = Language.GetString("Name") + ": " + MyWorld.gWorld.m_socialProfile.name + " " + MyWorld.gWorld.m_socialProfile.lastName;
			_strings[1].text = Language.GetString("GamesPlayed") + ": " + MyWorld.gWorld.m_gameProfile.gamesPlayed;
			_strings[2].text = Language.GetString("GamesWon") + ": " + MyWorld.gWorld.m_gameProfile.gamesWon;
			_strings[3].text = Language.GetString("EloRating") + ": " + MyWorld.gWorld.m_gameProfile.elo;
			readjustLayout();
			_initialized = true;
		}
		
		//override protected function autoSizeIfNeeded():Boolean
		//{
			//var b: Boolean = super.autoSizeIfNeeded();
			////if (_initialized)
			////{
				////_shape.graphics.lineStyle(1, Color.YELLOW);
				////_shape.graphics.drawRect(x-1, y-1, width + 1, height + 1);
			////}
			//return b;
		//}
		
	}

}