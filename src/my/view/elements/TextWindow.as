package my.view.elements 
{

	
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyEntity;
	import starling.events.Event;	
	import starling.text.TextField;
	import starling.display.Quad
	import starling.text.TextFieldAutoSize;
	
	public class TextWindow extends MyEntity 
	{
		protected var _text: TextField;
		protected var _quad: Quad;
		protected var _alignment: int = 0;
		
		public static const NO_RESIZE: int = 0;
		public static const VERTICAL_RESIZE: int = 1;
		public static const HORIZONTAL_RESIZE: int = 2;
		public static const BOTH_DIRECTIONS_RESIZE: int = 3;
		public static const AUTOSCALE_RESIZE: int = 4;
		
		public static const ALIGNMENT_CENTER: int = 0;
		public static const ALIGNMENT_TOP_THIRD: int = 1;
		public static const ALIGNMENT_BOTTOM_THIRD: int = 2;
		
		
		//public function SetALignment(alignment: int): void
		//{
			//_alignment = alignment;
			//if (alignment === ALIGNMENT_CENTER)
			//{
				//_text.y = height / 2;
			//}
			//else if (alignment === ALIGNMENT_TOP_THIRD)
			//{
				//_text.y -= height / 6;
			//}
			//else if (alignment === ALIGNMENT_BOTTOM_THIRD)
			//{
				//_text.y += height / 6;
			//}			
		//}
		
		override public function get width(): Number
		{
			return _quad.width;
		}
		
		override public function  get height(): Number 
		{
			return _quad.height;
		}
		
		public function TextWindow(text: String, resizeType: int, width: int, height: int, fontSize: int = 16, alignment: int = ALIGNMENT_CENTER) 
		{
			alpha = 0.75;
			touchable = false;
			var fsz: int = fontSize;
			if (fontSize <= 0)
				fsz = 16;
			_text = new TextField(width, height, text, "Regular", fsz, 0xFFFFFFFF);
			
			if (resizeType === AUTOSCALE_RESIZE)
				_text.autoScale = true;
			else if (resizeType === VERTICAL_RESIZE)
				_text.autoSize = TextFieldAutoSize.VERTICAL;
			else if (resizeType === HORIZONTAL_RESIZE)
				_text.autoSize = TextFieldAutoSize.HORIZONTAL;
			else if (fontSize === BOTH_DIRECTIONS_RESIZE)
				_text.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			if (alignment === ALIGNMENT_TOP_THIRD)
			{
				_text.y -= height / 6;
			}
			else if (alignment === ALIGNMENT_BOTTOM_THIRD)
			{
				_text.y += height / 6;
			}
			_quad = new Quad(width, _text.height, 0xFF9D989E);
			//_quad.alpha = 0.75;
			//_text.alpha = 0.75;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnAddedToStage(ev:Event)
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_quad);
			addChild(_text);			
		}
		
	}

}