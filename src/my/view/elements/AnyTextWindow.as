package my.view.elements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyWorld;
	import my.system.Language;
	import feathers.controls.Button;
	import starling.display.Image;
	import starling.events.Event;
	import my.MyAssets;
	import feathers.text.BitmapFontTextFormat;
	import starling.utils.Color;
	import starling.events.TouchEvent;
	
	import feathers.core.ITextRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.controls.Panel;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import my.view.InterfaceFactories;
	import my.view.elements.StandardButton;
	import feathers.layout.VerticalLayout;
	
	public class AnyTextWindow extends Panel 
	{
		var _text: TextField;
		var _okButton: Button;
		var _noOk: Boolean = false;
		
		public function AnyTextWindow(text: String) 
		{
			visible = false;
			touchable = true;			
			_text = new TextField(400, 0, text, "Regular", 16, Color.WHITE);
			_text.autoSize = TextFieldAutoSize.VERTICAL;
			_okButton = new StandardButton(Language.GetString("OK"));
			_okButton.addEventListener(Event.TRIGGERED, OnTriggered);
			
			InterfaceFactories.SkinThisVerticalPanel(this,VerticalLayout.HORIZONTAL_ALIGN_CENTER);
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnTriggered(ev:Event): void
		{
			visible = false;
			MyWorld.gWorld.dispatchEvent(new Event("anyTextWindowOkPressed"));
		}
		
		public function OnAddedToStage(ev:Event)
		{
			//super.OnAddedToStage(ev);
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_text);
			addChild(_okButton);
		}
		
		public function SetText(text: String,noOk:Boolean = false)
		{
			_noOk = noOk;
			removeChild(_text);
			removeChild(_okButton);
			_text = new TextField(400, 0, text, "Regular", 16, Color.WHITE);
			_text.autoSize = TextFieldAutoSize.VERTICAL;
			addChild(_text);
			if (!_noOk)
				addChild(_okButton);
		}
		
		public function GetHeight():Number 
		{
			if (_noOk)
				return _text.height + (VerticalLayout(layout).padding * 2);
			else
				return _text.height+_okButton.defaultSkin.height+VerticalLayout(layout).gap+(VerticalLayout(layout).padding*2);
		}
		
	}

}