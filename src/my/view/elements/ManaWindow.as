package my.view.elements 
{
	import my.MyEntity;
	
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyEntity;
	import starling.events.Event;	
	import starling.text.TextField;
	import starling.display.Quad
	import starling.text.TextFieldAutoSize;
	import my.Ball;
		
	public class ManaWindow extends MyEntity 
	{
		private var numberOfRows: int = 3;
		private var numberOfColumns: int = 2;
		private var spacing: int = 5;
		
		private var _quad : Quad;		
		private var _manaText: Vector.<TextField> = new Vector.<TextField>;
		
		override public function get width(): Number
		{
			return _quad.width;
		}
		
		override public function  get height(): Number 
		{
			return _quad.height;
		}
		
		public function ManaWindow() 
		{
			touchable = false;
			var textX: int = spacing;
			var textY: int = spacing;
			var n: int = 0;
			for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			{
				var textField: TextField = new TextField(40, 25, "0", "Regular", 20, Ball.GetBallColorByType(i));
				textField.x = textX;
				textField.y = textY;
				textX += textField.width + spacing;
				n++;
				if (n >= numberOfColumns)
				{
					n = 0;
					textX = spacing;
					textY += textField.height + spacing;
				}
				_manaText.push(textField);
			}							
				
			_quad = new Quad(spacing+numberOfColumns*(spacing+_manaText[0].width), spacing+numberOfRows*(spacing+_manaText[0].height), 0xFF9D989E);
			//_quad.visible = false;
			//_quad.alpha = 0.75;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function Refresh(mana: Vector.<int>)
		{
			for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			{
				_manaText[i].text = String(mana[i]);
			}
		}
		
		public function OnAddedToStage(ev:Event)
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_quad);
			for (var i: int = 0; i < _manaText.length; i++)
				addChild(_manaText[i]);			
		}
		
	}

}