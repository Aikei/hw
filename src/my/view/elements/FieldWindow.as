package my.view.elements 
{
	import my.MyEntity;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.events.Event;
	import starling.display.Image;
	import flash.geom.Point;
	
	import my.MyAssets;
	import my.MyWorld;
	
	public class FieldWindow extends MyEntity 
	{
		public static const DISTANCE_FROM_PLAYER_WINDOW_TO_FIELD: int = 11;
		public static const ADDITIONAL_START_DISTANCE_FOR_HOLDER: int = 6;
		public static const DISTANCE_BETWEEN_HOLDERS: int = 2;
		
		private var _fieldHolder: Image = null;
		private var _stoneHolderTexture: Texture = null;
		private var _stoneHolders: Vector.<Image> = new Vector.<Image>;		
		
		public function FieldWindow() 
		{
			_fieldHolder = new Image(MyAssets.UiAtlas.getTexture("fieldHolder"));			
			_stoneHolderTexture = MyAssets.StonesAtlas.getTexture("bg");			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnAddedToStage(event: Event)
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_fieldHolder);
			//for (var i: int = 0; i < MyWorld.FIELD_WIDTH; i++)
			//{
				//for (var j: int = 0; j < MyWorld.FIELD_HEIGHT; j++)
				//{
					//var stoneHolder: Image = new Image(_stoneHolderTexture);
					//stoneHolder.x = ADDITIONAL_START_DISTANCE_FOR_HOLDER + (i * (stoneHolder.width+DISTANCE_BETWEEN_HOLDERS));
					//stoneHolder.y = ADDITIONAL_START_DISTANCE_FOR_HOLDER + (j * (stoneHolder.height+DISTANCE_BETWEEN_HOLDERS));
					//_stoneHolders.push(stoneHolder);
					//addChild(stoneHolder);
				//}
			//}
		}
		
		public function GetStoneHolderWidth(): Number
		{
			return _stoneHolderTexture.width;
		}
		
		public function GetStoneHolderHeight(): Number
		{
			return _stoneHolderTexture.height;
		}
		
		//public function GetFieldHolderWidth(): Number
		//{
			//return _fieldHolder.width;
		//}
		//
		//public function GetFieldHolderHeight(): Number
		//{
			//return _fieldHolder.height;
		//}				
		
		override public function get width(): Number
		{
			return _fieldHolder.width;
		}
		
		override public function get height(): Number
		{
			return _fieldHolder.height;
		}		
		
	}

}