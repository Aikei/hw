package my.view.elements 
{
	
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.text.TextField;
	import starling.textures.Texture;
	import my.MyEntity;	
	import starling.display.Image;
	import starling.events.Event;
	import my.MyAssets;
	import my.Preferences;
	import my.MyWorld;
	import starling.text.TextFieldAutoSize;
	import my.system.Language;
	
	public class TimerTurnElement extends MyEntity 
	{
		private var _frame: Image;
		private var _leftTurnThing: Image;
		private var _rightTurnThing: Image;
		private var _yourTurnTexture: Texture = MyAssets.UiAtlas.getTexture("turn_player");
		private var _enemyTurnTexture: Texture = MyAssets.UiAtlas.getTexture("turn_enemy");
		private var _inactiveTurnTexture: Texture = MyAssets.UiAtlas.getTexture("turn_inactive");
		private var _yourTurnText: TextField;
		private var _enemyTurnText: TextField;
		
		private var yourTurnColorOnYourTurn: uint = 0x9d410e;
		private var yourTurnColorOnEnemyTurn: uint = 0x16191f;
		private var enemyTurnColorOnHisTurn: uint = 0xffffff;
		private var enemyTurnColorOnYourTurn: uint = 0x16191f;
		
		public function TimerTurnElement() 
		{
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			_frame = new Image(MyAssets.UiAtlas.getTexture("timerFrame"));
			_leftTurnThing = new Image(_inactiveTurnTexture);
			_rightTurnThing = new Image(_inactiveTurnTexture);
			MyWorld.gWorld.addEventListener("NewTurn", OnNewTurn);
			_yourTurnText = new TextField(40, 40, Language.GetString("YourTurn"), "Regular", 15, yourTurnColorOnEnemyTurn);
			_yourTurnText.autoSize = TextFieldAutoSize.HORIZONTAL;
			_yourTurnText.alignPivot();
			_enemyTurnText = new TextField(40, 40, Language.GetString("EnemyTurn"), "Regular", 15, enemyTurnColorOnYourTurn);
			_enemyTurnText.autoSize = TextFieldAutoSize.HORIZONTAL;
			_enemyTurnText.alignPivot();
			
			_leftTurnThing.x = 12;
			_leftTurnThing.y = _frame.height / 2 - _leftTurnThing.height / 2;
			_yourTurnText.x = _leftTurnThing.x + _leftTurnThing.width / 2;
			_yourTurnText.y = _leftTurnThing.y + _leftTurnThing.height / 2;
			
			_rightTurnThing.x = _frame.width - _rightTurnThing.width - 12;
			_rightTurnThing.y = _frame.height / 2 - _rightTurnThing.height / 2;
			_rightTurnThing.scaleX = -1;
			_rightTurnThing.x += _rightTurnThing.width;
			_enemyTurnText.x = _rightTurnThing.x + _rightTurnThing.width / 2 - _rightTurnThing.width ;
			_enemyTurnText.y = _rightTurnThing.y + _rightTurnThing.height / 2;			
		}
		
		public function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);

			addChild(_frame);
			addChild(_leftTurnThing);
			addChild(_rightTurnThing);
			addChild(_yourTurnText);
			addChild(_enemyTurnText);
		}
		
		public function OnNewTurn(event: Event): void
		{
			if (event.data == true)
			{
				_leftTurnThing.texture = _yourTurnTexture;
				_yourTurnText.color = yourTurnColorOnYourTurn;
				_rightTurnThing.texture = _inactiveTurnTexture;
				_enemyTurnText.color = enemyTurnColorOnYourTurn;
			}
			else
			{
				_leftTurnThing.texture = _inactiveTurnTexture;
				_yourTurnText.color = yourTurnColorOnEnemyTurn;
				_rightTurnThing.texture = _enemyTurnTexture;
				_enemyTurnText.color = enemyTurnColorOnHisTurn;
			}
		}
		
		override public function get width(): Number
		{
			return _frame.width;
		}
		
		override public function get height(): Number
		{
			return _frame.height;
		}		
		
	}

}