package my.view.elements 
{

	import my.Ball;
	import my.logic.abilities.Ability;
	import my.logic.PlayerData;
	import my.MyEntity;
	import my.MyWorld;
	import my.Preferences;
	import my.view.TextEntity;
	import my.view.elements.ManaWindow;
	import my.MyAssets;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import my.view.DisappearingText;
	
	import flash.geom.Point;	
	import flash.utils.setTimeout;	
	import starling.display.Image;
	import starling.events.Event;	
	import starling.text.TextFieldAutoSize;
	import starling.utils.Color;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import my.system.NameGenerator;
	/**
	 * ...
	 * @author Aikei
	 */
	public class PlayerWindow extends BaseInterfaceElement 
	{
		private var _portraitFrame: Image;
		private var _portrait: Image;
		private var _hpText: TextField;
		private var _shieldText: TextField;
		private var _nameText: TextField;
		private var _nameTextColor: uint = 0x9d410e;
		private var _abilityButtons: Vector.<AbilityButton> = new Vector.<AbilityButton>;
		//private var _manaWindow: ManaWindow;
		private var _lastY: Number;
		private var _playerData: PlayerData = null;
		private var _isEnemy: Boolean;
		private var _abilityButtonX: int;
		
		private const NAME_TEXT_X: Number = 94;
		private const NAME_TEXT_Y: Number = 164;
		
		public function PlayerWindow(isEnemy: Boolean) 
		{
			_isEnemy = isEnemy;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);			
		}
		
		public function OnAddedToStage(event: Event): void
		{
			MyWorld.gWorld.addEventListener("RenewPlayerData", OnRenew);
			MyWorld.gWorld.addEventListener("RefreshPlayerData", OnRefresh);
			MyWorld.gWorld.addEventListener("NewPlayerCreated", OnNewPlayerCreated);
			_portraitFrame = new Image(MyAssets.UiAtlas.getTexture("portraitFrame"));
			_portrait = new Image(MyAssets.PortraitsAtlas.getTexture("1"));
			_portrait.x = 10 + 84;
			_portrait.y = 28 + 84;
			_portrait.alignPivot();
			//_portrait.y = 13
			//_portrait.x = (_portraitFrame.width - _portrait.width) / 2;
			
			_hpText = new TextField(60, 30, "100", "Bold", 20, 0xffffffff);
			_hpText.autoSize = TextFieldAutoSize.HORIZONTAL;
			_hpText.x = 30;
			_hpText.y = 73;
			_hpText.alignPivot();
			addChild(_portrait);
			addChild(_portraitFrame);
			addChild(_hpText);
			
			_shieldText = new TextField(60, 30, "(50)", "Bold", 20, Color.BLUE);
			_shieldText.autoSize = TextFieldAutoSize.HORIZONTAL;
			_shieldText.x = _hpText.x;
			_shieldText.y = _hpText.y + _hpText.height;
			_shieldText.alignPivot();
			_shieldText.visible = false;
			addChild(_shieldText);
			
			_nameText = new TextField(60, 30, NameGenerator.GenerateName(), "Regular", 16, _nameTextColor);
			_nameText.autoSize = TextFieldAutoSize.HORIZONTAL;
			_nameText.x = NAME_TEXT_X;
			_nameText.y = NAME_TEXT_Y;
			_nameText.alignPivot();
			addChild(_nameText);
			//_manaWindow = new ManaWindow();
			//if (!_isEnemy)
				//_manaWindow.x = _portraitFrame.x - _manaWindow.width - 10;
			//else
				//_manaWindow.x = _portraitFrame.x + _portraitFrame.width + 10;
			//_manaWindow.y = _portraitFrame.y;
			//addChild(_manaWindow);
			
			_abilityButtonX = 54;
		}
		
		public function InitializeOnGameStart(): void
		{
			if (_isEnemy)
				_nameText.text = MyWorld.gWorld.m_enemySocialProfile.name;
			else
				_nameText.text = MyWorld.gWorld.m_socialProfile.name;
			_nameText.alignPivot();
		}
		
		public function OnNewPlayerCreated(event: Event)
		{
			//if (PlayerData(event.data).isEnemy === _playerData.isEnemy)
			//{
				//addChild(PlayerData(event.data));
				//PlayerData(event.data).x = _portrait.x + _portrait.width / 2;
				//PlayerData(event.data).y = _portrait.y + _portrait.height / 2;
			//}
		}
		
		public function AddAbilityButtonsVector(v: Vector.<Ability>): void
		{
			for (var i: int = 0; i < Ball.NUMBER_OF_MANAS; i++)
			{
				for (var j: int = 0; j < v.length; j++)
				{
					if (v[j].GetChargeType() === Preferences.OrderOfColors[i])
					{
						AddAbilityButton(v[j]);
						break;
					}
				}
			}
		}
		
		public function AddAbilityButton(ability: Ability): void
		{
			var button: AbilityButton = new AbilityButton(ability, _isEnemy);						
			_abilityButtons.push(button);
			button.y = _lastY;
			button.x = _abilityButtonX;
			addChild(button);
			var addY: Number = button.GetHeightWithSquares() + 35;
			_lastY = button.y+addY;
		}
		
		public function GetWidth(): Number
		{
			return _portraitFrame.width;
		}
		
		public function GetHeight(): Number
		{
			return _portraitFrame.height;
		}				
		
		public function OnRefresh(event: Event): void
		{
			Refresh(event);
		}
		
		public function SetHpText(newHp: int)
		{
			_hpText.text = String(newHp);
		}
		
		public function TweenHpText(newHp: int)
		{
			TweenLite.to(_playerData, 1, { hp : newHp, 
				onUpdate : function(): void 
					{ 
						_hpText.text = String(_playerData.hp);
						_hpText.alignPivot();
					}
				} );
		}
		
		public function TweenShieldText(newShield: int)
		{
			TweenLite.to(_playerData, 1, { shield : newShield, 
				onUpdate : function(): void 
					{ 
						_shieldText.text = "(" + String(_playerData.shield) + ")"; 
						_shieldText.alignPivot(); 
					}
				} );
		}		
		
		private function Refresh(event: Event): void
		{
			for (var i: int = 0; i < event.data.length; i++)
			{
				if (event.data[i].isEnemy != _isEnemy)
					continue;
				var text: DisappearingText;
				
				var shieldDiff = event.data[i].shield - _playerData.shield;
				if (shieldDiff != 0)
				{
					if (shieldDiff > 0)
					{
						text = new DisappearingText("+" + String(shieldDiff), 80, Color.BLUE, 60, 0, true, 4);
					}
					else
					{
						text = new DisappearingText(String(event.data[i].lastDamageToShield), 80, Color.BLUE, 60, 0, true, 4);
					}
					_playerData.addChild(text);	
					TweenShieldText(event.data[i].shield);
				}
				//else
				//{
					//text = new DisappearingText("+" + String(shieldDiff), 80, Color.BLUE, 60, 0, true, 4);
				//}
				
				if (event.data[i].shield > 0)
				{
					_shieldText.visible = true;
				}
				else
				{
					setTimeout(function(shieldText): void { shieldText.visible = false; }, 1000, _shieldText);
				}
									
				var diff: int = event.data[i].hp - _playerData.hp;
				if (diff != 0)
				{
					//var text: DisappearingText;
					if (diff > 0)
					{
						text = new DisappearingText("+" + String(diff), 80, Color.GREEN, 0, 0, true, 4);
					}
					else
					{
						text = new DisappearingText(String(diff), 80, Color.RED, 0, 0, true, 4);
					}					
					_playerData.addChild(text);
					TweenHpText(event.data[i].hp);
				}								
				var manaVector: Vector.<int> = new Vector.<int>;
				for (var q: int = 0; q < event.data[i].mana.length; q++)
					manaVector.push(event.data[i].mana[q]);				
				for (var j: int = 0, l: int = event.data[i].abilities.length; j < l; j++)
				{
					for (var k: int = 0; k < _abilityButtons.length; k++)
					{
						if (event.data[i].abilities[j].type === _abilityButtons[k].ability.type)
						{
							_abilityButtons[k].SetCharge(manaVector);
						}
					}
				}
			}	
		}
		
		public function OnRenew(event: Event): void
		{
			Renew(PlayerData(event.data));
		}
				
		public function Renew(playerData: PlayerData): void
		{
			if (playerData.isEnemy != _isEnemy)
				return;
			if (_playerData !== null)
				_playerData.removeFromParent(true);				
			_hpText.text = String(playerData.hp);
			_playerData = playerData;
			_portrait.texture = MyAssets.PortraitsAtlas.getTexture(playerData.portraitName);
			for (var i: int = 0; i < _abilityButtons.length; i++)
				_abilityButtons[i].Destroy();
			_abilityButtons = new Vector.<AbilityButton>;
			_lastY = 219;
			AddAbilityButtonsVector(playerData.abilities);
			addChild(_playerData);
			_playerData.x = _portrait.x;
			_playerData.y = _portrait.y;			
		}
		
		public function AddHealthChangeText(change: int)
		{
			var t: DisappearingText;
			if (change < 0)
			{
				t = new DisappearingText(String(change), 25, Color.RED, _hpText.x + 50, _hpText.y, false);
			}
			else
			{
				t = new DisappearingText('+'+String(change), 25, Color.GREEN, _hpText.x + 50, _hpText.y, false);
			}			
			addChild(t);
			t.y -= (t.height - _hpText.height)/2;
		}
		
		public function DisableAbilityButtons(): void
		{
			for (var i: int = 0, l: int = _abilityButtons.length; i < l; i++)
			{
				_abilityButtons[i].Disable();
			}
		}
		
		public function EnableAbilityButtons(enable: Boolean = true): void
		{
			for (var i: int = 0, l: int = _abilityButtons.length; i < l; i++)
			{
				_abilityButtons[i].Enable(enable);
			}
		}		
		
	}

}