package my.view.elements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyWorld;
	import my.system.Language;
	import feathers.controls.Button;
	import starling.display.Image;
	import starling.events.Event;
	import my.MyAssets;
	import feathers.text.BitmapFontTextFormat;
	import starling.utils.Color;
	import starling.events.TouchEvent;
	
	import feathers.core.ITextRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.controls.Panel;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import my.view.InterfaceFactories;
	import my.view.elements.StandardButton;
	import feathers.layout.VerticalLayout;
	
	public class PickTargetTextWindow extends Panel 
	{
		var _text: TextField;
		var _cancelButton: Button;
		public function PickTargetTextWindow(text: String) 
		{
			//super(Language.GetString("PickTargetStone"), TextWindow.NO_RESIZE, 250, 170, 18, TextWindow.ALIGNMENT_TOP_THIRD);
			visible = false;
			touchable = true;			
			_text = new TextField(0, 30, text, "Regular", 18, Color.WHITE);
			_text.autoSize = TextFieldAutoSize.HORIZONTAL;
			//_cancelButton = new Button();
			//_cancelButton.defaultSkin = new Image(MyAssets.LobbyAtlas.getTexture("button"));
			//_cancelButton.hoverSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_s"));
			//_cancelButton.downSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_s"));
			//_cancelButton.label = Language.GetString("Cancel");
			//_cancelButton.labelFactory = function(): ITextRenderer
			//{
				//return new TextFieldTextRenderer();
			//}			
			//_cancelButton.defaultLabelProperties = new TextFieldTextRenderer;
			//_cancelButton.defaultLabelProperties = new BitmapFontTextFormat(MyAssets.FONT, 16, Color.WHITE, "center");
			_cancelButton = new StandardButton(Language.GetString("Cancel"));
			//_cancelButton.x = x + (width / 2) - (_cancelButton.defaultSkin.width / 2);
			//_cancelButton.y = y + height - _cancelButton.defaultSkin.height - 20;
			_cancelButton.addEventListener(Event.TRIGGERED, OnTriggered);
			
			InterfaceFactories.SkinThisVerticalPanel(this,VerticalLayout.HORIZONTAL_ALIGN_CENTER);
			//InterfaceFactories.SkinThisAnchorPanel(this);	
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnTriggered(ev:Event): void
		{
			MyWorld.gWorld.dispatchEvent(new Event("cancelPickTarget"));
		}
		
		public function OnAddedToStage(ev:Event)
		{
			//super.OnAddedToStage(ev);
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_text);
			addChild(_cancelButton);
		}
		
	}

}