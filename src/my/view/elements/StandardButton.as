package my.view.elements 
{
	import feathers.controls.Button;
	import feathers.core.ITextRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import my.MyAssets;
	import starling.display.Image;
	import flash.text.TextFormat;
	import starling.utils.Color;
	/**
	 * ...
	 * @author Aikei
	 */
	public class StandardButton extends Button 
	{
		
		public function StandardButton(l: String = "") 
		{
			super();
			defaultSkin = new Image(MyAssets.LobbyAtlas.getTexture("button"));
			hoverSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_s"));
			downSkin = new Image(MyAssets.LobbyAtlas.getTexture("button"));
			label = l;
			labelFactory = function(): ITextRenderer
			{
				var tf: TextFieldTextRenderer = new TextFieldTextRenderer;
				tf.textFormat = new TextFormat("Regular", 18, Color.WHITE);
				return tf;
			}						
		}
		
		override public function get width(): Number
		{
			return defaultSkin.width;
		}
		
		override public function get height(): Number
		{
			return defaultSkin.height;
		}
		
	}

}