package my.view.elements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.geom.Point;
	import starling.display.DisplayObjectContainer;
	import my.MyAssets;
	import my.view.TextEntity;
	import starling.display.Image;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.events.Event
	
	import feathers.controls.ToggleButton;
	
	import my.logic.abilities.Ability;
	import my.mp.Controller;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import feathers.core.PopUpManager;
	import starling.text.TextFieldAutoSize;
	import my.view.InterfaceFactories;
	import flash.utils.setTimeout;
	import my.view.elements.AbilityTooltipWindow;
	
	public class AbilityPickButton extends ToggleButton
	{		

		private var _ability: Ability;
		//private var _selector: Image = null;
		private var _holder: Image = null;
		//private var _holderNormalTexture: Texture = MyAssets.LobbyAtlas.getTexture("normal");
		private var _holderSelectedTexture: Texture = MyAssets.LobbyAtlas.getTexture("selected");
		private var _isEnabled: Boolean = false;
		private var _textWindow: AbilityTooltipWindow
		private var _hovered: Boolean = false;
		private var _host: LobbyAbilityPickWindow;
		//private var _text: TextField = null;
		//private var id: int;
		//public static var lastId: int = 0;					
		
		public function get ability(): Ability
		{
			return _ability;
		}		
		
		public function AbilityPickButton(ability: Ability) 
		{
			defaultSkin = new Image(ability.buttonPushedTexture);
			upSkin = new Image(ability.buttonTexture);
			disabledSkin = new Image(ability.buttonTexture);
			
			downSkin = new Image(ability.buttonPushedTexture);
			hoverSkin = new Image(ability.buttonPushedTexture);			
			
			
			//_selector = new Image(ability.buttonSelectorTexture);
			//_selector.x = x + defaultSkin.width / 2;
			//_selector.y = y + defaultSkin.height / 2;
			
			_holder = new Image(_holderSelectedTexture);
			_holder.visible = false;
			_holder.x = x + defaultSkin.width / 2;
			_holder.y = y + defaultSkin.height / 2;
			_holder.alignPivot();
			_ability = ability;
			ScaleTo(70, 70);			
			addEventListener(Event.CHANGE, OnChange);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addEventListener(TouchEvent.TOUCH, OnTouch);

		}
		
		public function OnTouch(ev: TouchEvent): void
		{
			var touch:Touch = ev.getTouch(this, null);
			if (touch === null)
			{
				if (_hovered)
				{
					_hovered = false;
					PopUpManager.removePopUp(_textWindow, true);
					_textWindow = null;
				}
				return;
			}
			if (touch.phase === TouchPhase.HOVER)
			{
				_hovered = true;
				if (_textWindow)
				{
					PopUpManager.removePopUp(_textWindow, true);
					_textWindow = null;
				}
				_textWindow = new AbilityTooltipWindow(_ability);
				PopUpManager.addPopUp(_textWindow, false, false);				
				_textWindow.x = touch.globalX - _textWindow.width/2;
				_textWindow.y = touch.globalY - _textWindow.height;
			}
		}		
		
		//public function OnTouch(ev: TouchEvent): void
		//{
			//var touch:Touch = ev.getTouch(this, null);
			//if (touch == null)
				//return;
			//if (touch.phase === TouchPhase.HOVER)
			//{
				//if (_text)
					//PopUpManager.removePopUp(_text, true);
				//_text = new TextField(300, 200, _ability.help, "Regular", 18, 0xffffffff);
				////_text.autoSize = TextFieldAutoSize.VERTICAL;
				//_text.autoScale = true;
				//_text.alpha = 0.75;
				//_text.x = touch.globalX - _text.width/2;
				//_text.y = touch.globalY - _text.height*1.2;
				//PopUpManager.addPopUp(_text, false, false);
				////setTimeout(RemovePopUp,250);
			//}
		//}		
		
		public function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_holder);
			//_selector = new Image(_ability.buttonSelectorTexture);			
			_isEnabled = false;
			upSkin.alpha = 0.7;
			//addChild(_selector);
			//_selector.visible = false;
			_host = LobbyAbilityPickWindow(parent);
			//for (var i: int = 0; i < Preferences.CHARGE_SQUARES_NUMBER; i++)
			//{
				//_squares.push(new Image(_squaresEmptyTexture));
				//_squares[i].x = i*(_squares[i].width+8);
				//_squares[i].y = GetHeight() + 10;
				//addChild(_squares[i]);
			//}
			//var p: Point = localToGlobal(new Point(_squares[1].x+_squares[1].width+4, _squares[1].y));
			//_ability.chargerPos = p;
		}		
		
		public function GetWidth() : Number
		{
			return defaultSkin.width*scaleX;
		}
		
		public function GetHeight(): Number
		{
			return defaultSkin.height*scaleY;
		}			
		
		public function OnChange(event: Event): void
		{
			var b: Boolean = isSelected;
			if (b)
			{
				_host.host.PickAbility(_ability.GetChargeType(),true);
				if (_host.host.abilitiesPicked[_ability.GetChargeType()] === 1)
				{					
					Controller.PickAbility(_ability);
					_holder.visible = true;
					//_selector.visible = true;
				}
				else
				{
					trigger();
				}
			}
			else
			{
				_host.host.PickAbility(_ability.GetChargeType(),false);
				Controller.UnpickAbility(_ability);
				_holder.visible = false;
				//_selector.visible = false;
			}
		}
		
		public function Destroy(): void
		{
			removeEventListener(Event.CHANGE, OnChange);
			removeEventListener(TouchEvent.TOUCH, OnTouch);
			removeFromParent(true);
		}
		
		public function Enable(): void
		{
			_isEnabled = true;
		}
		
		public function Disable(): void
		{
			_isEnabled = false;
		}
		
		public function AlignCenter(): void
		{
			x -= GetWidth() / 2;
			y -= GetHeight() / 2;
		}
		
		public function ScaleTo(scaleWidth: Number, scaleHeight: Number): void
		{
			scaleX = scaleWidth / defaultSkin.width;
			scaleY = scaleHeight / defaultSkin.height;
			_holder.scaleX = 1/scaleX;
			_holder.scaleY = 1/scaleY;
		}		
		
	}

}