package my.view.elements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.logic.abilities.Ability;
	import my.view.elements.AbilityPickButton;
	import my.Ball;
	import my.Preferences;
	import starling.display.Image;
	import my.MyAssets;
	import my.view.LobbyView;
	import starling.events.Event;
	import my.Preferences;
	
	public class LobbyAbilityPickWindow extends BaseInterfaceElement
	{
		public var abilityButtonsByAbilityType: Vector.<Vector.<AbilityPickButton>> = new Vector.<Vector.<AbilityPickButton>>;
		
		private var abilityHolders: Vector.<Image> = new Vector.<Image>;
		private var _overallWidth: Number = 0;
		private var _overallHeight: Number = 0;
		//private var _abilitiesPicked: Vector.<int> = new Vector.<int>;
		//private var _abilitiesPicked: int = 0;
		public var host: LobbyView;
		
		private const FIRST_ABILITY_BUTTON_X: Number = 230;
		private const FIRST_ABILITY_BUTTON_Y: Number = 205;		
		private const X_SPACING: Number = 6;
		private const Y_SPACING: Number = 28;
		
		
		public function LobbyAbilityPickWindow() 
		{			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			host = LobbyView(parent);
		}
		
		public function GetWidth(): Number
		{
			return _overallWidth;
		}
		
		public function GetHeight(): Number 
		{
			return _overallHeight;
		}
		
		public function Initialize()
		{
			for (var i: int = 0; i < 4; i++)
			{
				abilityButtonsByAbilityType.push(new Vector.<AbilityPickButton>);
			}
			for (i = 0; i < Preferences.AllAbilities.length; i++)
			{
				var ability: Ability = Ability.CreateAbility(Preferences.AllAbilities[i],-1);
				var abilityPickButton: AbilityPickButton = new AbilityPickButton(ability);
				abilityButtonsByAbilityType[ability.GetChargeType()].push(abilityPickButton);
				//addChild(abilityPickButton);
			}
			var lastY: Number = 0;
			for (i = 0; i < abilityButtonsByAbilityType.length; i++)
			{
				var lastX: Number = 0;
				
				for (var j: int = 0; j < 8; j++)
				{
					var normalImage: Image = new Image(MyAssets.LobbyAtlas.getTexture("normal"));
					if (j < abilityButtonsByAbilityType[Preferences.OrderOfColors[i]].length)
					{						
						normalImage.x = FIRST_ABILITY_BUTTON_X + j * (normalImage.width + X_SPACING);
						normalImage.y = FIRST_ABILITY_BUTTON_Y + i * (normalImage.height + Y_SPACING);
						normalImage.alignPivot();
						addChild(normalImage);
						abilityButtonsByAbilityType[Preferences.OrderOfColors[i]][j].x = FIRST_ABILITY_BUTTON_X + j * (normalImage.width + X_SPACING); //- (abilityButtonsByAbilityType[Preferences.OrderOfColors[i]][j].GetWidth() / 2);
						abilityButtonsByAbilityType[Preferences.OrderOfColors[i]][j].y = FIRST_ABILITY_BUTTON_Y + i * (normalImage.height + Y_SPACING);// - (abilityButtonsByAbilityType[Preferences.OrderOfColors[i]][j].GetHeight() / 2);
						abilityButtonsByAbilityType[Preferences.OrderOfColors[i]][j].AlignCenter();
						addChild(abilityButtonsByAbilityType[Preferences.OrderOfColors[i]][j]);
						lastX += normalImage.width;
					}
					else
					{
						var image: Image = new Image(MyAssets.LobbyAtlas.getTexture("locked"));
						image.x = FIRST_ABILITY_BUTTON_X + j * (normalImage.width + X_SPACING);
						image.y = FIRST_ABILITY_BUTTON_Y + i * (normalImage.height + Y_SPACING);
						image.alignPivot();
						addChild(image);
						lastX += normalImage.width;
					}
					
				}
				if (lastX > _overallWidth)
					_overallWidth = lastX;
				lastX += abilityButtonsByAbilityType[0][0].GetWidth() + 8;
			}
			if (lastY > _overallHeight)
				_overallHeight = lastY;
		}
		
		//public function PickAbility(color: int, value: int): void
		//{
			//if (color < _abilitiesPicked.length)
				//_abilitiesPicked[color] = value;
		//}	
		
		//public function set abilitiesPicked(num: int): void
		//{
			//_abilitiesPicked = num;
			//_host.abilitiesPicked = num;
		//}
		//
		//public function get abilitiesPicked(): int
		//{
			//return _abilitiesPicked;
		//}
		
	}

}