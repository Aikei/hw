package my.view.elements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.MyAssets;
	import my.MyEntity;
	import starling.events.Event;		
	import my.MyWorld;
	import starling.display.Quad
	import starling.display.Image;
	import my.Ball;
	public class ShowNextWindow extends MyEntity 
	{
		private var _nextStones: Vector.<Image> = new Vector.<Image>;
		private var _number: int = 5;
		private var _spacing: Number = 16;
		
		public function ShowNextWindow() 
		{
			touchable = false;
			visible = false;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			MyWorld.gWorld.addEventListener("entropyChanged", OnEntropyChanged);
		}
		
		public function OnEntropyChanged(event: Event): void 
		{
			Refresh(event.data as Array);
		}
		
		public function OnAddedToStage(ev:Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			var quad: Quad = new Quad(MyWorld.OVERALL_FIELD_WIDTH, 128, 0xFF9D989E);
			quad.alpha = 0.75;
			addChild(quad);
			var ty: Number = quad.height/2-Ball.BALL_HALF_HEIGHT;
			for (var i: int = 0; i < _number; i++)
			{
				var image: Image = new Image(MyAssets.GetTextureByBallType(0, false));
				image.alpha = 0.75;
				image.scaleX = 0.5;
				image.scaleY = 0.5;
				image.y = ty;
				image.x = _spacing + i * (_spacing + image.width);
				_nextStones.push(image);
				addChild(image);
			}
		}
		
		public function Initialize(): void
		{
			visible = false;
		}
		
		public function Refresh(entropy: Array): void
		{
			if (!entropy)
				return;
			for (var i: int = 0, l: int = _nextStones.length, l2: int = entropy.length-1; i < l && i <= l2; i++)
			{
				_nextStones[i].texture = MyAssets.GetTextureByBallType(entropy[l2-i],false);
			}
		}
		
		override public function Destroy(): void
		{
			MyWorld.gWorld.removeEventListener("entropyChanged", OnEntropyChanged);
			super.Destroy();
		}
	}

}