package my.view.elements 
{
	import flash.geom.Point;
	import my.logic.abilities.Ability;
	import my.logic.Logic;
	import my.logic.PlayerData;
	import my.view.elements.AbilityTooltipWindow;
	import my.*;
	import my.view.TextEntity;
	import starling.display.DisplayObjectContainer;
	
	import starling.text.TextField;	
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.events.Event
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import starling.display.MovieClip;
	import starling.core.Starling;
	
	import feathers.core.PopUpManager;
	import feathers.controls.Button;
	
				
	/**
	 * ...
	 * @author Aikei
	 */
	public class AbilityButton extends Button 
	{
		//private var _squaresEmptyTextures: Vector.<Texture> = new Vector.<Texture>;
		//private var _squaresFullTextures: Vector.<Texture> = new Vector.<Texture>;
		//private var _squaresNumberByManaColor: Vector.<int> = new Vector.<int>;
		//private var _squares: Vector.<Image> = new Vector.<Image>;		
		//private var _chargePerOneSquare: int;
		
		private var _ability: Ability;
		private var _isEnemy: Boolean;

		private var _selector: Image = null;
		private var _manaImage: Image = null;
		private var _chargeText: TextField = null;
		
		private var id: int;
		private var _textWindow: AbilityTooltipWindow
		private var _hovered: Boolean = false;		
		private var _abilityCanBeUsed: Boolean = false;
		private var _buttonCanBePressed: Boolean = false;
		private var _chargeLeft: int = 0;
		private var _manaNumber: int = 0;
		private var _readyClip: MovieClip;
		
		public static var lastId: int = 0;
		
		//private static var CHARGE_PER_ONE_SQUARE: int;
		
		public function get ability(): Ability
		{
			return _ability;
		}
		
		public function get isEnemy(): Boolean
		{
			return _isEnemy;
		}
		
		public function AbilityButton(ability: Ability, isEnemy: Boolean) 
		{
			id = lastId++;
			_ability = ability;
			_isEnemy = isEnemy;
			_readyClip = new MovieClip(MyAssets.AbiltiyReadyAtlas.getTextures("ready_"));
			var i: int, l: int;
			if (_ability.color === Ball.BLUE_BALL) 
			{
				_manaImage = new Image(MyAssets.UiAtlas.getTexture("blue_mana"));
			}
			else if (_ability.color === Ball.RED_BALL)
			{
				_manaImage = new Image(MyAssets.UiAtlas.getTexture("red_mana"));
			}
			else if (_ability.color === Ball.PURPLE_BALL)
			{
				_manaImage = new Image(MyAssets.UiAtlas.getTexture("purple_mana"));
			}
			else if (_ability.color === Ball.GREEN_BALL)
			{
				_manaImage = new Image(MyAssets.UiAtlas.getTexture("green_mana"));
			}		
			_manaNumber = _ability.color;
			_manaImage.alignPivot();
			
			_chargeText = new TextField(40, 40, String(ability.manaCost[_manaNumber]), "Bold", 30, 0xffffffff);
			_chargeText.alignPivot();
			_chargeText.color = 0xffffff;
			if (_isEnemy)
				_isEnabled = false;

			defaultSkin = new Image(ability.buttonTexture);
			//upSkin = new Image(ability.buttonTexture);
			disabledSkin = new Image(ability.buttonTexture);
			downSkin = new Image(ability.buttonPushedTexture);
			hoverSkin = new Image(ability.buttonPushedTexture);			
			
			_selector = new Image(ability.buttonSelectorTexture);			
			addEventListener(Event.TRIGGERED, OnTriggered);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addEventListener(TouchEvent.TOUCH, OnTouch);
		}
		
		public function OnTouch(ev: TouchEvent): void
		{
			var touch:Touch = ev.getTouch(this, null);
			if (touch === null)
			{
				if (_hovered)
				{
					_hovered = false;
					PopUpManager.removePopUp(_textWindow, true);
					_textWindow = null;
				}
				return;
			}
			if (touch.phase === TouchPhase.HOVER)
			{
				_hovered = true;
				if (_textWindow)
				{	
					PopUpManager.removePopUp(_textWindow, true);
					_textWindow = null;
				}
				_textWindow = new AbilityTooltipWindow(_ability);
				PopUpManager.addPopUp(_textWindow, false, false);
				_textWindow.x = touch.globalX - _textWindow.width/2;
				_textWindow.y = touch.globalY - _textWindow.height;
			}
		}			
		
		public function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);

			var point: Point = localToGlobal(new Point(0, 0));

			
			var plusx: Number = (defaultSkin.width - 70) / 2;
			var plusy: Number = (defaultSkin.height - 70) / 2;
			ScaleTo(70, 70);
			x += plusx;
			y += plusy;			
			
			
			var p: DisplayObjectContainer = parent;
			removeFromParent();			
			p.addChild(_readyClip);
			_readyClip.play();
			_readyClip.visible = false;
			_readyClip.x = point.x-8;// + GetWidth() / 2;
			_readyClip.y = point.y-14;// + GetHeight() / 2;
			//_readyClip.alignPivot();
			Starling.juggler.add(_readyClip);
			p.addChild(this);
		
			_selector = new Image(ability.buttonSelectorTexture);			
			_isEnabled = true;
			defaultSkin.alpha = 0.6;
			addChild(_selector);
			_selector.visible = false;
			addChild(_manaImage);
			addChild(_chargeText);
			if (_ability.manaCost[_manaNumber] === 0 && !_ability.passive)
				SetReady(true);
		}
		
		public function SetCharge(mana: Vector.<int>): void
		{
			if (_ability.passive)
				return;
			var charge: int = _ability.manaCost[_manaNumber] - mana[_manaNumber];
			if (charge < 0)
				charge = 0;
			_chargeText.text = String(charge);			
			if (charge === 0 && !_ability.used)
			{
				SetReady(true);
			}
			else
			{
				SetReady(false);
			}
		}
		
		public function SetReady(ready: Boolean): void 
		{
			if (ready)
			{
				if (!isEnemy)
				{
					_abilityCanBeUsed = true;				
					defaultSkin.alpha = 1;
				}
				_readyClip.visible = true;
				_selector.visible = true;
				_chargeText.visible = false;
			}
			else
			{
				if (!isEnemy)
					_abilityCanBeUsed = false;				
				defaultSkin.alpha = 0.6;
				_selector.visible = false;
				_readyClip.visible = false;
				_chargeText.visible = true;				
			}
		}
		
		//public function SetCharge(mana: Vector.<int>): void
		//{
			//var squaresChargedByManaColor: Vector.<int> = new Vector.<int>;
			//var enoughMana: Boolean = true;
			//var lastSquareNumber = 0;
			//for (var i: int = 0, l: int = ability.manaCost.length; i < l; i++)
			//{
				//squaresChargedByManaColor.push(false);				
				//if (ability.manaCost[i] > 0)
				//{
					//var numberOfSquaresOfThisManaColorCharged: int = int(mana[i] / _chargePerOneSquare);
					//for (var j = 0; j < _squaresNumberByManaColor[i]; j++, lastSquareNumber++)
					//{
						//if (numberOfSquaresOfThisManaColorCharged > j)
							//_squares[lastSquareNumber].texture = _squaresFullTextures[i];
						//else
							//_squares[lastSquareNumber].texture = _squaresEmptyTextures[i];
					//}
				//}
				//if (ability.manaCost[i] > mana[i])
					//enoughMana = false;
			//}
						//
			//if (enoughMana)
			//{
				//if (!isEnemy)
					//_abilityCanBeUsed = true;
				//defaultSkin.alpha = 1;				
				//_selector.visible = true;
			//}
			//else
			//{
				//if (!isEnemy)
					//_abilityCanBeUsed = false;				
				//defaultSkin.alpha = 0.7;
				//_selector.visible = false;
			//}
		//}
		
		public function GetWidth() : Number
		{
			return defaultSkin.width*scaleX;
		}
		
		public function GetHeight(): Number
		{
			return defaultSkin.height*scaleY;
		}
		
		public function GetHeightWithSquares(): Number
		{
			return defaultSkin.height*scaleY;
		}
		
		public function AlignCenter(): void
		{
			x -= GetWidth() / 2;
			y -= GetHeight() / 2;
		}		
		
		public function OnTriggered(event: Event): void
		{
			if (MyWorld.gWorld.m_logic.phase === Logic.PHASE_SELECT_BALL)
			{
				if (_abilityCanBeUsed && _buttonCanBePressed)
				{
					var playerData: PlayerData = MyWorld.gWorld.m_logic.GetSelf();
					//for (var i: int = 0, l: int = playerData.mana.length; i < l; i++)
					//{
						//playerData.mana[i] -= ability.manaCost[i];
					//}
					//SetCharge(playerData.mana);
					if (_textWindow)
					{
						PopUpManager.removePopUp(_textWindow, true);
						_textWindow = null;
					}
					MyWorld.gWorld.dispatchEvent(new Event("abilityButtonPressed", false, _ability));
				}
			}
			else if (MyWorld.gWorld.m_logic.phase === Logic.PHASE_PICKING_YOUR_ABILITY_AS_TARGET)
			{
				if (!_isEnemy)
					MyWorld.gWorld.dispatchEvent(new Event("yourAbilitySelectedAsTarget", false, _ability));
				else
					MyWorld.gWorld.dispatchEvent(new Event("enemyAbilitySelectedAsTarget", false, _ability));
			}
		}
		
		public function ScaleTo(scaleWidth: Number, scaleHeight: Number): void
		{
			scaleX = scaleWidth / defaultSkin.width;
			scaleY = scaleHeight / defaultSkin.height;
		}		
		
		public function Disable(): void
		{
			//isEnabled = false;
			_buttonCanBePressed = false;
		}
		
		public function Enable(enable: Boolean = true): void
		{
			if (!_ability.passive)
				_buttonCanBePressed = enable;
		}		
		
		public function Destroy(): void
		{
			removeEventListener(Event.TRIGGERED, OnTriggered);
			removeEventListener(TouchEvent.TOUCH, OnTouch);
			_readyClip.removeFromParent(true);
			removeFromParent(true);
		}
	}

}