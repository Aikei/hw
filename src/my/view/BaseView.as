package my.view 
{
	import starling.events.Event;
	import my.MyEntity;
	import my.view.elements.BaseInterfaceElement;
	/**
	 * ...
	 * @author Aikei
	 */
	public class BaseView extends MyEntity
	{
		private var _isVisible: Boolean = true;
		protected var _elements : Vector.<BaseInterfaceElement> = new Vector.<BaseInterfaceElement>;
		
		public function BaseView() : void
		{
			
		}
		
		public function Update(): void
		{
			
		}
		
		public function AddElement(e: BaseInterfaceElement): void
		{
			_elements.push(e);
			addChild(e);
		}
		
		public function ProcessInput(event: Event): Boolean
		{
			for (var i: int = 0, l: int = _elements.length; i < l; i++)
			{
				if (_elements[i].ProcessInput(event))
					return true;
			}
			return false;
		}		
		
		public function get isVisible() : Boolean
		{
			return _isVisible;			
		}
		
		public function set isVisible(isVisible: Boolean): void
		{
			this.visible = isVisible;
			_isVisible = isVisible;
		}
		
		//override public function draw(): void
		//{
			//if (_isVisible)
			//{
				//super.draw();
			//}
		//}
		
		//override public function update(): void
		//{
			//if (_isVisible)
			//{
				//super.update();
			//}
		//}		
	}

}