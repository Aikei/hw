package my.view 
{
	import starling.display.Image;
	import starling.events.Event;
	import feathers.controls.Button;
	import my.MyAssets;
	import my.view.elements.Button;
	import my.MyWorld;
	import my.events.ButtonPressedEvent;
	import my.system.Language;
	import my.view.elements.LobbyAbilityPickWindow;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import my.Ball;
	/**
	 * ...
	 * @author Aikei
	 */
	
	public class LobbyView extends BaseView 
	{
		private var findMatchButton: Button;
		private var findMatchButtonText: TextField;
		private var pickSpellsText: TextField;
		private var lookingForGameText: TextEntity;
		private var abilityPickWindow: LobbyAbilityPickWindow;
		private var _mainFrame: Image;
		private var _abilitiesPicked: Vector.<int> = new Vector.<int>;
		
		private const BUTTON_X: int = 621;
		private const BUTTON_Y: int = 71;
		private const PICK_SPELLS_TEXT_X: int = 234;
		private const PICK_SPELLS_TEXT_Y: int = 98;
		private const PICK_SPELLS_TEXT_COLOR: uint = 0x9d410e;
		
		public function LobbyView() 
		{
			for (var i = 0; i < Ball.NUMBER_OF_MANAS; i++)
				_abilitiesPicked.push(0);			
			_mainFrame = new Image(MyAssets.LobbyAtlas.getTexture("interface"));
			//findMatchButton = new Button(MyAssets.LobbyAtlas.getTexture("button"), Language.GetString("Find match"), MyAssets.LobbyAtlas.getTexture("button_s"));
			findMatchButton = new Button;
			//findMatchButton.label = Language.GetString("Find match");
			findMatchButton.defaultSkin = new Image(MyAssets.LobbyAtlas.getTexture("button"));
			findMatchButton.downSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_s"));
			findMatchButton.hoverSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_s"));
			findMatchButton.disabledSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_inactive"));
			findMatchButton.x = BUTTON_X;
			findMatchButton.y = BUTTON_Y;
			findMatchButton.isEnabled = false;
			findMatchButton.addEventListener(Event.TRIGGERED, OnButtonPressed);
			
			findMatchButtonText = new TextField(30, 20, Language.GetString("Find match"), "Regular", 16, 0xffffff);
			findMatchButtonText.autoSize = TextFieldAutoSize.HORIZONTAL;
			findMatchButtonText.alignPivot();
			findMatchButtonText.x = findMatchButton.x + findMatchButton.defaultSkin.width / 2;
			findMatchButtonText.y = findMatchButton.y + findMatchButton.defaultSkin.height / 2;
			findMatchButtonText.touchable = false;
			
			pickSpellsText = new TextField(30, 20, Language.GetString("PickSpells"), "Regular", 16, PICK_SPELLS_TEXT_COLOR);
			pickSpellsText.autoSize = TextFieldAutoSize.HORIZONTAL;
			pickSpellsText.alignPivot();
			pickSpellsText.x = PICK_SPELLS_TEXT_X;
			pickSpellsText.y = PICK_SPELLS_TEXT_Y;
			
			lookingForGameText = new TextEntity(Language.GetString("Looking for game"), 20, 400, 60, 300, 60);
			lookingForGameText.visible = false;
			lookingForGameText.x -= lookingForGameText.width / 2;
			
			abilityPickWindow = new LobbyAbilityPickWindow;
			
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnAddedToStage(ev: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_mainFrame);
			addChild(abilityPickWindow);
			addChild(findMatchButton);
			addChild(findMatchButtonText);
			addChild(pickSpellsText);
			addChild(lookingForGameText);
		}
		
		public function Initialize(): void
		{
			abilityPickWindow.Initialize();		
			//abilityPickWindow.x = (findMatchButton.x + findMatchButton.width / 2) - abilityPickWindow.GetWidth() / 2;
			//abilityPickWindow.y = findMatchButton.y + 100;
		}
		
		public function OnButtonPressed(ev: Event): void
		{
			//if (ButtonPressedEvent(ev).buttonName === "Find match")
			//{
				lookingForGameText.visible = true;
				MyWorld.gWorld.SetPhase(MyWorld.PHASE_WAITING_FOR_NEW_GAME);
			//}
		}
		
		override public function set isVisible(isVisible: Boolean): void
		{
			super.isVisible = isVisible;
			lookingForGameText.visible = false;
		}
		
		private function BlockManaButtons(mana: int): void
		{
			
		}
		
		public function get abilitiesPicked(): Vector.<int>
		{
			return _abilitiesPicked;
		}
		
		public function PickAbility(color: int, pick: Boolean): void
		{
			if (color < _abilitiesPicked.length)
			{
				if (pick)
					_abilitiesPicked[color]++;
				else
					_abilitiesPicked[color]--;
			}
			var b: Boolean = true;
			for (var i = 0; i < Ball.NUMBER_OF_MANAS; i++)
			{
				if (_abilitiesPicked[i] === 0)
				{
					b = false;
					break;
				}
			}
			findMatchButton.isEnabled = b;
		}
		
		
		
		
	}

}