package my.view 
{
	import my.view.elements.StandardButton;
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Panel;
	import my.MyAssets;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import flash.display.BlendMode;
	import starling.display.Quad;
	import feathers.core.PopUpManager;
	import feathers.controls.Alert;
	import feathers.layout.*;
	import feathers.controls.text.TextFieldTextRenderer;	
	/**
	 * ...
	 * @author Aikei
	 */
	public class InterfaceFactories 
	{
		
		public function InterfaceFactories() 
		{
			
		}
		
		static public function InitInterface()
		{
			Callout.calloutFactory = InterfaceFactories.CreateCallout;
			PopUpManager.overlayFactory = InterfaceFactories.OverlayFactory;
			//Alert.alertFactory
		}
		
		static public function CreateCallout(): Callout
		{
			var callout:Callout = new Callout();
			callout.backgroundSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-skin"));
			callout.bottomArrowSkin = new Image(MyAssets.ThemeAtlas.getTexture("callout-arrow-bottom-skin"));
			callout.topArrowSkin = new Image(MyAssets.ThemeAtlas.getTexture("callout-arrow-top-skin"));
			callout.leftArrowSkin = new Image(MyAssets.ThemeAtlas.getTexture("callout-arrow-left-skin"));
			callout.rightArrowSkin = new Image(MyAssets.ThemeAtlas.getTexture("callout-arrow-right-skin"));
			//callout.blendMode = BlendMode.NORMAL;
			callout.alpha = 1;
			return callout;			
		}
		
		static public function OverlayFactory(): DisplayObject
		{
			var quad: Quad = new Quad(300, 200, 0xFFB80BE3);
			quad.alpha = 0.75;
			return quad;
		}
		
		static public function AlertFactory(): Alert
		{
			var alert: Alert = new Alert();
			//alert.styleNameList = "simple-alert";
			alert.backgroundSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-skin"));
			alert.backgroundDisabledSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-popup-skin"));
			alert.padding = 20;
			return alert;
		}
		
		static public function GetStandardVerticalPanel(horizontalAlign: String = VerticalLayout.HORIZONTAL_ALIGN_LEFT): Panel
		{
			var panel: Panel = new Panel;
			SkinThisVerticalPanel(panel,horizontalAlign);
			return panel;
		}
		
		static public function SkinThisVerticalPanel(p: Panel,horizontalAlign: String = VerticalLayout.HORIZONTAL_ALIGN_LEFT): void
		{
			p.backgroundSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-skin"));
			//p.backgroundDisabledSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-skin"));
			p.padding = 15;
			var layout: VerticalLayout = new VerticalLayout;
			layout.horizontalAlign = horizontalAlign;
			layout.gap = 30;
			p.layout = layout;				
		}
		
		static public function GetStandardHorizontalPanel(verticalAlign: String = VerticalLayout.VERTICAL_ALIGN_MIDDLE): Panel
		{
			var panel: Panel = new Panel;
			SkinThisHorizontalPanel(panel,verticalAlign);
			return panel;
		}
		
		static public function SkinThisHorizontalPanel(p: Panel,verticalAlign: String = VerticalLayout.VERTICAL_ALIGN_MIDDLE): void
		{
			p.backgroundSkin = new Image(MyAssets.ThemeAtlas.getTexture("check-down-icon"));
			//p.backgroundDisabledSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-skin"));
			p.padding = 20;
			var layout: HorizontalLayout = new HorizontalLayout;
			layout.gap = 40;
			layout.verticalAlign = verticalAlign;
			p.layout = layout;				
		}
		
		static public function GetStandardAnchorPanel(): Panel
		{
			var panel: Panel = new Panel;
			SkinThisHorizontalPanel(panel);
			return panel;
		}
		
		static public function SkinThisAnchorPanel(p: Panel): void
		{
			p.backgroundSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-disabled-skin"));
			p.backgroundDisabledSkin = new Image(MyAssets.ThemeAtlas.getTexture("background-skin"));
			p.padding = 20;
			var layout: AnchorLayout = new AnchorLayout;
			p.layout = layout;				
		}		
		
		//static public function GetStandardButton(): StandardButton
		//{
			//var button = new StandardButton;
			//return button;			
		//}
		
	}

}