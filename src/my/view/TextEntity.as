package my.view {
	import starling.events.Event;
	import starling.text.TextField;
	import my.MyEntity;
	import my.traits.BasicTrait;
	import my.Ball;
	import my.MyWorld;
	import starling.utils.Color;
	import starling.utils.HAlign;
	/**
	 * ...
	 * @author Aikei
	 */
	public class TextEntity extends MyEntity
	{
		//private var m_text: Text;
		//private var m_colorWhite: AxColor = new AxColor(1, 1, 1, 1);
		//private var m_colorPink: AxColor = new AxColor(1, 0, 1, 1);
		//private var m_colorPurple: AxColor = new AxColor(0.5, 0, 0.5, 1);
		protected var _basicTrait: BasicTrait;
		protected var _text: TextField;
		
		public function TextEntity(textString:String, fontSize: int, w: Number, h: Number, x: Number = 0, y: Number = 0) 
		{
			super(x, y);
			_text = new TextField(w, h, textString,"Regular",fontSize);
			//_text.fontSize = fontSize;
			_text.color = Color.WHITE;
			//_text.autoScale = true;			
			_basicTrait = new BasicTrait(this);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
			
		protected function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_text);
		}
		
		public function get textField(): TextField
		{
			return _text;
		}
		
		public function get text(): String
		{
			return _text.text;
		}
		
		public function set text(text: String): void
		{
			_text.text = text;
		}		
		
		public function BindToBall(ball: Ball): void
		{
			x = ball.x;
			y = ball.y;
			x -= (width / 2);
			y -= (height / 2);				
			//x -= width / 2;
			//y -= height / 2;
				
		}
		
		override public function set color(clr: uint): void
		{
			super.color = clr;
			_text.color = clr;
		}
		
		//public function SetColor(newColor: AxColor): void
		//{
			//color = newColor;
		//}
		
		public function SetChainColor(chainLength: uint): void
		{
			if (chainLength < 5)
				this.color = Color.WHITE;
			else if (chainLength < 9)
				this.color = Color.MAROON;
			else
				this.color = Color.PURPLE;			
		}
		
		public function SetText(text:String): void
		{
			this._text.text = text;
		}
		
		public function InitText(textString:String, x: Number, y: Number): void
		{
			_basicTrait.Init(x, y);
			this._text.text = textString;
		}
		
		public function SetHAlignment(align: String): void
		{
			_text.hAlign = align;
		}
		
		//public function set isVisible(isVisible: Boolean): void
		//{
			//visible = isVisible;
		//}
	}

}