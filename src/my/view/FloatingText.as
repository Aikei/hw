package my.view 
{
	import starling.events.Event;
	import my.Tweens;
	import flash.utils.setTimeout;
	import starling.text.TextFieldAutoSize;
	/**
	 * ...
	 * @author Aikei
	 */
	
	public class FloatingText extends DisappearingText
	{
		
		public function FloatingText(textString:String, fontSize: int, color: uint, x: int, y: int, centered: Boolean=true) 
		{
			super(textString, fontSize, color, x, y, centered);
			_endAlpha = 0.5;
		}
		
		override protected function OnAddedToStage(event: Event): void
		{
			super.OnAddedToStage(event);
			Tweens.TweenFloatingUp(this, 2, 250);
		}				
		
	}

}