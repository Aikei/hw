package my.view 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Tweens;
	import starling.events.Event;
	import starling.text.TextFieldAutoSize;
	import flash.utils.setTimeout;
	public class DisappearingText extends TextEntity
	{
		protected var _endAlpha = 0;
		private var _centered;
		private var _time = 2;
		public function DisappearingText(textString:String, fontSize: int, color: uint, x: int, y: int, centered: Boolean=true, time = 2) 
		{
			super(textString, fontSize, 0, 0, x, y);
			this.color = color;
			_centered = centered;
			_time = time;
			//_text.autoSize = 
			//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		override protected function OnAddedToStage(event: Event): void
		{
			super.OnAddedToStage(event);
			_text.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			if (_centered)
			{
				x -= _text.width / 2;
				y -= _text.height / 2;
			}
			Tweens.TweenDisappearing(this, _time, _endAlpha);
			setTimeout(this.Destroy, _time*1000);
		}				
		
	}

}