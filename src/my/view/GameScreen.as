
package my.view {
	/**
	 * ...
	 * @author Aikei
	 */

	import flash.geom.Point;
	import my.events.*;
	import my.dbg.MConsole;	
	import my.*;
	import my.logic.PlayerData;
	import my.mp.Controller;
	import my.mp.messages.*;
	import my.view.elements.AnyTextWindow;
	import my.view.elements.PlayerWindow;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.text.*;
	import my.logic.Logic;
	import flash.utils.*;	
	import flash.ui.Keyboard;
	import starling.utils.Color;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import starling.core.Starling;
	import starling.events.*;
	import my.view.FloatingText;
	import my.view.elements.ShowNextWindow;
	import my.view.elements.FieldWindow;
	import my.view.elements.TimerTurnElement;
	import my.view.elements.PickTargetTextWindow;
	import my.system.Language;
	//import flash.ui.*;
	
	public class GameScreen extends BaseView
	{		
		public var m_visuals: MyEntity = new MyEntity;
		public var m_fieldText: MyEntity = new MyEntity;
		public var m_ballsGroup: MyEntity = new MyEntity;
		public var m_ballsParticleSystems: Vector.<Vector.<BallParticleSystem>> = null;
		public var m_showNextWindow: ShowNextWindow = new ShowNextWindow;
		public var m_pickTargetWindow: PickTargetTextWindow = new PickTargetTextWindow(Language.GetString("PickTargetStone"));
		public var m_pickYourAbilityWindow: PickTargetTextWindow = new PickTargetTextWindow(Language.GetString("PickTargetYourAbility"));
		public var m_timerTurnElement: TimerTurnElement = new TimerTurnElement;
		public var m_anyTextWindow: AnyTextWindow = new AnyTextWindow("");
		
		private var m_topTextView: TopTextView;
		private var m_chainLengthText: TextEntity = null;		
		private var m_playerWindows: Vector.<PlayerWindow> = new Vector.<PlayerWindow>;
		private var m_width: int;
		private var m_height: int;
		private var m_elements: Vector.<MyEntity> = new Vector.<MyEntity>;
		
		private var _ballsColorTween: TweenLite = null;
		
		private var DISTANCE_FROM_PLAYER_WINDOW_TO_FIELD: int = 11;
		
		private var m_fieldWindow: FieldWindow;
		
		public function GameScreen(w: int, h: int) 
		{
			width = w;
			height = h;
			m_width = w;
			m_height = h;
			
			m_fieldWindow = new FieldWindow;
			
			MyWorld.gWorld.addEventListener(BallSelectedEvent.BALL_SELECTED, OnBallSelected);
			MyWorld.gWorld.addEventListener(BallsDeselectedEvent.BALLS_DESELECTED, OnBallsDeselected);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function Initialize(): void
		{
			//m_ballsGroup.x = m_width / 2 - (MyWorld.OVERALL_FIELD_HEIGHT / 2);
			//m_ballsGroup.y = m_height / 2 - (MyWorld.OVERALL_FIELD_HEIGHT / 2);
			
			m_fieldWindow.x = 210;
			m_fieldWindow.y = 196;
			m_ballsGroup.x = m_fieldWindow.x + FieldWindow.ADDITIONAL_START_DISTANCE_FOR_HOLDER;
			m_ballsGroup.y = m_fieldWindow.y + FieldWindow.ADDITIONAL_START_DISTANCE_FOR_HOLDER;			
			
			m_visuals.x = m_ballsGroup.x;
			m_visuals.y = m_ballsGroup.y;
			
			m_fieldText.x = m_ballsGroup.x;
			m_fieldText.y = m_ballsGroup.y;			
			
			m_timerTurnElement.x = m_fieldText.x;
			m_timerTurnElement.y = m_fieldText.y - m_timerTurnElement.height - 18;
			
			m_pickTargetWindow.x = m_ballsGroup.x + (MyWorld.OVERALL_FIELD_WIDTH / 2) - (m_pickTargetWindow.width / 2);
			m_pickTargetWindow.y = m_ballsGroup.y - m_pickTargetWindow.height - 16;
			m_pickYourAbilityWindow.x = m_ballsGroup.x + (MyWorld.OVERALL_FIELD_WIDTH / 2) - (m_pickYourAbilityWindow.width / 2);
			m_pickYourAbilityWindow.y = m_ballsGroup.y - m_pickYourAbilityWindow.height - 16;
			
			m_anyTextWindow.x = m_ballsGroup.x + (MyWorld.OVERALL_FIELD_WIDTH / 2) - (m_anyTextWindow.width / 2);
			m_anyTextWindow.y = 8;			
			
			m_showNextWindow.x = m_ballsGroup.x;
			m_showNextWindow.y = m_ballsGroup.y + MyWorld.OVERALL_FIELD_HEIGHT + 16;			
			m_showNextWindow.Initialize();
			m_elements.push(m_ballsGroup);
			m_elements.push(m_timerTurnElement);
			for (var i: int = 0; i < 2; i++)
			{
				//0 (false) you, 1 (true) - enemy
				m_playerWindows.push(new PlayerWindow(i));
				m_elements.push(m_playerWindows[i]);
				//m_playerWindows[i].y = m_ballsGroup.y;
				m_playerWindows[i].y = 15;
				m_playerWindows[i].x = m_fieldWindow.x;
				addChild(m_playerWindows[i]);	
				if (i === MyWorld.gWorld.m_logic.myNumber)
				{
					m_playerWindows[i].x -= (m_playerWindows[i].GetWidth()+DISTANCE_FROM_PLAYER_WINDOW_TO_FIELD);
				}
				else
				{
					m_playerWindows[i].x += m_fieldWindow.width+DISTANCE_FROM_PLAYER_WINDOW_TO_FIELD;
				}			
				
				//m_playerWindows[i].y -= m_playerWindows[i].GetHeight() / 2;			
			}
			
			m_ballsParticleSystems = new Vector.<Vector.<BallParticleSystem>>;
			for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
			{
				m_ballsParticleSystems.push(new Vector.<BallParticleSystem>);
				for (var j = 0; j < MyWorld.FIELD_HEIGHT; j++)
				{
					m_ballsParticleSystems[i].push(new BallParticleSystem());
					m_ballsParticleSystems[i][j].x = m_ballsGroup.x + i * (Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X) + Ball.BALL_HALF_WIDTH;
					m_ballsParticleSystems[i][j].y = m_ballsGroup.y + j * (Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_X) + Ball.BALL_HALF_HEIGHT;
					//Starling.juggler.add(m_ballsParticleSystems[i][j]);
					addChild(m_ballsParticleSystems[i][j]);
				}
			}
									
		}
		
		public function ShowAnyText(text: String, noOkButton: Boolean = false): void
		{
			m_anyTextWindow.SetText(text,noOkButton);
			//m_anyTextWindow.x = m_ballsGroup.x + (MyWorld.OVERALL_FIELD_WIDTH / 2) - (m_anyTextWindow.width / 2);
			//m_anyTextWindow.y = 8;
			//m_anyTextWindow.y = m_ballsGroup.y - m_anyTextWindow.GetHeight() - 16;	
			m_anyTextWindow.visible = true;
		}
		
		public function HideAnyText(): void
		{
			m_anyTextWindow.visible = false;
		}
		
		public function ChangeAlphaAll(to: Number): void
		{
			for (var i: int = 0; i < m_elements.length; i++)
			{
				m_elements[i].SetChildrenAlpha(to);
			}
		}
		
		public function InitializeOnGameStart()
		{
			for (var i: int = 0; i < m_playerWindows.length; i++)
			{
				m_playerWindows[i].InitializeOnGameStart();
			}
		}
		
		public function ShakeField(shakeTime: Number = 0.5, repeatTimes: Number = 2): void
		{			
			for (var i: int = 0, l: int = m_ballsGroup.numChildren; i < l; i++)
			{
				Tweens.TweenShake(Ball(m_ballsGroup.getChildAt(i)), shakeTime, repeatTimes, 5, 5);
			}			
		}
		
		public function OnAddedToStage(ev: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(m_fieldWindow);
			addChild(m_ballsGroup);
			addChild(m_timerTurnElement);
			addChild(m_visuals);
			addChild(m_fieldText);
			addChild(m_showNextWindow);
			addChild(m_pickTargetWindow);
			addChild(m_pickYourAbilityWindow);
			addChild(m_anyTextWindow);
			//addChild(m_ballsGroup);
			//addChild(m_visuals);
			//addChild(m_fieldText);
			//addChild(m_topTextView);
			//for (var i: int = 0; i < 2; i++)
			//{
				////0 (false) you, 1 (true) - enemy
				//m_playerWindows.push(new PlayerWindow(i));
				////m_playerWindows[i].y = m_ballsGroup.y;
				//m_playerWindows[i].y = 15;
				//m_playerWindows[i].x = m_ballsGroup.x;
				//addChild(m_playerWindows[i]);	
				//if (i === MyWorld.gWorld.m_logic.myNumber)
				//{
					//m_playerWindows[i].x -= (m_playerWindows[i].GetWidth()+50);
				//}
				//else
				//{
					//m_playerWindows[i].x += MyWorld.OVERALL_FIELD_WIDTH+50;
				//}			
				//
				////m_playerWindows[i].y -= m_playerWindows[i].GetHeight() / 2;			
			//}
			//
			//m_ballsParticleSystems = new Vector.<Vector.<BallParticleSystem>>;
			//for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
			//{
				//m_ballsParticleSystems.push(new Vector.<BallParticleSystem>);
				//for (var j = 0; j < MyWorld.FIELD_HEIGHT; j++)
				//{
					//m_ballsParticleSystems[i].push(new BallParticleSystem());
					//m_ballsParticleSystems[i][j].x =m_ballsGroup.x + i * (Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X) + Ball.BALL_HALF_WIDTH;
					//m_ballsParticleSystems[i][j].y = m_ballsGroup.y + j * (Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_X) + Ball.BALL_HALF_HEIGHT;
					//Starling.juggler.add(m_ballsParticleSystems[i][j]);
					//addChild(m_ballsParticleSystems[i][j]);
				//}
			//}			
		}
		
		override public function Update(): void
		{
			//m_topTextView.Update();
		}		
		
		public function TweenHsvAllBalls(time: Number, h: Number, s: Number, v: Number): void
		{
			if (time === 0)
			{				
				Tweens.SetHsv(m_ballsGroup, h, s, v);
			}
			else
			{
				Tweens.TweenHsv(time, m_ballsGroup, h, s, v);
			}
		}
		
		public function TweenAlphaAllBalls(time: Number, target: Number): void
		{
			for (var i: int = 0, l: int = m_ballsGroup.numChildren; i < l; i++)
			{
				Tweens.TweenAlpha(time, Ball(m_ballsGroup.getChildAt(i)), target);
			}
		}		
		
		public function TweenAllBallsColorToNormal(time: Number): void
		{
			if (time === 0)
				Tweens.SetToBaseColor(m_ballsGroup);
			else
				Tweens.TweenToBaseColor(time, m_ballsGroup);
			//for (var i: int = 0, l: int = m_ballsGroup.numChildren; i < l; i++)
			//{
				//Tweens.TweenToOpaqueWhite(time, Ball(m_ballsGroup.getChildAt(i)));
			//}
			
			//for (var i: int = 0; i < MyWorld.FIELD_WIDTH; i++)
			//{
				//for (var j: int = 0; j < MyWorld.FIELD_HEIGHT; j++)
				//{
					//Tweens.TweenToOpaqueWhite(time, m_ballsGroup[i][j]);
				//}
			//}
		}		
		
		
		public function CreateVisual(textures: Vector.<Texture>, x: Number, y: Number, fps: Number = 12): Visual
		{
			//var newVisual: Visual = Visual(m_visualsGroup.recycle());
			//if (newVisual)
			//{
				//newVisual.revive();
				//newVisual.InitVisual(graphic, x, y, frameWidth, frameHeight);
			//}
			//else
			//{
				//newVisual = new Visual(graphic, x, y, frameWidth, frameHeight);
				//m_visualsGroup.add(newVisual);
			//}
			var newVisual: Visual = new Visual(textures, x, y, fps);
			m_visuals.addChild(newVisual);
			return newVisual;
		}
		
		public function TweenScaleBall(ball: Ball, by: Number, time: Number): TweenLite
		{
			BallToTop(ball);
			return Tweens.TweenScale(ball, by, time);	
		}
		
		public function BallToTop(ball: Ball): void
		{
			m_ballsGroup.removeChild(ball);
			m_ballsGroup.addChild(ball);
		}
		
		public function OnDeleteEffect(v: Vector.<Ball>): void
		{
			for (var i: int = 0, l: int = v.length; i < l; i++)
			{
				//v[i].SetSelected(true);
				m_ballsParticleSystems[v[i].m_index.x][v[i].m_index.y].StartDeleteParticles(0.35);
				setTimeout(v[i].SetInvisible, 500, true);				
			}
		}
		
		public function OnDestroyEffect(v: Vector.<Ball>): void
		{
			var l : int = v.length;
			if (l === 0)
				return;
			var result: Vector.<Number> = MyWorld.gWorld.m_logic.CountDamage(v);
			//for (var i: int = 0; i < l; i++)
			//{
				//if (v[i].m_specialType === Ball.BALL_SPECIAL_TYPE_HEALTH)
					//heal += Preferences.HEALTH_STONE_HEAL;
			//}
			//var damage: int = Math.floor(l / Preferences.BALLS_PER_ONE_DAMAGE);
			
			var textPos: Vec2 = new Vec2(m_ballsGroup.x + v[l - 1].x, m_ballsGroup.y + v[l - 1].y);
			if (result[0] > 0)
			{				
				var floatingDamageText: FloatingText = new FloatingText("-" + String(result[0]), 60, Color.RED, textPos.x, textPos.y);
				m_playerWindows[int(MyWorld.gWorld.m_logic.yourTurn)].AddHealthChangeText(-result[0]);			
				addChild(floatingDamageText);
			}
			if (result[1] > 0)
			{
				var floatingHealText: FloatingText = new FloatingText("+" + String(result[1]), 60, Color.GREEN, textPos.x, textPos.y);
				m_playerWindows[int(!MyWorld.gWorld.m_logic.yourTurn)].AddHealthChangeText(result[1]);
				setTimeout(addChild,250,floatingHealText);
			}
			var playerData: PlayerData = MyWorld.gWorld.m_logic.GetCurrentPlayerData();
			mainLoop: for (var i = 0; i < l; i++)
			{
				v[i].SetSelected(true);
				var p: Point;
				
				//TweenScaleBall(v[i], 1.2, 0.5);
				//for (var j: int = 0; j < playerData.abilities.length; j++)
				//{
					//if (playerData.abilities[j].chargedByType === v[i].m_type)
					//{
						//p = m_ballsGroup.globalToLocal(playerData.abilities[j].chargerPos);
						//setTimeout(TweenLite.to, 500, v[i], 0.65, { x: p.x, y: p.y } );	
						//setTimeout(TweenScaleBall, 500, v[i], 0.1, 0.65);
						//continue mainLoop;
					//}
				//}

				p = localToGlobal(new Point(v[i].x, v[i].y));
				m_ballsParticleSystems[v[i].m_index.x][v[i].m_index.y].StartDestroyParticles(0.75);
				setTimeout(v[i].SetInvisible, 500, true);
				//v[i].SetInvisible(true);
			}
		}
		
		public function AddFloatingText(text: String, textPosX: int, textPosY: int, a_color: uint = Color.WHITE): void
		{
			var floatingText: FloatingText = new FloatingText(text, 60, a_color, textPosX, textPosY);
			addChild(floatingText);
		}
		
		public function AddDisappearingText(text: String, textPosX: int, textPosY: int, a_color: uint = Color.WHITE, time: Number = 2): void
		{
			var disappearingText: DisappearingText = new DisappearingText(text, 60, a_color, textPosX, textPosY,true,time);
			addChild(disappearingText);
		}
		
		public function AddDisappearingImage(texture: Texture, x: int, y: int, time: Number = 2, centered: Boolean = true): void
		{
			var disappearingImage: DisappearingImage = new DisappearingImage(texture, x, y, time, centered);
			addChild(disappearingImage);
		}
		
		private function OnBallSelected(event: BallSelectedEvent): void
		{
			AddChainLengthText(event.chainLength, event.ball);
		}
		
		private function OnBallsDeselected(event: BallsDeselectedEvent): void
		{
			RemoveChainLengthText();
		}
		
		private function AddChainLengthText(chainLength: int, ball: Ball): void
		{
			if (m_chainLengthText)
			{				
				m_chainLengthText.text = String(chainLength);
			}
			else
			{
				m_chainLengthText = new TextEntity(String(chainLength), 30, 40, 40, ball.x, ball.y);
				m_chainLengthText.textField.autoSize = TextFieldAutoSize.HORIZONTAL;
				m_fieldText.addChild(m_chainLengthText);
				m_fieldText.visible = true;
			}
			m_chainLengthText.BindToBall(ball);
			m_chainLengthText.SetChainColor(chainLength);
		}		
					
		//private function AddChainLengthText(chainLength: int, ball: Ball): void
		//{
			//if (m_chainLengthText)
			//{
				//m_chainLengthText.destroy();
				//m_chainLengthText = null;
			//}
			//m_chainLengthText = TextEntity(m_fieldTextGroup.recycle());
			//if (m_chainLengthText)
			//{
				//m_chainLengthText.revive();
				//m_chainLengthText.InitText(String(chainLength), ball.x, ball.y);				
			//}
			//else
			//{
				//m_chainLengthText = new TextEntity(String(chainLength), MyWorld.BIG_FONT, ball.x, ball.y);
				//m_fieldTextGroup.add(m_chainLengthText);
			//}
			//m_chainLengthText.BindToBall(ball);
			//m_chainLengthText.SetChainColor(chainLength);
		//}
		
		private function RemoveChainLengthText(): void
		{
			if (m_chainLengthText)
			{
				m_chainLengthText.Destroy();
				m_chainLengthText = null;
			}
		}		
		
		override public function ProcessInput(event: Event): Boolean
		{
			if (super.ProcessInput(event))
				return true;
			if (event.type === KeyboardEvent.KEY_DOWN)
			{
				//var keyboardEvent: KeyboardEvent = KeyboardEvent(event);
				//if (keyboardEvent.keyCode === Keyboard.TAB)
				//{
					//if (MConsole.showConsole)
						//MConsole.Write("^ Closed console ^");
					//else
						//MConsole.Write("v Called console v");
					//MConsole.showConsole = !MConsole.showConsole;
					//return true;
				//}
				if (KeyboardEvent(event).keyCode === Keyboard.SPACE)
				{
					MConsole.Write("Paused");
					if (MyWorld.gWorld.m_bPaused)
						MyWorld.gWorld.m_bPaused = false;
					else
						MyWorld.gWorld.m_bPaused = true;
					return true;
				}
			}
			else if (event.type === TouchEvent.TOUCH)
			{
				var touch: Touch = TouchEvent(event).getTouch(this);
				if (!touch)
					return false;
				if (touch.phase === TouchPhase.BEGAN)
				{				
					if (MyWorld.gWorld.m_logic.phase === Logic.PHASE_SELECT_BALL && MyWorld.gWorld.m_logic.yourTurn && !MyWorld.gWorld.m_logic.chainSelectionDone)
					{
						var localPos: Point = touch.getLocation(m_ballsGroup);
						if (localPos.x >= 0 && localPos.x < MyWorld.OVERALL_FIELD_WIDTH && localPos.y >= 0 && localPos.y < MyWorld.OVERALL_FIELD_HEIGHT)
							MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_CHAIN);
					}
				}
				else if (touch.phase === TouchPhase.ENDED)
				{
					if (MyWorld.gWorld.m_logic.phase === Logic.PHASE_SELECT_CHAIN && MyWorld.gWorld.m_logic.yourTurn === true)
					{
						//MyWorld.gWorld.m_interfaceView.RemoveChainLengthText();
						//var v: Vector.<Ball> = new Vector.<Ball>;
						dispatchEvent(new BallsDeselectedEvent(BallsDeselectedEvent.BALLS_DESELECTED, null));
						if (MyWorld.gWorld.m_logic.m_selectedBalls.length > 2)
						{
							MyWorld.gWorld.m_logic.chainSelectionDone = true;
							MyWorld.gWorld.m_logic.RemoveSelected();
							//MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_REMOVING);
						}
						else
						{
							MyWorld.gWorld.m_logic.DeselectAll();
							Controller.DeselectAll();
							//var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksDeselectAll, { gameId : MyWorld.gWorld.m_logic.m_gameId } );
							//MyWorld.gWorld.m_multiplayer.SerializeAndSend(msg);
							MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
						}
					}
				}
				localPos = touch.getLocation(m_ballsGroup);
				var ball: Ball = MyWorld.gWorld.FindBallByPosition(localPos.x, localPos.y);
				if (ball != null)
				{
					var squaredDistance: Number = Misc.GetSquaredDistance(localPos.x, localPos.y, ball.x, ball.y);
					MyWorld.gWorld.m_logic.OnBallTouch(ball, squaredDistance, touch);
				}
			}
			return false;
		}
		
		public function DisableAbilityButtons(): void
		{
			for (var i = 0; i < m_playerWindows.length; i++)
			{
				m_playerWindows[i].DisableAbilityButtons();
			}
		}
		
		public function EnableAbilityButtons(enable: Boolean = true): void
		{
			for (var i = 0; i < m_playerWindows.length; i++)
			{
				m_playerWindows[i].EnableAbilityButtons(enable);
			}
		}		
				
	}

}