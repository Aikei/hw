package my.view 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	
	import starling.events.Event;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.ParticleSystem;
	import starling.core.Starling;
	import starling.textures.Texture;
	
	import my.MyEntity;
	import my.MyAssets;
	import my.Ball;
	
	public class BallParticleSystem extends MyEntity
	{
		public var destroyParticleSystem : PDParticleSystem;
		public var deleteParticleSystem: PDParticleSystem;
		
		public function BallParticleSystem() 
		{
			super(MyAssets.BallPsConfig, MyAssets.BallPsTexture);
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function StartDestroyParticles(duration: Number = Number.MAX_VALUE): void
		{
			destroyParticleSystem.start(duration);
		}
		
		public function StartDeleteParticles(duration: Number = Number.MAX_VALUE): void
		{
			deleteParticleSystem.start(duration);
		}
		
		public function OnAddedToStage(ev: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			destroyParticleSystem = new PDParticleSystem(XML(new MyAssets.BALL_DESTROY_PARTICLE_CONFIG()), Texture.fromBitmap(new MyAssets.BALL_DESTROY_PARTICLE_TEXTURE()));
			Starling.juggler.add(destroyParticleSystem);
			addChild(destroyParticleSystem);
			destroyParticleSystem.width = Ball.BALL_WIDTH/2;
			destroyParticleSystem.height = Ball.BALL_HEIGHT / 2;
			
			deleteParticleSystem = new PDParticleSystem(XML(new MyAssets.BALL_DELETE_PARTICLE_CONFIG()), Texture.fromBitmap(new MyAssets.BALL_DELETE_PARTICLE_TEXTURE()));
			Starling.juggler.add(deleteParticleSystem);
			addChild(deleteParticleSystem);
			deleteParticleSystem.width = Ball.BALL_WIDTH/2;
			deleteParticleSystem.height = Ball.BALL_HEIGHT / 2;
			
			//this.
			//startSize = 40;
			//endSize = 40;			
		}
		
	}

}