package my.view {
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.MyEntity;
	import my.traits.BasicSpriteTrait;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.display.Image;
	import starling.text.TextField;
	import flash.utils.setTimeout;
	import starling.display.MovieClip;
	import starling.core.Starling;
	import starling.utils.Color;
	
	public class Visual extends MyEntity
	{
		
		private var _basicTrait: BasicSpriteTrait;
		//private var _image : Image;
		private var _clip: MovieClip;
		private var _text: TextField = null;
		
		public function Visual(textures: Vector.<Texture>, x:Number, y:Number, fps: Number = 12) 
		{
			super (x, y);			
			//_image = new Image(texture);
			_clip = new MovieClip(textures, fps);
			_basicTrait = new BasicSpriteTrait(this);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function CreateText(text: String, fontSize: int): void
		{
			_text = new TextField(_clip.width, _clip.height, text, "Regular", fontSize, Color.WHITE);
			_text.x = _clip.x;
			_text.y = _clip.y;
			addChild(_text);
		}
		
		public function SetText(text: String): void
		{
			if (!_text)
				CreateText(text, 20);
			else
				_text.text = text;
		}
		
		public function OnAddedToStage(ev: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_clip);
			_clip.play();
			Starling.juggler.add(_clip);
		}
		
		//public function InitVisual(texture: Texture, x: Number, y: Number): void
		//{
			//_basicTrait.Init(x, y);
			//_image.texture = texture;
		//}
		
		public function SetSelfDestruct(millis: Number)
		{
			setTimeout(Destroy, millis);
		}
		
		override public function ScaleTo(scaleWidth: Number, scaleHeight: Number): void
		{
			scaleX = scaleWidth / _clip.width;
			scaleY = scaleHeight / _clip.height;
		}
		
		override public function set color(clr: uint): void
		{
			super.color = clr;
			_clip.color = _color;
			if (_text)
				_text.color = _color;
		}		
	}

}