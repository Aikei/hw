package my.view {
	
	import my.MyEntity;
	import my.Vec2;
	import my.MyWorld;
	import my.view.TextEntity;
	import my.Ball;
	import starling.events.*;
	import my.events.*;
	/**
	 * ...
	 * @author Aikei
	 */
	public class TopTextView extends BaseView
	{
		private var m_playerHPText: TextEntity;
		private var m_enemyHPText: TextEntity;		
		private var m_width: Number;
		private var m_height: Number;
		
		public function TopTextView(x: Number, y: Number, w: Number, h: Number) : void
		{
			this.x = x;
			this.y = y;
			m_width = w;
			m_height = h;						
			m_playerHPText = new TextEntity("0", 20, 32, 60, 0, 0);			
			m_enemyHPText = new TextEntity("0", 20, 32, 60, m_width-32, 0);	
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function OnAddedToStage(ev: Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(m_playerHPText);
			addChild(m_enemyHPText);
		}
				
		override public function Update(): void
		{
			if (MyWorld.gWorld.phase != MyWorld.PHASE_CONNECTING_TO_SERVER && MyWorld.gWorld.phase != MyWorld.PHASE_WAITING_FOR_NEW_GAME)
			{
				m_playerHPText.SetText(String(MyWorld.gWorld.m_logic.m_playerHP));
				m_enemyHPText.SetText(String(MyWorld.gWorld.m_logic.m_enemyHP));
			}
		}
		
		//override public function ProcessInput(event: Event): Boolean
		//{		
			//return false;
		//}		
	
	}
	
}