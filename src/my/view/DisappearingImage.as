package my.view 
{
	import my.MyEntity;
	import starling.textures.Texture;
	import starling.display.Image;
	import starling.events.Event;
	import my.Tweens;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Aikei
	 */

	public class DisappearingImage extends MyEntity 
	{
		private var _image : Image;
		private var _centered : Boolean;
		private var _time: Number;
		protected var _endAlpha = 0;
		
		public function DisappearingImage(texture: Texture, x:Number, y:Number, time: Number = 2, centered: Boolean = true) 
		{
			super(x, y);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			_centered = centered;
			_image = new Image(texture);
			_time = time;
		}
		
		protected function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
			if (_centered)
			{
				x -= _image.width / 2;
				y -= _image.height / 2;				
			}
			Tweens.TweenDisappearing(this, _time, _endAlpha);
			setTimeout(this.Destroy, _time*1000);			
		}
		
	}

}