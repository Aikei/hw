package my.view 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyWorld;
	import my.view.elements.profileview.ProfileText;
	import starling.events.Event;
	import feathers.controls.Button;
	import my.MyAssets;
	import my.system.Language;
	import feathers.controls.text.TextFieldTextRenderer;
	import flash.text.TextFormat;
	import starling.utils.Color;
	import my.Preferences;
	import feathers.core.ITextRenderer;
	import starling.display.Image;
	import my.view.elements.StandardButton;
	public class ProfileView extends BaseView 
	{
		private var _profileText: ProfileText = new ProfileText;
		private var _toLibraryButton: Button;
		
		public function ProfileView() 
		{
			super();
			_toLibraryButton = new StandardButton(Language.GetString("ToLibrary"));
			//_toLibraryButton.defaultSkin = new Image(MyAssets.LobbyAtlas.getTexture("button"));
			//_toLibraryButton.hoverSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_s"));
			//_toLibraryButton.downSkin = new Image(MyAssets.LobbyAtlas.getTexture("button_s"));
			//_toLibraryButton.label = Language.GetString("ToLibrary");
			//_toLibraryButton.labelFactory = function(): ITextRenderer
			//{
				//var tf: TextFieldTextRenderer = new TextFieldTextRenderer;
				//tf.textFormat = new TextFormat("Regular", 18, Color.WHITE);
				//return tf;
			//}			
			_toLibraryButton.x = (Preferences.WINDOW_WIDTH / 2) - (_toLibraryButton.defaultSkin.width / 2);
			_toLibraryButton.y = (Preferences.WINDOW_HEIGHT / 2) - (_toLibraryButton.defaultSkin.height / 2);
			_toLibraryButton.addEventListener(Event.TRIGGERED, OnTriggered);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);			
		}
		
		private function OnTriggered(event: Event): void
		{
			MyWorld.gWorld.SetPhase(MyWorld.PHASE_IN_LOBBY);
		}
		
		private function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);			
			addChild(_profileText);
			addChild(_toLibraryButton);
		}
		
		public function Initialize(): void
		{
			_profileText.Initialize();
			_profileText.x = (Preferences.WINDOW_WIDTH / 2) - _profileText.width / 2;
			_profileText.y = 30;			
		}		
	}

}