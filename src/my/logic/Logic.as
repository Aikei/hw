package my.logic {
	/**
	 * ...
	 * @author Aikei
	 */
	import my.logic.abilities.Ability;
	import my.logic.tutorial.Tutorial;
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.*;
	import starling.utils.Color;
	
	import my.events.*;
	import my.mp.messages.*;
	import flash.utils.*;
	import my.dbg.MConsole;
	import my.*;
	import my.logic.effects.Effect;
	import my.mp.Controller;
	import my.system.Language;
	import my.logic.globaleffects.*;
	
	public class Logic
	{		
		public static const PHASE_SELECT_BALL: int = 0;
		public static const PHASE_SELECT_CHAIN: int = 1;
		public static const PHASE_REMOVING: int = 2;
		public static const PHASE_PICKING_STONE_AS_TARGET: int = 3;
		public static const PHASE_EXECUTING_ABILITY_EFFECT: int = 4;
		public static const PHASE_PICKING_YOUR_ABILITY_AS_TARGET: int = 5;
		
		
		public static const TURN_PHASE_NORMAL: int = 0;
		public static const TURN_PHASE_PASSING_TURN: int = 1;
		
		public var m_entropy: Array = null;
		public var m_newEntropy: Array = null;
		public var m_playerHP: int = 100;
		public var m_enemyHP: int = 100;
		
		private var m_messageData: * = null;
		private var m_myNumber: int = 0;
		private var m_enemyNumber: int;
		
		public var m_selectedBalls:Vector.<Ball> = new Vector.<Ball>;		
		public var yourTurn: Boolean = false;
		public var nextTurnYours: Boolean = false;
		public var chainSelectionDone: Boolean = false;
		
		private var removeCompleted: Boolean = false;
		private var nextTurnMessageReceived: Boolean = false;
		private var secondNextTurnMessageReceived: Boolean = false;
		private var noTurnsMessageReceived: Boolean = false;
		private var newEntropyMessageReceived: Boolean = false;
		public var m_gameId: uint = 0;
		private var m_phase: int = PHASE_SELECT_BALL;
		private var _targetingAbility: Ability = null;
		private var _turnPhase: int = TURN_PHASE_NORMAL;
		private var _turnNumber: int = 0;
		private var _newTurnNumber: int = 0;
		private var _tutorial: Tutorial = new Tutorial;
		
		public var m_dice: Array = null;
		public var m_newDice: Array = null;
		
		public var m_players: Vector.<PlayerData> = null;				
		public var m_functionsOnPhaseSelectBall: Array = new Array;
		public var m_functionsOnNextTurn: Array = new Array;
		public var m_globalEffects: Vector.<GlobalEffect> = new Vector.<GlobalEffect>;
		
		public var m_ballDamageAddition: Vector.<Vector.<Number>> = null;
		public var m_ballDamageMultiplier: Vector.<Vector.<Number>> = null;
		public var m_ballManaMultiplier: Vector.<Vector.<Number>> = null;
		public var m_ballManaAddition: Vector.<Vector.<Number>> = null;
		
		public function get turnPhase(): int
		{
			return _turnPhase;
		}
		
		public function get phase(): int
		{
			return m_phase;
		}
		
		public function get myNumber(): int
		{
			return m_myNumber;
		}
		
		public function get enemyNumber(): int
		{
			return m_enemyNumber;
		}		
		
		//public function get enemyNumber(): int
		//{
			//return m_enemyNumber;
		//}
		
		public function Logic() : void
		{

			MyWorld.gWorld.addEventListener("abilityButtonPressed", OnAbilityButtonPressed);
			//MyWorld.gWorld.addEventListener("abilityAnimationEnded", OnAbilityAnimationEnded);
			MyWorld.gWorld.addEventListener("removeBallsWithAbility", OnRemoveBallsWithAbility);
			MyWorld.gWorld.addEventListener("deleteBallsWithAbility", OnDeleteBallsWithAbility);
			MyWorld.gWorld.addEventListener("quietlyRemoveBalls", OnQuietlyRemoveBalls);
			MyWorld.gWorld.addEventListener("cancelPickTarget", OnCancelPickTarget);
		}
		
		public function OnCancelPickTarget(ev: Event): void
		{
			SetPhase(PHASE_SELECT_BALL);
			DetargetAll();
			Controller.DetargetAll();
		}
		
		public function DetargetAll(): void
		{
			for (var i: int = 0; i < MyWorld.gWorld.m_balls.length; i++)
			{
				for (var j: int = 0; j < MyWorld.gWorld.m_balls[i].length; j++)
				{
					MyWorld.gWorld.m_balls[i][j].SetTargeted(false);
				}
			}
		}
		
		public function GetSelf(): PlayerData
		{
			return m_players[myNumber];
		}
		
		public function GetEnemy(): PlayerData
		{
			return m_players[enemyNumber];
		}		
		
		public function DeleteAllBalls(fall: Boolean = true, changeEntropy: Boolean = false, fallDelay: int = 300): void
		{
			//SetPhase(PHASE_EXECUTING_ABILITY_EFFECT);
			var v: Vector.<Ball> = new Vector.<Ball>;
			for (var i: int = 0, l: int = MyWorld.gWorld.m_balls.length; i < l; i++)
			{
				for (var j: int = 0, l2: int = MyWorld.gWorld.m_balls[i].length; j < l2; j++)
				{
					v.push(MyWorld.gWorld.m_balls[i][j]);
				}
			}
			MyWorld.gWorld.m_gameScreen.OnDeleteEffect(v);					
			ActuallyDeleteBallsVector(v);	
			if (fall)
			{
				if (fallDelay <= 0)
					SetFallAfterRemoving(changeEntropy);
				else
					setTimeout(SetFallAfterRemoving,fallDelay,changeEntropy);
			}			
			//setTimeout(SetPhase,300,PHASE_SELECT_BALL);
		}
		
		public function OnQuietlyRemoveBalls(event: Event): void
		{
			var v: Vector.<Ball> = Vector.<Ball>(event.data);
			DeleteBalls(v);
		}
		
		public function OnRemoveBallsWithAbility(event: Event): void
		{
			var v: Vector.<Ball> = Vector.<Ball>(event.data);
			RemoveBalls(v, true);
		}
		
		public function OnDeleteBallsWithAbility(event: Event): void
		{
			var v: Vector.<Ball> = Vector.<Ball>(event.data);
			DeleteBallsWithAbility(v);
		}		
		
		public function OnAbilityButtonPressed(event: Event): void
		{
			if (Ability(event.data).targeting === Ability.TARGET_AUTOMATIC)
			{
				SetPhase(PHASE_EXECUTING_ABILITY_EFFECT);
				Ability(event.data).Use(null);			
			}
			else if (Ability(event.data).targeting === Ability.TARGET_PICK_STONE || Ability(event.data).targeting === Ability.TARGET_PICK_STONE_THEN_ARGUMENTS)
			{				
				_targetingAbility = Ability(event.data);
				SetPhase(PHASE_PICKING_STONE_AS_TARGET);
				Controller.ActivateAbilityButton(_targetingAbility);				
			}
			else if (Ability(event.data).targeting === Ability.TARGET_PICK_YOUR_ABILITY)
			{
				_targetingAbility = Ability(event.data);
				SetPhase(PHASE_PICKING_YOUR_ABILITY_AS_TARGET);
			}
			else if (Ability(event.data).targeting === Ability.TARGET_ENEMY_HERO)
			{
				if (yourTurn)
				{	
					var v: Vector.<GameEntity> = new Vector.<GameEntity>;
					v.push(m_players[m_enemyNumber]);
					SetPhase(PHASE_EXECUTING_ABILITY_EFFECT);
					Ability(event.data).Use(v, undefined);
				}
			}
			else if (Ability(event.data).targeting === Ability.TARGET_YOUR_HERO)
			{
				if (yourTurn)
				{	
					v = new Vector.<GameEntity>;
					v.push(m_players[m_myNumber]);
					SetPhase(PHASE_EXECUTING_ABILITY_EFFECT);
					Ability(event.data).Use(v, undefined);
				}				
			}
		}
		
		public function OnAbilityAnimationEnded(event: Event): void
		{
			_targetingAbility = null;
			SetPhase(PHASE_SELECT_BALL);
		}
		
		public function Update() : void
		{
			MakeCheck();
		}
		
		public function GetCrossNeighbours(ball: Ball): Vector.<Ball>
		{
			var results: Vector.<Ball> = new Vector.<Ball>;
			if (ball.m_index.x > 0)
				results.push(MyWorld.gWorld.m_balls[ball.m_index.x - 1][ball.m_index.y]);
			if (ball.m_index.x < MyWorld.FIELD_WIDTH-1)
				results.push(MyWorld.gWorld.m_balls[ball.m_index.x + 1][ball.m_index.y]);				
			if (ball.m_index.y > 0)
				results.push(MyWorld.gWorld.m_balls[ball.m_index.x][ball.m_index.y - 1]);
			if (ball.m_index.y < MyWorld.FIELD_HEIGHT-1)
				results.push(MyWorld.gWorld.m_balls[ball.m_index.x][ball.m_index.y + 1]);					
			return results;	
		}
		
		public function GetBallNeighbours(ball: Ball, excludeThisBall: Boolean = false): Vector.<Ball>
		{
			MConsole.Write("Get ball neighbours");
			return GetNeighbours(ball.m_index.x, ball.m_index.y, excludeThisBall);
		}				
		
		public function GetNeighbours(x: int, y: int, excludeThisBall: Boolean = false): Vector.<Ball>
		{
			var startx: int = x - 1;
			if (startx < 0)
				startx = 0;
			var starty: int = y - 1;
			if (starty < 0)
				starty = 0;			
			var endx: int = x + 1;
			if (endx >= MyWorld.FIELD_WIDTH)
				endx = MyWorld.FIELD_WIDTH - 1;
			var endy: int = y + 1;
			if (endy >= MyWorld.FIELD_HEIGHT)
				endy = MyWorld.FIELD_HEIGHT - 1;
			var results: Vector.<Ball> = new Vector.<Ball>;
			for (var i: int = startx; i <= endx; i++)
			{
				for (var j: int = starty; j <= endy; j++)
				{
					if (excludeThisBall && i == x && j == y)
						continue;					
					results.push(MyWorld.gWorld.m_balls[i][j]);
				}
			}
			return results;
		}
		
		public function GetDice(): int
		{
			return m_dice.pop() as int;
		}
		
		public function OnBallTouch(ball: Ball, squaredDistanceToCenter: Number, touch: Touch): void
		{
			if (yourTurn)
			{
				if (phase === PHASE_SELECT_CHAIN)
				{
					var msg : MessageContainer;
					if (!ball.IsSelected() && (m_selectedBalls.length == 0 || (squaredDistanceToCenter <= 32*32 && BallsNearAndOfTheSameType(ball,m_selectedBalls[m_selectedBalls.length-1]))))
					{			
						if (SelectBall(ball))
						{
							Controller.SelectBall(ball);
						}
					}
					else if (ball.IsSelected() && squaredDistanceToCenter <= 32*32 && m_selectedBalls.length > 1 && ball.Equals(m_selectedBalls[m_selectedBalls.length - 2]))
					{
						var deselectedBallPos: Vec2 = new Vec2;
						deselectedBallPos.CopyFrom(m_selectedBalls[m_selectedBalls.length - 1].m_index);
						DeselectPreviousBall();
						Controller.DeselectBall(deselectedBallPos);
					}
				}
				else if (phase === PHASE_PICKING_STONE_AS_TARGET)
				{
					if (touch.phase === TouchPhase.ENDED)
					{												
						var v: Vector.<GameEntity> = new Vector.<GameEntity>;
						v.push(ball);
						if (_targetingAbility.CheckTargets(v))
						{							
							if (_targetingAbility.targeting === Ability.TARGET_PICK_STONE)
							{
								SetPhase(PHASE_EXECUTING_ABILITY_EFFECT);
								_targetingAbility.Use(v);
							}
							else if (_targetingAbility.targeting === Ability.TARGET_PICK_STONE_THEN_ARGUMENTS)
							{
								if (_targetingAbility.argumentsTargeting === Ability.TARGET_PICK_YOUR_ABILITY)
								{
									SetPhase(PHASE_PICKING_YOUR_ABILITY_AS_TARGET);
								}
								else
								{
									SetPhase(PHASE_EXECUTING_ABILITY_EFFECT);
								}
								_targetingAbility.PickArguments(v);
							}
							_targetingAbility.DetargetLast();
						}
					}
					else
					{
						if (_targetingAbility.lastTargeted === null || !_targetingAbility.lastTargeted.Equals(GameEntity(ball)))
						{
							_targetingAbility.DetargetLast();
							_targetingAbility.Target(ball);
						}
					}
				}
			}
		}
		
		private function NextTurn(first: Boolean = false): void
		{
			if (m_gameId === 0)
				return;
			if (first)
			{
				nextTurnMessageReceived = false;
				secondNextTurnMessageReceived = false;
			}

			_turnPhase = TURN_PHASE_PASSING_TURN;
			_turnNumber++;
			MConsole.Write("Next turn");
			RefreshAbilities();
			if (!first)
			{
				TickEffects(true); //true means tick balls meant to tick before moving balls
				SetFallAfterRemoving(true);
				TickEffects(false); //false means tick balls meant to tick after moving balls
			}
			yourTurn = nextTurnYours;
			if (yourTurn)
			{
				chainSelectionDone = false;
			}
			
			SetPhase(PHASE_SELECT_BALL);
			
			for (var i = 0; i < m_functionsOnNextTurn.length; i++)
			{
				if (m_functionsOnNextTurn[i].turn === _turnNumber)
				{
					m_functionsOnNextTurn[i].func();					
				}
				if (m_functionsOnNextTurn[i].turn <= _turnNumber)
				{
					m_functionsOnNextTurn.splice(i, 1);
				}
			}
			if (secondNextTurnMessageReceived)
				secondNextTurnMessageReceived = false;
			else
				nextTurnMessageReceived = false;
			removeCompleted = false;
			nextTurnYours = false;
			newEntropyMessageReceived = false;
			noTurnsMessageReceived = false;
			
			m_messageData = null;
			MyWorld.gWorld.m_gameScreen.TweenAllBallsColorToNormal(0);
			//if (yourTurn)
			//{
				//MyWorld.gWorld.m_gameScreen.TweenAllBallsColorToNormal(1.0);
			//}
			if (!yourTurn)
			{
				MyWorld.gWorld.m_gameScreen.TweenHsvAllBalls(1.0, 0, 0, -0.4);
			}
			_turnPhase = TURN_PHASE_NORMAL;
		}
		
		private function TickEffects(before: Boolean = true): void
		{
			for (var i: int = 0; i < MyWorld.FIELD_WIDTH; i++)
			{
				for (var j: int = 0; j < MyWorld.FIELD_HEIGHT; j++)
				{
					if (MyWorld.gWorld.m_balls[i][j])
						MyWorld.gWorld.m_balls[i][j].TickEffects(before);
				}
			}
			
			for (i = 0; i < m_players.length; i++)
			{
				for (j = 0; j < m_players[i].effects.length; j++)
				{
					if (before)
					{
						if (m_players[i].effects[j].TickBeforeMoving())
							j--;
					}
					else
					{
						if (m_players[i].effects[j].TickAfterMoving())
							j--;						
					}
				}
			}
			
			for (i = 0; i < m_globalEffects.length; i++)
			{
				if (before)
				{
					if (m_globalEffects[i].TickBeforeMoving())
						i--;
				}
				else
				{
					if (m_globalEffects[i].TickAfterMoving())
						i--;					
				}
					
			}
		}
		
		public function GetNumberOfSelectedBalls(): int
		{
			return m_selectedBalls.length;
		}
		
		public function GetLastSelectedBall(): Ball
		{
			if (m_selectedBalls.length == 0)
				return null;
			else
				return m_selectedBalls[m_selectedBalls.length - 1];
		}
		
		private function DamageEnemy(value: int = 1): void
		{
			MConsole.Write("Damage enemy");
			if (phase === PHASE_REMOVING)
			{				
				if (yourTurn)
					m_enemyHP -= value;
				else
					m_playerHP -= value;
			}
		}
		
		private function Explosion(ball: Ball): void
		{
			MConsole.Write("Explosion");
			var v: Vector.<Ball> = GetBallsForExplosion(ball);
			for (var i: int = 0, l: int = v.length; i < l; i++)
				RemoveBall(v[i]);
		}
		
		public function GetBallsForExplosion(ball: Ball): Vector.<Ball>
		{
			MConsole.Write("Get Balls for Explosion");
			var result: Vector.<Ball> = new Vector.<Ball>;
			var start: Vec2 = new Vec2;
			start.CopyFrom(ball.m_index);
			start = start.SubtractScalar(1);
			if (start.x < 0)
				start.x = 0;
			if (start.y < 0)
				start.y = 0;
				
			var end: Vec2 = new Vec2;
			end.CopyFrom(ball.m_index);
			end = end.AddScalar(1);
			if (end.x >= MyWorld.FIELD_WIDTH)
				end.x = MyWorld.FIELD_WIDTH - 1;
			if (end.y >= MyWorld.FIELD_HEIGHT)
				end.y = MyWorld.FIELD_HEIGHT - 1;
			for (var i: int = start.x; i <= end.x; i++)
			{
				for (var j: int = start.y; j <= end.y; j++)
				{
					if (i !== ball.m_index.x || j !== ball.m_index.y)
						result.push(MyWorld.gWorld.m_balls[i][j]);
				}
			}
			return result;
		}
		
		public function GetBallsForEliminateRows(ball: Ball, excludeThisBall: Boolean = true): Vector.<Ball>
		{
			MConsole.Write("Get Balls for Eliminate Rows");
			var result: Vector.<Ball> = new Vector.<Ball>;
			var i: int;
			for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
			{
				if (!excludeThisBall || i !== ball.m_index.x)
					result.push(MyWorld.gWorld.m_balls[i][ball.m_index.y]);
			}
			for (i = 0; i < MyWorld.FIELD_HEIGHT; i++)
			{
				if (!excludeThisBall || i !== ball.m_index.y)
					result.push(MyWorld.gWorld.m_balls[ball.m_index.x][i]);
			}
			return result;
		}
		
		private function EliminateRows(ball: Ball): void
		{
			MConsole.Write("Eliminate Rows");
			var v: Vector.<Ball> = GetBallsForEliminateRows(ball);
			for (var i: int, l: int = v.length; i < l; i++)
				RemoveBall(v[i]);						
		}
		
		//public function GetBallPositionInVector(ball: Ball): Vec2
		//{
			//return ball.m_index;
		//}
		
		public function RemoveSameBalls(v: Vector.<Ball>, andThisBall: Ball = null): Vector.<Ball>
		{
			MConsole.Write("Remove same balls");
			var newVector: Vector.<Ball> = new Vector.<Ball>;
			for (var i: int = 0, l: int = v.length; i < l; i++)
			{
				var alreadyHere: Boolean = false;
				for (var j: int = 0, l2: int = newVector.length; j < l2; j++)
				{
					if (v[i].Equals(newVector[j]) || (andThisBall !== null && v[i].Equals(andThisBall)))
						alreadyHere = true;
				}
				if (!alreadyHere)
					newVector.push(v[i]);
			}
			return newVector;
		}
		
		//private function RemoveBalls()
		//{
			//
		//}
		
		
		private function DeleteBall(ball: Ball): void
		{
			MConsole.Write("Delete ball()");
			if (!ball || !ball.IsAlive())
				return;	
			ball.OnDestroy();
			ball.Destroy();
			MyWorld.gWorld.m_balls[ball.m_index.x][ball.m_index.y] = null;				
		}		
		
		private function RemoveBall(ball: Ball): void
		{
			MConsole.Write("Remove ball()");
			if (!ball || !ball.IsAlive())
				return;
			//if (ball.m_type == Ball.RED_BALL)
				//DamageEnemy();			
			ball.OnDestroy();
			ball.Destroy();
			MyWorld.gWorld.m_balls[ball.m_index.x][ball.m_index.y] = null;				
		}		
				
		public function RemoveBalls(v: Vector.<Ball> = null, doNotEndTurn: Boolean = false): void
		{
			SetPhase(PHASE_REMOVING);
			if (m_gameId === 0)
				return;
			MConsole.Write("RemoveBalls()");
			if (v === null)
				v = GetAllRemovedBalls();
			AffectBalls(v);
			MyWorld.gWorld.m_gameScreen.OnDestroyEffect(v);
			setTimeout(ActuallyRemoveBallsVector, 1300, v, doNotEndTurn);
		}
		
		public function AffectBalls(v: Vector.<Ball>): void
		{
			for (var i: int = 0; i < v.length; i++)
			{
				if (v[i].HasEffect(Effect.FROZEN))
				{
					if (v[i].targeted)
						v[i].SetTargeted(false);
					v[i].RemoveEffect(Effect.FROZEN);
					v.splice(i, 1);
					i--;
				}
			}
		}
		
		public function ActuallyDeleteBallsVector(v: Vector.<Ball>): void
		{
			for (var i: int = 0, l: int = v.length; i < l; i++)
				DeleteBall(v[i]);			
		}
		
		public function DeleteBalls(v: Vector.<Ball>): void
		{
			if (m_gameId === 0)
				return;
			MConsole.Write("DeleteBalls()");
			MyWorld.gWorld.m_gameScreen.OnDeleteEffect(v);
			ActuallyDeleteBallsVector(v);			
		}
		
		public function DeleteBallsWithAbility(v: Vector.<Ball>): void
		{
			if (m_gameId === 0)
				return;
			MConsole.Write("DeleteBalls()");
			MyWorld.gWorld.m_gameScreen.OnDeleteEffect(v);
			ActuallyDeleteBallsVector(v);
			SetFallAfterRemoving(false);
			//setTimeout(SetPhase,500,PHASE_SELECT_BALL);
		}				
		
		public function GetCurrentPlayerData(): PlayerData
		{
			if (yourTurn)
				return m_players[m_myNumber];
			return m_players[m_enemyNumber];
			//if (m_myNumber === 0)
				//return m_players[1];
			//else
				//return m_players[0];
		}
		
		private function ActuallyRemoveBallsVector(v : Vector.<Ball>, doNotEndTurn = false): void
		{
			if (m_gameId === 0)
				return;
			MConsole.Write("Actually Remove Balls Vector");
			var l: int = v.length;			
			for (var i: int = 0; i < l; i++)
			{
				RemoveBall(v[i]);
			}
			
			if (!doNotEndTurn)
			{
				StartNextTurnClientPhase();
				//removeCompleted = true;
				//yourTurn = false;
				//if (nextTurnMessageReceived)
					//NextTurn();
				//else
					//nextTurnYours = false;
			}
			else
			{
				SetFallAfterRemoving(false);
				//SetPhase(PHASE_SELECT_BALL);
			}
		}
		
		private function SetFallAfterRemoving(changeEntropy: Boolean): Boolean
		{
			MConsole.Write("Set fall after removing");
			SetPhase(PHASE_REMOVING);
			if (m_entropy == null)
				m_entropy = m_newEntropy;
			//trace("Player entropy before use: ", m_entropy);	
			var newBallsHeight: Vector.<int> = new Vector.<int>(MyWorld.FIELD_WIDTH);
			var i: int;
			var j: int;
			for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
				newBallsHeight[i] = -Ball.BALL_HEIGHT-Ball.BALL_HALF_HEIGHT;			
			var foundAny:Boolean;
			do
			{
				foundAny = false;
				for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
				{
					for (j = 0; j < MyWorld.FIELD_HEIGHT; j++)
					{
						if (MyWorld.gWorld.m_balls[i][j] == null)
						{							
							var topBall:Ball;
							if (j == 0)
							{								
								topBall = MyWorld.gWorld.CreateRandomBall(i * (Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X), newBallsHeight[i],new Vec2(i,j));
								newBallsHeight[i] -= (Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_Y + Ball.BALL_HALF_HEIGHT);
							}
							else
							{
								foundAny = true;
								topBall = MyWorld.gWorld.m_balls[i][j - 1];
								topBall.m_index.x = i;
								topBall.m_index.y = j;
								MyWorld.gWorld.m_balls[i][j - 1] = null;
							}
							MyWorld.gWorld.m_balls[i][j] = topBall;
							topBall.MyMoveTo(i * (Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X) + Ball.BALL_HALF_WIDTH + MyWorld.FIELD_SPACING_X / 2, 
													j * (Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_Y) + Ball.BALL_HALF_HEIGHT + MyWorld.FIELD_SPACING_Y / 2,3);
						}
					}
				}
			} while (foundAny)
			//trace("Player entropy after use: ", m_entropy);
			if (changeEntropy)
			{
				m_entropy = m_newEntropy;
				MyWorld.gWorld.dispatchEvent(new Event("entropyChanged", false, m_entropy));
				m_newEntropy = null;
				m_dice = m_newDice;
				m_newDice = null;
			}
			return foundAny;
		}						
		
		private function GetAllRemovedBalls(): Vector.<Ball>
		{
			var i: int;
			var l: int;
			MConsole.Write("Get All Removed Balls");
			if (m_messageData === null)
			{
				l = m_selectedBalls.length
				var lastSelectedBall: Ball = m_selectedBalls.pop();
				if (l >= Preferences.FIRST_EFFECT_CHAIN_LENGTH && l < Preferences.SECOND_EFFECT_CHAIN_LENGTH)
				{
					m_selectedBalls = m_selectedBalls.concat(GetCrossNeighbours(lastSelectedBall));
					//m_selectedBalls = m_selectedBalls.concat(GetBallsForExplosion(lastSelectedBall));
				}
				else if (l >= Preferences.SECOND_EFFECT_CHAIN_LENGTH)
				{
					m_selectedBalls = m_selectedBalls.concat(GetBallsForExplosion(lastSelectedBall));
					//m_selectedBalls = m_selectedBalls.concat(GetBallsForEliminateRows(lastSelectedBall));
				}
				m_selectedBalls = RemoveSameBalls(m_selectedBalls,lastSelectedBall);
				m_selectedBalls.push(lastSelectedBall);
				if (yourTurn)
				{
					Controller.RemoveBalls(m_selectedBalls);
				}
				return m_selectedBalls;
			}
			else
			{
				var v: Vector.<Ball> = new Vector.<Ball>;
				for (i = 0, l = m_messageData.length; i < l; i++)
				{
					var ball: Ball = MyWorld.gWorld.m_balls[m_messageData[i].x][m_messageData[i].y];
					v.push(ball);
				}
				return v;
			}	
		}
		
		private function PrintGameStartMessage(m: MessageContainer) : void
		{
			MConsole.Write("Printing field: ");
			for (var i: int = MyWorld.FIELD_HEIGHT-1; i >= 0; i--)
			{
				var str: String = "";
				for (var j: int = 0; j < MyWorld.FIELD_WIDTH; j++)
				{
					str += String(m.m_data.field[j][i].type)+" ";
				}
				MConsole.Write(str);
				//MConsole.Write(" ");
			}
		}
		
		public function StartNewGame(m:MessageContainer): void
		{
			//m_yourBallDamageAddition: Vector.<Number> = new Vector.<Number>;
			//m_enemyBallDamageAddition: Vector.<Number> = new Vector.<Number>;
			//m_yourBallDamageMultiplier: Vector.<Number> = new Vector.<Number>;
			//m_enemyBallDamageMultiplier: Vector.<Number> = new Vector.<Number>;			
			//for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			//{
				//m_yourBallDamageAddition.push(0);
				//m_enemyBallDamageAddition.push(0);
				//m_yourBallDamageMultiplier.push(1);
				//m_enemyBallDamageMultiplier.push(1);
			//}
			//_tutorial.Start();
			
			_newTurnNumber = 0;
			_turnNumber = 0;
			var i: int;
			var j: int;
			var l: int;
			var l2: int;
			m_globalEffects = new Vector.<GlobalEffect>;
			m_selectedBalls = new Vector.<Ball>;
			m_ballDamageAddition = new Vector.<Vector.<Number>>;
			m_ballDamageMultiplier = new Vector.<Vector.<Number>>;
			m_ballManaAddition = new Vector.<Vector.<Number>>;
			m_ballManaMultiplier = new Vector.<Vector.<Number>>;
			m_functionsOnPhaseSelectBall = new Array;
			
			for (i = 0; i < 2; i++)
			{
				m_ballDamageAddition.push(new Vector.<Number>);
				m_ballDamageMultiplier.push(new Vector.<Number>);
				m_ballManaAddition.push(new Vector.<Number>);
				m_ballManaMultiplier.push(new Vector.<Number>);
				for (j = 0; j < Ball.NUMBER_OF_TYPES; j++)
				{
					m_ballDamageAddition[i].push(0);
					m_ballDamageMultiplier[i].push(1);
					m_ballManaAddition[i].push(0);
					m_ballManaMultiplier[i].push(1);
				}
			}
			
			PrintGameStartMessage(m);
			m_playerHP = m.m_data.maxHP;
			m_enemyHP = m.m_data.maxHP;
			m_myNumber = m.m_data.number;
			if (m_myNumber === 0)
				m_enemyNumber = 1;
			else
				m_enemyNumber = 0;
			m_gameId = m.m_data.gameId;
			m_entropy = m.m_data.entropy;
			MyWorld.gWorld.dispatchEvent(new Event("entropyChanged", false, m_entropy));
			m_dice = m.m_data.dice;
			m_players = new Vector.<PlayerData>;
			if (m_myNumber === 0)
			{
				m_players.push(new PlayerData(false,m.m_data.heroClasses[0]));
				m_players.push(new PlayerData(true,m.m_data.heroClasses[1]));				
			}
			else
			{
				m_players.push(new PlayerData(true,m.m_data.heroClasses[0]));
				m_players.push(new PlayerData(false,m.m_data.heroClasses[1]));
			}
				

			
			//m_players[0].AddAbility(Ability.TURN);
			//m_players[1].AddAbility(Ability.TURN);
			MyWorld.gWorld.m_gameScreen.m_showNextWindow.visible = false;
			for (i = 0, l = m.m_data.abilities.length; i < l; i++)
			{
				m_players[i].ClearAbilities();
				for (j = 0, l2 = m.m_data.abilities[i].length; j < l2; j++)
				{
					if (i === myNumber && m.m_data.abilities[i][j] === Ability.SEE_NEXT)
						MyWorld.gWorld.m_gameScreen.m_showNextWindow.visible = true;
					m_players[i].AddAbility(m.m_data.abilities[i][j]);
				}
			}
			InitializePassives();
			
			for (i = 0; i < m_players.length; i++)
			{
				m_players[i].hp = m.m_data.maxHP;
				MyWorld.gWorld.dispatchEvent(new Event("RenewPlayerData", false, m_players[i] ));
			}
			//m_ballsGroup.entityAlpha = 0;
			if (MyWorld.gWorld.m_balls != null)
			{
				for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
				{
					for (j = 0; j < MyWorld.FIELD_HEIGHT; j++)
					{
						//MyWorld.gWorld.remove(MyWorld.gWorld.m_balls[i][j]);
						if (MyWorld.gWorld.m_balls[i][j] == null)
							MConsole.Write("Destroying null ball!");
						MyWorld.gWorld.m_balls[i][j].Destroy();
						MyWorld.gWorld.m_balls[i][j] = null
					}
				}
			}
			//TweenLite.to(m_ballsGroup, 1, { entityAlpha: 1 } );
			var createNew: Boolean = false;
			if (MyWorld.gWorld.m_balls == null)
			{
				createNew = true;
				MyWorld.gWorld.m_balls = new Vector.<Vector.<Ball>>;
			}
			for (i = 0; i < MyWorld.FIELD_WIDTH; i++)
			{
				if (createNew)
					MyWorld.gWorld.m_balls.push(new Vector.<Ball>);
				for (j = 0; j < MyWorld.FIELD_HEIGHT; j++)
				{
					var ball: Ball = MyWorld.gWorld.CreateNewBall(i * (Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X), j * (Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_Y), new Vec2(i, j), m.m_data.field[i][j].type);
					if (createNew)
						MyWorld.gWorld.m_balls[i].push(ball);
					else
						MyWorld.gWorld.m_balls[i][j] = ball;
				}
			}
			if (m.m_data.number == m.m_data.firstTurn)
				nextTurnYours = true;
			else
				nextTurnYours = false;
			MyWorld.gWorld.dispatchEvent(new Event("NewTurn", false, nextTurnYours));
			NextTurn(true);
		}
		
		private function InitializePassives(): void
		{
			var numberOfRages: int;
			var rage: Ability;
			var ab: Ability;
			
			for (var i: int = 0; i < m_players.length; i++)
			{
				numberOfRages = 0;
				rage = null;
				for (var j: int = 0; j < m_players[i].abilities.length; j++)
				{
					ab = m_players[i].abilities[j];
					if (ab.passive && ab.subType === "rage")
					{
						numberOfRages++;
						rage = ab;
					}
				}
				if (numberOfRages >= 2)
					rage.Use(null, new Array);
				for (j = 0; j < m_players[i].abilities.length; j++)
				{										
					if (m_players[i].abilities[j].passive && (ab.subType !== "rage" || numberOfRages < 2))
					{
						m_players[i].abilities[j].Use(null);
					}
				}
			}
		}
				
		private function MakeCheck(): void
		{
			if (phase === PHASE_SELECT_BALL)
			{

			}
			else if (phase === PHASE_SELECT_CHAIN)
			{
				

			}
			else if (phase === PHASE_REMOVING)
			{
				var anyMoving:Boolean = false;
				for (var i:int = 0; i < MyWorld.FIELD_WIDTH; i++)
				{
					for (var j:int = 0; j < MyWorld.FIELD_HEIGHT; j++)
					{
						if (MyWorld.gWorld.m_balls[i][j].IsMoving())
							anyMoving = true;
					}
				}
				if (!anyMoving)
				{
					//yourTurn = false;
					SetPhase(PHASE_SELECT_BALL);
				}
			}
		}
								
		public function DeselectAll(): void
		{
			MConsole.Write("Deselect all");
			MyWorld.gWorld.dispatchEvent(new BallsDeselectedEvent(BallsDeselectedEvent.BALLS_DESELECTED, m_selectedBalls));
			for (var i: int = 0, l: int = m_selectedBalls.length; i < l; i++)
				m_selectedBalls[i].SetSelected(false);
			m_selectedBalls = new Vector.<Ball>;
			//MyWorld.gWorld.m_interfaceView.RemoveChainLengthText();
		}
		
		private function SelectBall(ball: Ball): Boolean
		{
			MConsole.Write("Select ball");
			if (ball.HasEffect(Effect.FROZEN))
				return false;
			for (var i: int = 0; i < ball.effects.length; i++)
			{
				if (ball.effects[i].triggerOnSelect)
				{
					if (ball.effects[i].Trigger(yourTurn) && ball.effects[i].removeOnTrigger)
					{
						ball.effects[i].Remove();
						i--;
						continue;
					}
				}
				if (ball.effects[i].removeOnSelect)
				{
					ball.effects[i].Remove();
					i--;
				}
			}
			ball.SetSelected(true);
			m_selectedBalls.push(ball);
			var v: Vector.<Ball>;
			if (m_selectedBalls.length >= Preferences.FIRST_EFFECT_CHAIN_LENGTH && m_selectedBalls.length < Preferences.SECOND_EFFECT_CHAIN_LENGTH)
			{
				v = GetCrossNeighbours(m_selectedBalls[m_selectedBalls.length - 2]);
				for (i = 0; i < v.length; i++)
				{
					v[i].SetTargeted(false);
				}
				v = GetCrossNeighbours(ball);
				for (i = 0; i < v.length; i++)
				{
					if (!v[i].IsSelected())
						v[i].SetTargeted(true);
				}
			}
			else if (m_selectedBalls.length >= Preferences.SECOND_EFFECT_CHAIN_LENGTH)
			{
				if (m_selectedBalls.length - 1 >= Preferences.SECOND_EFFECT_CHAIN_LENGTH)
					v = GetBallNeighbours(m_selectedBalls[m_selectedBalls.length - 2],true);
				else
					v = GetCrossNeighbours(m_selectedBalls[m_selectedBalls.length - 2]);
				for (i = 0; i < v.length; i++)
				{
					v[i].SetTargeted(false);
				}
				v = GetBallNeighbours(ball,true);
				for (i = 0; i < v.length; i++)
				{
					if (!v[i].IsSelected())
						v[i].SetTargeted(true);
				}
			}			
			MyWorld.gWorld.dispatchEvent(new BallSelectedEvent(BallSelectedEvent.BALL_SELECTED, ball, m_selectedBalls.length));
			return true;
			//var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksBallSelected, { gameId : m_gameId, selectedBall : { x: ball.m_index.x, y: ball.m_index.y } } );			
			//MyWorld.gWorld.m_multiplayer.SerializeAndSend(msg);			
			//MyWorld.gWorld.m_interfaceView.AddChainLengthText(m_selectedBalls.length, ball);
		}
		
		private function DeselectPreviousBall(): void
		{
			MConsole.Write("Deselect previous ball");
			var ball: Ball = m_selectedBalls.pop();
			ball.SetSelected(false);
			var v: Vector.<Ball>;
			
			if (m_selectedBalls.length + 1 >= Preferences.FIRST_EFFECT_CHAIN_LENGTH && m_selectedBalls.length + 1 < Preferences.SECOND_EFFECT_CHAIN_LENGTH)
			{
				v = GetCrossNeighbours(ball);
				for (i = 0; i < v.length; i++)
				{
					v[i].SetTargeted(false);
				}				
			}					
			else if (m_selectedBalls.length + 1 >= Preferences.SECOND_EFFECT_CHAIN_LENGTH)
			{
				v = GetBallNeighbours(ball,true);
				for (i = 0; i < v.length; i++)
				{
					v[i].SetTargeted(false);
				}					
			}			
			
			if (m_selectedBalls.length >= Preferences.FIRST_EFFECT_CHAIN_LENGTH && m_selectedBalls.length < Preferences.SECOND_EFFECT_CHAIN_LENGTH)
			{
				v = GetCrossNeighbours(m_selectedBalls[m_selectedBalls.length - 1]);
				for (var i: int = 0; i < v.length; i++)
				{
					if (!v[i].IsSelected())
						v[i].SetTargeted(true);
				}								
			}
			else if (m_selectedBalls.length >= Preferences.SECOND_EFFECT_CHAIN_LENGTH)
			{
				v = GetBallNeighbours(m_selectedBalls[m_selectedBalls.length - 1]);
				for (i = 0; i < v.length; i++)
				{
					if (!v[i].IsSelected())
						v[i].SetTargeted(true);
				}
			}
			
			ball = m_selectedBalls[m_selectedBalls.length - 1];
			MyWorld.gWorld.dispatchEvent(new BallSelectedEvent(BallSelectedEvent.BALL_SELECTED, ball, m_selectedBalls.length));
		}
		
		public function BallsNearAndOfTheSameType(ball1: Ball, ball2: Ball): Boolean
		{
			if (ball1.m_type != ball2.m_type)
				return false;
			if (Math.abs(ball1.m_index.x - ball2.m_index.x) > 1 || Math.abs(ball1.m_index.y - ball2.m_index.y) > 1)
				return false;
			return true;
		}		
				
		private function SwapBallsInVector(ball1:Ball, ball2:Ball): void
		{
			MConsole.Write("Swap balls in vector");
			var firstx: int, firsty: int;
			var secondx: int, secondy: int;		
			MyWorld.gWorld.m_balls[ball1.m_index.x][ball1.m_index.y] = ball2;
			MyWorld.gWorld.m_balls[ball2.m_index.x][ball2.m_index.y] = ball1;
		}
		
		public function RemoveSelected(): void
		{
			MyWorld.gWorld.dispatchEvent(new BallsDeselectedEvent(BallsDeselectedEvent.BALLS_DESELECTED, m_selectedBalls));
			RemoveBalls();
			m_selectedBalls = new Vector.<Ball>;			
		}
		
		public function SetPhase(newPhase: int): void
		{
			//if (m_phase === newPhase)
				//return;				
			var oldPhase = m_phase;	
			m_phase = newPhase;	
			
			if (oldPhase === PHASE_SELECT_BALL)
			{
				MyWorld.gWorld.m_gameScreen.DisableAbilityButtons();
			}
			else if (oldPhase === PHASE_PICKING_STONE_AS_TARGET)
			{
				if (yourTurn)
					MyWorld.gWorld.m_gameScreen.m_pickTargetWindow.visible = false;
			}
			else if (oldPhase === PHASE_PICKING_YOUR_ABILITY_AS_TARGET)
			{
				if (yourTurn)
					MyWorld.gWorld.m_gameScreen.m_pickYourAbilityWindow.visible = false;
			}									
			
			if (newPhase === PHASE_PICKING_STONE_AS_TARGET)
			{
				if (yourTurn)
					MyWorld.gWorld.m_gameScreen.m_pickTargetWindow.visible = true;
			}
			else if (newPhase === PHASE_PICKING_YOUR_ABILITY_AS_TARGET)
			{
				if (yourTurn)
					MyWorld.gWorld.m_gameScreen.m_pickYourAbilityWindow.visible = true;
			}
			else if (newPhase === PHASE_SELECT_BALL)
			{
				if (yourTurn)
					MyWorld.gWorld.m_gameScreen.EnableAbilityButtons();
				for (var i = 0; i < m_functionsOnPhaseSelectBall.length; i++)
				{
					var f = m_functionsOnPhaseSelectBall[i];
					m_functionsOnPhaseSelectBall.splice(0, 1);
					f();					
					//m_functionsOnPhaseSelectBall[i]();
				}
				m_functionsOnPhaseSelectBall = new Array;
			}			
			var event: Event = new Event("logicPhaseChanged",false, {newPhase: newPhase, oldPhase: oldPhase} );
			MyWorld.gWorld.dispatchEvent(event);						
		}
		
		public function CountDamage(balls : Vector.<Ball>): Vector.<Number>
		{
			var result: Vector.<Number> = new Vector.<Number>;
			result.push(0);
			result.push(0);
			for (var i = 0, l = balls.length; i < l; i++)
			{
				if (balls[i].m_specialType === Ball.BALL_SPECIAL_TYPE_ATTACK)
					result[0] += Preferences.ATTACK_STONE_DAMAGE;
				else if (balls[i].m_specialType === Ball.BALL_SPECIAL_TYPE_HEALTH)
					result[1] += Preferences.HEALTH_STONE_HEAL;
				var n: int = m_myNumber;
				if (!yourTurn)
					n = m_enemyNumber;					
				if (balls[i].m_type !== Ball.BLACK_BALL)
				{
					result[0] += balls[i].damage*m_ballDamageAddition[n][balls[i].m_type] + m_ballDamageMultiplier[n][balls[i].m_type];
				}
				else
				{
					result[0] += balls[i].damage*m_ballDamageAddition[n][balls[i].m_type]*Preferences.BLACK_STONE_DAMAGE_MULTIPLIER + m_ballDamageMultiplier[n][balls[i].m_type];
				}
				//else if (balls[i].m_type === Ball.YELLOW_BALL)
					//result[1] += 1;
			}
			if (yourTurn)
				result[0] = Math.floor(result[0] / Preferences.BALLS_PER_ONE_DAMAGE);
			else
				result[0] = Math.floor(result[0] / Preferences.BALLS_PER_ONE_DAMAGE);
			return result;
		}
		
		public function GetRandomBalls(num: uint, withThese: Vector.<Ball> = null): Vector.<Ball>
		{
			var v: Vector.<Ball>;
			if (withThese === null)
				v = new Vector.<Ball>;
			else
				v = withThese;
			var sameBall: Boolean;
			var d: int, x: int, y: int;
			var numberOfBalls: int = MyWorld.FIELD_WIDTH * MyWorld.FIELD_HEIGHT;
			for (var i: uint = 0; i < num; i++)
			{				
				var ball: Ball;
				do
				{
					sameBall = false;
					if (m_dice.length === 0)
					{
						MConsole.Write("_ii","WARNING! Out of dice!");
						return null;
					}
					d = m_dice.pop() % numberOfBalls;
					y = int(d / MyWorld.FIELD_WIDTH);
					x = d % MyWorld.FIELD_WIDTH;
					ball = MyWorld.gWorld.m_balls[x][y];
					for (var j: int = 0; j < v.length; j++)
					{
						if (v[j].Equals(ball))
						{
							sameBall = true;
							break;
						}
					}
				} while (sameBall);
				v.push(ball);
			}
			return v;
		}
		
		public function GetChain(ball: Ball): Vector.<Ball>
		{
			var chain: Vector.<Ball> = new Vector.<Ball>;
			chain.push(ball);
			var n: int = 0;
			do
			{
				var neighbours: Vector.<Ball> = this.GetBallNeighbours(chain[n],true)
				iLoop: for (var i = 0; i < neighbours.length; i++)
				{
					for (var j = 0; j < chain.length; j++)
					{
						if (chain[j].Equals(neighbours[i]))
							continue iLoop;
					}
					if (neighbours[i].m_type === chain[n].m_type)
					{
					   chain.push(neighbours[i]);
					}
				}
				n++;
			} while (n < chain.length);							
			return chain;
		}
		
		//public function RemoveWithWhenNoMoves(a_entropy: Array, a_usedEntropy: Array, a_dice: Array, a_usedDice: Array): void
		//{
			//m_entropy = a_usedEntropy;
			//m_newEntropy = a_entropy;
			//m_dice = a_usedDice;
			//m_newDice = a_dice;			
			//MyWorld.gWorld.m_gameScreen.AddFloatingText(Language.GetString("NoMoves"), 
								//MyWorld.gWorld.m_gameScreen.m_ballsGroup.x + MyWorld.OVERALL_FIELD_WIDTH / 2, 
								//MyWorld.gWorld.m_gameScreen.m_ballsGroup.y + MyWorld.OVERALL_FIELD_HEIGHT / 2, 
								//Color.RED);
			//setTimeout(MyWorld.gWorld.m_gameScreen.AddFloatingText, 500,
								//"-" + String(Preferences.DAMAGE_IF_NO_TURNS), 
								//MyWorld.gWorld.m_gameScreen.m_ballsGroup.x + MyWorld.OVERALL_FIELD_WIDTH / 2, 
								//MyWorld.gWorld.m_gameScreen.m_ballsGroup.y + MyWorld.OVERALL_FIELD_HEIGHT / 2, 
								//Color.RED);			
			//DeleteAllBalls(true, true);			
		//}
		
		public function GetGlobalEffectIndex(type: String)
		{
			for (var i: int = 0; i < m_globalEffects.length; i++)
			{
				if (m_globalEffects[i].type === type)
					return i;
			}
			return -1;
		}
		
		public function RemoveGlobalEffect(type: String): Boolean
		{
			var index: int = GetGlobalEffectIndex(type)
			if (index !== -1)
			{
				m_globalEffects[index].OnRemoved();
				m_globalEffects.splice(index, 1);
				return true;
			}
			return  false;			
		}
		
		public function RefreshAbilities(): void
		{
			for (var i: int = 0; i < m_players.length; i++)
			{
				for (var a: int = 0; a < m_players[i].abilities.length; a++)
				{
					m_players[i].abilities[a].used = false;
				}
			}
		}
		
		public function AddGlobalEffect(globalEffect: GlobalEffect): void 
		{
			//var c: Class = getDefinitionByName("my.logic.globaleffects.GlobalEffect" + type) as Class;
			//var obj: GlobalEffect = GlobalEffect(new c());
			globalEffect.OnAdded();
			m_globalEffects.push(globalEffect);
		}
		
		public function IsGlobalEffectPresent(type: String): Boolean 
		{
			return (GetGlobalEffectIndex(type) !== -1);
		}
		
		public function StartNextTurnServerPhase(): void
		{
			if (!nextTurnMessageReceived)
				nextTurnMessageReceived = true;
			else
				secondNextTurnMessageReceived = true;
				
			//nextTurnMessageReceived = true;	
				
			if (removeCompleted)
				NextTurn();
		}
		
		public function StartNextTurnClientPhase(): void
		{
			//yourTurn = false;
			removeCompleted = true;
			if (nextTurnMessageReceived)
				NextTurn();
			else
				nextTurnYours = false;
		}
		
		public function IsPhase(a_phase: int): Boolean
		{
			return (this.phase === a_phase);
		}
		
		public function OnGameMessage(m: MessageContainer): void
		{
			var i: int;
			if (m.s !== MessageNames.ksGameStart && m.s !== MessageNames.ksRefreshPlayerData && m_gameId !== m.m_data.gameId)
				return;
			if (m.s === MessageNames.ksGameStart)
			{
				MConsole.Write("Start game message received");
				StartNewGame(m);
			}
			else if (m.s === MessageNames.ksNewEntropy)
			{		
				MConsole.Write("New entropy message received");
				
				newEntropyMessageReceived = true;			
				
				m_newEntropy = m.m_data.entropy;
				m_newDice = m.m_data.dice;
				
				//if (m_entropy == null)
					//m_entropy = m.m_data.entropy;
				//else
					//m_newEntropy = m.m_data.entropy;
				//if (m_dice == null)				
					//m_dice = m.m_data.dice;
				//else
					//m_newDice = m.m_data.dice;
			}
			else if (m.s === MessageNames.ksBallSelected)
			{
				MConsole.Write("Ball selected message received");
				SelectBall(MyWorld.gWorld.m_balls[m.m_data.selectedBall.x][m.m_data.selectedBall.y]);
			}
			else if (m.s === MessageNames.ksLastBallDeselected)
			{
				MConsole.Write("Deselect previous ball message received");
				DeselectPreviousBall();
			}
			else if (m.s === MessageNames.ksDeselectAll)
			{
				MConsole.Write("Deselect all message received");
				DeselectAll();
			}
			else if (m.s === MessageNames.ksNextTurn)
			{
				MConsole.Write("Next turn message received");
				if (_newTurnNumber < m.m_data.turnNumber)
					_newTurnNumber = m.m_data.turnNumber;
				var f = function ()
				{
					m_entropy = m.m_data.entropy;
					m_dice = m.m_data.dice;
				}
				var obj = { func: f, turn: m.m_data.turnNumber }
				m_functionsOnNextTurn.push(obj);
				if (_newTurnNumber === m.m_data.turnNumber)
				{
					if (m.m_data.turn == m_myNumber)
						nextTurnYours = true;
					else
						nextTurnYours = false;
				}
				MyWorld.gWorld.dispatchEvent(new Event("NewTurn", false, nextTurnYours));	
				StartNextTurnServerPhase();
				//if (removeCompleted)
					//NextTurn();
			}
			else if (m.s === MessageNames.ksRemoveBalls)
			{
				MConsole.Write("Remove balls message received");
				m_messageData = m.m_data.balls;
				RemoveSelected();
				//SetPhase(PHASE_REMOVING);
			}
			else if (m.s === MessageNames.ksEndGame)
			{
				MConsole.Write("End game message received");
				m_selectedBalls = new Vector.<Ball>;
				m_gameId = 0;
				m_entropy = null;
				m_newEntropy = null;
				m_dice = null;
				m_newDice = null;
				//MyWorld.gWorld.SetPhase(MyWorld.PHASE_IN_LOBBY);
			}			
			else if (m.s === MessageNames.ksRefreshPlayerData)
			{
				MConsole.Write("Refresh player data message received");
				m_playerHP = m.m_data.playerData[m_myNumber].hp;
				m_enemyHP = m.m_data.playerData[m_enemyNumber].hp;
				//if (m_myNumber === 0)
					//m_enemyHP = m.m_data.playerData[1].hp;
				//else
					//m_enemyHP = m.m_data.playerData[0].hp;
				MyWorld.gWorld.dispatchEvent(new Event("RefreshPlayerData", false, m.m_data.playerData));
			}			
			//else if (m.s === MessageNames.ksHpNow)
			//{
				//MConsole.Write("Player hp message received");
				//m_playerHP = m.m_data.hp[m_myNumber];
				//if (m_myNumber == 0)
					//m_enemyHP = m.m_data.hp[1];
				//else
					//m_enemyHP = m.m_data.hp[0];
			//}
			else if (m.s === MessageNames.ksAbilityButtonActivated)
			{
				if (yourTurn)
					_targetingAbility = Ability.CreateAbility(m.m_data.abilityType, m_myNumber);
				else
					_targetingAbility = Ability.CreateAbility(m.m_data.abilityType, m_enemyNumber);
				SetPhase(PHASE_PICKING_STONE_AS_TARGET);
			}
			else if (m.s === MessageNames.ksSelectedAbilityTargets)
			{
				for (i = 0; i < m.m_data.targets.length; i++)
				{
					MyWorld.gWorld.m_balls[m.m_data.targets[i].x][m.m_data.targets[i].y].SetTargeted(true);
				}
			}
			else if (m.s === MessageNames.ksDeselectedAbilityTargets)
			{
				for (i = 0; i < m.m_data.targets.length; i++)
				{
					MyWorld.gWorld.m_balls[m.m_data.targets[i].x][m.m_data.targets[i].y].SetTargeted(false);
				}				
			}
			else if (m.s === MessageNames.ksAbilityUsed)
			{
				var v: Vector.<GameEntity> = new Vector.<GameEntity>;
				if (m.m_data.targets.length > 0)
				{
					if (m.m_data.targets[0].x === -1)
					{
						v.push(m_players[m_myNumber]);
					}
					else if (m.m_data.targets[0].x === -2)
					{
						v.push(m_players[m_enemyNumber]);
						//if (m_myNumber == 0)
							//v.push(m_players[1]);
						//else
							//v.push(m_players[0]);
					}
					else
					{
						for (i = 0; i < m.m_data.targets.length; i++)
						{
							v.push(MyWorld.gWorld.m_balls[m.m_data.targets[i].x][m.m_data.targets[i].y]);
						}
					}
				}
				if (yourTurn)
					_targetingAbility = Ability.CreateAbility(m.m_data.abilityType, m_myNumber);
				else
					_targetingAbility = Ability.CreateAbility(m.m_data.abilityType, m_enemyNumber);
				_targetingAbility.Use(v,m.m_data.args);
			}
			else if (m.s === MessageNames.ksNoMoves)
			{
				noTurnsMessageReceived = true;				
				var func: Function = function () 
				{ 
					m_entropy = m.m_data.usedEntropy;
					m_newEntropy = m.m_data.entropy;
					m_dice = m.m_data.usedDice;
					m_newDice = m.m_data.dice;
					var text: String;
					if (m.m_data.whoNumber === myNumber)
						text = Language.GetString("YouHaveNoMoves");
					else
						text = Language.GetString("EnemyHasNoMoves");
					MyWorld.gWorld.m_gameScreen.AddFloatingText(text, 
										MyWorld.gWorld.m_gameScreen.m_ballsGroup.x + MyWorld.OVERALL_FIELD_WIDTH / 2, 
										MyWorld.gWorld.m_gameScreen.m_ballsGroup.y + MyWorld.OVERALL_FIELD_HEIGHT / 2, 
										Color.RED);
					setTimeout(MyWorld.gWorld.m_gameScreen.AddFloatingText, 500,
										"-" + String(Preferences.DAMAGE_IF_NO_TURNS), 
										MyWorld.gWorld.m_gameScreen.m_ballsGroup.x + MyWorld.OVERALL_FIELD_WIDTH / 2, 
										MyWorld.gWorld.m_gameScreen.m_ballsGroup.y + MyWorld.OVERALL_FIELD_HEIGHT / 2, 
										Color.RED);	
					RemoveGlobalEffect(GlobalEffect.FREEZING_RAIN);
					DeleteAllBalls(true, true, 0);
					if (m.m_data.causedByPlayer)
						StartNextTurnClientPhase();								
				}				
				m_functionsOnPhaseSelectBall.push(func);
				if (phase === PHASE_SELECT_BALL)
				{
					SetPhase(PHASE_SELECT_BALL);
				}
			}
			else if (m.s === MessageNames.ksDetargetAll)
			{
				DetargetAll();
			}
		}
	}

}