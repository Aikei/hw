package my.logic.newability 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.GameEntity;
	import my.logic.Logic;
	import my.logic.PlayerData;
	import my.mp.Controller;
	
	public class AbilityAction 
	{
		protected var _affect: ActionAffect;
		protected var _showRange: ActionRange;
		protected var _pickRange: ActionRange;		
		protected var _visualize: ActionVisualize;
		protected var _messageData: Object;
		
		public function get showRange(): ActionRange
		{
			return _showRange;
		}
		
		public function get pickRange(): ActionRange
		{
			return _pickRange;
		}		
		
		public function get messageData(): Object
		{
			return _messageData;
		}
		
		public function AbilityAction() 
		{
			
		}
		
		public function HighlightTargets(highlighted: Array): void
		{
			_showRange.GetTargets(highlighted);
			Controller.NewHighlightAbilityTargets(_showRange.targets);
		}
		
		public function DehighlightTargets(): void
		{
			if (_showRange.targets.length > 0)
				Controller.NewDehighlightAbilityTargets(_showRange.targets);
		}		
		
		public function StartPick(): Boolean
		{
			return _showRange.StartPick();
		}
		
		public function PickTargets(highlighted: Array): void
		{
			if (highlighted !== null)
				_pickRange.GetTargets(highlighted);
			_messageData = { targets: [] };
			AddTargets(pickRange.targets);
			//for (var i = 0; i < _pickRange.targets.length; i++)
			//{
				//var obj = { };	
				//if (_pickRange.targets[i] is Ball)
				//{
					//obj = { x: _pickRange.targets[i].x, y: _pickRange.targets[i].y, type: "b" }	
				//}
				//else if (_pickRange.targets[i] is PlayerData)
				//{
					//obj = { x: _pickRange.targets[i].x, y: _pickRange.targets[i].y, type: "p" }	
				//}
				//else
				//{
					//obj = _pickRange.targets[i];
				//}
				//_messageData.targets.push(obj);
			//}
		}
		
		public function AddTargets(additionalTargets: Array): void
		{
			for (var i = 0; i < additionalTargets.length; i++)
			{
				var obj = { };	
				if (additionalTargets[i] is Ball)
				{
					obj = { x: additionalTargets[i].x, y: additionalTargets[i].y, type: "b" }	
				}
				else if (_pickRange.targets[i] is PlayerData)
				{
					obj = { x: additionalTargets[i].x, y: additionalTargets[i].y, type: "p" }	
				}
				else
				{
					obj = additionalTargets[i];
				}
				_messageData.targets.push(obj);
			}			
		}
		
		//public function Use(messageData: * ): void
		//{
			//
		//}
	}

}