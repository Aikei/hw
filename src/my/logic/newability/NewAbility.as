package my.logic.newability 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.GameEntity;
	import my.logic.Logic;
	import my.mp.Controller;
	import my.MyWorld;
	import my.mp.messages.*;
	
	public class NewAbility 
	{
		protected var _actions: Vector.<AbilityAction>;
		protected var _currentAction: int = 0;
		protected var _type: String = "";
		protected var _messageData: Object;
		
		public function get messageData(): Object 
		{
			return _messageData;
		}
		
		public function get type(): String
		{
			return _type;
		}
		
		public function NewAbility() 
		{
			
		}
		
		public function StartPick(): void 
		{
			if (_currentAction === 0)
				_messageData = { actions: [] };

			if (!_actions[_currentAction].StartPick())
			{
				PickTargets(null);
			}			
		}
		
		public function HighlightTargets(highlighted: Array): void
		{
			_actions[_currentAction].HighlightTargets(highlighted);
		}
		
		public function PickTargets(highlighted: Array): void
		{
			_actions[_currentAction].PickTargets(highlighted);
			_messageData.actions.push(_actions[_currentAction].messageData);
			PickNextAction();
		}		
		
		private function PickNextAction(): void
		{
			_currentAction++;
			if (_currentAction >= _actions.length)
			{
				_currentAction = 0;
				MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_EXECUTING_ABILITY_EFFECT);
				CompletePick();
			}
			else
			{
				StartPick();
			}
		}
		
		private function CompletePick(): void
		{
			for (var i: int = 0; i < _actions.length; i++)
			{
				_messageData.actions.push(_actions[i].messageData);
			}
			//Controller.UseNewAbility(
		}
		
	}

}