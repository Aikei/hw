package my.logic.newability 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.GameEntity;
	import my.MyWorld;
	import starling.events.Event;
	import flash.utils.setTimeout;
	import starling.events.Event;
	
	public class ActionVisualize 
	{
		
		public static const VISUALIZE_NONE: int = 0;
		public static const VISUALIZE_SHAKE: int = 1;
		public static const VISUALIZE_CREATE_BALL_VISUAL: int = 2;
		
		//public static const EVENT_VISUALIZATION_COMPLETE: String = "abilityVisualizationComplete";
		
		protected var _visualizeType: int = 0;
		protected var _timeOut: Number = 0;
		
		public function ActionVisualize(visualizeType: int)
		{
			_visualizeType = visualizeType;
		}
		
		public function Visualize(targets: Array) : Number  //returns timeout to start affect
		{
			switch (_visualizeType)
			{
				case VISUALIZE_SHAKE:
					MyWorld.gWorld.m_gameScreen.ShakeField(0.25, 2);
					_timeOut = 500;
					break;
			}
			return _timeOut;
		}
		
	}

}