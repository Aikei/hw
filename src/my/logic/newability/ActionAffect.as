package my.logic.newability 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.GameEntity;
	import my.MyWorld;
	import starling.events.Event;
	
	public class ActionAffect 
	{
		public static const AFFECT_NONE: int = 0;
		public static const AFFECT_REMOVE_BALLS: int = 1;
		public static const AFFECT_DESTROY_BALLS: int = 2;
		public static const AFFECT_DESTROY_BALLS_BUT_DONT_GIVE_MANA: int = 3;
		public static const AFFECT_APPLY_EFFECT: int = 4;
		
		protected var _affectType: int = 0;
		
		public function ActionAffect(affectType: int) 
		{
			_affectType = affectType;
		}
		
		public function Affect(targets: Array) : void
		{
			switch (_affectType)
			{
				case AFFECT_REMOVE_BALLS:
					var eventString: String = "deleteBallsWithAbility";
					MyWorld.gWorld.dispatchEvent(new Event(eventString, false, targets));
					break;
				
				case AFFECT_DESTROY_BALLS:
					eventString = "removeBallsWithAbility";
					MyWorld.gWorld.dispatchEvent(new Event(eventString, false, targets));
					break;
			}
		}
	}

}