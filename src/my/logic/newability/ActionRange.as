package my.logic.newability {
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.GameEntity;
	import my.MyAssets;
	import my.MyWorld;
	import my.logic.Logic;
	
	public class ActionRange 
	{
		public static const RANGE_NONE: uint = 0;
		
		public static const RANGE_SELECTED_STONE_ONLY: uint = 1;
		public static const RANGE_SELECTED_STONE_WITH_NEIGHBOURS: uint = 2;
		public static const RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS: uint = 4;
		
		public static const SELECT_STONE: uint = 15;
		
		public static const RANGE_YOUR_HERO: uint = 16;
		public static const RANGE_ENEMY_HERO: uint = 32;
		
		public static const RANGE_SELECTED_ABILTIY: uint = 64;
				
		protected var _targets: Array;
		protected var _rangeType: int = 0;
		protected var _selectType: int = 0;
		
		public function get rangeType(): int
		{
			return _rangeType;
		}
		
		public function get targets(): Array
		{
			return _targets;
		}
		
		public function ActionRange(rangeType: int) 
		{
			_rangeType = rangeType;
		}
		
		public function StartPick(): Boolean
		{
			if (_rangeType & SELECT_STONE)
			{
				MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_PICKING_STONE_AS_TARGET);
				return true;
			}
			
			GetTargets(null);
			return false;
		}
		
		public function GetTargets(highlighted: Array): Array
		{
			_targets = new Array;
			var ballTargets: Vector.<Ball> = new Vector.<Ball>;
			switch (_rangeType)
			{
				case RANGE_SELECTED_STONE_ONLY:
					_targets = _targets.concat(highlighted);
					break
					
				case RANGE_SELECTED_STONE_WITH_NEIGHBOURS:					
					for (var i: int = 0; i < highlighted.length; i++)
					{
						ballTargets = ballTargets.concat(MyWorld.gWorld.m_logic.GetBallNeighbours(Ball(highlighted[i])));
					}
					for (i = 0; i < ballTargets.length; i++)
						_targets.push(ballTargets[i]);
					break;
					
				case RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS:
					for (i = 0; i < highlighted.length; i++)
					{
						ballTargets = ballTargets.concat(MyWorld.gWorld.m_logic.GetCrossNeighbours(Ball(highlighted[i])));
						ballTargets.push(Ball(highlighted[i]));
					}
					for (i = 0; i < ballTargets.length; i++)
						_targets.push(ballTargets[i]);
					break;
				
				case RANGE_YOUR_HERO:
					_targets.push(GameEntity(MyWorld.gWorld.m_logic.GetSelf()));
					break;
				
				case RANGE_ENEMY_HERO:
					_targets.push(GameEntity(MyWorld.gWorld.m_logic.GetEnemy()));
					break;
			}
			return _targets;
		}
		
	}

}