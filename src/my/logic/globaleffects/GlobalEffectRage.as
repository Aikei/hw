package my.logic.globaleffects 
{
	import my.MyWorld;
	import my.Ball;
	/**
	 * ...
	 * @author Aikei
	 */
	public class GlobalEffectRage extends GlobalEffect
	{
		private var _ballType: int;
		private var _mult1: Number;
		private var _mult2: Number;
		private var _manaMult: Number;
		
		public function get ballType(): int
		{
			return _ballType;
		}
		
		public function GlobalEffectRage(ownerNumber: int, ballType: int, mult1: Number, mult2: Number, manaMult: Number) 
		{
			super(GlobalEffect.RAGE, ownerNumber);
			_mult1 = mult1;
			_mult2 = mult2;
			_ballType = ballType;
			_manaMult = manaMult;
			_time = -1;
		}
		
		override public function OnAdded(): void
		{
			for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			{
				MyWorld.gWorld.m_logic.m_ballDamageMultiplier[_ownerNumber][i] += _mult1;
				MyWorld.gWorld.m_logic.m_ballManaMultiplier[_ownerNumber][i] *= _manaMult;
			}			
			
			//if (_ballType === -1)
			//{
				//for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
				//{
					//MyWorld.gWorld.m_logic.m_ballDamageMultiplier[_ownerNumber][i] = _mult2;
					//MyWorld.gWorld.m_logic.m_ballManaMultiplier[_ownerNumber][i] = 0;
				//}
			//}
			//else
			//{
				//MyWorld.gWorld.m_logic.m_ballDamageMultiplier[_ownerNumber][_ballType] = _mult1;
				//MyWorld.gWorld.m_logic.m_ballManaMultiplier[_ownerNumber][_ballType] = 0;
			//}
		}
		
		override public function OnRemoved(): void
		{
			for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			{
				MyWorld.gWorld.m_logic.m_ballDamageMultiplier[_ownerNumber][i] = 1;
				MyWorld.gWorld.m_logic.m_ballManaMultiplier[_ownerNumber][i] = 1;
			}
				
			//if (_ballType === -1)
			//{
				//for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
				//{
					//MyWorld.gWorld.m_logic.m_ballDamageMultiplier[_ownerNumber][i] = 1;
					//MyWorld.gWorld.m_logic.m_ballManaMultiplier[_ownerNumber][i] = 1;
				//}
			//}
			//else
			//{
				//MyWorld.gWorld.m_logic.m_ballDamageMultiplier[_ownerNumber][_ballType] = 1;
				//MyWorld.gWorld.m_logic.m_ballManaMultiplier[_ownerNumber][_ballType] = 1;
			//}
		}		
		
	}

}