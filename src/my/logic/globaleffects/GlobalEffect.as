package my.logic.globaleffects {
	import my.MyWorld;
	/**
	 * ...
	 * @author Aikei
	 */
	public class GlobalEffect 
	{
		public static const FREEZING_RAIN: String = "FreezingRain";
		public static const RAGE: String = "Rage";
		
		protected var _type: String;
		protected var _time: int = 0; //-1: infinite duration
		protected var _tickInterval: int = 1;
		protected var _tickIntervalCounter: int = 0;		
		protected var _ownerNumber: int = -1; //-1: no one
		
		public function get type(): String
		{
			return _type;
		}
		
		public function GlobalEffect(type: String, ownerNumber = -1) 
		{
			_type = type;
			_ownerNumber = ownerNumber;
		}
		
		public function OnAdded(): void
		{
			
		}
		
		public function OnRemoved(): void
		{
			
		}
		
		protected function Tick()
		{
			_tickIntervalCounter++;
			if (_tickIntervalCounter === _tickInterval)
			{
				_tickIntervalCounter = 0;				
				if (_time > -1)
				{
					_time--;
					OnTick();
					if (_time === 0)
					{
						Remove();
						return true;
					}
				}				
				else
				{
					OnTick();
				}
			}
			return false;			
		}
		
		public function TickBeforeMoving(): Boolean 
		{
			return Tick();
		}		
		
		public function TickAfterMoving(): Boolean 
		{
			return false;
		}
		
		public function Remove(): void
		{
			MyWorld.gWorld.m_logic.RemoveGlobalEffect(_type);
		}		
		
		protected function OnTick(): void
		{
			
		}
				
		
	}

}