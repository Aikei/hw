package my.logic.globaleffects 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class GlobalEffectFreezingRain extends GlobalEffect
	{
		
		public function GlobalEffectFreezingRain(time: int = 1) 
		{
			super(GlobalEffect.FREEZING_RAIN);
			_time = time;
		}
		
		override public function TickBeforeMoving(): Boolean 
		{
			return false;
		}		
		
		override public function TickAfterMoving(): Boolean 
		{
			return Tick();
		}
			
	}

}