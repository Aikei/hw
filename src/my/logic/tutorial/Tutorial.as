package my.logic.tutorial 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyWorld;
	import my.system.Language;
	import starling.events.Event;
	
	public class Tutorial 
	{
		private var _phases: Vector.<TutorialPhase> = new Vector.<TutorialPhase>;
		private var _currentPhase: int = 0;
		
		public function Tutorial() 
		{
			//phase1
			var highlightFunction: Function = function() { }
			var text: String = Language.GetString("Tutorial1");
			var phase: TutorialPhase = new TutorialPhase(text, highlightFunction);
			phase.SetNextPhaseEvent("anyTextWindowOkPressed");
			_phases.push(phase);
			
			highlightFunction = function() 
			{ 
				MyWorld.gWorld.m_gameScreen.ChangeAlphaAll(0.3); 
				MyWorld.gWorld.m_balls[0][0].alpha = 1;  
			}
			text = Language.GetString("Tutorial2");
			phase = new TutorialPhase(text, highlightFunction);
			phase.SetNextPhaseEvent("anyTextWindowOkPressed");
			phase.SetNoOkButton(true);
			_phases.push(phase);
			
			highlightFunction = function() { }
			text = Language.GetString("Tutorial1");
			phase = new TutorialPhase(text, highlightFunction);
			phase.SetNextPhaseEvent("anyTextWindowOkPressed");
			_phases.push(phase);			
		}
		
		public function Start(): void
		{
			MyWorld.gWorld.addEventListener("nextTutorialPhase", OnNextTutorialPhase);
			MyWorld.gWorld.addEventListener("previousTutorialPhase", OnPreviousTutorialPhase);
			_currentPhase = 0;
			LaunchCurrentPhase();
		}
		
		public function End(): void
		{
			MyWorld.gWorld.removeEventListener("nextTutorialPhase", OnNextTutorialPhase);
			MyWorld.gWorld.removeEventListener("previousTutorialPhase", OnPreviousTutorialPhase);			
		}
		
		public function OnNextTutorialPhase(event: Event): void 
		{
			LaunchNextPhase();
		}
		
		public function OnPreviousTutorialPhase(event: Event): void 
		{
			LaunchPreviousPhase();
		}		
		
		public function LaunchCurrentPhase(): void
		{
			if (_currentPhase >= _phases.length)
			{
				End();
				return;
			}
			_phases[_currentPhase].Launch();
		}
		
		public function LaunchNextPhase():void 
		{
			_phases[_currentPhase].Stop();
			_currentPhase++;
			LaunchCurrentPhase();
		}
		
		public function LaunchPreviousPhase():void 
		{
			_phases[_currentPhase].Stop();
			_currentPhase--;
			LaunchCurrentPhase();
		}
	}

}