package my.logic.tutorial 
{
	
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyWorld;
	import my.view.elements.AnyTextWindow;
	import starling.events.Event;
	
	public class TutorialPhase 
	{
		private var _text: String;
		private var _highlightFunction: Function;
		private var _nextPhaseEventCheckFunction: Function = null;
		private var _previousPhaseEventCheckFunction: Function = null;		
		private var _nextPhaseEvent: String = null;
		private var _previousPhaseEvent: String = null;
		private var _noOk: Boolean = false;
		
		public function TutorialPhase(text: String, highlightFunction: Function) 
		{
			_text = text;
			_highlightFunction = highlightFunction;
			//_okToContinue = okToContinue;
		}
		
		public function SetNoOkButton(noOk: Boolean):void 
		{
			_noOk = noOk;
		}
		
		public function SetNextPhaseEvent(eventText: String): void
		{
			_nextPhaseEvent = eventText;
		}
		
		public function SetPreviousPhaseEvent(eventText: String): void 
		{
			_previousPhaseEvent = eventText;
		}
		
		public function SetNextPhaseEventCheckFunction(func: Function): void 
		{
			_nextPhaseEventCheckFunction = func;
		}
		
		public function SetPreviousPhaseEventCheckFunction(func: Function): void 
		{
			_previousPhaseEventCheckFunction = func;
		}		
		
		public function SetText(text: String): void
		{
			_text = text;
		}
		
		public function SetHighlightFunction(func: Function): void
		{
			_highlightFunction = func;
		}
		
		public function Launch():void 
		{
			ShowText();
			Highlight();
			if (_nextPhaseEvent)
				MyWorld.gWorld.addEventListener(_nextPhaseEvent, OnNextPhaseEvent);
			if (_previousPhaseEvent)
				MyWorld.gWorld.addEventListener(_previousPhaseEvent, OnPreviousPhaseEvent);
		}
		
		public function Stop():void 
		{
			MyWorld.gWorld.m_gameScreen.ChangeAlphaAll(1);
			if (_nextPhaseEvent)
				MyWorld.gWorld.removeEventListener(_nextPhaseEvent, OnNextPhaseEvent);
			if (_previousPhaseEvent)
				MyWorld.gWorld.removeEventListener(_previousPhaseEvent, OnPreviousPhaseEvent);			
		}
		
		public function OnNextPhaseEvent(event: Event):void 
		{
			if (_nextPhaseEventCheckFunction === null || _nextPhaseEventCheckFunction(event))
			{
				Stop();
				MyWorld.gWorld.dispatchEvent(new Event("nextTutorialPhase"));
			}
		}
		
		public function OnPreviousPhaseEvent(event: Event):void 
		{
			if (_previousPhaseEventCheckFunction === null || _previousPhaseEventCheckFunction(event))
			{			
				Stop();
				MyWorld.gWorld.dispatchEvent(new Event("previousTutorialPhase"));
			}
		}		
		
		public function ShowText(): void
		{
			MyWorld.gWorld.m_gameScreen.ShowAnyText(_text,_noOk);
		}
		
		public function Highlight(): void
		{
			_highlightFunction();
		}
		
	}

}