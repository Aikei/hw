package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.events.Event;
	import flash.utils.*;	
	import my.*;
	import my.system.Language;	
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;
	
	public class AbilityEarthquake extends BaseAbilityDestroyOrRemoveBalls 
	{
				
		public function AbilityEarthquake(ownerNumber: int) 
		{
			super(Ability.EARTHQUAKE, ownerNumber, false);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.RED_BALL] = 6;
			_color = Ball.RED_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("earthquake");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("earthquake_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityEarthquakeHelp");
		}		
		
		override protected function DisplayVisualEffect(): void 
		{
			MyWorld.gWorld.m_gameScreen.ShakeField(0.25,2);			
			setTimeout(OnEffectEnded, 500);
		}				
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<Ball> = new Vector.<Ball>;
			var balls: Vector.<Vector.<Ball>> = MyWorld.gWorld.m_balls;
			for (var i: int = 0; i < balls.length; i++)
			{
				v = v.concat(balls[i]);
			}
			return Vector.<GameEntity>(v);
		}			
				
	}	

}