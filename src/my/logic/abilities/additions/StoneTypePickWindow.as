package my.logic.abilities.additions 
{
	import my.MyAssets;
	import my.MyEntity;
	import starling.display.Image;
	import starling.events.Event;
	import my.Ball;
	import my.MyWorld;
	/**
	 * ...
	 * @author Aikei
	 */
	import feathers.controls.Button;
	
	public class StoneTypePickWindow extends MyEntity 
	{
		//var _buttonBlue: Button = new Button;
		//var _buttonRed: Button = new Button;
		//var _buttonYellow: Button = new Button;
		//var _buttonGreen: Button = new Button;
		private var _buttons: Vector.<StonePickButton> = new Vector.<StonePickButton>;
		
		private const NUMBER_OF_ROWS: int = 2;
		private const NUMBER_OF_COLUMNS: int = 3;
		
		private const NUMBER_OF_BUTTONS = 6;
		
		private const BUTTON_WIDTH = Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X;
		private const BUTTON_HEIGHT = Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_Y;
		private const OVERALL_BUTTONS_WIDTH: int = BUTTON_WIDTH * 3;
		private const OVERALL_BUTTONS_HEIGHT: int = BUTTON_HEIGHT * 2;
		private const OVERALL_WIDTH: int = BUTTON_WIDTH * 3.2;
		private const OVERALL_HEIGHT: int = BUTTON_HEIGHT * 2.2;
		private const BUTTONS_START_X: int = (OVERALL_WIDTH - OVERALL_BUTTONS_WIDTH) / 2;
		private const BUTTONS_START_Y: int = (OVERALL_HEIGHT - OVERALL_BUTTONS_HEIGHT) / 2;
		
		private var _lastStartX: int = BUTTONS_START_X;
		private var _lastStartY: int = BUTTONS_START_Y;
		
		public function StoneTypePickWindow(x:Number=0, y:Number=0) 
		{
			super(x, y);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		override public function get width(): Number
		{
			return OVERALL_WIDTH;
		}
		
		override public function get height(): Number
		{
			return OVERALL_HEIGHT;
		}		
		
		protected function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			for (var i = 0; i < Ball.NUMBER_OF_TYPES; i++)
			{
				var button: StonePickButton = new StonePickButton(i);
				button.defaultSkin = new Image(MyAssets.GetTextureByBallType(i, false));
				button.defaultSkin.scaleX = 0.5;
				button.defaultSkin.scaleY = 0.5;
				button.x = _lastStartX;
				button.y = _lastStartY;
				_lastStartX += BUTTON_WIDTH;
				if (_lastStartX+BUTTON_WIDTH > OVERALL_WIDTH)
				{
					_lastStartX = BUTTONS_START_X;
					_lastStartY += BUTTON_HEIGHT;
				}
				addChild(button);
				MyWorld.gWorld.addEventListener("stoneTypePickedInPopup", OnStoneTypePicked);
			}						
		}
		
		protected function OnStoneTypePicked(event: Event): void
		{
			removeEventListener("stoneTypePickedInPopup", OnStoneTypePicked);
			Destroy();
		}		
		
		//protected function OnBlueButtonPressed(event: Event): void
		//{
			//_buttonBlue.removeEventListener(Event.TRIGGERED, OnBlueButtonPressed);
			//MyWorld.gWorld.dispatchEvent(new Event("stoneTypePickedInPopup", false, Ball.BLUE_BALL));
			//Destroy();
		//}
		//
		//protected function OnGreenButtonPressed(event: Event): void
		//{
			//_buttonGreen.removeEventListener(Event.TRIGGERED, OnGreenButtonPressed);
			//MyWorld.gWorld.dispatchEvent(new Event("stoneTypePickedInPopup", false, Ball.GREEN_BALL));
			//Destroy();
		//}
		//
		//protected function OnRedButtonPressed(event: Event): void
		//{
			//_buttonRed.removeEventListener(Event.TRIGGERED, OnRedButtonPressed);
			//MyWorld.gWorld.dispatchEvent(new Event("stoneTypePickedInPopup", false, Ball.RED_BALL));
			//Destroy();
		//}
		//
		//protected function OnYellowButtonPressed(event: Event): void
		//{
			//_buttonYellow.removeEventListener(Event.TRIGGERED, OnYellowButtonPressed);
			//MyWorld.gWorld.dispatchEvent(new Event("stoneTypePickedInPopup", false, Ball.YELLOW_BALL));
			//Destroy();
		//}		
		
	}

}