package my.logic.abilities.additions 
{
	import feathers.controls.Button;
	import starling.events.Event;
	import my.MyWorld;
	/**
	 * ...
	 * @author Aikei
	 */
	public class StonePickButton extends Button
	{
		public var stoneType: int = 0;
		
		public function StonePickButton(stoneType: int) 
		{
			this.stoneType = stoneType;
			addEventListener(Event.TRIGGERED, OnButtonPressed);
		}
		
		public function OnButtonPressed(): void
		{
			removeEventListener(Event.TRIGGERED, OnButtonPressed);
			MyWorld.gWorld.dispatchEvent(new Event("stoneTypePickedInPopup", false, stoneType));		
		}
		
	}

}