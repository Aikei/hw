package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.events.Event;
	import flash.utils.*;	
	import my.*;
	import my.system.Language;	
	import my.view.Visual;
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;
	
	public class AbilityMeteoriteRain extends BaseAbilityDestroyOrRemoveBalls 
	{
		private var _number: int = 10;
		
		public function AbilityMeteoriteRain(ownerNumber: int) 
		{
			super(Ability.METEORITE_RAIN, ownerNumber);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.RED_BALL] = 8;
			_color = Ball.RED_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("fireButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("fireButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityMeteoriteRainHelp");
			_help = _help.replace("%number", _number);
		}		
		
		override protected function DisplayVisualEffect(): void
		{
			for (var i: int = 0, l: int = _allTargets.length; i < l; i++)
			{
				var visual: Visual = MyWorld.gWorld.m_gameScreen.CreateVisual(MyAssets.StonesAtlas.getTextures("fireball"), _allTargets[i].x, _allTargets[i].y);
				visual.alignPivot();
				visual.SetSelfDestruct(1000);
				Tweens.TweenFromToHere(visual, 1, visual.x-200, visual.y-200);		
			}
			setTimeout(OnEffectEnded, 1000);
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<GameEntity> = Vector.<GameEntity>(MyWorld.gWorld.m_logic.GetRandomBalls(_number));
			return v;
		}			
		
	}	

}