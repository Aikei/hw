package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.*;
	import starling.events.Event;
	import flash.utils.*;
	import my.system.Language;	
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;
	
	public class AbilityFire extends BaseAbilityDestroyOrRemoveBalls 
	{
		
		public function AbilityFire(ownerNumber: int) 
		{
			super(Ability.FIRE,ownerNumber);
			_targeting = Ability.TARGET_PICK_STONE;
			_manaCost[Ball.RED_BALL] = 6;
			_color = Ball.RED_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("fireButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("fireButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityFireHelp");
		}				
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetCrossNeighbours(Ball(entity));
			v.push(Ball(entity));
			return Vector.<GameEntity>(v);
		}				
		
	}	
	
}