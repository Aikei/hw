package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.logic.abilities.additions.StoneTypePickWindow;
	import my.GameEntity;
	import feathers.controls.Callout;
	import my.MyWorld;
	import starling.events.Event;
	import my.Ball;
	import my.MyAssets;
	import my.system.Language;
	import my.logic.Logic;
	
	public class AbilityTurn extends Ability 
	{
		var _window: StoneTypePickWindow;
		var _callout: Callout;
		
		public function AbilityTurn(ownerNumber: int) 
		{
			super(Ability.TURN, ownerNumber);
			_targeting = TARGET_PICK_STONE_THEN_ARGUMENTS;
			_manaCost[Ball.PURPLE_BALL] = 4;
			_color = Ball.PURPLE_BALL;
			//_chargedByType = Ball.PURPLE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("turnButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("turnSelector");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilityTurnHelp");
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			if (!(targets[0] is Ball))
				return;
			_targets = targets;
			_allTargets = targets;
			for (var i = 0; i < _targets.length; i++)
			{
				Ball(_targets[i]).TweenToType(args[0]);
				//Ball(_targets[i]).SetType(args[0]);
			}
			super.Use(targets, args);
			MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
			//MyWorld.gWorld.dispatchEvent(new Event("abilityAnimationEnded"));					
		}
		
		override public function PickArguments(targets: Vector.<GameEntity>): void
		{
			super.PickArguments(targets);
			_window = new StoneTypePickWindow(targets[0].x, targets[0].y);
			_callout = Callout.show(_window, targets[0]);				
			MyWorld.gWorld.addEventListener("stoneTypePickedInPopup", OnStoneTypePicked);
		}		
		
		protected function OnStoneTypePicked(event: Event): void
		{
			MyWorld.gWorld.removeEventListener("stoneTypePickedInPopup", OnStoneTypePicked);
			_callout.close();
			var args: Array = new Array;
			args.push(int(event.data));
			this.Use(_targets, args);
		}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<Ball> = new Vector.<Ball>;
			v.push(entity);
			return Vector.<GameEntity>(v);
		}			
		
	}

}