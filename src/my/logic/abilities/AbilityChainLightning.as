package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.*;
	import starling.events.Event;
	import flash.utils.*;
	import my.system.Language;	
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;
	
	public class AbilityChainLightning extends BaseAbilityDestroyOrRemoveBalls 
	{
		var _number: uint = 3;
		
		public function AbilityChainLightning(ownerNumber: int) 
		{
			super(Ability.CHAIN_LIGHTNING,ownerNumber);
			_targeting = Ability.TARGET_PICK_STONE;
			_manaCost[Ball.BLUE_BALL] = 4;
			_color = Ball.BLUE_BALL;
			//_chargedByType = Ball.RED_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("chain_lightning");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("chain_lightning_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilityChainLightningHelp");
			_help = _help.replace("%number", String(_number));
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<GameEntity> = new Vector.<GameEntity>;
			v.push(entity);
			v = Vector.<GameEntity>(MyWorld.gWorld.m_logic.GetRandomBalls(_number, Vector.<Ball>(v)));
			return v;
		}
		
		override public function GetShowTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<GameEntity> = new Vector.<GameEntity>;
			v.push(entity);
			return v;
		}				
		
	}	
	
}