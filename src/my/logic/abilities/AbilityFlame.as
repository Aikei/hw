package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.GameEntity;
	import my.logic.PlayerData;
	import my.MyAssets;
	
	public class AbilityFlame extends Ability 
	{
		//private var _damage: int = 6;
		public function AbilityFlame(ownerNumber: int) 
		{
			super(Ability.FLAME,ownerNumber);
			_targeting = Ability.TARGET_ENEMY_HERO;
			_manaCost[Ball.RED_BALL] = 8;
			_color = Ball.RED_BALL;
			//_chargedByType = Ball.RED_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("fireButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("fireButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");								
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			for (var i = 0; i < targets.length; i++)
			{
				if (!(targets[i] is PlayerData))
					continue;
				//targets[i].DamageEntity(_damage);
				//var text: DisappearingText = new DisappearingText("-"+String(_damage), 80, Color.RED, 0, 0, true, 4);
				//targets[i].addChild(text);
			}
			_allTargets = targets;
			super.Use(targets, args);
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<GameEntity>;
			v.push(entity);
			return v;
		}				
		
	}

}