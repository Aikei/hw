package my.logic.abilities.base 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import flash.utils.*;	
	import my.logic.abilities.Ability;
	import starling.events.Event;	
	import my.*;
	import my.logic.globaleffects.GlobalEffect;
	import my.logic.Logic;
	
	 
	public class BaseAbilityApplyGlobalEffect extends Ability
	{		
		protected var _effectClass: Class;
		protected var _effectArguments: Array = new Array;
		
		public function BaseAbilityApplyGlobalEffect(type:String, effectType: String, ownerNumber:int) 
		{
			super(type, ownerNumber);
			_targeting = TARGET_AUTOMATIC;
			_effectClass = getDefinitionByName("my.logic.globaleffects.GlobalEffect" + effectType) as Class;
	
		}
		
		protected function SetEffectArguments(... args): void
		{
			_effectArguments = args;
		}
		
		protected function CreateGlobalEffect(): GlobalEffect
		{
			switch(_effectArguments.length)
			{
				case 0:
					return new _effectClass() as GlobalEffect;
					break;
				case 1:
					return new _effectClass(_effectArguments[0]) as GlobalEffect;
					break;
				case 2:
					return new _effectClass(_effectArguments[0],_effectArguments[1]) as GlobalEffect;
					break;
				case 3:
					return new _effectClass(_effectArguments[0],_effectArguments[1],_effectArguments[2]) as GlobalEffect;
					break;
				case 4:
					return new _effectClass(_effectArguments[0],_effectArguments[1],_effectArguments[2],_effectArguments[3]) as GlobalEffect;
					break;
				case 5:
					return new _effectClass(_effectArguments[0],_effectArguments[1],_effectArguments[2],_effectArguments[3],_effectArguments[4]) as GlobalEffect;
					break;
				case 6:
					return new _effectClass(_effectArguments[0],_effectArguments[1],_effectArguments[2],_effectArguments[3],_effectArguments[4],_effectArguments[5]) as GlobalEffect;
					break;					
			}
			return null;
		}		
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			targets = null;
			_allTargets = null;
			//MyWorld.gWorld.addEventListener("effectAnimationEnded", OnEffectEnded);
			MyWorld.gWorld.m_logic.AddGlobalEffect(CreateGlobalEffect());
			super.Use(targets);
		}
		
		//public function OnEffectEnded(event: Event): void
		//{
			//MyWorld.gWorld.removeEventListener("effectAnimationEnded", OnEffectEnded);
			//MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
		//}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<Ball> = new Vector.<Ball>;			
			v.push(entity);
			return Vector.<GameEntity>(v);
		}		
		
	}

}