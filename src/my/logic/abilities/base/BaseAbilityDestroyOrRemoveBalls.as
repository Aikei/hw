package my.logic.abilities.base 
{	
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.logic.abilities.Ability;	
	import my.*;
	import starling.events.Event;
	
	public class BaseAbilityDestroyOrRemoveBalls extends Ability 
	{
		protected var _destroyBalls: Boolean = true; //if balls are not destroyed, they are removed, granting no bonuses.
		
		public function BaseAbilityDestroyOrRemoveBalls(type:String, ownerNumber:int, destroyBalls: Boolean = true) 
		{
			super(type, ownerNumber);
			_destroyBalls = destroyBalls;
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			_allTargets = new Vector.<GameEntity>;
			if (targets && targets.length > 0)
			{
				for (var i: int = 0, l: int = targets.length; i < l; i++)
				{
					if (!(targets[i] is Ball))
						return;				
					_allTargets = _allTargets.concat(GetTargets(targets[i]));			
				}
			}
			else
			{
				_allTargets = GetTargets(null);
			}
			super.Use(targets);
		}
		
		override protected function OnEffectEnded(): void
		{
			var eventString: String = "removeBallsWithAbility";
			if (!_destroyBalls)
				eventString = "deleteBallsWithAbility";
			MyWorld.gWorld.dispatchEvent(new Event(eventString,false,_allTargets));
		}		
	}

}