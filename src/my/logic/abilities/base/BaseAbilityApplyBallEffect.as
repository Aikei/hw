package my.logic.abilities.base 
{
	import my.logic.abilities.Ability;
	
	/**
	 * ...
	 * @author Aikei
	 */
	import my.*;
	import flash.utils.*;
	import my.logic.effects.Effect;
	import my.logic.Logic;
	import starling.events.Event;
	
	public class BaseAbilityApplyBallEffect extends Ability 
	{
		protected var _effectClass: Class;
		protected var _effectArguments: Array = new Array;
		protected var _effectOwning: Boolean = true;
		
		public function BaseAbilityApplyBallEffect(type:String, effectType: String, ownerNumber:int) 
		{
			super(type, ownerNumber);
			_targeting = TARGET_PICK_STONE;
			_effectClass = getDefinitionByName("my.logic.effects.Effect" + effectType) as Class;
	
		}
		
		protected function SetEffectArguments(... args): void
		{
			_effectArguments = args;
		}
		
		protected function CreateEffect(): Effect
		{
			var thisEffectArguments: Array = _effectArguments.concat();
			if (_effectOwning)
			{
				var effectOwner: int = Effect.OWNER_YOU;
				if (!MyWorld.gWorld.m_logic.yourTurn)
					effectOwner = Effect.OWNER_ENEMY;				
				thisEffectArguments.push(effectOwner);
			}
			switch(thisEffectArguments.length)
			{
				case 0:
					return new _effectClass() as Effect;
					break;
				case 1:
					return new _effectClass(thisEffectArguments[0]) as Effect;
					break;
				case 2:
					return new _effectClass(thisEffectArguments[0],thisEffectArguments[1]) as Effect;
					break;
				case 3:
					return new _effectClass(thisEffectArguments[0],thisEffectArguments[1],thisEffectArguments[2]) as Effect;
					break;
				case 4:
					return new _effectClass(thisEffectArguments[0],thisEffectArguments[1],thisEffectArguments[2],thisEffectArguments[3]) as Effect;
					break;
				case 5:
					return new _effectClass(thisEffectArguments[0],thisEffectArguments[1],thisEffectArguments[2],thisEffectArguments[3],thisEffectArguments[4]) as Effect;
					break;
				case 6:
					return new _effectClass(thisEffectArguments[0],thisEffectArguments[1],thisEffectArguments[2],thisEffectArguments[3],thisEffectArguments[4],thisEffectArguments[5]) as Effect;
					break;					
			}
			return null;
		}
		
		override public function CheckTargets(targets: Vector.<GameEntity>): Boolean
		{
			if (!_effectOwning)
				return true;
			for (var i: int = 0; i < targets.length; i++)
			{
				if (!targets[i] is Ball)
					return false;
				if (Ball(targets[i]).HasOwningEffect())
					return false;
			}
			return true;
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			_allTargets = new Vector.<GameEntity>;

			for (var i: int = 0, l: int = targets.length; i < l; i++)
			{
				_allTargets = _allTargets.concat(GetTargets(targets[i]));			
			}
			
			for (var i2: int = 0, l2: int = _allTargets.length; i2 < l2; i2++)
			{
				_allTargets[i2].AddEffect(CreateEffect());					
			}			
			
			super.Use(targets);
		}
		
		//public function OnEffectEnded(event: Event): void
		//{
			//MyWorld.gWorld.removeEventListener("effectAnimationEnded", OnEffectEnded);
			//MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
		//}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<GameEntity> = new Vector.<GameEntity>;			
			v.push(entity);
			return v;
		}				
		
	}

}