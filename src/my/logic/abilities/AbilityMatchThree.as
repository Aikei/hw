package my.logic.abilities 
{

	/**
	 * ...
	 * @author Aikei
	 */
	import starling.events.Event;
	import flash.utils.*;	
	import my.*;
	import my.system.Language;	
	import my.view.Visual;
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;	
	 
	public class AbilityMatchThree extends BaseAbilityDestroyOrRemoveBalls 
	{
		
		public function AbilityMatchThree(ownerNumber:int) 
		{
			super(Ability.MATCH_THREE, ownerNumber, true);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.GREEN_BALL] = 4;
			_color = Ball.GREEN_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("fireButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("fireButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityMatchThreeHelp");	
		}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var result: Vector.<Vector.<Ball>> = new Vector.<Vector.<Ball>>;
			var tempBallsX: Vector.<Vector.<Ball>> = new Vector.<Vector.<Ball>>;
			var tempBallsY: Vector.<Vector.<Ball>> = new Vector.<Vector.<Ball>>;
			for (var i: int = 0; i < MyWorld.gWorld.m_balls.length; i++)
			{
				tempBallsX.push(MyWorld.gWorld.m_balls[i].concat());
				tempBallsY.push(MyWorld.gWorld.m_balls[i].concat());
			}
			var balls: Vector.<Vector.<Ball>> = MyWorld.gWorld.m_balls;
			
			for (i = 0; i < balls.length; i++)
			{
				for (var j: int = 0; j < balls[i].length; j++)
				{
					for (var axis: int = 0; axis < 2; axis++) //axis: 0 = x, 1 = y
					{
						var nFound: int = 1; //number of found matching balls of the same color
						var checkx: int = i;
						var checky: int = j;
						for (var k: int = 1; k < 6; k++)
						{
							if (axis === 0)
								checkx++;
							else
								checky++;
							if (checkx < balls.length && checky < balls[0].length && balls[i][j].m_type === balls[checkx][checky].m_type
								&& ((axis === 0 && tempBallsX[i][j] !== null) ||  (axis === 1 && tempBallsY[i][j] !== null)) )
							{
								nFound++;
							}
							else if (nFound < 3)
							{
								break;
							}
							else
							{
								var v: Vector.<Ball> = new Vector.<Ball>;
								for (var a: int = 0; a < nFound; a++)
								{
									if (axis === 0)
									{
										tempBallsX[i + a][j] = null;
										v.push(balls[i + a][j]);
									}
									else
									{
										tempBallsY[i][j + a] = null;
										v.push(balls[i][j + a]);
									}
								}
								result.push(v);
								break;
							}
						}
					}
				}
			}
			var fullResult: Vector.<Ball> = new Vector.<Ball>;
			for (i = 0; i < result.length; i++)
			{
				fullResult = fullResult.concat(result[i]);
			}
			fullResult = MyWorld.gWorld.m_logic.RemoveSameBalls(fullResult);
			return Vector.<GameEntity>(fullResult);
		}
		
	}

}