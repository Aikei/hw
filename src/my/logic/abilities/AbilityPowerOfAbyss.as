package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.logic.Logic;
	import my.MyWorld;
	import my.GameEntity;
	import my.MyAssets;
	import my.system.Language;
	
	public class AbilityPowerOfAbyss extends Ability
	{
		private var _selfDamage: int = 2;
		private var _additionalMana: int = 2;
		
		public function AbilityPowerOfAbyss(ownerNumber: int) 
		{
			super(Ability.POWER_OF_ABYSS, ownerNumber);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.PURPLE_BALL] = 0;
			_color = Ball.PURPLE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("madness");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("madness_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");		
			_help = Language.GetString("AbilityPowerOfAbyssHelp");
			_help = _help.replace("%yourDamage", String(_selfDamage));
			_help = _help.replace("%additionalMana", String(_additionalMana));
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			targets = null;
			_allTargets = null;
			//MyWorld.gWorld.m_logic.AddGlobalEffect(new GlobalEffectFreezingRain);
			MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
			super.Use(targets, args);
		}		
		
		//override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		//{
			//var v: Vector.<GameEntity>;
			//v.push(entity);
			//return v;
		//}			
		
	}

}