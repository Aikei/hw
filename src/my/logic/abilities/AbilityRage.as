package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.GameEntity;
	import my.Ball;
	import my.logic.globaleffects.GlobalEffectRage;
	import starling.utils.Color;
	import my.view.DisappearingText;
	import my.MyAssets;
	import my.logic.PlayerData;
	import my.system.Language;
	import my.MyWorld;
	import my.logic.Logic;
	
	public class AbilityRage extends Ability 
	{
		protected var _mult1: Number = 0.25;
		protected var _mult2: Number = 1.5;
		protected var _manaMult: Number = 0.3;
		protected var _ballType: int;
		
		
		public function AbilityRage(abilityType: String, ballType: int, ownerNumber: int) 
		{
			super(abilityType, ownerNumber);
			_subType = "rage2";
			_ballType = ballType;
			_passive = true;
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[ballType] = 0;
			_help = Language.GetString("AbilityRageHelp");
			_help = _help.replace(/%mult1/g, String(_mult1*100)+"%");
			_help = _help.replace(/%manaMult/g, String(_manaMult*100)+"%");
			//_buttonTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption");
			//_buttonPushedTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption_s");
			//_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");		
			//_help = Language.GetString("AbilityRedRageHelp");
			//_help = _help.replace(/%mult1/g, String(_mult1));
			//_help = _help.replace(/%mult2/g, String(_mult2));
		}

		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			if (args === undefined)
				MyWorld.gWorld.m_logic.AddGlobalEffect(new GlobalEffectRage(_ownerNumber, _ballType, _mult1, _mult2, _manaMult));
			else
				MyWorld.gWorld.m_logic.AddGlobalEffect(new GlobalEffectRage(_ownerNumber, -1, _mult1, _mult2, _manaMult));
		}			
		
	}

}