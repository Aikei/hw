package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.GameEntity;
	import my.Ball;
	import my.logic.effects.Effect;
	import my.MyWorld;
	import my.MyAssets;
	import starling.events.Event;	
	import my.system.Language;
	import my.logic.Logic;
	import my.logic.effects.Effect;
	import my.logic.abilities.base.BaseAbilityApplyBallEffect;
	
	public class AbilityGoblinMine extends BaseAbilityApplyBallEffect
	{
		
		private var _damage: int = 10;
		
		public function AbilityGoblinMine(ownerNumber: int) 
		{
			super(Ability.GOBLIN_MINE, Effect.HAS_GOBLIN_MINE, ownerNumber);
			_manaCost[Ball.RED_BALL] = 4;
			_color = Ball.RED_BALL;	
			_buttonTexture = MyAssets.UiAtlas.getTexture("goblin_bomb");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("goblin_bomb_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityGoblinMineHelp");
			_help = _help.replace(/%damage/g, String(_damage));
			SetEffectArguments(_damage);
		}
		
		//override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		//{
			//if (!(targets[i] is Ball))
				//return;
			//_allTargets = null;
			//MyWorld.gWorld.addEventListener("goblinMineEffectEnded", OnEffectEnded);
			//var owner = Effect.OWNER_YOU;
			//if (!MyWorld.gWorld.m_logic.yourTurn)
				//owner = Effect.OWNER_ENEMY;			
			//for (var i: int = 0, l: int = targets.length; i < l; i++)
			//{
				//_allTargets = GetTargets(targets[i]);
				//for (var i2: int = 0, l2: int = _allTargets.length; i2 < l2; i2++)
				//{
					//Ball(_allTargets[i2]).AddEffect(new EffectGoblinMine(_damage,owner));					
				//}				
			//}
			//super.Use(targets);
		//}
		//
		//public function OnEffectEnded(event: Event): void
		//{
			//MyWorld.gWorld.removeEventListener("goblinMineEffectEnded", OnEffectEnded);
			//MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
			////MyWorld.gWorld.dispatchEvent(new Event("abilityAnimationEnded"));
		//}
		//
		//override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		//{			
			//var v: Vector.<Ball> = new Vector.<Ball>;			
			//v.push(entity);
			//return Vector.<GameEntity>(v);
		//}				
		
	}

}