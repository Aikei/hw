package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.*;
	import my.logic.effects.Effect;
	import my.view.Visual;
	import my.logic.Logic;
	import my.system.Language;
	import flash.utils.*;
	
	public class AbilityInfection extends Ability 
	{
		var _number: int = 1;
		
		public function AbilityInfection(ownerNumber:int) 
		{
			super(Ability.INFECTION, ownerNumber);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.RED_BALL] = 3;
			_color = Ball.RED_BALL;
			//_chargedByType = Ball.RED_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("fireButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("fireButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityInfectionHelp");
			_help = _help.replace("%number", _number);						
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{				
			//targets = Vector.<GameEntity>(MyWorld.gWorld.m_logic.GetRandomBalls(_number));
			//_allTargets = targets;
			//for (var i: int = 0, l: int = targets.length; i < l; i++)
			//{
				//var visual: Visual = MyWorld.gWorld.m_gameScreen.CreateVisual(MyAssets.StonesAtlas.getTextures("fireball"), targets[i].x, targets[i].y);
				//visual.alignPivot();
				////var time: Number = Misc.Random(0.5, 1.0);
				//visual.SetSelfDestruct(1000);
				//Tweens.TweenFromToHere(visual, 1, visual.x-200, visual.y-200);		
			//}
			//setTimeout(OnEffectEnded, 1000);
			targets = new Vector.<GameEntity>;
			for (var i: int = 0; i < MyWorld.gWorld.m_balls.length; i++)
			{
				for (var j: int = 0; j < MyWorld.gWorld.m_balls[i].length; j++)
				{
					var ball: Ball = MyWorld.gWorld.m_balls[i][j];
					for (var e: int = 0; e < ball.effects.length; e++)
					{
						if ((MyWorld.gWorld.m_logic.yourTurn && ball.effects[e].owner === Effect.OWNER_YOU) || (!MyWorld.gWorld.m_logic.yourTurn && ball.effects[e].owner === Effect.OWNER_ENEMY))
							targets.push(ball as GameEntity);
					}
				}
			}
			_allTargets = new Vector.<GameEntity>;
			for (i = 0; i < targets.length; i++)
			{
				var neighbours: Vector.<Ball> = MyWorld.gWorld.m_logic.GetBallNeighbours(Ball(targets[i]), true);
				for (j = 0; j < neighbours.length; j++)
				{
					if (neighbours[j].HasOwningEffect())
					{
						neighbours.splice(j, 1);
						j--;
					}
				}
				if (neighbours.length > 0)
				{
					var n = 0;
					if (neighbours.length > 1)
					{
						n = MyWorld.gWorld.m_logic.GetDice() % neighbours.length;
					}
					_allTargets.push(neighbours[n] as GameEntity);
					for (e = 0; e < targets[i].effects.length; e++)
					{
						if ((MyWorld.gWorld.m_logic.yourTurn && targets[i].effects[e].owner === Effect.OWNER_YOU) || (!MyWorld.gWorld.m_logic.yourTurn && targets[i].effects[e].owner === Effect.OWNER_ENEMY))
						{
							neighbours[n].AddEffect(targets[i].effects[e].CreateIdenticalEffect());
						}
					}
				}
			}
			super.Use(targets);
			setTimeout(OnEffectEnded, 500);
		}
		
		public function OnEffectEnded(): void
		{
			MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
			//MyWorld.gWorld.dispatchEvent(new Event("removeBallsWithAbility",false,_allTargets));
		}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<GameEntity>;
			v.push(entity);
			return v;
		}					
	}

}