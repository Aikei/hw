package my.logic.abilities {
	
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.*;
	import starling.events.Event;
	import my.logic.effects.EffectFrozen;
	import my.system.Language;
	
	public class AbilityPowerDrain extends Ability 
	{
		private var _shortestChain: int = 5;
		private var _receiveMana: int = 4;
		
		public function AbilityPowerDrain(ownerNumber:int) 
		{
			super(Ability.POWER_DRAIN, ownerNumber);
			
			_targeting = TARGET_PICK_STONE_THEN_ARGUMENTS;
			_argumentsTargeting = TARGET_PICK_YOUR_ABILITY;
			
			_manaCost[Ball.GREEN_BALL] = 1;
			_color = Ball.GREEN_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("frost_shot");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("frost_shot_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilityPowerDrainHelp");
			_help = _help.replace(/%shortestChain/g, String(_shortestChain));
			_help = _help.replace(/%receiveMana/g, String(_receiveMana));		
		}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{						
			var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetChain(Ball(entity));			
			return Vector.<GameEntity>(v);
		}				
		
		override public function PickArguments(targets: Vector.<GameEntity>): void
		{
			super.PickArguments(targets);
			MyWorld.gWorld.addEventListener("yourAbilitySelectedAsTarget", OnAbilityPicked);
		}				
		
		protected function OnAbilityPicked(event: Event): void
		{
			MyWorld.gWorld.removeEventListener("yourAbilitySelectedAsTarget", OnAbilityPicked);
			var args: Array = new Array;
			args.push(Ability(event.data).color);
			Use(_targets, args);
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			_allTargets = _allTargets = new Vector.<GameEntity>;
			for (var i: int = 0; i < targets.length; i++)
			{
				_allTargets = _allTargets.concat(GetTargets(targets[i]));
			}
			for (i = 0; i < _allTargets.length; i++)
			{
				Ball(_allTargets[i]).AddEffect(new EffectFrozen(1, 0.5));					
			}					
			super.Use(targets, args);
		}
		
		override public function CheckTargets(targets: Vector.<GameEntity>): Boolean
		{
			var all: Vector.<GameEntity> = new Vector.<GameEntity>;
			for (var i: int = 0; i < targets.length; i++)
			{
				all = all.concat(GetTargets(targets[i]));
			}
			if (all.length >= _shortestChain)
				return true;
			else
				return false;
		}		
		
	}

}