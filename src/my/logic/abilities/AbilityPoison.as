package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.GameEntity;
	import my.Ball;
	import my.logic.effects.Effect;
	import my.logic.effects.EffectPoison;
	import my.MyWorld;
	import my.MyAssets;
	import starling.events.Event;	
	import my.system.Language;
	import my.logic.Logic;
	import my.logic.abilities.base.BaseAbilityApplyBallEffect;
	
	public class AbilityPoison extends BaseAbilityApplyBallEffect 
	{
		private var _damage: int = 2;
		
		public function AbilityPoison(ownerNumber: int) 
		{
			super(Ability.POISON, Effect.HAS_POISON, ownerNumber);			
			_manaCost[Ball.GREEN_BALL] = 3;
			_color = Ball.GREEN_BALL;		
			_buttonTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityPoisonHelp");
			_help = _help.replace(/%damage/g, String(_damage));
			SetEffectArguments(_damage);
		}		
		
		//public function AbilityPoison(ownerNumber: int) 
		//{
			//super(Ability.POISON, ownerNumber);
			//_targeting = 1;
			//_manaCost[Ball.GREEN_BALL] = 3;
			//_color = Ball.GREEN_BALL;
			////_chargedByType = Ball.GREEN_BALL;			
			//_buttonTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption");
			//_buttonPushedTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption_s");
			//_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			//_help = Language.GetString("AbilityPoisonHelp");
			//_help = _help.replace(/%damage/g, String(_damage));
		//}
		
		//override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		//{
			//if (!(targets[i] is Ball))
				//return;
			//_allTargets = null;
			//MyWorld.gWorld.addEventListener("poisonEffectEnded", OnEffectEnded);
			//var owner = Effect.OWNER_YOU;
			//if (!MyWorld.gWorld.m_logic.yourTurn)
				//owner = Effect.OWNER_ENEMY;
			//for (var i: int = 0, l: int = targets.length; i < l; i++)
			//{
				//_allTargets = GetTargets(targets[i]);
				//for (var i2: int = 0, l2: int = _allTargets.length; i2 < l2; i2++)
				//{
					//Ball(_allTargets[i2]).AddEffect(new EffectPoison(_damage,owner));					
				//}				
			//}
			//super.Use(targets);
		//}
		//
		//public function OnEffectEnded(event: Event): void
		//{
			//MyWorld.gWorld.removeEventListener("poisonEffectEnded", OnEffectEnded);
			////MyWorld.gWorld.dispatchEvent(new Event("abilityAnimationEnded"));
			//MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
		//}
		//
		//override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		//{			
			//var v: Vector.<Ball> = new Vector.<Ball>;			
			//v.push(entity);
			//return Vector.<GameEntity>(v);
		//}		
		
	}

}