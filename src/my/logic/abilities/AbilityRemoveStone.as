package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import starling.events.Event;
	import flash.utils.*;	
	import my.*;
	import my.system.Language;	
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;
	
	public class AbilityRemoveStone extends BaseAbilityDestroyOrRemoveBalls 
	{
				
		public function AbilityRemoveStone(ownerNumber: int) 
		{
			super(Ability.REMOVE_STONE, ownerNumber, false);
			_targeting = Ability.TARGET_PICK_STONE;
			_manaCost[Ball.RED_BALL] = 2;
			_color = Ball.RED_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("earthquake");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("earthquake_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityRemoveStoneHelp");
		}		
				
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			return GetThisTargetOnly(entity);
		}			
				
	}	

}