package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */	
	import my.GameEntity;
	import my.Ball;
	import my.logic.effects.Effect;
	import my.MyWorld;
	import my.MyAssets;
	import starling.events.Event;
	import my.system.Language;
	import my.logic.Logic;
	
	import my.logic.abilities.base.BaseAbilityApplyBallEffect;

	public class AbilityFreeze extends BaseAbilityApplyBallEffect 
	{
		
		public function AbilityFreeze(ownerNumber: int) 
		{
			super(Ability.FREEZE,Effect.FROZEN,ownerNumber);
			_manaCost[Ball.PURPLE_BALL] = 3;
			_color = Ball.PURPLE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("frost_shot");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("frost_shot_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilityFreezeHelp");
			_effectOwning = false;
			//SetEffectArguments();
		}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetBallNeighbours(Ball(entity));
			
			//var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetChain(Ball(entity));
			
			return Vector.<GameEntity>(v);
		}						
		
	}

}