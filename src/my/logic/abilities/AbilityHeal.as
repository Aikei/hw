package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.GameEntity;
	import my.Ball;
	import starling.utils.Color;
	import my.view.DisappearingText;
	import my.MyAssets;
	import my.logic.PlayerData;
	import my.system.Language;
	import my.MyWorld;
	import my.logic.Logic;
	
	public class AbilityHeal extends Ability
	{
		
		private var _heal: int = 7;
		
		public function AbilityHeal(ownerNumber: int) 
		{
			super(Ability.HEAL, ownerNumber);
			_targeting = Ability.TARGET_YOUR_HERO;
			_manaCost[Ball.GREEN_BALL] = 6;
			_color = Ball.GREEN_BALL;
			//_chargedByType = Ball.GREEN_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("ability_heal");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("ability_heal_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");		
			_help = Language.GetString("AbilityHealHelp");
			_help = _help.replace("%heal", String(_heal));
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			for (var i = 0; i < targets.length; i++)
			{
				if (!(targets[i] is PlayerData))
					continue;
				//targets[i].DamageEntity(_damage);
				//var text: DisappearingText = new DisappearingText("+"+String(_heal), 80, Color.GREEN, 0, 0, true, 4);
				//targets[i].addChild(text);
			}
			MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
			_allTargets = targets;
			super.Use(targets, args);
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<GameEntity>;
			v.push(entity);
			return v;
		}						
		
	}

}