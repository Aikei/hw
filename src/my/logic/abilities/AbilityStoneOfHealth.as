package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.GameEntity;
	import my.Ball;
	import my.logic.effects.Effect;
	import my.MyWorld;
	import my.logic.Logic;
	import my.MyAssets;
	import starling.events.Event;	
	import my.system.Language;	
	import my.logic.effects.Effect;
	import my.logic.abilities.base.BaseAbilityApplyBallEffect;
	
	public class AbilityStoneOfHealth extends BaseAbilityApplyBallEffect 
	{		
		private var _heal: int = 2;
		
		public function AbilityStoneOfHealth(ownerNumber: int) 
		{
			super(Ability.STONE_OF_HEALTH, Effect.STONE_OF_HEALTH, ownerNumber);
			_manaCost[Ball.GREEN_BALL] = 3;
			_color = Ball.GREEN_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("seal_of_healing");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("seal_of_healing_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityStoneOfHealthHelp");
			_help = _help.replace(/%heal/g, String(_heal));
			SetEffectArguments(_heal);
		}
		
		//override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		//{
			//if (!(targets[i] is Ball))
				//return;
			//_allTargets = null;
			//MyWorld.gWorld.addEventListener("stoneOfHealthEffectApplicationEnded", OnEffectEnded);
			//var owner = Effect.OWNER_YOU;
			//if (!MyWorld.gWorld.m_logic.yourTurn)
				//owner = Effect.OWNER_ENEMY;			
			//for (var i: int = 0, l: int = targets.length; i < l; i++)
			//{
				//_allTargets = GetTargets(targets[i]);
				//for (var i2: int = 0, l2: int = _allTargets.length; i2 < l2; i2++)
				//{
					//Ball(_allTargets[i2]).AddEffect(new EffectStoneOfHealth(_heal,owner));					
				//}				
			//}
			//super.Use(targets);
		//}
		//
		//public function OnEffectEnded(event: Event): void
		//{
			//MyWorld.gWorld.removeEventListener("stoneOfHealthEffectApplicationEnded", OnEffectEnded);
			////MyWorld.gWorld.dispatchEvent(new Event("abilityAnimationEnded"));
			//MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
		//}
		//
		//override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		//{			
			//var v: Vector.<Ball> = new Vector.<Ball>;			
			//v.push(entity);
			//return Vector.<GameEntity>(v);
		//}				
		
	}

}