package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.*;
	import my.logic.globaleffects.GlobalEffect;
	import my.system.Language;
	import my.logic.Logic;
	
	import my.logic.abilities.base.BaseAbilityApplyGlobalEffect;
	
	public class AbilityFreezingRain extends BaseAbilityApplyGlobalEffect
	{
		
		public function AbilityFreezingRain(ownerNumber: int) 
		{
			super(Ability.FREEZING_RAIN, GlobalEffect.FREEZING_RAIN, ownerNumber);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.BLUE_BALL] = 3;
			_color = Ball.BLUE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("freezeButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("freezeButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");		
			_help = Language.GetString("AbilityFreezingRainHelp");
		}							
		
	}		

}