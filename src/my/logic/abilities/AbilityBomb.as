package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.*;
	import my.logic.effects.Effect;
	import starling.events.Event;	
	import my.system.Language;
	import my.logic.Logic;
	import my.logic.effects.Effect;
	import my.logic.abilities.base.BaseAbilityApplyBallEffect;
	
	public class AbilityBomb extends BaseAbilityApplyBallEffect 
	{
		
		private var _damage: int = 10;
		private var _time: int = 5;
		
		public function AbilityBomb(ownerNumber: int) 
		{
			super(Ability.BOMB,Effect.HAS_BOMB,ownerNumber);
			_manaCost[Ball.PURPLE_BALL] = 4;
			_color = Ball.PURPLE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("arcane_bomb");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("arcane_bomb_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilityBombHelp");
			_help = _help.replace(/%damage/g, String(_damage));
			_help = _help.replace(/%time/g, String(_time));
			SetEffectArguments(_damage, _time);
		}		
		
	}

}