package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.GameEntity;
	import my.logic.PlayerData;
	import my.MyAssets;
	
	public class AbilityFrost extends Ability
	{
		
		public function AbilityFrost(ownerNumber: int) 
		{
			super(Ability.FROST, ownerNumber);
			_targeting = 3;
			_manaCost[Ball.BLUE_BALL] = 8;
			_color = Ball.BLUE_BALL;
			//_chargedByType = Ball.BLUE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("freezeButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("freezeButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");								
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			for (var i = 0; i < targets.length; i++)
			{
				if (!(targets[i] is PlayerData))
					continue;
				//targets[i].DamageEntity(_damage);
				//var text: DisappearingText = new DisappearingText("-"+String(_damage), 80, Color.RED, 0, 0, true, 4);
				//targets[i].addChild(text);
			}
			_allTargets = targets;
			super.Use(targets, args);
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<GameEntity>;
			v.push(entity);
			return v;
		}	
		
	}

}