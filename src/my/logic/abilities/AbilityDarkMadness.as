package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.*;
	import starling.events.Event;
	import flash.utils.*;
	import my.system.Language;	
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;
	
	public class AbilityDarkMadness extends BaseAbilityDestroyOrRemoveBalls 
	{
		
		public function AbilityDarkMadness(ownerNumber: int) 
		{
			super(Ability.DARK_MADNESS,ownerNumber);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.PURPLE_BALL] = 6;
			_color = Ball.PURPLE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("madness");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("madness_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");
			_help = Language.GetString("AbilityDarkMadnessHelp");
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<GameEntity> = new Vector.<GameEntity>;
			var balls: Vector.<Vector.<Ball>> = MyWorld.gWorld.m_balls;
			for (var i: int = 0; i < balls.length; i++)
			{
				for (var j: int = 0; j < balls[i].length; j++)
				{
					if (balls[i][j].m_type === Ball.PURPLE_BALL)
					{
						v.push(balls[i][j]);
					}
				}
			}			
			return Vector.<GameEntity>(v);
		}		
		
	}

}