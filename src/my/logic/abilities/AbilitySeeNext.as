package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.GameEntity;
	import my.MyWorld;
	import my.logic.Logic;
	import my.Tweens;
	import my.view.Visual;
	import starling.events.Event;
	import flash.utils.*;
	import my.MyAssets;
	import starling.utils.Color;
	import my.system.Language;
	import my.Misc;
	
	public class AbilitySeeNext extends Ability 
	{
		
		public function AbilitySeeNext(ownerNumber: int) 
		{
			super(Ability.SEE_NEXT, ownerNumber);
			_targeting = Ability.TARGET_AUTOMATIC;
			_manaCost[Ball.PURPLE_BALL] = 4;
			_color = Ball.PURPLE_BALL;
			//_chargedByType = Ball.PURPLE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("madness");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("madness_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilitySeeNextHelp");
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{				
			targets = null;
			_allTargets = null;
			for (var i: int = 0; i < 5; i++)
			{
				MyWorld.gWorld.m_logic.m_entropy.pop();
				MyWorld.gWorld.dispatchEvent(new Event("entropyChanged", false, MyWorld.gWorld.m_logic.m_entropy));
			}
			MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
			super.Use(targets);
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<GameEntity>;
			v.push(entity);
			return v;
		}			
		
	}

}