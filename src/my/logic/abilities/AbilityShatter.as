package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.GameEntity;
	import my.MyWorld;
	import my.logic.Logic;
	import my.Tweens;
	import starling.events.Event;
	import flash.utils.*;
	import my.MyAssets;
	import starling.utils.Color;
	import my.system.Language;
	import my.view.Visual;
	import my.logic.effects.EffectFrozen;
	import my.logic.abilities.base.BaseAbilityDestroyOrRemoveBalls;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class AbilityShatter extends BaseAbilityDestroyOrRemoveBalls 
	{
		
		public function AbilityShatter(ownerNumber: int) 
		{
			super(Ability.SHATTER, ownerNumber, false);
			_targeting = Ability.TARGET_PICK_STONE;
			_manaCost[Ball.BLUE_BALL] = 4;
			_color = Ball.BLUE_BALL;
			//_chargedByType = Ball.BLUE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("freezeButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("freezeButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilityShatterHelp");
		}		
		
		override protected function DisplayVisualEffect(): void
		{
			for (var i2: int = 0, l2: int = _allTargets.length; i2 < l2; i2++)
			{
				_allTargets[i2].AddEffect(new EffectFrozen);
				_allTargets[i2].SendToTop();
				Tweens.RelativeTweenTo(_allTargets[i2], 0.50, 0, -100);
				var toY: Number = _allTargets[i2].y;
				TweenLite.to(_allTargets[i2], 0.75, { y: toY, delay: 0.75, ease: Expo.easeIn } );
			}
			setTimeout(OnEffectEnded, 1500);
		}
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{			
			var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetBallsForEliminateRows(Ball(entity),false);
			return Vector.<GameEntity>(v);
		}		
		
	}

}