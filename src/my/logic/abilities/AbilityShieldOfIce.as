package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.GameEntity;
	import my.Ball;
	import my.logic.Logic;
	import my.MyWorld;
	import starling.utils.Color;
	import my.view.DisappearingText;
	import my.MyAssets;
	import my.logic.PlayerData;
	import my.system.Language;
	
	public class AbilityShieldOfIce extends Ability
	{
		
		private var _shield: int = 3;
		
		public function AbilityShieldOfIce(ownerNumber: int) 
		{
			super(Ability.SHIELD_OF_ICE, ownerNumber);
			_targeting = Ability.TARGET_YOUR_HERO;
			_manaCost[Ball.BLUE_BALL] = 2;
			_color = Ball.BLUE_BALL;
			//_chargedByType = Ball.BLUE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("freezeButton");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("freezeButtonPushed");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");		
			_help = Language.GetString("AbilityShieldOfIceHelp");
			_help = _help.replace("%shield", String(_shield));
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			for (var i = 0; i < targets.length; i++)
			{
				if (!(targets[i] is PlayerData))
					continue;
			}
			MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
			_allTargets = targets;
			super.Use(targets, args);
		}		
		
		override public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<GameEntity>;
			v.push(entity);
			return v;
		}						
		
	}

}