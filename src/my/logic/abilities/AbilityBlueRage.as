package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.MyAssets;
	import my.system.Language;
	
	public class AbilityBlueRage extends AbilityRage 
	{
		
		public function AbilityBlueRage(ownerNumber: int) 
		{
			super(Ability.BLUE_RAGE, Ball.BLUE_BALL, ownerNumber);
			_color = Ball.BLUE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");		
			//_help = Language.GetString("AbilityBlueRageHelp");
			//_help = _help.replace(/%mult1/g, String(_mult1));
			//_help = _help.replace(/%mult2/g, String(_mult2));
		}	
		
	}

}