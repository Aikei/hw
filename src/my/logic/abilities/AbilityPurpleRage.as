package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.MyAssets;
	import my.system.Language;
	
	public class AbilityPurpleRage extends AbilityRage 
	{
		
		public function AbilityPurpleRage(ownerNumber: int) 
		{
			super(Ability.PURPLE_RAGE, Ball.PURPLE_BALL, ownerNumber);
			_color = Ball.PURPLE_BALL;
			_buttonTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("seal_of_corruption_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("fireButtonSelector");		
			//_help = Language.GetString("AbilityPurpleRageHelp");
			//_help = _help.replace(/%mult1/g, String(_mult1));
			//_help = _help.replace(/%mult2/g, String(_mult2));
		}	
		
	}

}