package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import my.*;
	import my.logic.effects.Effect;
	import starling.events.Event;	
	import my.system.Language;
	import my.logic.Logic;
	import my.logic.effects.Effect;
	import my.logic.abilities.base.BaseAbilityApplyBallEffect;	 
	 
	public class AbilityIceBlock extends BaseAbilityApplyBallEffect 
	{
		
		public function AbilityIceBlock(ownerNumber:int) 
		{
			super(Ability.ICE_BLOCK, Effect.ICE_BLOCK, ownerNumber);
			_manaCost[Ball.BLUE_BALL] = 3;
			_color = Ball.BLUE_BALL;
			_targeting = TARGET_YOUR_HERO;
			_buttonTexture = MyAssets.UiAtlas.getTexture("frost_shot");
			_buttonPushedTexture = MyAssets.UiAtlas.getTexture("frost_shot_s");
			_buttonSelectorTexture = MyAssets.UiAtlas.getTexture("freezeButtonSelector");
			_help = Language.GetString("AbilityIceBlockHelp");
			_effectOwning = true;			
		}
		
		override public function Use(targets: Vector.<GameEntity>, args: Array = undefined): void
		{
			super.Use(targets, args);
			MyWorld.gWorld.m_logic.StartNextTurnClientPhase();
		}
		
	}

}