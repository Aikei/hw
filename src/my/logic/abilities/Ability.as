package my.logic.abilities 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import flash.geom.Point;
	import my.system.Language;
	import starling.textures.Texture;
	import my.GameEntity;
	import my.Ball;
	import my.logic.effects.Effect;
	import my.MyAssets;
	import my.MyWorld;
	import my.mp.messages.*;
	import my.mp.Controller;
	import my.Preferences;
	import starling.utils.Color;
	import starling.events.Event;
	import my.logic.Logic;	 
	import flash.utils.getDefinitionByName;
	
	public class Ability 
	{
		public static const FREEZE: String = "Freeze";
		public static const FIRE: String = "Fire";
		public static const TURN: String = "Turn";
		public static const FLAME: String = "Flame";
		public static const HEAL: String = "Heal";
		public static const FROST: String = "Frost";
		public static const POISON: String = "Poison";
		public static const BOMB: String = "Bomb";
		public static const METEORITE_RAIN: String = "MeteoriteRain";
		public static const SHATTER: String = "Shatter";
		public static const SEE_NEXT: String = "SeeNext";
		public static const EARTHQUAKE: String = "Earthquake";
		public static const STONE_OF_HEALTH: String = "StoneOfHealth";
		public static const SHIELD_OF_ICE: String = "ShieldOfIce";
		public static const DARK_MADNESS: String = "DarkMadness";
		public static const CHAIN_LIGHTNING: String = "ChainLightning";
		public static const GOBLIN_MINE: String = "GoblinMine";
		public static const FREEZING_RAIN: String = "FreezingRain";
		public static const GREEN_RAGE: String = "GreenRage";
		public static const RED_RAGE: String = "RedRage";
		public static const BLUE_RAGE: String = "BlueRage";
		public static const PURPLE_RAGE: String = "PurpleRage";
		public static const POWER_OF_ABYSS: String = "PowerOfAbyss";
		public static const INFECTION: String = "Infection";
		public static const MATCH_THREE: String = "MatchThree";
		public static const REMOVE_STONE: String = "RemoveStone";
		public static const POWER_DRAIN: String = "PowerDrain";
		public static const ICE_BLOCK: String = "IceBlock";
		
		public static const TARGET_AUTOMATIC: int = 0;
		public static const TARGET_PICK_STONE: int = 1;
		public static const TARGET_PICK_STONE_THEN_ARGUMENTS: int = 2;
		public static const TARGET_ENEMY_HERO: int = 3;
		public static const TARGET_YOUR_HERO: int = 4;
		public static const TARGET_PICK_YOUR_ABILITY: int = 5;
		public static const TARGET_PICK_ENEMY_ABILITY: int = 6;
		public static const TARGET_PICK_ANY_ABILITY: int = 7;
		public static const TARGET_OTHER: int = 8;
		
		//public static const MAX_CHARGE: int = 12;
		
		private var _type: String;
		private var _used: Boolean = false;
		protected var _buttonTexture: Texture;
		protected var _buttonPushedTexture: Texture;
		protected var _buttonSelectorTexture: Texture;
		protected var _targeting: int;
		protected var _argumentsTargeting: int;
		protected var _lastTargeted: GameEntity = null;
		protected var _targets: Vector.<GameEntity> = null;
		protected var _allTargets: Vector.<GameEntity> = null;
		protected var _help: String = "";
		protected var _ownerNumber: int = -1;
		protected var _passive: Boolean = false;
		protected var _subType: String = "none";
		protected var _color: int = -1;
		
		public function get used(): Boolean
		{
			return _used;
		}
		
		public function set used(u: Boolean): void 
		{
			_used = u;
		}
		
		public function get color(): int
		{
			return _color;
		}
		
		public function get subType() : String
		{
			return _subType;
		}		
		
		public function get passive(): Boolean
		{
			return _passive;
		}

		protected var _manaCost: Vector.<int> = new Vector.<int>;
		
		public var chargerPos: Point = new Point;
		
		
		public function Ability(type: String, ownerNumber: int) 
		{
			_type = type;
			_ownerNumber = ownerNumber;
			_targeting = TARGET_OTHER;
			_argumentsTargeting = TARGET_OTHER;
			for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			{
				_manaCost.push(0);
			}
		}
		
		public function GetChargeType(): int
		{
			return _color;
			//for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
			//{
				//if (_manaCost[i] !== 0)
					//return i;
			//}
			return 0;
		}
		
		public function get manaCost(): Vector.<int>
		{
			return _manaCost;
		}
		
		//public function get chargedByType(): int
		//{
			//return _chargedByType;
		//}
		
		public function get help(): String
		{
			return _help;
		}
		
		static public function CreateAbility(type: String, ownerNumber: int): Ability
		{
			var c: Class = getDefinitionByName("my.logic.abilities.Ability" + type) as Class;
			return new c(ownerNumber) as Ability;
		}
		
		public function get targeting(): int
		{
			return _targeting;
		}
		
		public function get argumentsTargeting(): int
		{
			return _argumentsTargeting;
		}
		
		public function get buttonTexture(): Texture
		{
			return _buttonTexture;
		}
		
		public function get buttonPushedTexture(): Texture
		{
			return _buttonPushedTexture;
		}
		
		public function get buttonSelectorTexture(): Texture
		{
			return _buttonSelectorTexture;
		}		
		
		public function get lastTargeted(): GameEntity
		{
			return _lastTargeted;
		}		
					
		public function Use(targets: Vector.<GameEntity>, args: Array = undefined) : void
		{
			if (_passive)
				return;
			DisplayVisualEffect();
			used = true;
			if (MyWorld.gWorld.m_logic.yourTurn)
			{
				Controller.UseAbility(this, targets, _allTargets, args);
			}
			else
			{
				var x1: int = MyWorld.gWorld.m_gameScreen.m_ballsGroup.x + MyWorld.OVERALL_FIELD_WIDTH / 2 - 32;
				var y1: int = MyWorld.gWorld.m_gameScreen.m_ballsGroup.y + MyWorld.OVERALL_FIELD_HEIGHT / 2 - 32;
				MyWorld.gWorld.m_gameScreen.AddDisappearingText(Language.GetString("EnemyUsedAbility"),x1, y1,Color.RED, 4)
				MyWorld.gWorld.m_gameScreen.AddDisappearingImage(_buttonTexture, x1, y1 + _buttonTexture.height + 20, 4);
			}
		}
		
		public function PickArguments(targets: Vector.<GameEntity>): void
		{
			_targets = targets;
		}
		
		public function get type(): String
		{
			return _type;
		}
		
		public function GetTargets(entity: GameEntity): Vector.<GameEntity>
		{
			return null;
		}
		
		public function GetShowTargets(entity: GameEntity): Vector.<GameEntity>
		{
			return GetTargets(entity);
		}
		
		public function CheckTargets(targets: Vector.<GameEntity>): Boolean
		{
			return true;
		}
		
		public function Target(entity: GameEntity): void
		{
			_lastTargeted = entity;			
			var v: Vector.<GameEntity> = GetShowTargets(entity);
			Controller.HighlightAbilityTargets(v);
		}
		
		public function Detarget(entity: GameEntity): void
		{
			var v: Vector.<GameEntity> = GetShowTargets(entity);
			Controller.DehighlightAbilityTargets(v);
		}
		
		public function DetargetLast(): void
		{
			if (_lastTargeted === null)
				return;
			Detarget(_lastTargeted);
			_lastTargeted = null;
		}
		
		protected function DisplayVisualEffect(): void 
		{
			//MyWorld.gWorld.addEventListener("effectAnimationEnded", OnEffectEnded);
			OnEffectEnded();
		}
		
		protected function OnEffectEnded(): void
		{
			//MyWorld.gWorld.removeEventListener("effectAnimationEnded", OnEffectEnded);
			MyWorld.gWorld.m_logic.SetPhase(Logic.PHASE_SELECT_BALL);
		}
		
		protected function GetThisTargetOnly(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<GameEntity> = new Vector.<GameEntity>;
			v.push(entity);
			return v;
		}
		
		protected function GetTargetChain(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetChain(Ball(entity));
			return Vector.<GameEntity>(v);
		}
		
		protected function GetTargetNeighbours(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetBallNeighbours(Ball(entity));
			return Vector.<GameEntity>(v);
		}
		
		protected function GetTargetCrossNeighbours(entity: GameEntity): Vector.<GameEntity>
		{
			var v: Vector.<Ball> = MyWorld.gWorld.m_logic.GetCrossNeighbours(Ball(entity));
			return Vector.<GameEntity>(v);
		}
		
		protected function GetTargetAllBalls(): Vector.<GameEntity>
		{
			var v: Vector.<Ball> = new Vector.<Ball>;
			var balls: Vector.<Vector.<Ball>> = MyWorld.gWorld.m_balls;
			for (var i: int = 0; i < balls.length; i++)
			{
				v = v.concat(balls[i]);
			}
			return Vector.<GameEntity>(v);			
		}		
		
	}

}