package my.logic.effects 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyAssets;
	import my.GameEntity;
	import my.Ball;
	import my.MyWorld;
	import my.view.GameScreen;
	import my.Tweens;
	import starling.events.Event;
	import flash.utils.*;
	import flash.geom.Point;
	import starling.utils.Color;
	import starling.display.BlendMode;
	
	public class EffectBomb extends Effect 
	{
		//private var _isEnemy_s: Boolean;
		private var _damage: int;
		
		public function EffectBomb(damage: int, time: int, owner: int) 
		{
			super(Effect.HAS_BOMB,owner);
			//_isEnemy_s = isEnemy_s;
			_startingTime = time;
			_time = time;
			_damage = damage;
		}
		
		override public function OnAdded(entity: GameEntity): void
		{
			if (!(entity is Ball))
				return;			
			super.OnAdded(entity);			
			_visual = MyWorld.gWorld.m_gameScreen.CreateVisual(MyAssets.EffectsAtlas.getTextures("arcane_bomb"), 0, 0);
			_visual.CreateText(String(_time), 50);						
			entity.addChild(_visual);
			
			_visual.x = _visual.x - _visual.width / 2 + _addAfterScale;
			_visual.y = _visual.y - _visual.height / 2 + _addAfterScale;								
			_visual.scaleX = 1 / entity.scaleX;
			_visual.scaleY = 1 / entity.scaleY;	
			
			_visual.scaleX *= 0.7;
			_visual.scaleY *= 0.7;
			//setTimeout(Tweens.TweenAlpha, 300, 0.001, _visual, 0.75);
			////Tweens.TweenBaseHsvRelative(0.001, _visual, 1, 0, 1);
			//if (owner === OWNER_ENEMY)
			//{
				//setTimeout(Tweens.TweenBaseColorTo, 150, 0.1, _visual, Color.RED);
			//}
			//else
			//{
				//setTimeout(Tweens.TweenBaseColorTo, 150, 0.1, _visual, Color.GREEN);
			//}						
			
			setTimeout(OnTweenEnd,500);
		}
		
		//public function OnTweenEnd(): void
		//{
			//MyWorld.gWorld.dispatchEvent(new Event("bombEffectAnimationEnded"));
		//}
		
		override protected function OnTick(): void
		{
			_visual.SetText(String(_time));
			if (_time === 0)
			{
				var p: Point = _attachedTo.parent.localToGlobal(new Point(_attachedTo.x, _attachedTo.y));
				MyWorld.gWorld.m_gameScreen.AddFloatingText("-" + String(_damage), p.x + (_attachedTo.width * _attachedTo.scaleX) / 2, p.y + (_attachedTo.height * _attachedTo.scaleY) / 2, Color.RED);
				var v: Vector.<Ball> = new Vector.<Ball>;
				v.push(Ball(_attachedTo));
				MyWorld.gWorld.dispatchEvent(new Event("quietlyRemoveBalls", false, v));				
			}
		}
		
		override public function CreateIdenticalEffect(): Effect
		{
			return new EffectBomb(_damage, _startingTime, owner);
		}			
		
	}

}