package my.logic.effects {

	
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.utils.setTimeout;
	import my.logic.effects.Effect;
	import my.logic.PlayerData;
	import my.*;
	
	public class EffectIceBlock extends Effect 
	{
		
		public function EffectIceBlock(owner: int) 
		{
			super(Effect.ICE_BLOCK, owner);
			_time = 2;
			_startingTime = _time;
			_owningEffect = true;
		}
		
		override public function OnAdded(entity: GameEntity): void
		{
			super.OnAdded(entity);
			if (!(entity is PlayerData))
				return;
			_visual = MyWorld.gWorld.m_gameScreen.CreateVisual(MyAssets.StonesAtlas.getTextures("ice"), 0, 0);
			entity.addChild(_visual);
			_visual.alpha = 0;
			_visual.alignPivot();
			Tweens.TweenAlpha(0.5, _visual, 0.65);
			setTimeout(OnTweenEnd,500);
		}		
		
		override public function CreateIdenticalEffect(): Effect
		{
			return new EffectIceBlock(owner);
		}			
		
	}

}