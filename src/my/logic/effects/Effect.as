package my.logic.effects {

	import com.greensock.TweenLite;
	import my.GameEntity;
	import my.view.Visual;
	import starling.textures.TextureAtlas;
	import my.MyAssets;
	import my.Ball;
	import my.MyWorld;
	import flash.utils.*;
	import my.logic.effects.*;
	import starling.events.Event;
	/**
	 * ...
	 * @author Aikei
	 */
	public class Effect 
	{
		//stone
		public static const FROZEN: String = "Frozen";
		public static const HAS_POISON: String = "Poison";
		public static const HAS_BOMB: String = "Bomb";
		public static const STONE_OF_HEALTH = "StoneOfHealth";
		public static const HAS_GOBLIN_MINE = "GoblinMine";
		
		//player		
		public static const ICE_BLOCK: String = "IceBlock";
		
		static public const OWNER_NONE: int = 0;
		static public const OWNER_YOU: int = 1;
		static public const OWNER_ENEMY: int = 2;		
		
		static public const NOT_HIDDEN: int = 0;
		static public const HIDDEN_FOR_YOU: int = 1;
		static public const HIDEEN_FOR_ENEMY: int = 2;		
		
		private var _type: String;
		private var _owner: int;
		public var hidden: int = NOT_HIDDEN;
		
		protected var _visual: Visual;
		protected var _time: int = 0;
		protected var _startingTime: int = 0;
		protected var _attachedTo: GameEntity;
		protected var _tweens: Vector.<TweenLite> = new Vector.<TweenLite>;
		protected var _tickInterval: int = 1;
		protected var _tickIntervalCounter: int = 0;
		protected var _triggerOnSelect: Boolean = false;
		protected var _removeOnSelect: Boolean = false;
		protected var _removeOnTrigger: Boolean = false;
		protected var _owningEffect: Boolean = true;
		protected const _addAfterScale: Number = 12;
				
		public function get owningEffect(): Boolean
		{
			return _owningEffect;
		}
		
		public function get triggerOnSelect(): Boolean
		{
			return _triggerOnSelect;
		}
		
		public function get removeOnTrigger(): Boolean
		{
			return _removeOnTrigger;
		}
		
		public function get removeOnSelect(): Boolean
		{
			return _removeOnSelect;
		}		
		
		public function Effect(type: String, owner = OWNER_NONE) 
		{
			_type = type;
			_owner = owner;
		}
		
		public function get owner(): int
		{
			return _owner;
		}
		
		public function CreateEffect(type: String): Effect
		{
			if (type === FROZEN)
				return new EffectFrozen;
			//else if (type === HAS_POISON)
				//return new EffectPoison;
			return null;
		}
		
		public function get type(): String
		{
			return _type;
		}
		
		public function OnAdded(entity: GameEntity): void
		{
			_attachedTo = entity;
		}
		
		public function OnRemoved(entity: GameEntity): void
		{
			for (var i: int = 0; i < _tweens.length; i++)
			{
				_tweens[i].seek(0);
				_tweens[i].kill(0);
			}
			if (_visual)
				_visual.Destroy();
		}		
		
		public function Remove(): void
		{
			_attachedTo.RemoveEffect(_type);
		}
		
		protected function Tick(): Boolean
		{
			_tickIntervalCounter++;
			if (_tickIntervalCounter === _tickInterval)
			{
				_tickIntervalCounter = 0;				
				if (_time > -1)
				{
					_time--;
					OnTick();
					if (_time === 0)
					{
						Remove();
						return true;
					}
				}				
				else
				{
					OnTick();
				}
			}
			return false;
		}
		
		public function TickBeforeMoving(): Boolean
		{
			return Tick();
		}
		
		public function TickAfterMoving(): Boolean 
		{
			return false;
		}
		
		protected function OnTick(): void
		{
			
		}
		
		public function Trigger(yourTurn: Boolean): Boolean
		{
			return false;
		}
		
		public function get time(): int
		{
			return _time;
		}
		
		public function CreateIdenticalEffect(): Effect
		{
			return null;
		}
		
		public function OnTweenEnd(): void
		{
			//MyWorld.gWorld.removeEventListener("freezeTweenEnded", OnTweenEnd);
			MyWorld.gWorld.dispatchEvent(new Event("effectAnimationEnded"));
		}
		
		//static public function CreateEffect(type: String, owner: int): Effect
		//{
			//switch (type)
			//{
				//case HAS_POISON:
					//return new EffectPoison(
			//}
		//}
		
	}

}