package my.logic.effects 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import flash.geom.Point;
	import my.MyAssets;
	import my.GameEntity;
	import my.Ball;
	import my.MyWorld;
	import my.view.GameScreen;
	import my.Tweens;
	import starling.events.Event;
	import flash.utils.*;
	import my.view.FloatingText;
	import starling.utils.Color;
	import starling.display.BlendMode;
	
	public class EffectPoison extends Effect 
	{
		private var _damage: int = 1;		
		//private var _isEnemy_s: Boolean;
		
		public function EffectPoison(damage: int, owner: int) 
		{
			super(Effect.HAS_POISON,owner);
			_damage = damage;
			//_isEnemy_s = isEnemy_s;
			_startingTime = -1;
			_time = -1;
			_tickInterval = 2;
			_tickIntervalCounter = 1;
		}
		
		override public function OnAdded(entity: GameEntity): void
		{
			if (!(entity is Ball))
				return;			
			super.OnAdded(entity);			
			_visual = MyWorld.gWorld.m_gameScreen.CreateVisual(MyAssets.EffectsAtlas.getTextures("corruption_"), 0, 0);
			entity.addChild(_visual);
			
			_visual.x = _visual.x - _visual.width / 2 + _addAfterScale;
			_visual.y = _visual.y - _visual.height / 2 + _addAfterScale;							
			_visual.scaleX = 1 / entity.scaleX;
			_visual.scaleY = 1 / entity.scaleY;
			
			_visual.scaleX *= 0.7;
			_visual.scaleY *= 0.7;
			//setTimeout(Tweens.TweenAlpha, 300, 0.1, _visual, 0.75);
			////Tweens.TweenBaseHsvRelative(0.001, _visual, 1, 0, 1);
			//if (owner === OWNER_ENEMY)
			//{
				//setTimeout(Tweens.TweenBaseColorTo, 150, 0.1, _visual, Color.RED);
			//}
			//else
			//{
				//setTimeout(Tweens.TweenBaseColorTo, 150, 0.1, _visual, Color.GREEN);
			//}
			
			setTimeout(OnTweenEnd,500);
		}		
		
		override protected function OnTick(): void
		{
			//var floatingText: FloatingText = new FloatingText("-" + String(_damage), 80, Color.RED, _visual.x+_visual.width/2, _visual.y+_visual.height/2);
			var p: Point = _attachedTo.parent.localToGlobal(new Point(_attachedTo.x, _attachedTo.y));
			MyWorld.gWorld.m_gameScreen.AddFloatingText("-" + String(_damage), p.x + (_attachedTo.width * _attachedTo.scaleX) / 2, p.y + (_attachedTo.height * _attachedTo.scaleY) / 2, Color.RED);
		}
		
		override public function CreateIdenticalEffect(): Effect
		{
			return new EffectPoison(_damage, owner);
		}		
	}

}