package my.logic.effects 
{
	
	/**
	 * ...
	 * @author Aikei
	 */
	import my.MyAssets;
	import my.GameEntity;
	import my.Ball;
	import my.MyWorld;
	import my.view.GameScreen;
	import my.Tweens;
	import starling.events.Event;
	import flash.utils.*;
	
	public class EffectFrozen extends Effect
	{
		protected var _animationDuration: Number = 0.5;
		public function EffectFrozen(time: int = 2, animationDuration: Number = 1) 
		{
			super(Effect.FROZEN);
			_time = time;
			_startingTime = time;
			_animationDuration = animationDuration;
			_owningEffect = false;
		}
		
		override public function OnAdded(entity: GameEntity): void
		{
			super.OnAdded(entity);
			if (!(entity is Ball))
				return;
			_visual = MyWorld.gWorld.m_gameScreen.CreateVisual(MyAssets.StonesAtlas.getTextures("ice"), 0, 0);
			entity.addChild(_visual);
			_visual.alpha = 0;
			Tweens.TweenAlpha(_animationDuration, _visual, 0.65);
			setTimeout(OnTweenEnd,500);
		}
		
		override public function CreateIdenticalEffect(): Effect
		{
			return new EffectFrozen(_startingTime, _animationDuration);
		}			
		
		
	}

}