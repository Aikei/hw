package my.logic.effects 
{
	/**
	 * ...
	 * @author Aikei
	 */

	import flash.geom.Point;
	import my.MyAssets;
	import my.GameEntity;
	import my.Ball;
	import my.MyWorld;
	import my.view.GameScreen;
	import my.Tweens;
	import starling.events.Event;
	import flash.utils.*;
	import my.view.FloatingText;
	import starling.utils.Color;	 
	import my.system.Language;
	import my.logic.abilities.Ability;
	
	public class EffectGoblinMine extends Effect 
	{
		
		private var _damage: int = 3;		
		//private var _isEnemy_s: Boolean;
		
		public function EffectGoblinMine(damage: int, owner: int) 
		{
			super(Effect.HAS_GOBLIN_MINE,owner);
			_damage = damage;
			//_isEnemy_s = isEnemy_s;
			_time = -1;
			_startingTime = _time;
			_tickInterval = 2;
			_tickIntervalCounter = 1;
			_triggerOnSelect = true;
			_removeOnTrigger = true;
		}
		
		override public function OnAdded(entity: GameEntity): void
		{
			if (!(entity is Ball))
				return;			
			super.OnAdded(entity);
			if (owner === OWNER_YOU)
			{
				_visual = MyWorld.gWorld.m_gameScreen.CreateVisual(MyAssets.EffectsAtlas.getTextures("goblin_mine"), 0, 0);
				
				_visual.x = _visual.x - _visual.width / 2 + _addAfterScale;
				_visual.y = _visual.y - _visual.height / 2 + _addAfterScale;						
				
				_visual.scaleX = 1 / entity.scaleX;
				_visual.scaleY = 1 / entity.scaleY;				
				
				entity.addChild(_visual);
				Tweens.TweenAlpha(1.0, _visual, 0.75);			
				_visual.baseColor = 0xFF20F52E;
			}
			else
			{
				hidden = HIDDEN_FOR_YOU;
			}
			setTimeout(OnTweenEnd,500);
		}		
		
		override public function Trigger(yourTurn: Boolean): Boolean
		{
			//if (yourTurn === _isEnemy_s)
			if ((yourTurn && owner === OWNER_ENEMY) || (!yourTurn && owner === OWNER_YOU))
			{
				var p: Point = _attachedTo.parent.localToGlobal(new Point(_attachedTo.x, _attachedTo.y));
				MyWorld.gWorld.m_gameScreen.AddFloatingText(Language.GetString(Ability.GOBLIN_MINE), p.x + (_attachedTo.width * _attachedTo.scaleX) / 2, p.y + (_attachedTo.height * _attachedTo.scaleY) / 2, Color.RED);
				setTimeout(MyWorld.gWorld.m_gameScreen.AddFloatingText, 500, "-" + String(_damage), p.x + (_attachedTo.width * _attachedTo.scaleX) / 2, p.y + (_attachedTo.height * _attachedTo.scaleY) / 2, Color.RED);
				return true;
			}
			return false;
		}
		
		override public function CreateIdenticalEffect(): Effect
		{
			return new EffectGoblinMine(_damage, owner);
		}					
		
		//override protected function OnTick(): void
		//{
			////var floatingText: FloatingText = new FloatingText("-" + String(_damage), 80, Color.RED, _visual.x+_visual.width/2, _visual.y+_visual.height/2);
			////var p: Point = _attachedTo.parent.localToGlobal(new Point(_attachedTo.x, _attachedTo.y));
			////MyWorld.gWorld.m_gameScreen.AddFloatingText("-" + String(_damage), p.x + (_attachedTo.width * _attachedTo.scaleX) / 2, p.y + (_attachedTo.height * _attachedTo.scaleY) / 2, Color.RED);
		//}		
		
	}

}