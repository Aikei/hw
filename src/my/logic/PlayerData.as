package my.logic 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.view.elements.PlayerWindow;
	import starling.events.Event;
	import my.GameEntity;
	import my.logic.abilities.Ability;
	import my.MyWorld;
	import my.Ball;
	import my.Misc;
	
	public class PlayerData extends GameEntity
	{
		private var _abilities: Vector.<Ability> = new Vector.<Ability>;
		private var _portraitName: String = "magePortrait";
		private var _isEnemy: Boolean;		
		private var _playerWindow: PlayerWindow;
		
		public var mana: Vector.<int> = new Vector.<int>;
		public var hp: int;
		public var shield: int;
		
		public function get isEnemy() : Boolean
		{
			return _isEnemy;
		}
		
		public function get portraitName(): String
		{
			return _portraitName;
		}
		
		public function get abilities(): Vector.<Ability>
		{
			return _abilities;
		}
		
		public function PlayerData(isEnemy: Boolean, heroClass: String) : void
		{
			super(isEnemy, 0, 0);
			_isEnemy = isEnemy;
			if (_isEnemy)
				m_index.x = -1;
			else
				m_index.x = -2;

			if (heroClass === "mage")
			{
				_portraitName = "magePortrait"
			}
			else if (heroClass === "shaman")
			{
				_portraitName = "shamanPortrait"
			}
			var portraitNumber = Misc.Random(1, 6);
			_portraitName = String(portraitNumber);
			for (var i: int = 0; i < Ball.NUMBER_OF_TYPES; i++)
				mana.push(0);
			MyWorld.gWorld.dispatchEvent(new Event("NewPlayerCreated", false, this));
		}
		
		public function AddAbility(name: String): void
		{
			if (_isEnemy)
				abilities.push(Ability.CreateAbility(name, MyWorld.gWorld.m_logic.enemyNumber));
			else
				abilities.push(Ability.CreateAbility(name, MyWorld.gWorld.m_logic.myNumber));
		}
		
		public function RemoveAbility(name: String): Boolean
		{
			for (var i: int = 0; i < _abilities.length; i++)
			{
				if (_abilities[i].type === name)
				{
					_abilities.splice(i, 1);
					return true;
				}
			}
			return false;
		}
		
		public function ClearAbilities(): void
		{
			_abilities = new Vector.<Ability>;
		}		
		
		override public function DamageEntity(damage: int)
		{
			hp -= damage;
		}		
		
	}

}