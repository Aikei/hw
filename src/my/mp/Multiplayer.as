package my.mp 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.net.*;
	import flash.utils.*;
	import flash.events.DataEvent;
	import flash.system.Security;
	import my.Misc;
	import my.mp.messages.*;
	import flash.errors.*;
	import flash.events.*;
	import my.dbg.MConsole;
	import my.MyWorld;
	import my.mp.Controller;
	import my.Preferences;
	import my.mp.SocialProfile;
	import my.mp.GameProfile;
	
	public class Multiplayer 
	{
		public static const kMessageTerminator: String = "\n";
		
		private var xmlSocket: XMLSocket;
		private var m_latency: Number = Number.MAX_VALUE;
		private var m_timeCompensation: Number = 0;
		private var _servers: Vector.<String> = new Vector.<String>;
		private var _serverPort: int = 8000;
		private var _httpPort: int = 10000;
		private var _dataReceived: String = "";
		private var _timeLostConnection: Number = 0;
		private var _serverNumber: int = 1;
		private var _connected: Boolean = false;
		private var vkApiVersion: String = "5.28";
		
		static public function Serialize(obj: Object): String
		{
			var s: String = JSON.stringify(obj)+kMessageTerminator;
			return s;
		}
		
		static public function DeSerialize(msg: String): Object
		{
			return JSON.parse(msg);
		}
		
		public function SetServer(n: int): void
		{
			if (n < _servers.length)
			{
				if (xmlSocket.connected)
					xmlSocket.close();		
			}			
		}
		
		public function Multiplayer() 
		{		
			if (MyWorld.gWorld.flashVars.viewer_id !== undefined)
			{
				GetVKInfo();
			}
			else
			{
				MConsole.Write("_ii","viewer id undefined");
				Start();
			}
		}
		
		private function Start()
		{
			_timeLostConnection = m_LocalTimeSeconds;
			_servers.push("178.62.202.250");
			_servers.push("localhost");
			Security.loadPolicyFile("http://" + _servers[_serverNumber] + ":10000/crossdomain.xml");
			MConsole.Write("creating xml socket...");
			xmlSocket = new XMLSocket();
			MConsole.Write("adding listeners...");
			xmlSocket.addEventListener(DataEvent.DATA, OnIncomingData);			
			xmlSocket.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			xmlSocket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			xmlSocket.addEventListener(Event.CONNECT, OnConnect);
			SyncClocks();			
		}
		
		public function OnConnect(event:Event): void
		{
			MConsole.Write("_ii", "connected to server!");
			//if (MyWorld.gWorld.GetPhase() == MyWorld.PHASE_CONNECTING_TO_SERVER)
			//{							
				//MyWorld.gWorld.SetPhase(MyWorld.PHASE_IN_LOBBY);
			//}
		}
		
		public function ioErrorHandler(ev:IOErrorEvent): void
		{
			MConsole.Write("e","async ioerror!", ev.text);
		}
		
		public function securityErrorHandler(ev:SecurityErrorEvent): void
		{
			MConsole.Write("e","async security error!", ev.text);
		}		
		
		public function Connect(): void
		{
			if (!xmlSocket.connected)
			{
				if (m_LocalTimeSeconds - _timeLostConnection >= 5)
				{
					_serverNumber++;
					if (_serverNumber >= _servers.length)
						_serverNumber = 0;
					_timeLostConnection = m_LocalTimeSeconds;
				}
				MConsole.Write("_ii","xmlSocket not connected");
				MConsole.Write("_ii","connecting to", _servers[_serverNumber],":", _serverPort);
				try 
				{				
					xmlSocket.connect(_servers[_serverNumber], _serverPort);
				}
				catch (ioError:IOError) 
				{
					MConsole.Write("e","ioerror!", ioError.message);
				} 
				catch (secError:SecurityError) 
				{
					MConsole.Write("e","securityError!", secError.message);
				}
			}
		}
		
		public function SerializeAndSend(obj: Object): void
		{
			if (xmlSocket.connected)
				xmlSocket.send(Serialize(obj));
		}
		
		/**
		 * Send clock synchronisation message
		 */
		private function SyncClocks(): void
		{
			Connect();
			if (xmlSocket.connected)
			{
				if (!_connected)
				{
					_connected = true;
					var mm: MessageContainer = Message.CreateDataMessage(MessageNames.kFirstConnect, null, MyWorld.gWorld.m_socialProfile);
					SerializeAndSend(mm);
				}
				else
				{
					var m: MessageContainer = Message.CreateDataMessage(MessageNames.kTime, null, { m_clientTime:m_LocalTimeSeconds } );
					var s: String = Serialize(m);
					xmlSocket.send(s);
				}
			}
			else
			{
				_connected = false;
			}
			// check again in 5 seconds
			setTimeout(SyncClocks, 5*1000);
		}
		 
		/**
		 * Get the time on this local machine, in seconds
		 */
		static public function get m_LocalTimeSeconds(): Number
		{
			return new Date().getTime()/1000.0;
		}		
		
		/**
		 * Syncronise clocks
		 * 
		 * @param message
		 *
		 */
		public function ProcessTimeMessage(message:MessageContainer): void
		{
			var now: Number = m_LocalTimeSeconds;
			var clientTime: Number = message.m_data.m_clientTime;
			var serverTime: Number = message.m_data.m_serverTime;
		 
			// round trip time in seconds
			var roundTripSeconds: Number = now-clientTime;
		 
			// latency is how long message took to get to server
			var latency: Number = roundTripSeconds/2;
		 
			// difference between server time and client time
			var serverDeltaSeconds: Number = serverTime-now;
		 
			// store averages
			if (m_latency != Number.MAX_VALUE)
			{
				m_latency = (m_latency+latency)*0.5;
			}
			else 
			{
				m_latency = latency;
			}
		 
			// this is the current compenstation
			var totalDeltaSeconds: Number = serverDeltaSeconds+m_latency;
			m_timeCompensation = totalDeltaSeconds;
			//MConsole.Write("latency: ", Misc.ConvertToFixedPoint(m_latency,1000));
			//MConsole.Write("time compensation: ", Misc.ConvertToFixedPoint(m_timeCompensation,1000));		 
		}				
		 
		private function OnIncomingData(event:DataEvent):void
		{
			//trace("[" + event.type + "] " + event.data);
			var i: int;
			_dataReceived += event.data;
			var regExp: RegExp = /\0/g;
			_dataReceived = _dataReceived.replace(regExp, "");
			var strings: Array = _dataReceived.split("\n");
			var lastMsg: int = strings.length - 1;
			if (String(strings[lastMsg]).length != 0)
			{
				_dataReceived += String(strings[lastMsg]);
			}
			else
			{
				_dataReceived = "";
			}
			
			for (i = 0; i < lastMsg; i++)
			{
				var m: MessageContainer = Message.Get(String(strings[i]));
				if (m.n === MessageNames.kTime)
				{
					ProcessTimeMessage(m);
				}
				else if (m.n === MessageNames.kGameMessage)
				{
					if (m.s === MessageNames.ksGameStart)
					{
						Controller.OnNewGame(m.m_data.gameId);
						
						MyWorld.gWorld.m_socialProfile = new SocialProfile();
						MyWorld.gWorld.m_socialProfile.InitProfileFromFullProfileData(m.m_data.yourProfile);
						MyWorld.gWorld.m_gameProfile = new GameProfile(m.m_data.yourProfile);
						
						MyWorld.gWorld.m_enemySocialProfile = new SocialProfile();
						MyWorld.gWorld.m_enemySocialProfile.InitProfileFromFullProfileData(m.m_data.enemyProfile);
						MyWorld.gWorld.m_enemyGameProfile = new GameProfile(m.m_data.enemyProfile);
						
						MyWorld.gWorld.InitializeOnGameStart();
					}
					else if (m.s === MessageNames.ksEndGame)
					{
						MyWorld.gWorld.m_gameProfile = new GameProfile(m.m_data.yourProfile);
						MyWorld.gWorld.InitializeAfterGame();
						MyWorld.gWorld.OnGameMessage(m);
					}
					if (m.s === MessageNames.ksCheckField)
					{
						if (m.m_data.match == true)
							MConsole.Write("##", "your field matches the server's");
						else
							MConsole.Write("##", "your field doesn't match the server's");
					}
					else
					{
						MyWorld.gWorld.OnGameMessage(m);
					}
				}
				else if (m.n === MessageNames.kCheater)
				{
					MConsole.Write("_ii","YOU ARE A CHEATER!");
					MConsole.showConsole = true;
				}
				else if (m.n === MessageNames.kServerInfo)
				{
					MConsole.Write("##","you are connected to server");
					MConsole.Write("##","ip: ", m.m_data.ip);
					MConsole.Write("##","port: ", m.m_data.port);
					MConsole.Write("##", "players online: ", m.m_data.numberOfPlayers);
					MConsole.Write("##", m.m_data.numberOfGames, "games are currently being played");
					MConsole.Write("##", m.m_data.lookingForGame, "players are currently looking for game");
					if (m.m_data.lookingForGame == 1)
					{
						MConsole.Write("##", "it appears server can't find you a match...");
					}
				}
				//else if (m.s == MessageNames.ksCheckField)
				//{
					//if (m.m_data.match == true)
						//MConsole.Write("##", "your field matches the server's");
					//else
						//MConsole.Write("##", "your field doesn't match the server's");
				//}
				else if (m.n === MessageNames.kListPlayers)
				{
					var numberOfPlayers: int = m.m_data.length;
					for (i = 0; i < numberOfPlayers; i++)
					{
						MConsole.Write("##"," ");
						if (m.m_data[i].inGame == null)						
							MConsole.Write("##", "not in game");
						else
							MConsole.Write("##", "in game #", m.m_data[i].inGame);								
						MConsole.Write("##", "ip: ", m.m_data[i].ip, ":", m.m_data[i].port);
						MConsole.Write("##", "player id: ", m.m_data[i].id);				
					}
				}
				else if (m.n === MessageNames.kServerMessage)
				{
					MConsole.Write("_ii","server:", m.m_data);
					MConsole.showConsole = true;
				}
				else if (m.n === MessageNames.kConfirmConnect)
				{
					MyWorld.gWorld.m_gameProfile = new GameProfile(m.m_data.profile);
					Preferences.Initialize(m.m_data);
					MyWorld.gWorld.Initialize();
					MyWorld.gWorld.SetPhase(MyWorld.PHASE_IN_PROFILE);
				}
			}
		}
		
		
		private function GetVKInfo(): void
		{
			
			//var url:String = "https://api.vk.com/method/users.get?user_id=3373076&v="+vkApiVersion;
			var url:String = "https://api.vk.com/method/users.get?user_id=" + String(MyWorld.gWorld.flashVars.viewer_id)+"&v="+vkApiVersion;
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.GET;

			var variables:URLVariables = new URLVariables();

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, OnGetVkInfoComplete);
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.load(request);			
		}
		
		private function OnGetVkInfoComplete(event: Event)
		{
			var obj: Object = JSON.parse(event.target.data);
			var name: String = obj.response[0].first_name;
			var lastName: String = obj.response[0].last_name;
			MConsole.Write("_ii", "Your name: ", name + " " + lastName);
			MyWorld.gWorld.m_socialProfile.InitProfile("vk", name, lastName, MyWorld.gWorld.flashVars.viewer_id);
			Start();
		}
		
		//private function AuthorizeVK(): void
		//{
			////var url:String = "https://oauth.vk.com/authorize?client_id=4807149&scope=notify&redirect_uri=https://oauth.vk.com/blank.html&display=popup&v=5.28&response_type=token";
			//var url:String = "https://oauth.vk.com/authorize"
			//var request:URLRequest = new URLRequest(url);
			//request.method = URLRequestMethod.GET;
//
			//var variables:URLVariables = new URLVariables();
			//variables.client_id = "4807149";
			//variables.scope = "notify";
			//variables.redirect_uri = "https://oauth.vk.com/blank.html";
			//variables.display = "popup";
			//variables.v = "5.28"
			//variables.response_type = "token";
			//request.data = variables;
//
			//var loader:URLLoader = new URLLoader();
			//loader.addEventListener(Event.COMPLETE, OnVkRequestComplete);
			//loader.dataFormat = URLLoaderDataFormat.TEXT;
			//loader.load(request);
		//}
		//
		//private function OnVkRequestComplete(event: Event): void
		//{
			//
		//}
		
	}

}