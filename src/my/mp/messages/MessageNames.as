package my.mp.messages 
{
	/**
	 * ...
	 * @author Aikei
	 */

	public class MessageNames
	{
		static public const kTime: String = "a";
		static public const kCreatePlayer: String = "b";
		static public const kReady: String = "c";
		static public const kCheater: String = "cheat";
		static public const kServerInfo: String = "si";
		static public const kListPlayers: String = "lp";
		static public const kServerMessage: String = "k";
		static public const kFindMatch: String = "n";
		static public const kPreferences: String = "prefs";
		static public const kFirstConnect: String = "x";
		static public const kConfirmConnect: String = "v";
		
		static public const kLobbyMessage: String = "s";
		
			static public const ksPickAbility: String = "t";
			static public const ksUnpickAbility: String = "u";
		
		static public const kGameMessage: String = "gm";
		
			static public const ksBallSelected: String = "d";
			static public const ksGameStart: String = "gs";
			static public const ksTurnDone: String = "e";
			static public const ksLastBallDeselected: String = "f";
			static public const ksDeselectAll: String = "g";
			static public const ksRemoveBalls: String = "h";
			static public const ksNewEntropy: String = "i";
			static public const ksNextTurn: String = "j";		
			static public const ksCheckField: String = "chkf";
			static public const ksEndGame: String = "l";
			static public const ksRefreshPlayerData: String = "m";
			static public const ksAbilityUsed: String = "o";
			static public const ksSelectedAbilityTargets: String = "p";
			static public const ksDeselectedAbilityTargets: String = "q";
			static public const ksAbilityButtonActivated: String = "r";
			static public const ksNoMoves: String = "w";
			static public const ksDetargetAll: String = "y";
	}
		
}