package my.mp.messages 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class MessageContainer
	{		
		/** Message name */
		public var n:String;
 
		/** Message string */
		public var s:String;
 
		/** Message data - to be filled in by client */
		public var m_data:*;		
	}

}