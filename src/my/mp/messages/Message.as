package my.mp.messages {
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.net.*;
	import flash.utils.*;
	 
	import my.system.Helpers;
	import my.mp.Multiplayer;	

	
	public class Message
	{ 
		/**
		 * Deserialise a message from a string
		 * 
		 * @param messageString
		 * @return MessageContainer
		 *
		 */			
		static public function Get(messageString:String): MessageContainer
		{
			var containerA: Object = Multiplayer.DeSerialize(messageString);
			
			//if (containerA.s != undefined)
			//{
				//containerA.m_data = Multiplayer.DeSerialize(containerA.s);
			//}
 
			// clone into concrete type
			var container: MessageContainer = Helpers.CloneIntoR(containerA, MessageContainer);
 
			return container;
		}
		
		static public function CreateDataMessage(t: String, str: String, data:*): MessageContainer
		{
			var messageContainer: MessageContainer = new MessageContainer;
			messageContainer.n = t;
			messageContainer.s = str;
			messageContainer.m_data = data;
			return messageContainer;
		}
		 
	}
}