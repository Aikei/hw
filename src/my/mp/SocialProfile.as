package my.mp 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class SocialProfile 
	{
		public var type: String = "test";
		public var name: String = "test";
		public var lastName: String = "test";
		public var id: uint = 0;
		
		public function SocialProfile() 
		{

		}
		
		public function InitProfile(type: String, name: String, lastName: String, id: uint)
		{
			this.type = type;
			this.name = name;
			this.lastName = lastName;
			this.id = id;
		}
		
		public function InitProfileFromFullProfileData(data:*)
		{
			this.type = data.socialProfile.type;
			this.name = data.socialProfile.name;
			this.lastName = data.socialProfile.lastName;
			this.id = data.socialProfile.id;			
		}
		
	}

}