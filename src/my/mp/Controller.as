package my.mp 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	import my.logic.abilities.Ability;
	import my.logic.newability.NewAbility;
	import my.logic.PlayerData;
	import my.mp.messages.*;
	import my.GameEntity;
	import my.MyWorld;
	import my.Vec2;
	import starling.events.Event;
	
	public class Controller 
	{
		public static var m_multiplayer: Multiplayer;
		public static var m_gameId: int = 0;
		
		public function Controller() 
		{
			m_multiplayer = new Multiplayer();
		}
		
		static public function OnNewGame(gameId: int): void
		{
			m_gameId = gameId;
		}
		
		static public function ActivateAbilityButton(ability: Ability): void
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksAbilityButtonActivated, { gameId: m_gameId, abilityType : ability.type } );
			m_multiplayer.SerializeAndSend(m);			
		}
		
		static public function ActivateNewAbilityButton(ability: NewAbility): void
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksAbilityButtonActivated, { gameId: m_gameId, abilityType : ability.type } );
			m_multiplayer.SerializeAndSend(m);			
		}		
		
		static public function SelectBall(ball: Ball): void
		{
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksBallSelected, { gameId : m_gameId, selectedBall : { x: ball.m_index.x, y: ball.m_index.y } } );			
			m_multiplayer.SerializeAndSend(msg);
		}
		
		static public function DeselectBall(deselectedBallPos: Vec2): void
		{		
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksLastBallDeselected, { gameId: m_gameId, deselectedBall : { x: deselectedBallPos.x, y: deselectedBallPos.y } } );			
			m_multiplayer.SerializeAndSend(msg);			
		}
		
		static public function RemoveBalls(balls: Vector.<Ball>): void
		{
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksRemoveBalls, { gameId: m_gameId, balls: [] } );
			for (var i: int = 0, l: int = balls.length; i < l; i++)
			{					
				msg.m_data.balls.push( { x : balls[i].m_index.x, y : balls[i].m_index.y } );					
			}
			m_multiplayer.SerializeAndSend(msg);			
		}
		
		static public function UseNewAbility(ability: NewAbility): void
		{
			
		}
		
		static public function UseAbility(ability: Ability, targets : Vector.<GameEntity> , allTargets : Vector.<GameEntity>, a_args: Array): void
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksAbilityUsed, 
						{ gameId: m_gameId, abilityType: ability.type, targets: [], allTargets: [], args: a_args } );
			if (targets)
			{
				for (var i: int = 0; i < targets.length; i++)
				{
					var obj = { x: targets[i].m_index.x, y: targets[i].m_index.y, type: "b" }; //b = ball
					if (targets[i] is PlayerData)
					{
						obj.type = "p"; //p = player
						if (obj.number = PlayerData(targets[i]).isEnemy)
							obj.number = MyWorld.gWorld.m_logic.enemyNumber;							
						else
							obj.number = MyWorld.gWorld.m_logic.myNumber;
					}
					m.m_data.targets.push(obj);
				}
			}
			if (allTargets)
			{
				for (i = 0; i < allTargets.length; i++)
				{
					obj = { x: allTargets[i].m_index.x, y: allTargets[i].m_index.y, type: "b" }; //b = ball
					if (allTargets[i] is PlayerData)
					{
						obj.type = "p"; //p = player
						if (obj.number = PlayerData(allTargets[i]).isEnemy)
							obj.number = MyWorld.gWorld.m_logic.enemyNumber;							
						else
							obj.number = MyWorld.gWorld.m_logic.myNumber;
					}					
					m.m_data.allTargets.push(obj);
				}				
			}
			m_multiplayer.SerializeAndSend(m);			
		}
		
		static public function NewHighlightAbilityTargets(targets: Array): void
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksSelectedAbilityTargets, { } );
			m.m_data.gameId = m_gameId;
			m.m_data.targets = [];			
			for (var i: int = 0, l: int = targets.length; i < l; i++)
			{
				targets[i].SetTargeted(true);
				m.m_data.targets.push( { x: GameEntity(targets[i]).m_index.x, y: GameEntity(targets[i]).m_index.y } );
			}
			m_multiplayer.SerializeAndSend(m);			
		}
		
		static public function NewDehighlightAbilityTargets(targets: Array)
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksDeselectedAbilityTargets, { } );
			m.m_data.gameId = m_gameId;
			m.m_data.targets = [];					
			for (var i: int = 0, l: int = targets.length; i < l; i++)
			{
				targets[i].SetTargeted(false);
				m.m_data.targets.push( { x: GameEntity(targets[i]).m_index.x, y: GameEntity(targets[i]).m_index.y } );
			}
			m_multiplayer.SerializeAndSend(m);		
		}		
		
		static public function HighlightAbilityTargets(targets: Vector.<GameEntity>): void
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksSelectedAbilityTargets, { } );
			m.m_data.gameId = m_gameId;
			m.m_data.targets = [];			
			for (var i: int = 0, l: int = targets.length; i < l; i++)
			{
				targets[i].SetTargeted(true);
				m.m_data.targets.push( { x: targets[i].m_index.x, y: targets[i].m_index.y } );
			}
			m_multiplayer.SerializeAndSend(m);			
		}
		
		static public function DehighlightAbilityTargets(targets: Vector.<GameEntity>)
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksDeselectedAbilityTargets, { } );
			m.m_data.gameId = m_gameId;
			m.m_data.targets = [];					
			for (var i: int = 0, l: int = targets.length; i < l; i++)
			{
				targets[i].SetTargeted(false);
				m.m_data.targets.push( { x: targets[i].m_index.x, y: targets[i].m_index.y } );
			}
			m_multiplayer.SerializeAndSend(m);		
		}
		
		static public function RequestServerInfo(): void
		{
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kServerInfo, null, null)
			m_multiplayer.SerializeAndSend(msg);			
		}
		
		static public function RequestCheckField(balls2D: Vector.<Vector.<Ball>>): void
		{
			var li: int = balls2D.length;
			var sendArray: Array = new Array(li);
			for (var i = 0; i < li; i++)
			{
				var lj: int = balls2D[i].length;
				sendArray[i] = new Array(lj);
				for (var j = 0; j < lj; j++)
				{
					var ball: Ball = balls2D[i][j];							
					sendArray[i][j] = {	x : i, y: j, type: ball.m_type }
				}
			}
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksCheckField, sendArray);
			m_multiplayer.SerializeAndSend(msg);			
		}
		
		static public function RequestListPlayers(): void
		{
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kListPlayers, null, null);
			m_multiplayer.SerializeAndSend(msg);			
		}
		
		static public function SendMessageToServer(n:String, s: String, data: * = undefined): void
		{
			var msg: MessageContainer = Message.CreateDataMessage(n, s, data);
			m_multiplayer.SerializeAndSend(msg);
		}
		
		static public function FindMatch()
		{
			var msg : MessageContainer = Message.CreateDataMessage(MessageNames.kFindMatch, undefined, undefined);
			m_multiplayer.SerializeAndSend(msg);			
		}
		
		static public function DeselectAll()
		{
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksDeselectAll, { gameId : m_gameId } );
			m_multiplayer.SerializeAndSend(msg);		
		}
		
		static public function PickAbility(ability: Ability)
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kLobbyMessage, MessageNames.ksPickAbility, { } );
			m.m_data.abilityName = ability.type;
			m_multiplayer.SerializeAndSend(m);
		}
		
		static public function UnpickAbility(ability: Ability)
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kLobbyMessage, MessageNames.ksUnpickAbility, { } );
			m.m_data.abilityName = ability.type;
			m_multiplayer.SerializeAndSend(m);
		}
		
		static public function DetargetAll()
		{
			var msg: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksDetargetAll, { gameId : m_gameId } );
			m_multiplayer.SerializeAndSend(msg);				
		}
	}

}