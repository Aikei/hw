package my.mp 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class GameProfile 
	{
		public var gamesPlayed: int = 0;
		public var gamesWon: int = 0;
		public var gamesLost: int = 0;
		public var elo: Number = 1000;
		
		public function GameProfile(messageData:*) 
		{
			InitProfile(messageData);
		}
		
		public function InitProfile(messageData:*): void
		{
			gamesPlayed = messageData.gamesPlayed;
			gamesWon = messageData.gamesWon;
			gamesLost = messageData.gamesLost;
			elo = messageData.elo;			
		}
	}

}