package my 
{
	/**
	 * ...
	 * @author Aikei
	 */

	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	import my.system.Classes;
	
	public class MyAssets 
	{
		//GlobalEffectFreezingRain;
		
		private static var _textureAtlases : Dictionary = new Dictionary;
		private static var _textures : Dictionary = new Dictionary;
		
		public static var additionalClasses = new Classes;
		
		[Embed(source = "../../media/NotoSerif-Regular.ttf", embedAsCFF = "false", fontFamily = "Regular")] public static const FONT:Class;
		[Embed(source="../../media/NotoSerif-Bold.ttf", embedAsCFF="false", fontFamily="Bold")] public static const BOLD_FONT:Class;		
		[Embed(source = 'ast/selected.png')] public static const SELECTOR_IMAGE:Class;
		//[Embed(source = '../../media/buttonLong_beige.png')] public static const BEIGE_BUTTTON:Class;
		[Embed(source = '../../media/buttonLong_beige_pressed.png')] public static const BEIGE_BUTTON:Class;
		
		[Embed(source = '../../media/stonesAtlas.png')] private static const STONES_TEXTURE:Class;
		[Embed(source = '../../media/ui.png')] private static const UI_TEXTURE:Class;
		[Embed(source = '../../media/metalworks_desktop.png')] private static const METALWORKS_UI_TEXTURE:Class;
		[Embed(source = '../../media/aeon_desktop.png')] private static const AEON_UI_TEXTURE:Class;
		[Embed(source = '../../media/lobby.png')] private static const LOBBY_TEXTURE:Class;
		[Embed(source = '../../media/effects.png')] private static const EFFECTS_TEXTURE:Class;
		[Embed(source = '../../media/abilityReady.png')] private static const ABILITY_READY_TEXTURE:Class;
		[Embed(source = '../../media/portraits.png')] private static const PORTRAITS_TEXTURE:Class;
		
		[Embed(source="../../media/stonesAtlas.xml", mimeType="application/octet-stream")] private static const STONES_ATLAS:Class;
		[Embed(source = "../../media/ui.xml", mimeType = "application/octet-stream")] private static const UI_ATLAS:Class;
		[Embed(source = "../../media/lobby.xml", mimeType = "application/octet-stream")] private static const LOBBY_ATLAS:Class;
		[Embed(source = "../../media/effects.xml", mimeType = "application/octet-stream")] private static const EFFECTS_ATLAS:Class;
		[Embed(source = "../../media/abilityReady.xml", mimeType = "application/octet-stream")] private static const ABILITY_READY_ATLAS:Class;
		[Embed(source = "../../media/portraits.xml", mimeType = "application/octet-stream")] private static const PORTRAITS_ATLAS:Class;
		
		[Embed(source = "../../media/aeon_desktop.xml", mimeType = "application/octet-stream")] public static const AEON_UI_ATLAS:Class;
		[Embed(source = "../../media/metalworks_desktop.xml", mimeType = "application/octet-stream")] public static const METALWORKS_UI_ATLAS:Class;
		//particles
		[Embed(source="../../media/starParticle.pex", mimeType="application/octet-stream")] public static const BALL_DESTROY_PARTICLE_CONFIG:Class;		 
		[Embed(source = "../../media/starTexture.png")] public static const BALL_DESTROY_PARTICLE_TEXTURE:Class;		
		
		[Embed(source="../../media/whiteParticle.pex", mimeType="application/octet-stream")] public static const BALL_DELETE_PARTICLE_CONFIG:Class;				
		[Embed(source = "../../media/whiteParticleTexture.png")] public static const BALL_DELETE_PARTICLE_TEXTURE:Class;	
		
		// instantiate embedded objects
		
		public static var BallPsConfig:XML;
		public static var BallPsTexture:Texture;	

		//public static const REAL_STONES_ATLAS: TextureAtlas = new TextureAtlas(Texture.fromBitmap(new STONES_TEXTURE), XML(STONES_ATLAS));
		public static var StonesAtlas: TextureAtlas;
		public static var UiAtlas: TextureAtlas;
		public static var LobbyAtlas: TextureAtlas;
		public static var ThemeAtlas: TextureAtlas;
		public static var AbiltiyReadyAtlas: TextureAtlas;
		public static var EffectsAtlas: TextureAtlas;
		public static var PortraitsAtlas: TextureAtlas;
		public static var BallTypeNames: Vector.<String> = new Vector.<String>;
		public static var SelectedBallTypeNames: Vector.<String> = new Vector.<String>;
		
		public static function InitAssests()
		{
			for (var i = 0; i < Ball.NUMBER_OF_TYPES; i++)
			{
				if (i === Ball.BLUE_BALL)
				{
					BallTypeNames.push("blue");
					SelectedBallTypeNames.push("blue_s");
				}
				else if (i === Ball.PURPLE_BALL)
				{
					BallTypeNames.push("purple");
					SelectedBallTypeNames.push("purple_s");
				}
				else if (i === Ball.RED_BALL)
				{
					BallTypeNames.push("red");
					SelectedBallTypeNames.push("red_s");					
				}
				else if (i === Ball.GREEN_BALL)
				{
					BallTypeNames.push("green");
					SelectedBallTypeNames.push("green_s");
				}
				else if (i === Ball.BLACK_BALL)
				{
					BallTypeNames.push("black");
					SelectedBallTypeNames.push("black_s");					
				}
				else if (i === Ball.YELLOW_BALL)
				{
					BallTypeNames.push("yellow");
					SelectedBallTypeNames.push("yellow_s");								
				}
			}
			var texture: Texture  = GetTexture("STONES_TEXTURE");
			var xml: XML = XML(new STONES_ATLAS);
			StonesAtlas = new TextureAtlas(texture, xml);
			
			texture  = GetTexture("UI_TEXTURE");
			xml = XML(new UI_ATLAS);
			UiAtlas = new TextureAtlas(texture, xml);
			
			texture = GetTexture("METALWORKS_UI_TEXTURE");
			xml = XML(new METALWORKS_UI_ATLAS);
			//texture = GetTexture("AEON_UI_TEXTURE");
			//xml = XML(new AEON_UI_ATLAS);			
			ThemeAtlas = new TextureAtlas(texture, xml);
			
			texture = GetTexture("LOBBY_TEXTURE");
			xml = XML(new LOBBY_ATLAS);
			LobbyAtlas = new TextureAtlas(texture, xml);
			
			texture = GetTexture("EFFECTS_TEXTURE");
			xml = XML(new EFFECTS_ATLAS);
			EffectsAtlas = new TextureAtlas(texture, xml);
			
			texture = GetTexture("ABILITY_READY_TEXTURE");
			xml = XML(new ABILITY_READY_ATLAS);
			AbiltiyReadyAtlas = new TextureAtlas(texture, xml);

			texture = GetTexture("PORTRAITS_TEXTURE");
			xml = XML(new PORTRAITS_ATLAS);
			PortraitsAtlas = new TextureAtlas(texture, xml);					
		}
		
		public static function GetTextureByBallType(type: int, selected: Boolean): Texture
		{
			if (!selected)
				return StonesAtlas.getTexture(BallTypeNames[type]);
			else
				return StonesAtlas.getTexture(SelectedBallTypeNames[type]);
		}
			
		
		//public static function GetTextureAtlas(textureName: String, atlasName: String) : TextureAtlas
		//{
			//if (_textureAtlases[atlasName] == undefined)
			//{
				//var texture: Texture = GetTexture(textureName);
				//var xml: XML = XML(new MyAssets[atlasName]());
				//_textureAtlases[atlasName] = new TextureAtlas(texture, xml);
			//}
			//return _textureAtlases[atlasName];
		//}
		
		public static function GetTexture(textureName: String): Texture
		{
			if (_textures[textureName] == undefined)
			{
				_textures[textureName] = Texture.fromBitmap(new MyAssets[textureName]());
			}
			return _textures[textureName];
		}

		
	}

}