package my 
{
	import my.logic.Logic;
	import starling.textures.Texture;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	import my.GameEntity;
	
	import my.traits.BasicSpriteTrait;
	import my.*;
	import my.view.Visual;
	import my.dbg.MConsole;
	import my.logic.effects.Effect;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureAtlas;
	import starling.events.*;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import com.greensock.TweenMax;
	import com.greensock.TweenLite;

	import starling.extensions.PDParticleSystem;
	import starling.extensions.ParticleSystem;
	import starling.core.Starling;
	import starling.utils.Color;
	import starling.display.Quad;
	import my.logic.globaleffects.GlobalEffect;
	import my.logic.effects.EffectFrozen;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class Ball extends GameEntity 
	{
		public static const BLUE_BALL:int = 0;
		public static const PURPLE_BALL:int = 1;
		public static const RED_BALL:int = 2;
		public static const GREEN_BALL:int = 3;
		public static const BLACK_BALL: int = 4;
		public static const YELLOW_BALL: int = 5;
		
		public static const NUMBER_OF_MANAS: int = 4;
		public static const NUMBER_OF_TYPES:int = 6;
		
		public static const BALL_SPECIAL_TYPE_HEALTH = 1;
		public static const BALL_SPECIAL_TYPE_ATTACK = 2;
		
		public static const BALL_WIDTH:Number = 64;
		public static const BALL_HEIGHT:Number = 64;
		public static const BALL_HALF_WIDTH: Number = BALL_WIDTH / 2;
		public static const BALL_HALF_HEIGHT: Number = BALL_HEIGHT / 2;
		public static const BALL_DIAGONAL: Number = Math.sqrt(BALL_WIDTH * BALL_WIDTH + BALL_HEIGHT * BALL_HEIGHT);
		public static const BALL_SQUARED_DIAGONAL: Number = BALL_WIDTH * BALL_WIDTH + BALL_HEIGHT * BALL_HEIGHT;
		public static const BALL_SPACING:Number = 16;	
		
		public static const HOLDER_BASE_WIDTH: Number = 68;
		public static const HOLDER_BASE_HEIGHT: Number = 68;
		public static const BALL_BASE_WIDTH: Number = 52;
		public static const BALL_BASE_HEIGHT: Number = 52;
		
		
		public var m_type: int = -1;
		public var m_specialType: int = 0;
		public var m_selected: Boolean = false;
		public var m_bMoving: Boolean = false;	
		//public var m_index: Vec2 = new Vec2;
		
		private var m_bAlive: Boolean = true;		
		private var m_speed: Number = 100;
		private var m_destination: Vec2 = new Vec2;
		private var m_velocity: Velocity = new Velocity;
		private var m_selector: Visual = null;
		private var _basicTrait: BasicSpriteTrait;
		private var _oldImage: Image = null;
		private var _currentImage: Image = null;
		
		private var _holderImage: Image = null;
		private var _holderNormalTexture: Texture = MyAssets.StonesAtlas.getTexture("bg");
		private var _holderEnemyEffectTexture: Texture = MyAssets.EffectsAtlas.getTexture("enemy_effect_holder");
		public var damage: int = 1;
		
		public function Ball(type:int, id:int, world:MyWorld, index: Vec2, x: Number, y: Number, specialType: int = 0)
		{
			super(id, x, y);
			m_specialType = specialType;
			m_index = index;			
			//_textureAtlas = MyAssets.GetTextureAtlas("STONES_TEXTURE", "STONES_ATLAS");
			_currentImage = new Image(MyAssets.StonesAtlas.getTexture("red"));
			
			_holderImage = new Image(_holderNormalTexture);
			//_holderImage.scaleX = scaleWidth / _currentImage.width;
			//_holderImage.scaleY = scaleHeight / _currentImage.height;
			
			//_holderImage.scaleX = 2.0;
			//_holderImage.scaleY = 2.0;
			
			_basicTrait = new BasicSpriteTrait(this);
			InitBall(type, x, y);
			addEventListener(Event.ENTER_FRAME, Update);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);			
		}
		
		public function OnAddedToStage(ev: Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_holderImage);
			addChild(_currentImage);
			pivotX = _currentImage.width / 2;
			pivotY = _currentImage.height / 2;
			
			_holderImage.x = _currentImage.x + _currentImage.width / 2;
			_holderImage.y = _currentImage.y + _currentImage.height / 2;
			_holderImage.alignPivot();
			if (MyWorld.gWorld.m_logic.IsGlobalEffectPresent(GlobalEffect.FREEZING_RAIN))
			{
				if (MyWorld.gWorld.m_logic.turnPhase === Logic.TURN_PHASE_PASSING_TURN)
					AddEffect(new EffectFrozen(1, 0.01));
				else
					AddEffect(new EffectFrozen(2, 0.01));
			}
			//m_particleSystem = new PDParticleSystem(MyAssets.psConfig, MyAssets.psTexture);
			//Starling.juggler.add(m_particleSystem);
			//addChild(m_particleSystem);

			//m_particleSystem.emitterX = width / scaleX / 2;
			//m_particleSystem.emitterY = height / scaleY / 2;
			//m_particleSystem.width = 64;
			//m_particleSystem.height = 64;			
			//m_particleSystem.startSize = 16;
			//m_particleSystem.endSize = 32;
		}
		
		public function SetInvisible(invisible: Boolean)
		{
			if (invisible)
			{
				this._currentImage.alpha = 0;
			}
		}
		
		//public function OnTouchEvent(ev: TouchEvent): void
		//{
			//var touch: Touch = ev.getTouch(this);
			//if (touch != null)
			//{
				//if (touch.phase == TouchPhase.STATIONARY)
					//MConsole.Write("Stationary touch 1!");
				//var localPos: Point = touch.getLocation(this);
				//MConsole.Write("Touch local pos: ", localPos.x, localPos.y);
				//localPos = localPos.subtract(new Point(width/scaleX/2, height/scaleY/2));
				//MyWorld.gWorld.m_logic.OnBallTouch(this, localPos.length);
			//}
			//else
			//{
				//touch = ev.getTouch(this, TouchPhase.STATIONARY);
				//if (touch != null)
					//MConsole.Write("Stationary touch 2!");
			//}
		//}
		
		public function Start(): void
		{
			
		}
		
		//public function destroy(): void
		//{
			////removeEventListener(Event.ENTER_FRAME);
			//removeFromParent(true);
		//}
		
		private function SetAnimation(selectedAnim: Boolean): void
		{
			if (!selectedAnim)
			{
				if (m_specialType === 0)
				{
					switch (m_type)
					{
						case BLUE_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("blue");
							break;
						case PURPLE_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("purple");
							break;
						case RED_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("red");
							break;						
						case GREEN_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("green");
							break;
						case BLACK_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("black");
							break;
						case YELLOW_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("yellow");
							break;							
					}
				}
				else if (m_specialType === BALL_SPECIAL_TYPE_HEALTH)
				{
					_currentImage.texture = MyAssets.StonesAtlas.getTexture("heal");
				}
				else if (m_specialType === Ball.BALL_SPECIAL_TYPE_ATTACK)
				{
					_currentImage.texture = MyAssets.StonesAtlas.getTexture("atk");
				}
			}
			else
			{
				if (m_specialType === 0)
				{
					switch (m_type)
					{
						case BLUE_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("blue_s");
							break;
						case PURPLE_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("purple_s");
							break;
						case RED_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("red_s");
							break;
						case GREEN_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("green_s");
							break;
						case BLACK_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("black_s");
							break;
						case YELLOW_BALL:
							_currentImage.texture = MyAssets.StonesAtlas.getTexture("yellow_s");
							break;									
					}
				}
				else if (m_specialType === BALL_SPECIAL_TYPE_HEALTH)
				{
					_currentImage.texture = MyAssets.StonesAtlas.getTexture("heal_s");
				}
				else if (m_specialType === Ball.BALL_SPECIAL_TYPE_ATTACK)
				{
					_currentImage.texture = MyAssets.StonesAtlas.getTexture("atk_s");
				}				
			}
			//_currentImage = new Image(MyAssets.GetTexture("BEIGE_BUTTON"));
			_currentImage.color = this.color;
		}
		
		public function InitBall(a_type:int, x:Number, y:Number): void
		{
			_basicTrait.Init(x, y);
			SetSelected(false);
			m_bAlive = true;
			//if (m_type != a_type)
			//{				
				m_type = a_type;
				SetAnimation(false);
				ScaleTo(BALL_BASE_WIDTH, BALL_BASE_HEIGHT);
				//ScaleTo(64, 64);				
			//}
			//alignPivot();
			//pivotX = _currentImage.width / 2;
			//pivotY = _currentImage.height / 2;			
			//animate("idle");
		}
		
		override public function ScaleTo(scaleWidth: Number, scaleHeight: Number): void
		{
			scaleX = scaleWidth / _currentImage.width;
			scaleY = scaleHeight / _currentImage.height;
			_holderImage.scaleX = 1 / scaleX;
			_holderImage.scaleY = 1 / scaleY;
		}
		
		public function SetSelected(selected: Boolean): void
		{
			if (selected && !m_selected)
			{
				SetAnimation(true);
				//TweenLite.to(this,1,
				_basicTrait.AddTween(MyWorld.gWorld.m_gameScreen.TweenScaleBall(this, 0.8, 0.5));
				_basicTrait.AddMaxTweensVector(Tweens.TweenShake(this));
				//_basicTrait.AddTweensVector(MyWorld.gWorld.m_gameScreen.TweenScaleBall(this, 0.8, 0.5));
				//m_velocity.rotation = Misc.PI / 8;
				//velocity.a = 20;
				//m_selector = m_world.CreateVisual(MyAssets.SELECTOR_IMAGE, globalX, globalY, 64, 64);
			}
			else if (!selected && m_selected)
			{
				_basicTrait.ReturnAndKillAllTweens();
				//m_velocity.rotation = 0;
				//Tweens.TweenAngle(this, 0.5, 0);
				SetAnimation(false);
			}
			m_selected = selected;
		}
		
		public function IsSelected(): Boolean
		{
			return m_selected;
		}
		
		public function IsAlive(): Boolean
		{
			return m_bAlive;
		}
		
		public function SetType(type: int): void
		{
			m_type = type;
			SetAnimation(false);
		}
		
		public function TweenToType(type: int): void
		{
			m_type = type;
			_oldImage = _currentImage;
			_currentImage = new Image(MyAssets.GetTextureByBallType(type, false));					
			_currentImage.alpha = 0;
			TweenLite.to(_oldImage, 1, { alpha : 0 } );
			TweenLite.to(_currentImage, 1, { alpha : 1 } );
			addChild(_currentImage);
			setTimeout(_oldImage.removeFromParent, 1000, true);
		}
		
		public function IsMoving(): Boolean
		{
			return m_bMoving;
		}		
		
		public function MyMoveTo(targetX: Number, targetY: Number, speedMultiplier:Number = 1): void
		{
			m_destination.x = targetX;
			m_destination.y = targetY;
			m_velocity.CopyFrom(m_destination);
			m_velocity.x -= x;
			m_velocity.y -= y;
			m_velocity.Normalize();
			m_velocity.Scale(m_speed * speedMultiplier);
			//velocity.x = m_velocity.x;
			//velocity.y = m_velocity.y;
			m_bMoving = true;
		}		
		
		public function OnDestroy(): void
		{
			m_bAlive = false;
			SetSelected(false);
		}
		
		override public function set color(clr: uint): void
		{
			super.color = clr;
			_currentImage.color = clr;
		}		
				
		public function Update():void
		{
			if (m_velocity.rotation != 0)
			{
				rotation += m_velocity.rotation * MyWorld.gWorld.m_elapsed;
			}
			
			if (m_bMoving)
			{
				x += m_velocity.x*MyWorld.gWorld.m_elapsed;
				y += m_velocity.y * MyWorld.gWorld.m_elapsed;				
				//if (Misc.GetSquaredDistance(x, y, m_destination.x, m_destination.y) <= 25)
				if (!Misc.IsMovingToDestination(new Vec2(x, y),m_destination,m_velocity))
				{
					x = m_destination.x;
					y = m_destination.y;
					m_bMoving = false;
					m_velocity.x = 0;
					m_velocity.y = 0;
				}
			}						
			
			//TO DO
			//if (hover())
			//{
				//m_world.m_logic.OnBallHover(this);
			//}
							
		}
		
		override public function SetTargeted(targeted: Boolean): void
		{
			if (_targeted === targeted)
				return;
			_targeted = targeted;
			if (targeted)
			{
				if (_targetQuad)
					_targetQuad.removeFromParent(true);
				_targetQuad = new Quad(_currentImage.width, _currentImage.height, Color.GREEN);
				//_targetQuad.alignPivot();
				_targetQuad.alpha = 0.5;
				//_targetQuad.alignPivot();
				addChild(_targetQuad);				
			}
			else
			{
				if (_targetQuad)
					_targetQuad.removeFromParent(true);
			}
		}
		
		static public function GetBallColorByType(type: int): uint
		{
			if (type === Ball.RED_BALL)
				return Color.RED;
			else if (type === Ball.BLUE_BALL)
				return Color.BLUE;
			else if (type === Ball.YELLOW_BALL)
				return Color.YELLOW;
			else if (type === Ball.PURPLE_BALL)
				return Color.PURPLE;
			else if (type === Ball.GREEN_BALL)
				return Color.GREEN;
			else if (type === Ball.BLACK_BALL)
				return Color.BLACK;
			return Color.WHITE;
		}
		
		public function HasOwningEffect(): Boolean 
		{
			for (var i: int = 0; i < _effects.length; i++)
			{
				if (_effects[i].owningEffect)
				{
					return true;
				}
			}
			return false;
		}
		
		override public function AddEffect(effect: Effect): void
		{
			super.AddEffect(effect);
			if (effect.owner === Effect.OWNER_ENEMY)
			{
				if (effect.hidden !== Effect.HIDDEN_FOR_YOU)
					_holderImage.texture = _holderEnemyEffectTexture;
			}
		}		
		
		override public function RemoveEffect(name: String): Boolean
		{
			var index = GetEffectIndex(name);
			if (index != -1)
			{
				if (_effects[index].owner === Effect.OWNER_ENEMY)
				{
					_holderImage.texture = _holderNormalTexture;
				}
				_effects[index].OnRemoved(this);
				_effects.splice(index, 1);
				return true;
			}
			return false;
		}		
		
	}

}