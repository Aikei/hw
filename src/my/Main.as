package my
{

    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
 
    import starling.core.Starling;
	import my.Preferences;
	
    [SWF(width="850", height="701", frameRate="60", backgroundColor="#272d38")]
    public class Main extends Sprite
    {
        private var mStarling:Starling;
 
        public function Main()
        {
			Preferences.WINDOW_WIDTH = 850;
			Preferences.WINDOW_HEIGHT = 701;
            // These settings are recommended to avoid problems with touch handling
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
			//stage.root.loaderInfo.parameters;
            // Create a Starling instance that will run the "Game" class
			Starling.handleLostContext = true;			
            mStarling = new Starling(MyWorld, stage);
            mStarling.start();
        }
    }
}
