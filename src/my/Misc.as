package my 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import Math;
	import my.MyEntity;
	import starling.utils.Color;
	
	public class Misc 
	{
		public static const PI: Number = 3.1415926535;
		
		public function Misc() 
		{
			
		}
		
		static public function Random(min:Number, max:Number):Number
		{
			return Math.floor(Math.random()*(1+max-min))+min;			
		}
		
		static public function GetDistance(fromX: Number, fromy: Number, toX: Number, toY: Number): Number
		{
			return Math.sqrt(GetSquaredDistance(fromX, fromy, toX, toY));
		}
					
		static public function GetSquaredDistance(fromX: Number, fromy: Number, toX: Number, toY: Number): Number
		{
			return Math.pow(toX - fromX, 2) + Math.pow(toY - fromy, 2);
		}		
		
		static public function GetSquaredDistanceBetweenEntities(entity1: MyEntity, entity2: MyEntity): Number
		{
			return GetSquaredDistance(entity1.x, entity1.y, entity2.x, entity2.y);
		}		
		
		static public function GetDistanceBetweenEntities(entity1: MyEntity,entity2: MyEntity):Number
		{
			return GetDistance(entity1.x, entity1.y, entity2.x, entity2.y);
		}				
		
		static public function GetVectorSquaredLength(x: Number, y: Number):Number
		{
			return x * x + y * y;
		}
		
		static public function GetDistanceBetweenVectors(from: Vec2, to: Vec2):Number
		{
			return Math.sqrt(GetSquaredDistanceBetweenVectors(from, to));
		}
					
		static public function GetSquaredDistanceBetweenVectors(from: Vec2, to: Vec2):Number
		{
			return Math.pow(to.x - from.x, 2) + Math.pow(to.x - from.y, 2);
		}				
		
		static public function GetVectorLength(x: Number, y: Number):Number
		{
			return Math.sqrt(GetVectorSquaredLength(x, y));
		}
		
		static public function ConvertToFixedPoint(number: Number, factor: int): Number
		{
			return Math.round(number * factor)/factor;
		}
		
		static public function IsCharCodePrintable(charCode: int): Boolean
		{
			if (charCode < 32 || charCode > 126) 
				return false;
			return true;
		}
		
		static public function IsStringPrintable(str:String): Boolean 
		{
			for (var i:int = 0; i < str.length; i++) 
			{
				var ch:Number = str.charCodeAt(i);
				if (ch < 32 || ch > 126) 
				{
					return false;
				}
			}
			return true;
		}
		
		static public function IsMovingToDestination(pos: Vec2, dest: Vec2, vel: Vec2): Boolean
		{
			var diff: Vec2 = dest.SubtractVector(pos);
			var dotProduct: Number = diff.DotProduct(vel);
			if (dotProduct > 0)
				return true;
			return false;
		}
		
		static public function BlendHexColors(color1: uint, color2: uint): uint
		{
			var a1: int = Color.getAlpha(color1);
			var r1: int = Color.getRed(color1);
			var g1: int = Color.getGreen(color1);
			var b1: int = Color.getBlue(color1);
			
			var a2: int = Color.getAlpha(color2);
			var r2: int = Color.getRed(color2);
			var g2: int = Color.getGreen(color2);
			var b2: int = Color.getBlue(color2);
			
			a1 = (a1 + a2) / 2;
			r1 = (r1 + r2) / 2;
			g1 = (g1 + g2) / 2;
			b1 = (b1 + b2) / 2;
			
			return Color.argb(a1, r1, g1, b1);
		}
	}

}