package my.events 
{
	import starling.events.Event;
	
	import my.Ball;
	/**
	 * ...
	 * @author Aikei
	 */
	public class ButtonPressedEvent extends Event
	{		
		public static const BUTTON_PRESSED:String = "buttonPressed";

		public var buttonName: String;

		public function ButtonPressedEvent(type:String, buttonName: String, bubbles:Boolean=false, data: Object=null)
		{
			super(type, bubbles, data);
			this.buttonName = buttonName;
		}

		public function clone(): Event
		{
			return new ButtonPressedEvent(type, buttonName, bubbles, data);
		}		
	}

}