package my.events 
{
	import starling.events.Event;
	import my.Ball;
	/**
	 * ...
	 * @author Aikei
	 */
	public class BallsDeselectedEvent extends Event
	{
		
		public static const BALLS_DESELECTED:String = "ballsDeselected";

		public var balls: Vector.<Ball>;

		public function BallsDeselectedEvent(type:String, balls:Vector.<Ball>, bubbles:Boolean=false, data: Object = null)
		{
			super(type, bubbles, data);
			this.balls = balls;
		}

		public function clone(): Event
		{
			return new BallsDeselectedEvent(type, balls, bubbles, data);
		}
		
	}

}