package my.events 
{
	import starling.events.Event;
	
	import my.Ball;
	/**
	 * ...
	 * @author Aikei
	 */
	public class BallSelectedEvent extends Event
	{
		
		public static const BALL_SELECTED:String = "ballSelected";

		public var ball: Ball;
		public var chainLength: int;
		
		public function BallSelectedEvent(type:String, ball: Ball, chainLength: int, bubbles:Boolean=false, data: Object = null)
		{
			super(type, bubbles, data);
			this.ball = ball;
			this.chainLength = chainLength;
		}

		public function clone(): Event
		{
			return new BallSelectedEvent(type, ball, chainLength, bubbles, data);
		}
		
	}

}