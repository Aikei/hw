package my 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Vec2;
	
	public class Velocity extends Vec2
	{
		public var rotation: Number;
		
		public function Velocity(x: Number=0, y: Number=0, rotation: Number=0) 
		{
			super(x, y);
			this.rotation = rotation;
		}
		
	}

}