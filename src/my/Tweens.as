package my 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;	
	
	import starling.utils.Color;
	import my.system.HsvColor;

	
	public class Tweens 
	{		
		
		public static function InitTweens(): void
		{
			TweenPlugin.activate([HexColorsPlugin]);
			TweenPlugin.activate([Ease]);
			TweenPlugin.activate([RoughEase]);
		}
		
		static public function TweenColorTo(time: Number, entity: MyEntity, toColor: uint) : TweenLite
		{
			return TweenLite.to(entity, time, {hexColors: { topColor: toColor } } );
		}
		
		static public function TweenBaseColorTo(time: Number, entity: MyEntity, toColor: uint) : TweenLite
		{
			return TweenLite.to(entity, time, {hexColors: { baseColor: toColor } } );
		}		
		
		//static public function TweenAxColorToAxColor(time: Number, source: AxColor, dest: AxColor) : TweenLite
		//{
			//return TweenLite.to(source, time, { red : dest.red, green: dest.green, blue: dest.blue, alpha: dest.alpha } );
		//}		
		
		//static public function TweenBrightenAxColor(time: Number, color: AxColor, brighter: Number): TweenLite
		//{
			//var hsv : HsvColor = new HsvColor(color.red, color.green, color.blue, color.alpha);
			//hsv.Add(0, 0, brighter);
			//var ax: AxColor = hsv.ToAxColor();
			//return TweenAxColorToAxColor(time, color, ax);
			////TweenLite.to(color, time, { r : ax.red, g: ax.green, b: ax.blue, a : ax.alpha } );
		//}
		
		static public function TweenHsv(time: Number, entity: MyEntity, h: Number, s: Number, v: Number): TweenLite
		{
			var hsv : HsvColor = new HsvColor(entity.topColor);
			hsv.Add(h, s, v);
			var newColor: uint = hsv.ToRgbUint();
			return TweenColorTo(time, entity, newColor);			
		}
		
		static public function SetHsv(entity: MyEntity, h: Number, s: Number, v: Number): void
		{
			var hsv : HsvColor = new HsvColor(entity.topColor);
			hsv.Add(h, s, v);
			var newColor: uint = hsv.ToRgbUint();
			entity.topColor = newColor;			
		}		
		
		static public function TweenHsvRelative(time: Number, entity: MyEntity, h: Number, s: Number, v: Number): TweenLite
		{
			var hsv : HsvColor = new HsvColor(entity.topColor);
			hsv.Multiply(h, s, v);
			var newColor: uint = hsv.ToRgbUint();
			return TweenColorTo(time, entity, newColor);			
		}
		
		static public function TweenBaseHsvRelative(time: Number, entity: MyEntity, h: Number, s: Number, v: Number): TweenLite
		{
			var hsv : HsvColor = new HsvColor(entity.baseColor);
			hsv.Multiply(h, s, v);
			var newColor: uint = hsv.ToRgbUint();
			return TweenBaseColorTo(time, entity, newColor);			
		}			
		
		static public function TweenAlpha(time: Number, entity: MyEntity, target: Number): TweenLite
		{
			return TweenLite.to(entity, time, { alpha: target } );	
		}
		
		static public function TweenToOpaqueWhite(time: Number, entity: MyEntity): TweenLite
		{
			return TweenColorTo(time, entity, 0xffffffff);
		}
		
		static public function TweenToBaseColor(time: Number, entity: MyEntity): TweenLite
		{
			return TweenLite.to(entity, time, {hexColors: { topColor: entity.baseColor } } );
			//return TweenColorTo(time, entity, entity.baseColor);
		}
		
		static public function SetToBaseColor(entity: MyEntity): void
		{
			entity.topColor = entity.baseColor;
			//return TweenColorTo(time, entity, entity.baseColor);
		}			
		
		static public function TweenAngle(entity: MyEntity, time: Number, to: Number): TweenLite
		{
			return TweenLite.to(entity, time, { rotation : to } );
		}
		
		static public function TweenScale(entity: MyEntity, by: Number, time: Number): TweenLite
		{
			var t_scaleX: Number = entity.scaleX * by;
			var t_scaleY: Number = entity.scaleY * by;
			return TweenLite.to(entity, time, { scaleX: t_scaleX, scaleY: t_scaleY } );
		}
		
		static public function TweenScaleAndPositionToMatch(entity: MyEntity, by: Number, time: Number): Vector.<TweenLite>
		{
			var ret: Vector.<TweenLite> = new Vector.<TweenLite>;
			var t_scaleX: Number = entity.scaleX * by;
			var t_scaleY: Number = entity.scaleY * by;
			ret.push(TweenLite.to(entity, 0.5, { scaleX: t_scaleX, scaleY: t_scaleY } ));
			var realWidth: Number = entity.width;
			var realHeight: Number = entity.height;
			var plusX: Number = (realWidth - by * realWidth) / 2;
			var plusY: Number = (realHeight - by * realHeight) / 2;
			var toX: Number = entity.x + plusX;
			var toY: Number = entity.y + plusY;
			ret.push(TweenLite.to(entity, 0.5, { x : toX, y: toY } ));	
			return ret;		
		}
		
		static private function ReturnTween(entity: MyEntity): void
		{
			entity.x = entity.saveX;
			entity.y = entity.saveY;
		}
		
		static public function TweenShake(entity: MyEntity, time: Number = 0.4, repeatTimes: Number = -1, dist: Number = 1, howStrong: Number = 2, length: Number = 15): Vector.<TweenMax>
		{
			var ret: Vector.<TweenMax> = new Vector.<TweenMax>;
			entity.saveX = entity.x;
			entity.saveY = entity.y;
			ret.push(TweenMax.fromTo(entity, 0.4, 
					{ repeat: repeatTimes, x: entity.x - dist, onComplete: Tweens.ReturnTween, onCompleteParams: [entity] }, 
					{repeat: repeatTimes, x: entity.x + dist, ease:RoughEase.ease.config( { strength: howStrong, points: length, template: Linear.easeNone, randomize: false } ), 
					onComplete: Tweens.ReturnTween, onCompleteParams: [entity] } )); 
			//,onComplete : function() : void { x = saveX; }  
			//;			
			//ret.push(TweenMax.to(item, 0.1, { repeat: -1, y: item.saveX, x:item.saveY, delay:0.3, ease:Expo.easeInOut } ));
			//ret.push(TweenMax.to(item, 0.1, { repeat: -1, y: item.y+(1+Math.random()*5), x:item.x+(1+Math.random()*5), delay:0.1, ease:Expo.easeInOut, onRepeat: function () : void { trace("Repeat!") } }));			
			return ret;
		}
		
		static public function TweenDisappearing(entity: MyEntity, time: Number, a_alpha=0): TweenLite
		{
			return TweenLite.to(entity, time, { alpha: a_alpha } );
		}		
		
		static public function TweenFloatingUp(entity: MyEntity, time: Number, height: Number): TweenLite
		{
			return TweenLite.to(entity, time, { y: entity.y - height } );
		}
		
		static public function TweenFloatingUpAndDisappearing(entity: MyEntity, time: Number, height: Number): TweenLite
		{
			return TweenLite.to(entity, time, { y: entity.y - height, alpha: 0 } );
		}
		
		static public function TweenFromToHere(entity: MyEntity, time: Number, fromX: Number, fromY: Number): TweenLite
		{
			return TweenLite.from(entity, time, { x: fromX, y: fromY, ease: Expo.easeIn });
		}
		
		static public function TweenFromToHereInTimeRange(entity: MyEntity, timeMin: Number, timeMax: Number, fromX: Number, fromY: Number): TweenLite
		{
			var time = Misc.Random(timeMin, timeMax);
			return TweenFromToHere(entity, time, fromX, fromY);
		}	
		
		static public function TweenTo(entity: MyEntity, time: Number, toX: Number, toY: Number): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY } );
		}
		
		static public function TweenToExpoIn(entity: MyEntity, time: Number, toX: Number, toY: Number): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Expo.easeIn } );
		}
		
		static public function TweenToExpoOut(entity: MyEntity, time: Number, toX: Number, toY: Number): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Expo.easeOut } );
		}			
		
		static public function RelativeTweenTo(entity: MyEntity, time: Number, relativeToX: Number, relativeToY: Number): TweenLite
		{
			var toX: Number = entity.x + relativeToX;
			var toY: Number = entity.y + relativeToY;
			return TweenTo(entity, time, toX, toY);
		}
		
		static public function RelativeTweenToExpoIn(entity: MyEntity, time: Number, relativeToX: Number, relativeToY: Number): TweenLite
		{
			var toX: Number = entity.x + relativeToX;
			var toY: Number = entity.y + relativeToY;
			return TweenToExpoIn(entity, time, toX, toY);
		}
		
		static public function RelativeTweenToExpoOut(entity: MyEntity, time: Number, relativeToX: Number, relativeToY: Number): TweenLite
		{
			var toX: Number = entity.x + relativeToX;
			var toY: Number = entity.y + relativeToY;
			return TweenToExpoOut(entity, time, toX, toY);
		}				
		
		//static public function TweenToNormal
		
	}

}