package my 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import my.Ball;
	public class Preferences 
	{
		//public static var FIELD_WIDTH: int = 6;
		//public static var FIELD_HEIGHT: int = 6;
		public static var MAX_CHARGE: int = 8;
		public static var CHARGE_SQUARES_NUMBER: int = 4;
		public static var BALLS_PER_ONE_DAMAGE: int = 1;
		public static var HEALTH_STONE_HEAL: int = 3;
		public static var ATTACK_STONE_DAMAGE: int = 3;
		public static var HEALTH_STONE_CHANCE: int = 20;
		public static var ATTACK_STONE_CHANCE: int = 20;
		public static var FIRST_EFFECT_CHAIN_LENGTH: int = 5;
		public static var SECOND_EFFECT_CHAIN_LENGTH: int = 9;
		public static var MIN_CHAIN_LENGTH: int = 3;
		public static var DAMAGE_IF_NO_TURNS: int = 5;
		public static var WINDOW_WIDTH: Number = 0;
		public static var WINDOW_HEIGHT: Number = 0;
		public static var OrderOfColors: Vector.<int> = new Vector.<int>;
		public static const PI: Number = 3.14159265359;
		public static var BLACK_STONE_DAMAGE_MULTIPLIER: Number = 1.5;
		
		public static var AllAbilities: Array = new Array;
		
		public function Preferences() 
		{
			
		}
		
		static public function Initialize(data: *): void
		{
			SetPreferences(data.preferences);
			AllAbilities = data.abilities;
			OrderOfColors.push(Ball.RED_BALL);
			OrderOfColors.push(Ball.PURPLE_BALL);
			OrderOfColors.push(Ball.BLUE_BALL);
			OrderOfColors.push(Ball.GREEN_BALL);
		}
		
		static public function SetPreferences(data: *): void
		{
			//FIELD_WIDTH = data.fieldWidth;
			//FIELD_HEIGHT = data.fieldHeight;
			FIRST_EFFECT_CHAIN_LENGTH = data.firstEffectChainLength;
			SECOND_EFFECT_CHAIN_LENGTH = data.secondEffectChainLength;
			HEALTH_STONE_CHANCE = data.healthStoneChance;
			ATTACK_STONE_CHANCE = data.attackStoneChance;
			BALLS_PER_ONE_DAMAGE = data.ballsPerOneDamage;
			HEALTH_STONE_HEAL = data.healthStoneHeal;
			ATTACK_STONE_DAMAGE = data.attackStoneDamage;
			MAX_CHARGE = data.maxCharge;
			MIN_CHAIN_LENGTH = data.minChainLength;
			DAMAGE_IF_NO_TURNS = data.damageIfNoTurns;
			BLACK_STONE_DAMAGE_MULTIPLIER = data.blackStoneDamageMultiplier;
			MyWorld.FIELD_WIDTH = data.fieldWidth;
			MyWorld.FIELD_HEIGHT = data.fieldHeight;
			MyWorld.OVERALL_FIELD_WIDTH = MyWorld.FIELD_WIDTH * (Ball.BALL_WIDTH + MyWorld.FIELD_SPACING_X);
			MyWorld.OVERALL_FIELD_HEIGHT = MyWorld.FIELD_HEIGHT * (Ball.BALL_HEIGHT + MyWorld.FIELD_SPACING_Y);			
		}
		
	}

}