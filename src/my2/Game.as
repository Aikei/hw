package my2 
{
	/**
	 * ...
	 * @author Aikei
	 */
    import starling.display.Quad;
    import starling.display.Sprite;
	import starling.text.TextField;
    import starling.utils.Color;
	
	public class Game extends Sprite
	{		
		public function Game() 
		{
			var quad:Quad = new Quad(200, 200, Color.RED);
			quad.x = 100;
			quad.y = 50;
			addChild(quad);	
			var textField: TextField = new TextField(100, 100, "Привет!", "Arial", 20, 0xffffffff);
			textField.x = 0;
			textField.y = 0;
			addChild(textField);
		}
		
	}
}